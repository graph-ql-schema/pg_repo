create schema test_data;
create table test_data.user
(
    id serial not null
        constraint user_pk
            primary key,
    created timestamptz default now() not null,
    name text,
    orders int,
    conversion float not null,
    active bool default true not null,
    test integer[]
);

create table test_data.phones
(
    id serial not null
        constraint phones_pk
            primary key,
    created timestamptz default now() not null,
    phone text,
    user_id int,

    constraint user_user_id_fk foreign key (user_id) references test_data."user" on update cascade
);

create table test_data.roles
(
    id serial not null
        constraint roles_pk
            primary key,
    name text
);

create table test_data.user_roles
(
    id serial not null
        constraint user_roles_pk
            primary key,
    role_id int not null,
    user_id int not null,

    constraint user_roles_roles_id_fk foreign key (role_id) references test_data.roles on update cascade,
    constraint user_roles_user_id_fk foreign key (user_id) references test_data."user" on update cascade
);

insert into test_data.user(name, orders, conversion, active, test) values
('anna', 10, 2.5, true, array[1,2]),
('megan', 12, 3.5, false, array[2,3]),
('jim', 3, 0.5, true, array[3,4]),
('dan', 120, 55.5, false, array[2,5,6]),
('nelson', 11, 3.2, false, null);

insert into test_data.phones(phone, user_id) values
('9601112233', 1),
('9601112443', 3),
('9601123753', 3),
('9603312233', 4),
('9601134233', 5),
('9601177233', 5),
('9601188233', 5),
('9601190233', 5);

insert into test_data.roles(name) values
('admin'),
('user');

insert into test_data.user_roles(role_id, user_id) values
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(2, 4),
(2, 5);

