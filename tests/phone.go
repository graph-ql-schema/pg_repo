package main

import (
	"time"

	"github.com/graphql-go/graphql"
)

// Сущность телефона пользователя
type Phone struct {
	Id      int64     `pg.field:"id" json:"id" pg.type:"int"`
	Created time.Time `pg.field:"created" json:"created_at" pg.type:"timestamptz"`
	Phone   string    `pg.field:"phone" json:"number" pg.type:"string" pg.alreadyLoad:"true"`
	UserId  int64     `pg.field:"user_id" json:"user_id" pg.type:"int"`
}

// Объект GraphQL API
var phoneObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "phone",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
				Name: "id",
			},
			"number": &graphql.Field{
				Type: graphql.NewNonNull(graphql.String),
				Name: "number",
			},
			"res": &graphql.Field{
				Type: graphql.NewNonNull(graphql.String),
				Name: "res",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return "test", nil
				},
			},
			"created_at": &graphql.Field{
				Type: graphql.DateTime,
				Name: "created_at",
			},
			"user_id": &graphql.Field{
				Type: graphql.ID,
				Name: "user_id",
			},
		},
		Description: "test entity",
	},
)
