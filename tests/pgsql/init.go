package pgsql

import (
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"log"
)

var Connection *sqlx.DB

func init() {
	d := &stdlib.DriverConfig{
		ConnConfig: pgx.ConnConfig{
			RuntimeParams: map[string]string{
				"standard_conforming_strings": "on",
			},
			PreferSimpleProtocol: true,
		},
	}
	stdlib.RegisterDriverConfig(d)

	// this Pings the database trying to connect, panics on error
	// use sqlx.Open() for sql.Open() semantics
	db, err := sqlx.Connect("pgx", d.ConnectionString("host=localhost user=postgres password=postgres dbname=test sslmode=disable"))
	if err != nil {
		log.Fatalln(err)
	}

	Connection = db
	Connection.SetMaxIdleConns(10)
	Connection.SetMaxOpenConns(20)
}
