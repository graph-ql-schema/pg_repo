package main

import (
	"bitbucket.org/graph-ql-schema/nullable"
	"time"

	"github.com/graphql-go/graphql"
)

// Сущность пользователя
type User struct {
	Id         int64     `pg.field:"id" json:"id" pg.type:"int"`
	Created    time.Time `pg.field:"created" json:"created_at" pg.type:"timestamptz"`
	Name       string    `pg.field:"name" json:"name" pg.type:"string"`
	Orders     *int64    `pg.field:"orders" json:"orders" pg.type:"int"`
	Conversion float64   `pg.field:"conversion" json:"conversion" pg.type:"float"`
	Active     bool      `pg.field:"active" json:"-" pg.type:"bool"`
	Items      []int64   `pg.field:"test" pg.storage:"array" json:"items" pg.type:"int"`
	PhonesId   []int64   `pg.storage:"rel" pg.type:"int" pg.rel.tbl:"test_data.phones" pg.rel.pkey:"id" pg.rel.fkey:"user_id" pg.rel.lkey:"id" json:"phones_id" pg.rel.conflict.drop:"drop" pg.rel.conflict.free:"null" pg.rel.conflict.busy:"replace"`
	RoleId     []int64   `pg.storage:"rtbl" pg.type:"int" pg.rtbl.tbl:"test_data.user_roles" pg.rtbl.target:"role_id" pg.rtbl.fkey:"user_id" pg.rtbl.lkey:"id" json:"roles_id"`
}

// Объект GraphQL API
var userObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "user",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
				Name: "id",
			},
			"name": &graphql.Field{
				Type: graphql.NewNonNull(graphql.String),
				Name: "name",
			},
			"created_at": &graphql.Field{
				Type: graphql.DateTime,
				Name: "created_at",
			},
			"orders": &graphql.Field{
				Type: nullable.NewNullable(graphql.Int),
				Name: "orders",
			},
			"conversion": &graphql.Field{
				Type: graphql.NewNonNull(graphql.Float),
				Name: "conversion",
			},
			"phones_id": &graphql.Field{
				Type: graphql.NewList(graphql.ID),
				Name: "phones_id",
			},
			"items": &graphql.Field{
				Type: graphql.NewList(graphql.Int),
				Name: "items",
			},
			"phones": &graphql.Field{
				Type: graphql.NewList(phoneObject),
				Name: "phones",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return nil, nil
				},
			},
			"roles_id": &graphql.Field{
				Type: graphql.NewList(graphql.ID),
				Name: "roles_id",
			},
			"roles": &graphql.Field{
				Type: graphql.NewList(roleObject),
				Name: "roles",
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return nil, nil
				},
			},
		},
		Description: "test entity",
	},
)
