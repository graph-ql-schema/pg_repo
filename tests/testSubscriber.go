package main

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"context"
	"database/sql"
	"github.com/sirupsen/logrus"
)

// Тестовый подписчик на события
type testSubscriber struct{}

// Обработка возникшего события
func (t testSubscriber) Process(ctx context.Context, event events.Event, tx *sql.Tx) error {
	logrus.WithFields(logrus.Fields{
		"event": event,
	}).Warning(`Processing event`)

	return nil
}

// Откат изменений
func (t testSubscriber) Rollback(context.Context, error) {}
