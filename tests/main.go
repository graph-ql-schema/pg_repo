package main

import (
	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"reflect"

	"bitbucket.org/graph-ql-schema/pg_repo/v2"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/tests/pgsql"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
)

func main() {
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(new(prefixed.TextFormatter))

	schemaBuilder := sbuilder.NewGraphQlSchemaBuilder()

	userRepo, err := pg_repo.NewPgRepository(userObject, reflect.TypeOf(User{}), "test_data.user", "id", pgsql.Connection)
	if nil != err {
		logrus.Fatalln(err.Error())
	}

	phonesRepo, err := pg_repo.NewPgRepository(phoneObject, reflect.TypeOf(Phone{}), "test_data.phones", "id", pgsql.Connection)
	if nil != err {
		logrus.Fatalln(err.Error())
	}

	rolesRepo, err := pg_repo.NewPgRepository(roleObject, reflect.TypeOf(Role{}), "test_data.roles", "id", pgsql.Connection)
	if nil != err {
		logrus.Fatalln(err.Error())
	}

	userRepo.SubscribeToEvents(testSubscriber{})
	phonesRepo.SubscribeToEvents(testSubscriber{})
	rolesRepo.SubscribeToEvents(testSubscriber{})

	schemaBuilder.RegisterEntity(userObject, sbuilder.EntityQueries{
		sbuilder.ListQuery: func(params types.Parameters) (interface{}, error) {
			return userRepo.GetListByGraphQlParameters(params, nil)
		},
		sbuilder.InsertMutation: func(params types.Parameters) (interface{}, error) {
			return userRepo.InsertEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.AggregateQuery: func(params types.Parameters) (interface{}, error) {
			return userRepo.GetAggregationByGraphQlParameters(params, nil)
		},
		sbuilder.UpdateMutation: func(params types.Parameters) (interface{}, error) {
			return userRepo.UpdateEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.DeleteMutation: func(params types.Parameters) (interface{}, error) {
			return userRepo.DeleteEntitiesByGraphQlParameters(params, nil)
		},
	})

	schemaBuilder.RegisterEntity(phoneObject, sbuilder.EntityQueries{
		sbuilder.ListQuery: func(params types.Parameters) (interface{}, error) {
			return phonesRepo.GetListByGraphQlParameters(params, nil)
		},
		sbuilder.InsertMutation: func(params types.Parameters) (interface{}, error) {
			return phonesRepo.InsertEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.AggregateQuery: func(params types.Parameters) (interface{}, error) {
			return phonesRepo.GetAggregationByGraphQlParameters(params, nil)
		},
		sbuilder.UpdateMutation: func(params types.Parameters) (interface{}, error) {
			return phonesRepo.UpdateEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.DeleteMutation: func(params types.Parameters) (interface{}, error) {
			return phonesRepo.DeleteEntitiesByGraphQlParameters(params, nil)
		},
	})

	schemaBuilder.RegisterEntity(roleObject, sbuilder.EntityQueries{
		sbuilder.ListQuery: func(params types.Parameters) (interface{}, error) {
			return rolesRepo.GetListByGraphQlParameters(params, nil)
		},
		sbuilder.InsertMutation: func(params types.Parameters) (interface{}, error) {
			return rolesRepo.InsertEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.AggregateQuery: func(params types.Parameters) (interface{}, error) {
			return rolesRepo.GetAggregationByGraphQlParameters(params, nil)
		},
		sbuilder.UpdateMutation: func(params types.Parameters) (interface{}, error) {
			return rolesRepo.UpdateEntitiesByGraphQlParameters(params, nil)
		},
		sbuilder.DeleteMutation: func(params types.Parameters) (interface{}, error) {
			return rolesRepo.DeleteEntitiesByGraphQlParameters(params, nil)
		},
	})

	schemaBuilder.SetServerConfig(sbuilder.GraphQlServerConfig{
		Host: "0.0.0.0",
		Port: 9000,
		Uri:  "/graphql",
	})

	server, err := schemaBuilder.BuildServer()
	if nil != err {
		logrus.Fatal(err)
	}

	logrus.Fatal(server.Run())
}
