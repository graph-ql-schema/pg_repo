package main

import (
	"github.com/graphql-go/graphql"
)

// Сущность телефона пользователя
type Role struct {
	Id   int64  `pg.field:"id" json:"id" pg.type:"int"`
	Name string `pg.field:"name" json:"name" pg.type:"string"`
}

// Объект GraphQL API
var roleObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "role",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.ID,
				Name: "id",
			},
			"name": &graphql.Field{
				Type: graphql.NewNonNull(graphql.String),
				Name: "name",
			},
		},
		Description: "test entity",
	},
)
