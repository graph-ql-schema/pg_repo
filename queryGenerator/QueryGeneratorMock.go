package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Подставка для тестирования
type QueryGeneratorMock struct {
	IsGenerateListQueryByGraphQlParametersError       bool
	GenerateListQueryByGraphQlParametersResult        string
	IsGenerateListQueryByCustomSqlError               bool
	GenerateListQueryByCustomSqlResult                string
	GenerateAggregationQueryByGraphQlParametersError  bool
	GenerateAggregationQueryByGraphQlParametersResult string
	GenerateInsertQueryByGraphQlParametersError       bool
	GenerateInsertQueryByGraphQlParametersResult      string
	GenerateUpdateQueryByGraphQlParametersError       bool
	GenerateUpdateQueryByGraphQlParametersResult      string
	GenerateDeleteQueryByGraphQlParametersError       bool
	GenerateDeleteQueryByGraphQlParametersResult      string
	GenerateGetIdsQueryByGraphQlParametersError       bool
	GenerateGetIdsQueryByGraphQlParametersResult      string
}

// Генерация SQL запроса получения списка ID сущностей по переданным параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateGetIdsQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if q.GenerateGetIdsQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateGetIdsQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateGetIdsQueryByGraphQlParametersResult, nil, nil
}

// Генерация SQL запроса удаления сущностей по переданным параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateDeleteQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if q.GenerateDeleteQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateDeleteQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateDeleteQueryByGraphQlParametersResult, nil, nil
}

// Генерация SQL запроса обновления сущностей по переданным параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateUpdateQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if q.GenerateUpdateQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateUpdateQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateUpdateQueryByGraphQlParametersResult, nil, nil
}

// Генерация sql запроса вставки сущности по параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateInsertQueryByGraphQlParameters(
	ctx context.Context,
	object objectsParser.Object,
) (sql *string, args []interface{}, err error) {
	if q.GenerateInsertQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateInsertQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateInsertQueryByGraphQlParametersResult, nil, nil
}

// Генерация sql запроса агрегации сущностей по параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateAggregationQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if q.GenerateAggregationQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateAggregationQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateAggregationQueryByGraphQlParametersResult, nil, nil
}

// Генерация sql запроса листинга сущностей по параметрам запроса GraphQL
func (q QueryGeneratorMock) GenerateListQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if q.IsGenerateListQueryByGraphQlParametersError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateListQueryByGraphQlParametersResult) {
		return nil, nil, nil
	}

	return &q.GenerateListQueryByGraphQlParametersResult, nil, nil
}

// Генерация sql запроса листинга сущностей при помощи модификатора запроса
func (q QueryGeneratorMock) GenerateListQueryByCustomSql(
	ctx context.Context,
	callback TSqlModificationCallback,
) (sql *string, args []interface{}, err error) {
	if q.IsGenerateListQueryByCustomSqlError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(q.GenerateListQueryByCustomSqlResult) {
		return nil, nil, nil
	}

	return &q.GenerateListQueryByCustomSqlResult, nil, nil
}
