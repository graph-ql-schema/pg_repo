package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/simple"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/variableGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/order"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/selectFields"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Тестирование генерации запроса получения листинга сущностей по параметрам GraphQL
func Test_queryGenerator_GenerateListQueryByGraphQlParameters(t *testing.T) {
	filterBuildResult := "test = 1"
	queryWithOrder := "select test from test p where test = 1 order by test asc"
	queryWithoutOrder := "select test from test p where test = 1"
	queryWithoutOrderWithPagination := "select test from test p where test = 1 limit 10 offset 5"

	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если ошибка генерации списка полей",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: true,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если не возвратились поля для выборки",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если ошибка генерации фильтра",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       true,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args:    args{},
			want:    &queryWithOrder,
			wantErr: false,
		},
		{
			name: "Нет ошибок. Нет сортировки.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args:    args{},
			want:    &queryWithoutOrder,
			wantErr: false,
		},
		{
			name: "Нет ошибок. Есть пагинация.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        &filterBuildResult,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 5,
						},
					},
				},
			},
			want:    &queryWithoutOrderWithPagination,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateListQueryByGraphQlParameters(context.Background(), tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateListQueryByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateListQueryByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации кастомного запроса листинга сущностей
func Test_queryGenerator_GenerateListQueryByCustomSql(t *testing.T) {
	query := "select test from test p"
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		callback TSqlModificationCallback
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если ошибка генерации списка полей",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: true,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        nil,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args: args{
				callback: func(sql string) (result string, args []interface{}) {
					return sql, nil
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "p",
				primaryTablePrimaryKey: "",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{
					IsBuildError: false,
					BuildResult:  "test",
				},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: whereOrHavingParser.NewSimpleOperation("test", 1, "test_f", "="),
					IsBuildError:       false,
					BuildResult:        nil,
				},
				orderGenerator: order.OrderSqlGeneratorMock{
					Result: "test asc",
				},
				aggregationGenerator:  nil,
				simpleFieldsProcessor: nil,
				variableGenerator:     &variableGeneratorMock{},
			},
			args: args{
				callback: func(sql string) (result string, args []interface{}) {
					return sql, nil
				},
			},
			want:    &query,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateListQueryByCustomSql(context.Background(), tt.args.callback)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateListQueryByCustomSql() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateListQueryByCustomSql() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса агрегации
func Test_queryGenerator_GenerateAggregationQueryByGraphQlParameters(t *testing.T) {
	test := "test"
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Тестирование генерации запроса агрегации",
			fields: fields{
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{
					IsError: true,
					Result:  "",
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование генерации запроса агрегации",
			fields: fields{
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{
					IsError: false,
					Result:  "test",
				},
			},
			args:    args{},
			want:    &test,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateAggregationQueryByGraphQlParameters(context.Background(), tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateAggregationQueryByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GenerateAggregationQueryByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса вставки сущности
func Test_queryGenerator_GenerateInsertQueryByGraphQlParameters(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		object objectsParser.Object
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Если не переданы поля для вставки",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator:  selectFields.SelectFieldsBuilderMock{},
				filterGenerator:        filter.WhereGeneratorMock{},
				orderGenerator:         order.OrderSqlGeneratorMock{},
				aggregationGenerator:   aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				object: map[string]interface{}{},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Если не найдена конфигурация поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				object: map[string]interface{}{
					"id": 1,
				},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "Пропуск простого поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.RelationFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				object: map[string]interface{}{
					"uuid": 1,
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Ошибка конвертации значения поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: true,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				object: map[string]interface{}{
					"uuid": 1,
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				object: map[string]interface{}{
					"uuid": 1,
				},
			},
			want:    "insert into test(uuid) values (1) returning id",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateInsertQueryByGraphQlParameters(context.Background(), tt.args.object)
			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateInsertQueryByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("GenerateInsertQueryByGraphQlParameters() got = %v, want %v", *got, tt.want)
				} else {
					t.Errorf("GenerateInsertQueryByGraphQlParameters() got = %v, want %v", got, want)
				}
			}
		})
	}
}

// Тестирование генерации запроса обновления по параметрам GraphQL
func Test_queryGenerator_GenerateUpdateQueryByGraphQlParameters(t *testing.T) {
	test := "id = 1"
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Если не переданы поля для вставки",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration:          []configuration.FieldsConfiguration{},
				selectFieldsGenerator:  selectFields.SelectFieldsBuilderMock{},
				filterGenerator:        filter.WhereGeneratorMock{},
				orderGenerator:         order.OrderSqlGeneratorMock{},
				aggregationGenerator:   aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{},
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Если не найдена конфигурация поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"id": 1,
						},
					},
				},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "Пропуск не простого поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.RelationFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "123",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"uuid": 1,
						},
					},
				},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "Ошибка конвертации значения поля",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator:       filter.WhereGeneratorMock{},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: true,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"uuid": 1,
						},
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Ошибка конвертации параметров фильтрации",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       true,
					BuildResult:        nil,
				},
				orderGenerator:       order.OrderSqlGeneratorMock{},
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"uuid": 1,
						},
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Нет ошибок. Фильтр не возвращает значения.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "name",
						FieldNum:        0,
						GraphQlName:     "name",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        nil,
				},
				orderGenerator:       order.OrderSqlGeneratorMock{},
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"uuid": 1,
							"name": 1,
						},
					},
				},
			},
			want:    "update test tt set uuid = 1, uuid = 1 returning id",
			wantErr: false,
		},
		{
			name: "Нет ошибок. Фильтр возвращает значения.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "name",
						FieldNum:        0,
						GraphQlName:     "name",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
				orderGenerator:       order.OrderSqlGeneratorMock{},
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: map[string]interface{}{
							"uuid": 1,
							"name": 1,
						},
					},
				},
			},
			want:    "update test tt set uuid = 1, uuid = 1 where id = 1 returning id",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateUpdateQueryByGraphQlParameters(context.Background(), tt.args.params)
			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateUpdateQueryByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("GenerateUpdateQueryByGraphQlParameters() got = %v, want %v", *got, tt.want)
				} else {
					t.Errorf("GenerateUpdateQueryByGraphQlParameters() got = %v, want %v", got, want)
				}
			}
		})
	}
}

// Тестирование генерации запроса удаления сущностей по GraphQL параметрам
func Test_queryGenerator_GenerateDeleteQueryByGraphQlParameters(t *testing.T) {
	test := "id = 1"
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
		filterGenerator        filter.WhereGeneratorInterface
		orderGenerator         order.OrderSqlGeneratorInterface
		aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
		simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
		variableGenerator      variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Ошибка конвертации параметров фильтрации",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       true,
					BuildResult:        nil,
				},
				orderGenerator:       order.OrderSqlGeneratorMock{},
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Нет ошибок. Фильтр не возвращает значения.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "name",
						FieldNum:        0,
						GraphQlName:     "name",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        nil,
				},
				orderGenerator:       order.OrderSqlGeneratorMock{},
				aggregationGenerator: aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{
					FieldCode:         "uuid",
					FieldValue:        "1",
					IsFieldValueError: false,
				},
				variableGenerator: &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{},
				},
			},
			want:    "delete from test tt returning id",
			wantErr: false,
		},
		{
			name: "Нет ошибок. Фильтр возвращает значения.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "uuid",
						FieldNum:        0,
						GraphQlName:     "uuid",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "name",
						FieldNum:        0,
						GraphQlName:     "name",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				selectFieldsGenerator: selectFields.SelectFieldsBuilderMock{},
				filterGenerator: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
				orderGenerator:        order.OrderSqlGeneratorMock{},
				aggregationGenerator:  aggregationQuery.AggregationQueryGeneratorMock{},
				simpleFieldsProcessor: simple.SimpleFieldProcessorMock{},
				variableGenerator:     &variableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{},
				},
			},
			want:    "delete from test tt where id = 1 returning id",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := queryGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				selectFieldsGenerator:  tt.fields.selectFieldsGenerator,
				filterGenerator:        tt.fields.filterGenerator,
				orderGenerator:         tt.fields.orderGenerator,
				aggregationGenerator:   tt.fields.aggregationGenerator,
				simpleFieldsProcessor:  tt.fields.simpleFieldsProcessor,
				variableGenerator:      tt.fields.variableGenerator,
			}
			got, _, err := q.GenerateDeleteQueryByGraphQlParameters(context.Background(), tt.args.params)
			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateDeleteQueryByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("GenerateDeleteQueryByGraphQlParameters() got = %v, want %v", *got, tt.want)
				} else {
					t.Errorf("GenerateDeleteQueryByGraphQlParameters() got = %v, want %v", got, want)
				}
			}
		})
	}
}
