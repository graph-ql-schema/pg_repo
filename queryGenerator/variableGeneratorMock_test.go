package queryGenerator

// Сервис генерации переменных по подставкам для запросов SQL
type variableGeneratorMock struct{}

// Замена подставок переменной в передаваемом SQL на реальные переменные
func (v variableGeneratorMock) SetVariables(sql string) string {
	return sql
}
