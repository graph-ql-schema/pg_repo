package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/simple"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/customizations"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/variableGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/order"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/selectFields"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"github.com/graphql-go/graphql"
)

// Генератор SQL запросов
type queryGenerator struct {
	primaryTable           string
	primaryTableAlias      string
	primaryTablePrimaryKey string
	configuration          []configuration.FieldsConfiguration
	selectFieldsGenerator  selectFields.SelectFieldsBuilderInterface
	filterGenerator        filter.WhereGeneratorInterface
	orderGenerator         order.OrderSqlGeneratorInterface
	aggregationGenerator   aggregationQuery.AggregationQueryGeneratorInterface
	simpleFieldsProcessor  simple.SimpleFieldProcessorInterface
	variableGenerator      variableGenerator.VariableGeneratorInterface
}

// Конструктор генератора
func NewQueryGenerator(
	object *graphql.Object,
	primaryTable string,
	primaryTableAlias string,
	primaryTablePrimaryKey string,
	configuration []configuration.FieldsConfiguration,
) QueryGeneratorInterface {
	return &queryGenerator{
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		configuration:          configuration,
		selectFieldsGenerator:  selectFields.NewSelectFieldsBuilder(primaryTableAlias, configuration),
		filterGenerator:        filter.NewWhereGenerator(object, primaryTableAlias, configuration),
		orderGenerator:         order.NewOrderSqlGenerator(),
		aggregationGenerator: aggregationQuery.NewAggregationQueryGenerator(
			object,
			primaryTable,
			primaryTablePrimaryKey,
			configuration,
		),
		simpleFieldsProcessor: simple.NewSimpleFieldProcessor(object),
		variableGenerator:     variableGenerator.VariableGenerator,
	}
}

// Конструктор генератора с кастомизацией
func NewQueryGeneratorWithCustomization(
	object *graphql.Object,
	primaryTable string,
	primaryTableAlias string,
	primaryTablePrimaryKey string,
	configuration []configuration.FieldsConfiguration,
	customizations []customizations.CustomizationInterface,
) QueryGeneratorInterface {
	return &queryGenerator{
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		configuration:          configuration,
		selectFieldsGenerator:  selectFields.NewSelectFieldsBuilder(primaryTableAlias, configuration),
		filterGenerator:        filter.NewWhereGeneratorWithCustomizations(object, primaryTableAlias, configuration, customizations),
		orderGenerator:         order.NewOrderSqlGenerator(),
		aggregationGenerator: aggregationQuery.NewAggregationQueryGeneratorWithCustomizations(
			object,
			primaryTable,
			primaryTablePrimaryKey,
			configuration,
			customizations,
		),
		simpleFieldsProcessor: simple.NewSimpleFieldProcessor(object),
		variableGenerator:     variableGenerator.VariableGenerator,
	}
}

// Генерация SQL запроса получения списка ID сущностей по переданным параметрам запроса GraphQL
func (q queryGenerator) GenerateGetIdsQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	where, args, err := q.filterGenerator.Build(ctx, q.filterGenerator.GetOperation(params))
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`failed to parse filter for ID's query: %v`, err.Error())
		return
	}

	sqlStr := fmt.Sprintf(
		`select %v.%v from %v %v`,
		q.primaryTableAlias,
		q.primaryTablePrimaryKey,
		q.primaryTable,
		q.primaryTableAlias,
	)

	if nil != where {
		sqlStr = fmt.Sprintf(`%v where %v`, sqlStr, *where)
	}

	sqlStr = q.variableGenerator.SetVariables(sqlStr)
	sql = &sqlStr

	return
}

// Генерация SQL запроса удаления сущностей по переданным параметрам запроса GraphQL
func (q queryGenerator) GenerateDeleteQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	where, args, err := q.filterGenerator.Build(ctx, q.filterGenerator.GetOperation(params))
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`failed to parse filter for delete: %v`, err.Error())
		return
	}

	sqlStr := fmt.Sprintf(`delete from %v %v`, q.primaryTable, q.primaryTableAlias)
	if nil != where {
		sqlStr = fmt.Sprintf(`%v where %v`, sqlStr, *where)
	}

	sqlStr = fmt.Sprintf(`%v returning %v`, sqlStr, q.primaryTablePrimaryKey)

	sqlStr = q.variableGenerator.SetVariables(sqlStr)
	sql = &sqlStr

	return
}

// Генерация SQL запроса обновления сущностей по переданным параметрам запроса GraphQL
func (q queryGenerator) GenerateUpdateQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if nil == params.Arguments.Set {
		err = fmt.Errorf(`noone field passed for update`)
		return
	}

	hasCustomFields := false
	updates := []string{}
	args = []interface{}{}
	for code, value := range params.Arguments.Set {
		select {
		case <-ctx.Done():
			return
		default:
		}

		field := q.getFieldConfigurationByGraphQlCode(code)
		if nil == field {
			return
		}

		if !q.isSimpleField(*field) {
			hasCustomFields = true
			continue
		}

		value, valueArgs, valErr := q.simpleFieldsProcessor.ParseFieldValue(ctx, *field, value)
		if nil != valErr {
			err = fmt.Errorf(`can't parse field '%v' value: %v`, field.GraphQlName, valErr.Error())
			return
		}

		args = append(args, valueArgs...)
		updates = append(updates, fmt.Sprintf(
			`%v = %v`,
			q.simpleFieldsProcessor.GetFieldCode(*field),
			value,
		))
	}

	if 0 == len(updates) {
		if hasCustomFields {
			return
		}

		err = fmt.Errorf(`noone field passed for update`)
		return
	}

	where, whereArgs, err := q.filterGenerator.Build(ctx, q.filterGenerator.GetOperation(params))
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`failed to parse filter for update: %v`, err.Error())
		return
	}

	sqlStr := fmt.Sprintf(`update %v %v set %v`, q.primaryTable, q.primaryTableAlias, strings.Join(updates, ", "))
	if nil != where {
		sqlStr = fmt.Sprintf(`%v where %v`, sqlStr, *where)
	}

	sqlStr = fmt.Sprintf(`%v returning %v`, sqlStr, q.primaryTablePrimaryKey)

	sqlStr = q.variableGenerator.SetVariables(sqlStr)
	sql = &sqlStr
	args = append(args, whereArgs...)

	return
}

// Генерация sql запроса вставки сущности по параметрам запроса GraphQL
func (q queryGenerator) GenerateInsertQueryByGraphQlParameters(
	ctx context.Context,
	object objectsParser.Object,
) (sql *string, args []interface{}, err error) {
	select {
	case <-ctx.Done():
		return
	default:
	}

	if 0 == len(object) {
		err = fmt.Errorf(`noone field passed to insert`)
		return
	}

	columns := []string{}
	values := []string{}
	args = []interface{}{}

	for code, value := range object {
		select {
		case <-ctx.Done():
			return
		default:
		}

		field := q.getFieldConfigurationByGraphQlCode(code)
		if nil == field {
			return
		}

		if !q.isSimpleField(*field) {
			continue
		}

		value, valueArgs, valErr := q.simpleFieldsProcessor.ParseFieldValue(ctx, *field, value)
		if nil != valErr {
			err = fmt.Errorf(`can't parse field '%v' value: %v`, field.GraphQlName, valErr.Error())
			return
		}

		columns = append(columns, q.simpleFieldsProcessor.GetFieldCode(*field))
		values = append(values, value)
		args = append(args, valueArgs...)
	}

	if 0 == len(columns) {
		err = fmt.Errorf(`noone field passed to insert`)
		return
	}

	sqlStr := fmt.Sprintf(
		`insert into %v(%v) values (%v) returning %v`,
		q.primaryTable,
		strings.Join(columns, ", "),
		strings.Join(values, ", "),
		q.primaryTablePrimaryKey,
	)

	sqlStr = q.variableGenerator.SetVariables(sqlStr)
	sql = &sqlStr

	return
}

// Проверяет, является ли переданное поле простым для вставки
func (q queryGenerator) isSimpleField(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ScalarFieldConfiguration); ok {
		return true
	}

	if _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Получение конфигурации поля по переданному коду GraphQL объекта
func (q queryGenerator) getFieldConfigurationByGraphQlCode(code string) *configuration.FieldsConfiguration {
	for _, field := range q.configuration {
		if field.GraphQlName == code {
			return &field
		}
	}

	return nil
}

// Генерация sql запроса агрегации сущностей по параметрам запроса GraphQL
func (q queryGenerator) GenerateAggregationQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	return q.aggregationGenerator.Generate(ctx, params)
}

// Генерация sql запроса листинга сущностей по параметрам запроса GraphQL
func (q queryGenerator) GenerateListQueryByGraphQlParameters(
	ctx context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	requestFields := params.Fields
	for _, config := range q.configuration {
		if !config.IsAlreadyLoad && q.primaryTablePrimaryKey != config.GraphQlName {
			continue
		}

		requestFields = append(requestFields, requestedFieldsParser.NewGraphQlRequestedField(config.GraphQlName, nil))
	}

	fields, err := q.selectFieldsGenerator.Build(requestFields)
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' list -> %v`, q.primaryTableAlias, err.Error())
		return
	}

	if 0 == len(fields) {
		err = fmt.Errorf(`'%v' list -> no one field selected`, q.primaryTableAlias)
		return
	}

	query := fmt.Sprintf(`select %v from %v %v`, fields, q.primaryTable, q.primaryTableAlias)

	filterStr, args, err := q.filterGenerator.Build(ctx, q.filterGenerator.GetOperation(params))
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' list -> %v`, q.primaryTableAlias, err.Error())
		return
	}

	if nil != filterStr {
		query = fmt.Sprintf(`%v where %v`, query, *filterStr)
	}

	orderStr := q.orderGenerator.Generate(params)
	if nil != orderStr {
		query = fmt.Sprintf(`%v order by %v`, query, *orderStr)
	}

	if nil != params.Arguments.Pagination {
		query = fmt.Sprintf(`%v limit %v offset %v`, query, params.Arguments.Pagination.Limit, params.Arguments.Pagination.Offset)
	}

	query = q.variableGenerator.SetVariables(query)
	sql = &query

	return
}

// Генерация sql запроса листинга сущностей при помощи модификатора запроса
func (q queryGenerator) GenerateListQueryByCustomSql(
	ctx context.Context,
	callback TSqlModificationCallback,
) (sql *string, args []interface{}, err error) {
	fields := requestedFieldsParser.GraphQlRequestedFields{}
	for _, config := range q.configuration {
		fields = append(fields, requestedFieldsParser.NewGraphQlRequestedField(config.GraphQlName, nil))
	}

	selectedFields, err := q.selectFieldsGenerator.Build(fields)
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' custom list -> %v`, q.primaryTableAlias, err.Error())
		return
	}

	query, args := callback(
		fmt.Sprintf(`select %v from %v %v`, selectedFields, q.primaryTable, q.primaryTableAlias),
	)

	query = q.variableGenerator.SetVariables(query)
	sql = &query

	return
}
