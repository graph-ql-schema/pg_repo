package queryGenerator

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
)

// Callback функция модификации SQL запроса
type TSqlModificationCallback = func(sql string) (result string, args []interface{})

// Генератор SQL запросов
type QueryGeneratorInterface interface {
	// Генерация sql запроса листинга сущностей по параметрам запроса GraphQL
	GenerateListQueryByGraphQlParameters(
		ctx context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)

	// Генерация sql запроса листинга сущностей при помощи модификатора запроса
	GenerateListQueryByCustomSql(
		ctx context.Context,
		callback TSqlModificationCallback,
	) (sql *string, args []interface{}, err error)

	// Генерация sql запроса агрегации сущностей по параметрам запроса GraphQL
	GenerateAggregationQueryByGraphQlParameters(
		ctx context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)

	// Генерация sql запроса вставки сущности по параметрам запроса GraphQL
	GenerateInsertQueryByGraphQlParameters(
		ctx context.Context,
		object objectsParser.Object,
	) (sql *string, args []interface{}, err error)

	// Генерация SQL запроса обновления сущностей по переданным параметрам запроса GraphQL
	GenerateUpdateQueryByGraphQlParameters(
		ctx context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)

	// Генерация SQL запроса удаления сущностей по переданным параметрам запроса GraphQL
	GenerateDeleteQueryByGraphQlParameters(
		ctx context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)

	// Генерация SQL запроса получения списка ID сущностей по переданным параметрам запроса GraphQL
	GenerateGetIdsQueryByGraphQlParameters(
		ctx context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)
}
