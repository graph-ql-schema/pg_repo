package pg_repo

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/primaryChanges"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/resolveMapBuilder"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/setParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"context"
	"database/sql"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/resultParser"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Тестирование получения листинга сущностей по параметрам GraphQL
func Test_pgRepository_GetListByGraphQlParameters(t *testing.T) {
	type Test struct {
		Field string
	}

	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []interface{}
		wantErr bool
	}{
		{
			name: "Если при генерации запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByGraphQlParametersError: true,
					GenerateListQueryByGraphQlParametersResult:  "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если генератор запроса не вернул результат",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByGraphQlParametersError: false,
					GenerateListQueryByGraphQlParametersResult:  "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при выполнении запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByGraphQlParametersError: false,
					GenerateListQueryByGraphQlParametersResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при парсинге запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByGraphQlParametersError: false,
					GenerateListQueryByGraphQlParametersResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: true,
					ParseListResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByGraphQlParametersError: false,
					GenerateListQueryByGraphQlParametersResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult: []interface{}{
						"test",
					},
				},
			},
			args: args{},
			want: []interface{}{
				"test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pgRepository{
				entityType:    tt.fields.entityType,
				primaryTable:  tt.fields.primaryTable,
				configuration: tt.fields.configuration,
				generator:     tt.fields.generator,
				executor:      tt.fields.executor,
				log:           tt.fields.log,
				resultParser:  tt.fields.resultParser,
			}
			got, err := p.GetListByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения списка сущностей по кастомному SQL запросу
func Test_pgRepository_GetListByCustomSql(t *testing.T) {
	type Test struct {
		Field string
	}

	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		callback queryGenerator.TSqlModificationCallback
		tx       *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []interface{}
		wantErr bool
	}{
		{
			name: "Если при генерации запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: true,
					GenerateListQueryByCustomSqlResult:  "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если генератор запроса не вернул результат",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при выполнении запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult:  nil,
				},
				log:          helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при парсинге запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: true,
					ParseListResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult: []interface{}{
						"test",
					},
				},
			},
			args: args{},
			want: []interface{}{
				"test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pgRepository{
				entityType:    tt.fields.entityType,
				primaryTable:  tt.fields.primaryTable,
				configuration: tt.fields.configuration,
				generator:     tt.fields.generator,
				executor:      tt.fields.executor,
				log:           tt.fields.log,
				resultParser:  tt.fields.resultParser,
			}
			got, err := p.GetListByCustomSql(context.Background(), tt.args.callback, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListByCustomSql() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListByCustomSql() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование запроса агрегации
func Test_pgRepository_GetAggregationByGraphQlParameters(t *testing.T) {
	type Test struct {
		Field string
	}

	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []aggregationConverter.Aggregation
		wantErr bool
	}{
		{
			name: "Если при генерации запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					GenerateAggregationQueryByGraphQlParametersError:  true,
					GenerateAggregationQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseAggregationError: false,
					ParseAggregationResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если генератор запроса не вернул результат",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					GenerateAggregationQueryByGraphQlParametersError:  false,
					GenerateAggregationQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult:  nil,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseAggregationError: false,
					ParseAggregationResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при выполнении запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					GenerateAggregationQueryByGraphQlParametersError:  false,
					GenerateAggregationQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult:  nil,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseAggregationError: false,
					ParseAggregationResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если при парсинге запроса произошла ошибка",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					GenerateAggregationQueryByGraphQlParametersError:  false,
					GenerateAggregationQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseAggregationError: true,
					ParseAggregationResult:  nil,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				entityType:   reflect.TypeOf(Test{}),
				primaryTable: "test",
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "Field",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				generator: queryGenerator.QueryGeneratorMock{
					GenerateAggregationQueryByGraphQlParametersError:  false,
					GenerateAggregationQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test_f"},
					},
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseAggregationError: false,
					ParseAggregationResult: []aggregationConverter.Aggregation{
						{
							Count: 1,
						},
					},
				},
			},
			args: args{},
			want: []aggregationConverter.Aggregation{
				{
					Count: 1,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pgRepository{
				entityType:    tt.fields.entityType,
				primaryTable:  tt.fields.primaryTable,
				configuration: tt.fields.configuration,
				generator:     tt.fields.generator,
				executor:      tt.fields.executor,
				log:           tt.fields.log,
				resultParser:  tt.fields.resultParser,
			}
			got, err := p.GetAggregationByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAggregationByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAggregationByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование вставки объектов
func Test_pgRepository_InsertEntitiesByGraphQlParameters(t *testing.T) {
	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *InsertUpdateResult
		wantErr bool
	}{
		{
			name: "Объекты вставки не переданы",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка старта транзакции",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: true,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка вставки основной сущности",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: true,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений 1 to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  true,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений Many to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: true,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка внутри подписчика",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: true,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок. Запрошен returning",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want: &InsertUpdateResult{
				AffectedRows: 1,
				Returning: []interface{}{
					"test",
				},
			},
			wantErr: false,
		},
		{
			name: "Нет ошибок. Не запрошен returning",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveInsertError:  false,
					ResolveInsertResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveInsertError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					InsertObjectError: false,
					InsertObjectId:    "1",
					InsertObjectEvent: &events.Event{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Objects: objectsParser.Objects{
							{"test": 1},
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
					},
				},
				tx: nil,
			},
			want: &InsertUpdateResult{
				AffectedRows: 1,
				Returning:    []interface{}{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := pgRepository{
				entityType:             tt.fields.entityType,
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
				resultParser:           tt.fields.resultParser,
				relationChange:         tt.fields.relationChange,
				tableChange:            tt.fields.tableChange,
				primaryChange:          tt.fields.primaryChange,
				resolveMapper:          tt.fields.resolveMapper,
				eventsProcessor:        tt.fields.eventsProcessor,
			}
			got, err := p.InsertEntitiesByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("InsertEntitiesByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InsertEntitiesByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование обновления сущностей
func Test_pgRepository_UpdateEntitiesByGraphQlParameters(t *testing.T) {
	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *InsertUpdateResult
		wantErr bool
	}{
		{
			name: "Поля обновления не переданы",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка старта транзакции",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: true,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка обновления основной сущности",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: true,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений 1 to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  true,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений Many to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: true,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка внутри подписчика",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: true,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок. Запрошен returning",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want: &InsertUpdateResult{
				AffectedRows: 1,
				Returning: []interface{}{
					"test",
				},
			},
			wantErr: false,
		},
		{
			name: "Нет ошибок. Не запрошен returning",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					IsGenerateListQueryByCustomSqlError: false,
					GenerateListQueryByCustomSqlResult:  "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveUpdateError:  false,
					ResolveUpdateResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveUpdateError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					UpdateError: false,
					UpdateIds:   []string{"1"},
					UpdateEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
					},
				},
				tx: nil,
			},
			want: &InsertUpdateResult{
				AffectedRows: 1,
				Returning:    []interface{}{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &pgRepository{
				entityType:             tt.fields.entityType,
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
				resultParser:           tt.fields.resultParser,
				relationChange:         tt.fields.relationChange,
				tableChange:            tt.fields.tableChange,
				primaryChange:          tt.fields.primaryChange,
				resolveMapper:          tt.fields.resolveMapper,
				subscribers:            tt.fields.subscribers,
				eventsProcessor:        tt.fields.eventsProcessor,
			}
			got, err := p.UpdateEntitiesByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateEntitiesByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateEntitiesByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения списка ID по переданным параметрам GraphQL
func Test_pgRepository_GetPrimaryKeyValuesByGraphQlParameters(t *testing.T) {
	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []interface{}
		wantErr bool
	}{
		{
			name: "Если возникла ошбка генерации запроса",
			fields: fields{
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  true,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet:  []interface{}{2},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если генератор запроса вернул пустой результат",
			fields: fields{
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet:  []interface{}{2},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если произошла ошибка выполнения запроса",
			fields: fields{
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet:  []interface{}{2},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если произошла ошибка парсинга результатов",
			fields: fields{
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     true,
						ColumnsPull: []string{"id"},
						ItemsToSet:  []interface{}{2},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet:  []interface{}{2},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    []interface{}{2, 2},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &pgRepository{
				entityType:             tt.fields.entityType,
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
				resultParser:           tt.fields.resultParser,
				relationChange:         tt.fields.relationChange,
				tableChange:            tt.fields.tableChange,
				primaryChange:          tt.fields.primaryChange,
				resolveMapper:          tt.fields.resolveMapper,
				subscribers:            tt.fields.subscribers,
				eventsProcessor:        tt.fields.eventsProcessor,
			}
			got, err := p.GetPrimaryKeyValuesByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetPrimaryKeyValuesByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetPrimaryKeyValuesByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование удаления сущностей
func Test_pgRepository_DeleteEntitiesByGraphQlParameters(t *testing.T) {
	type fields struct {
		entityType             reflect.Type
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
		configuration          []configuration.FieldsConfiguration
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
		resultParser           resultParser.ResultParserInterface
		relationChange         relation.ConflictResolverInterface
		tableChange            table.ChangeResolverInterface
		primaryChange          primaryChanges.PrimaryChangesServiceInterface
		resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
		subscribers            []events.EventsSubscriberInterface
		eventsProcessor        events.SubscriberProcessorInterface
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *DeleteResult
		wantErr bool
	}{
		{
			name: "Ошибка старта транзакции",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: true,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  false,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: false,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка удаления основной сущности",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  false,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: true,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений 1 to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  true,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: false,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка резолвинга отношений Many to Many",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  false,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: true,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: false,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка внутри подписчика",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  false,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: false,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: true,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
						requestedFieldsParser.NewGraphQlRequestedField(`returning`, nil),
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок.",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateGetIdsQueryByGraphQlParametersError:  false,
					GenerateGetIdsQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"test"},
						ItemsToSet:  []interface{}{1},
					},
					IsStartTransactionError: false,
				},
				log: helpers.NewLogger(`test`),
				resultParser: resultParser.ResultParserMock{
					IsParseListError: false,
					ParseListResult:  []interface{}{"test"},
				},
				relationChange: relation.ConflictResolverMock{
					ResolveDeleteError:  false,
					ResolveDeleteResult: []events.Event{},
				},
				tableChange: table.ChangeResolverMock{
					ResolveDeleteError: false,
				},
				primaryChange: primaryChanges.PrimaryChangesServiceMock{
					DeleteError: false,
					DeleteIds:   []string{"1"},
					DeleteEvents: []events.Event{{
						Type:            "test",
						Table:           "ttt",
						PrimaryKey:      "tt",
						PrimaryKeyValue: "1",
					}},
				},
				resolveMapper: resolveMapBuilder.ResolveMapBuilderMock{
					Result: map[string][]string{},
				},
				eventsProcessor: events.SubscriberProcessorMock{
					IsError: false,
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Set: setParser.Set{
							"test": 1,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField(`affected_rows`, nil),
					},
				},
				tx: nil,
			},
			want: &DeleteResult{
				AffectedRows: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &pgRepository{
				entityType:             tt.fields.entityType,
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				configuration:          tt.fields.configuration,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
				resultParser:           tt.fields.resultParser,
				relationChange:         tt.fields.relationChange,
				tableChange:            tt.fields.tableChange,
				primaryChange:          tt.fields.primaryChange,
				resolveMapper:          tt.fields.resolveMapper,
				subscribers:            tt.fields.subscribers,
				eventsProcessor:        tt.fields.eventsProcessor,
			}
			got, err := p.DeleteEntitiesByGraphQlParameters(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("DeleteEntitiesByGraphQlParameters() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DeleteEntitiesByGraphQlParameters() got = %v, want %v", got, tt.want)
			}
		})
	}
}
