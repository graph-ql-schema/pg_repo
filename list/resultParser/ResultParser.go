package resultParser

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"encoding/json"
	"github.com/graphql-go/graphql"
	"github.com/sirupsen/logrus"
	"reflect"
	"time"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/filler"
	"github.com/jmoiron/sqlx"
)

// Парсер результатов запросов
type resultParser struct {
	entityType           reflect.Type
	filler               filler.EntityFillerInterface
	log                  *logrus.Entry
	aggregationConverter aggregationConverter.ConverterInterface
}

// Конструктор парсера
func NewResultParser(
	object *graphql.Object,
	entityType reflect.Type,
) ResultParserInterface {
	return &resultParser{
		entityType:           entityType,
		filler:               filler.NewEntityFiller(),
		log:                  helpers.NewLogger(`resultParser`),
		aggregationConverter: aggregationConverter.NewConverter(object),
	}
}

// Парсинг результатов запроса листинга
func (r resultParser) ParseListRows(
	rows executor.Rows,
	fields []configuration.FieldsConfiguration,
) ([]interface{}, error) {
	defer rows.Close()

	result := []interface{}{}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	defer func() {
		finish := time.Now().UnixNano() / int64(time.Millisecond)
		r.log.WithFields(logrus.Fields{
			"code":          100,
			"result":        result,
			"executionTime": finish - starts,
		}).Debug(`Parsed list query result`)
	}()

	// Базовый шаг увеличения размера коллекции не обработанных результатов
	step := 100

	// Инициируем коллекцию не обработанных результатов
	stepNum := 1
	limit := stepNum * step
	collectedData := make([]map[string]interface{}, limit)
	collectedDataIdx := 0

	// Парсим строки из БД в коллекцию не обработанных результатов
	for rows.Next() {
		// Когда изначально коллекция дошла до своего предела, то расширяем ее
		// инициируя новую коллекцию
		if collectedDataIdx == limit {
			stepNum++
			limit = stepNum * step

			newCollectedData := make([]map[string]interface{}, limit)
			copy(newCollectedData, collectedData)

			collectedData = newCollectedData
		}

		data := map[string]interface{}{}
		err := sqlx.MapScan(rows, data)
		if nil != err {
			r.log.WithFields(logrus.Fields{
				"code": 500,
				"err":  err.Error(),
			}).Error(`Failed to parse get list query`)

			return nil, err
		}

		collectedData[collectedDataIdx] = data
		collectedDataIdx++
	}

	result = make([]interface{}, collectedDataIdx)
	for i := 0; i < collectedDataIdx; i++ {
		item, err := r.filler.Fill(r.entityType, fields, collectedData[i])
		if nil != err {
			r.log.WithFields(logrus.Fields{
				"code": 500,
				"err":  err.Error(),
			}).Error(`Failed to parse get list query`)

			return nil, err
		}

		result[i] = item
	}

	return result, nil
}

// Парсинг результатов запроса агрегации
func (r resultParser) ParseAggregationRows(rows executor.Rows) ([]aggregationConverter.Aggregation, error) {
	defer rows.Close()

	result := []aggregationConverter.Aggregation{}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	defer func() {
		finish := time.Now().UnixNano() / int64(time.Millisecond)
		r.log.WithFields(logrus.Fields{
			"code":          100,
			"result":        result,
			"executionTime": finish - starts,
		}).Debug(`Collected aggregation query data`)
	}()

	// Базовый шаг увеличения размера коллекции не обработанных результатов
	step := 100

	// Инициируем коллекцию не обработанных результатов
	stepNum := 1
	limit := stepNum * step
	collectedData := make([]json.RawMessage, limit)
	collectedDataIdx := 0

	// Парсим строки из БД в коллекцию не обработанных результатов
	for rows.Next() {
		// Когда изначально коллекция дошла до своего предела, то расширяем ее
		// инициируя новую коллекцию
		if collectedDataIdx == limit {
			stepNum++
			limit = stepNum * step

			newCollectedData := make([]json.RawMessage, limit)
			copy(newCollectedData, collectedData)

			collectedData = newCollectedData
		}

		var data json.RawMessage
		err := rows.Scan(&data)
		if nil != err {
			r.log.WithFields(logrus.Fields{
				"code": 500,
				"err":  err.Error(),
			}).Error(`Failed to parse aggregation result`)

			return nil, err
		}

		collectedData[collectedDataIdx] = data
		collectedDataIdx++
	}

	result = make([]aggregationConverter.Aggregation, collectedDataIdx)
	for i := 0; i < collectedDataIdx; i++ {
		var item aggregationConverter.Aggregation
		err := json.Unmarshal(collectedData[i], &item)
		if nil != err {
			r.log.WithFields(logrus.Fields{
				"code": 500,
				"err":  err.Error(),
			}).Error(`Failed to parse aggregation result`)

			return nil, err
		}

		convertedItem, err := r.aggregationConverter.Convert(item)
		if nil != err {
			r.log.WithFields(logrus.Fields{
				"code": 500,
				"err":  err.Error(),
				"item": item,
			}).Error(`Failed to convert aggregation query result`)

			return nil, err
		}

		result[i] = convertedItem
	}

	return result, nil
}
