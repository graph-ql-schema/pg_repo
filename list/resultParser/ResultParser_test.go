package resultParser

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/filler"
)

// Тестирование парсинга листинга запросов
func Test_resultParser_ParseListRows(t *testing.T) {
	type Test struct {
		Field string
	}

	type fields struct {
		entityType           reflect.Type
		filler               filler.EntityFillerInterface
		log                  *logrus.Entry
		aggregationConverter aggregationConverter.ConverterInterface
	}
	type args struct {
		rows   executor.Rows
		fields []configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []interface{}
		wantErr bool
	}{
		{
			name: "Если произошла ошибка парсинга",
			fields: fields{
				entityType: reflect.TypeOf(Test{}),
				filler: filler.EntityFillerMock{
					IsFillError: true,
					FillResult:  nil,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				rows: &executor.RowsStub{
					Items:       1,
					IsError:     false,
					ColumnsPull: []string{"test_f"},
				},
				fields: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				entityType: reflect.TypeOf(Test{}),
				filler: filler.EntityFillerMock{
					IsFillError: false,
					FillResult:  "string",
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				rows: &executor.RowsStub{
					Items:       1,
					IsError:     false,
					ColumnsPull: []string{"test_f"},
				},
				fields: nil,
			},
			want: []interface{}{
				"string",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := resultParser{
				entityType:           tt.fields.entityType,
				filler:               tt.fields.filler,
				log:                  tt.fields.log,
				aggregationConverter: tt.fields.aggregationConverter,
			}
			got, err := r.ParseListRows(tt.args.rows, tt.args.fields)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseListRows() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseListRows() got = %v, want %v", got, tt.want)
			}
		})
	}
}
