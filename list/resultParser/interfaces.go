package resultParser

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
)

// Парсер результатов запросов
type ResultParserInterface interface {
	// Парсинг результатов запроса листинга
	ParseListRows(
		rows executor.Rows,
		fields []configuration.FieldsConfiguration,
	) ([]interface{}, error)

	// Парсинг результатов запроса агрегации
	ParseAggregationRows(rows executor.Rows) ([]aggregationConverter.Aggregation, error)
}
