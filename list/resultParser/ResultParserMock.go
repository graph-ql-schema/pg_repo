package resultParser

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
)

// Подставка для тестирования
type ResultParserMock struct {
	IsParseListError        bool
	ParseListResult         []interface{}
	IsParseAggregationError bool
	ParseAggregationResult  []aggregationConverter.Aggregation
}

// Парсинг результатов запроса листинга
func (r ResultParserMock) ParseListRows(executor.Rows, []configuration.FieldsConfiguration) ([]interface{}, error) {
	if r.IsParseListError {
		return nil, fmt.Errorf(`test`)
	}

	return r.ParseListResult, nil
}

// Парсинг результатов запроса агрегации
func (r ResultParserMock) ParseAggregationRows(executor.Rows) ([]aggregationConverter.Aggregation, error) {
	if r.IsParseAggregationError {
		return nil, fmt.Errorf(`test`)
	}

	return r.ParseAggregationResult, nil
}
