package filler

import (
	"reflect"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Заполнитель структур
type EntityFillerInterface interface {
	// Заполнение сущности значениями. Заполняются поля, переданные в конфигурации
	Fill(entityType reflect.Type, fields []configuration.FieldsConfiguration, values map[string]interface{}) (interface{}, error)
}
