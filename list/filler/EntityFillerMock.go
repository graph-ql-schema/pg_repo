package filler

import (
	"fmt"
	"reflect"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type EntityFillerMock struct {
	IsFillError bool
	FillResult  interface{}
}

// Заполнение сущности значениями. Заполняются поля, переданные в конфигурации
func (e EntityFillerMock) Fill(reflect.Type, []configuration.FieldsConfiguration, map[string]interface{}) (interface{}, error) {
	if e.IsFillError {
		return nil, fmt.Errorf(`test`)
	}

	return e.FillResult, nil
}
