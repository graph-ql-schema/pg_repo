package filler

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"
)

// Заполнитель структур
type entityFiller struct {
	jsonType reflect.Type
}

// Заполнение сущности значениями. Заполняются поля, переданные в конфигурации
func (e entityFiller) Fill(
	entityType reflect.Type,
	fields []configuration.FieldsConfiguration,
	values map[string]interface{},
) (interface{}, error) {
	if entityType.Kind() != reflect.Struct {
		return nil, fmt.Errorf(`you shoul pass 'struct' type. current type: %v`, entityType.Kind().String())
	}

	res := reflect.New(entityType).Elem()
	for _, field := range fields {
		var val interface{}
		val, ok := values[strings.ToLower(field.GraphQlName)]
		if !ok {
			return nil, fmt.Errorf(`you should pass value for field '%v'`, field.Field)
		}

		fieldType := entityType.Field(field.FieldNum).Type.Kind()
		if val == nil {
			// Если поле - массив, необходимо вернуть пустой срез корректного типа,
			// поэтому передаем JSON эквивалент пустого среза в парсер
			if fieldType == reflect.Slice {
				val, err := e.parseJsonSlice([]byte("[]"), entityType.Field(field.FieldNum).Type.Elem().Kind())
				if nil != err {
					return nil, fmt.Errorf(`failed to parse json data for field '%v': %v`, field.Field, err.Error())
				}

				res.Field(field.FieldNum).Set(reflect.ValueOf(val))
			}

			continue
		}

		valueType := reflect.TypeOf(val).Kind()

		// Если при получении данных вернулся массив байт, а поле требует наличия строки,
		// то конвертим значение в строку.
		if bytesVal, ok := val.([]byte); fieldType == reflect.String && ok {
			val = string(bytesVal)
			valueType = reflect.TypeOf(val).Kind()
		}

		isValueSlice := false
		if valueType == reflect.Slice || valueType == reflect.Array || valueType == reflect.Map {
			isValueSlice = valueType == reflect.Slice
			valueType = reflect.TypeOf(val).Elem().Kind()
		}

		isFieldSlice := false
		if fieldType == reflect.Slice || fieldType == reflect.Array || fieldType == reflect.Map {
			isFieldSlice = fieldType == reflect.Slice
			fieldType = entityType.Field(field.FieldNum).Type.Elem().Kind()
		}

		if isValueSlice && valueType == e.jsonType.Kind() && isFieldSlice && fieldType != e.jsonType.Kind() {
			var err error
			val, err = e.parseJsonSlice(reflect.ValueOf(val).Bytes(), fieldType)

			if nil != err {
				return nil, fmt.Errorf(`failed to parse json data for field '%v': %v`, field.Field, err.Error())
			}
		}

		valueType = reflect.TypeOf(val).Kind()
		if valueType == reflect.Slice || valueType == reflect.Array || valueType == reflect.Map {
			valueType = reflect.TypeOf(val).Elem().Kind()
		}

		var valueToSet reflect.Value
		if fieldType == reflect.Ptr {
			fType := entityType.Field(field.FieldNum).Type.Elem()
			if fType.Kind() != valueType {
				if valueType != reflect.Ptr {
					return nil, fmt.Errorf(`incompatible value types for field '%v'. field type: %v, value type: %v`, field.Field, fieldType, valueType)
				}

				valueToSet = reflect.ValueOf(val)
			} else {
				toGetAddress := val
				p := reflect.New(fType)
				p.Elem().Set(reflect.ValueOf(toGetAddress))

				valueToSet = p
			}
		} else {
			if valueType != fieldType {
				return nil, fmt.Errorf(`incompatible value types for field '%v'. field type: %v, value type: %v`, field.Field, fieldType, valueType)
			}

			valueToSet = reflect.ValueOf(val)
		}

		res.Field(field.FieldNum).Set(valueToSet)
	}

	return res.Interface(), nil
}

// Парсинг JSON срезов для вставки в структуру. Объявляет срез с типом переданного поля для корректной
// конвертации значения из JSON
func (e entityFiller) parseJsonSlice(data json.RawMessage, fieldType reflect.Kind) (interface{}, error) {
	switch fieldType {
	case reflect.Int:
		var parsedValue []int
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Int8:
		var parsedValue []int8
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Int16:
		var parsedValue []int16
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Int32:
		var parsedValue []int32
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Int64:
		var parsedValue []int64
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uint:
		var parsedValue []uint
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uint8:
		var parsedValue []uint8
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uint16:
		var parsedValue []uint16
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uint32:
		var parsedValue []uint32
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uint64:
		var parsedValue []uint64
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Uintptr:
		var parsedValue []uintptr
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Float32:
		var parsedValue []float32
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Float64:
		var parsedValue []float64
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.String:
		var parsedValue []string
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Bool:
		var parsedValue []bool
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Complex64:
		var parsedValue []complex64
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.Complex128:
		var parsedValue []complex128
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	case reflect.TypeOf(time.Time{}).Kind():
		var parsedValue []time.Time
		err := json.Unmarshal(data, &parsedValue)
		return parsedValue, err
	}

	return nil, fmt.Errorf(`incompatible type for json slice parsing: %v`, fieldType)
}

// Фабрика заполнителя
func NewEntityFiller() EntityFillerInterface {
	return &entityFiller{
		jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
	}
}
