package filler

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование заполнения структур
func Test_entityFiller_Fill(t *testing.T) {
	var rawData json.RawMessage
	rawData, _ = json.Marshal([]int{1, 2})

	timeVal, _ := time.Parse(time.RFC3339, "2020-01-01T08:00:00.000Z")
	var timeRawData json.RawMessage
	timeRawData, _ = json.Marshal([]time.Time{timeVal})

	isoTime, _ := time.Parse(time.RFC3339, "2020-04-30T09:31:48.525455Z")
	var timeStringTimestampFromDbRawData json.RawMessage
	timeStringTimestampFromDbRawData, _ = json.Marshal([]string{"2020-04-30T09:31:48.525455Z"})

	var timeStringRawData json.RawMessage
	timeStringRawData, _ = json.Marshal([]string{"2020-01-01T08:00:00.000Z"})

	dateCol, _ := time.Parse(time.RFC3339, "2020-05-01T00:00:00Z")
	var dateColRawData json.RawMessage
	dateColRawData, _ = json.Marshal([]string{"2020-05-01T00:00:00Z"})

	timeCol, _ := time.Parse(time.RFC3339, "0000-01-01T18:26:08.726918Z")
	var timeColRawData json.RawMessage
	timeColRawData, _ = json.Marshal([]string{"0000-01-01T18:26:08.726918Z"})

	timestampCol, _ := time.Parse(time.RFC3339, "2020-05-01T18:26:08.734149Z")
	var timestampColRawData json.RawMessage
	timestampColRawData, _ = json.Marshal([]string{"2020-05-01T18:26:08.734149Z"})

	type TestStruct struct {
		FieldOne string
		FieldTwo int
	}

	type TestStructWithJson struct {
		FieldOne json.RawMessage
		FieldTwo int
	}

	type TestStructWithStrSlice struct {
		FieldOne []string
		FieldTwo int
	}

	type TestStructWithIntSlice struct {
		FieldOne []int
		FieldTwo int
	}

	type TestStructWithTimeSlice struct {
		FieldOne []time.Time
		FieldTwo int
	}

	type fields struct {
		jsonType reflect.Type
	}
	type args struct {
		entityType reflect.Type
		fields     []configuration.FieldsConfiguration
		values     map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Передан не структурный тип",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf("str"),
				fields:     []configuration.FieldsConfiguration{},
				values:     map[string]interface{}{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Не передано значение для поля",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStruct{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": "test",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Не корректный тип значения",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStruct{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": 1,
					"field_two": 1,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Корректные данные",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStruct{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": "str",
					"field_two": 1,
				},
			},
			want: TestStruct{
				FieldOne: "str",
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Не корректный тип значения для Json",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithJson{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": 1,
					"field_two": 1,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Корректный тип значения для Json",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithJson{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": rawData,
					"field_two": 1,
				},
			},
			want: TestStructWithJson{
				FieldOne: rawData,
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Не корректный тип значения для Slice (RawData)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithStrSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": rawData,
					"field_two": 1,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Не корректный тип значения для Slice",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithStrSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": []int{1},
					"field_two": 1,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Корректный тип значения для Slice",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithIntSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": []int{1},
					"field_two": 1,
				},
			},
			want: TestStructWithIntSlice{
				FieldOne: []int{1},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice (RawData)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithIntSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": rawData,
					"field_two": 1,
				},
			},
			want: TestStructWithIntSlice{
				FieldOne: []int{1, 2},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": timeRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{timeVal},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time (TimestampTz)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": timeStringTimestampFromDbRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{isoTime},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time (String time)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": timeStringRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{timeVal},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time (Date)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": dateColRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{dateCol},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time (Time)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": timeColRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{timeCol},
				FieldTwo: 1,
			},
			wantErr: false,
		},
		{
			name: "Корректный тип значения для Slice Time (Timestamp)",
			fields: fields{
				jsonType: reflect.TypeOf(json.RawMessage{}).Elem(),
			},
			args: args{
				entityType: reflect.TypeOf(TestStructWithTimeSlice{}),
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "FieldOne",
						FieldNum:        0,
						GraphQlName:     "field_one",
						DbConfiguration: nil,
					},
					{
						Field:           "FieldTwo",
						FieldNum:        1,
						GraphQlName:     "field_two",
						DbConfiguration: nil,
					},
				},
				values: map[string]interface{}{
					"field_one": timestampColRawData,
					"field_two": 1,
				},
			},
			want: TestStructWithTimeSlice{
				FieldOne: []time.Time{timestampCol},
				FieldTwo: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := entityFiller{
				jsonType: tt.fields.jsonType,
			}
			got, err := e.Fill(tt.args.entityType, tt.args.fields, tt.args.values)
			if (err != nil) != tt.wantErr {
				t.Errorf("Fill() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fill() got = %v, want %v", got, tt.want)
			}
		})
	}
}
