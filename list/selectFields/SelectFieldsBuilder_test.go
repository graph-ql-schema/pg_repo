package selectFields

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование генерации запроса
func Test_selectFieldsBuilder_Build(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		processors    []builderProcessorInterface
	}
	type args struct {
		fields requestedFieldsParser.GraphQlRequestedFields
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Когда передан пустой набор полей",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				processors:    []builderProcessorInterface{},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "Когда передано не известное поле",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []builderProcessorInterface{
					builderProcessorMock{
						IsAvailable:  true,
						IsBuildError: false,
						BuildResult:  "test",
					},
				},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{
					requestedFieldsParser.NewGraphQlRequestedField("test_n", nil),
				},
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "Когда нет доступного процессора",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []builderProcessorInterface{
					builderProcessorMock{
						IsAvailable:  false,
						IsBuildError: false,
						BuildResult:  "test",
					},
				},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{
					requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Когда процессор возвращает ошибку",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []builderProcessorInterface{
					builderProcessorMock{
						IsAvailable:  true,
						IsBuildError: true,
						BuildResult:  "test",
					},
				},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{
					requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Когда один из процессоров доступен",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []builderProcessorInterface{
					builderProcessorMock{
						IsAvailable:  true,
						IsBuildError: false,
						BuildResult:  "test",
					},
					builderProcessorMock{
						IsAvailable:  false,
						IsBuildError: false,
						BuildResult:  "test-2",
					},
				},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{
					requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
				},
			},
			want:    "test as test_f",
			wantErr: false,
		},
		{
			name: "Когда передано несколько полей",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_n",
						DbConfiguration: nil,
					},
				},
				processors: []builderProcessorInterface{
					builderProcessorMock{
						IsAvailable:  true,
						IsBuildError: false,
						BuildResult:  "test",
					},
					builderProcessorMock{
						IsAvailable:  false,
						IsBuildError: false,
						BuildResult:  "test-2",
					},
				},
			},
			args: args{
				fields: requestedFieldsParser.GraphQlRequestedFields{
					requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
					requestedFieldsParser.NewGraphQlRequestedField("test_n", nil),
				},
			},
			want:    "test as test_f, test as test_n",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := selectFieldsBuilder{
				configuration: tt.fields.configuration,
				processors:    tt.fields.processors,
			}
			got, err := s.Build(tt.args.fields)
			if (err != nil) != tt.wantErr {
				t.Errorf("Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
