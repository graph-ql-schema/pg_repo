package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации для массивов
type arrayProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подстроки SQL для переданного поля
func (a arrayProcessor) build(field configuration.FieldsConfiguration) (string, error) {
	dbConf := field.DbConfiguration.(configuration.ArrayFieldConfiguration)

	return fmt.Sprintf(`array_to_json(%v.%v)`, a.primaryTableAlias, dbConf.DbName), nil
}

// Фабрика процессора
func newArrayProcessor(primaryTableAlias string) builderProcessorInterface {
	return &arrayProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
