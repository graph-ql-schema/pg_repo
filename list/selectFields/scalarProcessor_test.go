package selectFields

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_scalarProcessor_isAvailable(t *testing.T) {
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Тестирование на не валидном значении",
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на валидном значении",
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := scalarProcessor{}
			if got := s.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_scalarProcessor_build(t *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				primaryTableAlias: "tt",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want:    "tt.test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := scalarProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			got, err := s.build(tt.args.field)
			if (err != nil) != tt.wantErr {
				t.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
