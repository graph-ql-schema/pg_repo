package selectFields

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Генератор подстроки запроса получения данных
type SelectFieldsBuilderInterface interface {
	// Построение списка полей для выборки.
	// Генерирует строку на основе конфигурации поле. Добавляет синонимы к выборке
	// в соответствии с названием в GraphQL схеме
	Build(fields requestedFieldsParser.GraphQlRequestedFields) (string, error)
}

// Процессор генерации запроса получения данных
type builderProcessorInterface interface {
	// Проверка доступности процессора
	isAvailable(field configuration.FieldsConfiguration) bool

	// Генерация подстроки SQL для переданного поля
	build(field configuration.FieldsConfiguration) (string, error)
}
