package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации подзапроса для отношений
type relationProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (r relationProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подстроки SQL для переданного поля
func (r relationProcessor) build(field configuration.FieldsConfiguration) (string, error) {
	dbConf := field.DbConfiguration.(configuration.RelationFieldConfiguration)

	return fmt.Sprintf(
		`array_to_json(array(select t.%v from %v t where t.%v = %v.%v))`,
		dbConf.PrimaryKey,
		dbConf.Table,
		dbConf.ForeignKey,
		r.primaryTableAlias,
		dbConf.LocalKey,
	), nil
}

// Фабрика процессора
func newRelationProcessor(primaryTableAlias string) builderProcessorInterface {
	return &relationProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
