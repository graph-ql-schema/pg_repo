package selectFields

import (
	"fmt"
	"sort"
	"strings"
	"sync"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Результат асинхронной генерации SQL подзапросов
type asyncFieldsGenerationResult struct {
	priority int
	result   string
}

// Генератор подстроки запроса получения данных
type selectFieldsBuilder struct {
	configuration []configuration.FieldsConfiguration
	processors    []builderProcessorInterface
}

// Построение списка полей для выборки.
// Генерирует строку на основе конфигурации поле. Добавляет синонимы к выборке
// в соответствии с названием в GraphQL схеме
func (s selectFieldsBuilder) Build(fields requestedFieldsParser.GraphQlRequestedFields) (string, error) {
	if 0 == len(fields) {
		return "", nil
	}

	var wg sync.WaitGroup
	itemChan := make(chan asyncFieldsGenerationResult)
	errorChan := make(chan error)
	resultChan := make(chan string)
	errorResultChan := make(chan error)
	completeChan := make(chan bool)

	defer close(itemChan)
	defer close(errorChan)
	defer close(resultChan)
	defer close(errorResultChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов генерации
	go s.readFieldSqlGenerationResults(itemChan, errorChan, resultChan, errorResultChan, completeChan)

	// Запускаем генерацию
	wg.Add(len(fields))
	for i, field := range fields {
		go s.asyncFieldSqlGeneration(&wg, itemChan, errorChan, i, field)
	}

	wg.Wait()
	completeChan <- true

	sql := <-resultChan
	err := <-errorResultChan

	if nil != err {
		return "", err
	}

	return sql, nil
}

// Асинхронное чтение результатов генерации подзапросов для полей
func (s selectFieldsBuilder) readFieldSqlGenerationResults(
	itemChan chan asyncFieldsGenerationResult,
	errorChan chan error,
	resultChan chan string,
	errorResultChan chan error,
	completeChan chan bool,
) {
	sql := []asyncFieldsGenerationResult{}
	errors := []error{}

	for {
		select {
		case item := <-itemChan:
			sql = append(sql, item)
			break
		case err := <-errorChan:
			if nil != err {
				errors = append(errors, err)
			}
			break
		case _ = <-completeChan:
			messages := []string{}
			for i, err := range errors {
				messages = append(messages, fmt.Sprintf(`%v) %v;`, i+1, err.Error()))
			}

			var err error
			if 0 != len(messages) {
				err = fmt.Errorf(`some errors occured: %v`, strings.Join(messages, " "))
			}

			sort.SliceStable(sql, func(i, j int) bool {
				return sql[i].priority < sql[j].priority
			})

			sqlStrings := []string{}
			for _, subQuery := range sql {
				sqlStrings = append(sqlStrings, subQuery.result)
			}

			resultChan <- strings.Join(sqlStrings, ", ")
			errorResultChan <- err

			return
		}
	}
}

// Асинхронная генерация sql подзапроса для переданного поля
func (s selectFieldsBuilder) asyncFieldSqlGeneration(
	wg *sync.WaitGroup,
	itemChan chan asyncFieldsGenerationResult,
	errorChan chan error,
	num int,
	field requestedFieldsParser.GraphQlRequestedFieldInterface,
) {
	defer wg.Done()

	if 0 != len(field.GetSubFields()) {
		return
	}

	fieldConf := s.findFieldConfiguration(field.GetFieldName())
	if nil == fieldConf {
		return
	}

	sqlStr, err := s.generateSqlForField(*fieldConf)
	if nil != err {
		errorChan <- err
		return
	}

	itemChan <- asyncFieldsGenerationResult{
		priority: num,
		result:   sqlStr,
	}
}

// Генерация SQL подзапроса для переданного поля.
// Использует один из процессоров для генерации. Если доступного процессора нет, возвращает ошибку.
func (s selectFieldsBuilder) generateSqlForField(field configuration.FieldsConfiguration) (string, error) {
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			sqlStr, err := processor.build(field)
			if nil != err {
				return "", fmt.Errorf(`field '%v': %v`, field.GraphQlName, err.Error())
			}

			return fmt.Sprintf(`%v as %v`, sqlStr, field.GraphQlName), nil
		}
	}

	return "", fmt.Errorf(`no available processor for field '%v'`, field.GraphQlName)
}

// Поиск конфигурации поля по названию в GraphQL
func (s selectFieldsBuilder) findFieldConfiguration(graphQlName string) *configuration.FieldsConfiguration {
	for _, field := range s.configuration {
		if field.GraphQlName == graphQlName {
			return &field
		}
	}

	return nil
}

// Фабрика генератора
func NewSelectFieldsBuilder(
	primaryTableAlias string,
	configuration []configuration.FieldsConfiguration,
) SelectFieldsBuilderInterface {
	return &selectFieldsBuilder{
		configuration: configuration,
		processors: []builderProcessorInterface{
			newScalarProcessor(primaryTableAlias),
			newArrayProcessor(primaryTableAlias),
			newRelationProcessor(primaryTableAlias),
			newTableProcessor(primaryTableAlias),
		},
	}
}
