package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации подзапроса для отношений Many to Many
type tableProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (t tableProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подстроки SQL для переданного поля
func (t tableProcessor) build(field configuration.FieldsConfiguration) (string, error) {
	dbConf := field.DbConfiguration.(configuration.TableFieldConfiguration)

	return fmt.Sprintf(
		`array_to_json(array(select t.%v from %v t where t.%v = %v.%v))`,
		dbConf.Target,
		dbConf.Table,
		dbConf.ForeignKey,
		t.primaryTableAlias,
		dbConf.LocalKey,
	), nil
}

// Фабрика процессора
func newTableProcessor(primaryTableAlias string) builderProcessorInterface {
	return &tableProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
