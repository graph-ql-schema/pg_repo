package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Подставка для тестирования
type SelectFieldsBuilderMock struct {
	IsBuildError bool
	BuildResult  string
}

// Построение списка полей для выборки.
// Генерирует строку на основе конфигурации поле. Добавляет синонимы к выборке
// в соответствии с названием в GraphQL схеме
func (s SelectFieldsBuilderMock) Build(requestedFieldsParser.GraphQlRequestedFields) (string, error) {
	if s.IsBuildError {
		return "", fmt.Errorf(`test`)
	}

	return s.BuildResult, nil
}
