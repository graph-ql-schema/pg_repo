package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type builderProcessorMock struct {
	IsAvailable  bool
	IsBuildError bool
	BuildResult  string
}

// Проверка доступности процессора
func (b builderProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return b.IsAvailable
}

// Генерация подстроки SQL для переданного поля
func (b builderProcessorMock) build(configuration.FieldsConfiguration) (string, error) {
	if b.IsBuildError {
		return "", fmt.Errorf(`test`)
	}

	return b.BuildResult, nil
}
