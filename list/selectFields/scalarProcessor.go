package selectFields

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор скалярных типов
type scalarProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (s scalarProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ScalarFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подстроки SQL для переданного поля
func (s scalarProcessor) build(field configuration.FieldsConfiguration) (string, error) {
	dbConf := field.DbConfiguration.(configuration.ScalarFieldConfiguration)

	return fmt.Sprintf(`%v.%v`, s.primaryTableAlias, dbConf.DbName), nil
}

// Фабрика процессора
func newScalarProcessor(primaryTableAlias string) builderProcessorInterface {
	return &scalarProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
