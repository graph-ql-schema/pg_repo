package selectFields

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_tableProcessor_isAvailable(t1 *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				primaryTableAlias: "p",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				primaryTableAlias: "p",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "rel_tbl",
						Target:     "id",
						ForeignKey: "test_id",
						LocalKey:   "uuid",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := t.isAvailable(tt.args.field); got != tt.want {
				t1.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_tableProcessor_build(t1 *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Тестирование генерации запроса",
			fields: fields{
				primaryTableAlias: "p",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "rel_tbl",
						Target:     "id",
						ForeignKey: "test_id",
						LocalKey:   "uuid",
					},
				},
			},
			want:    "array_to_json(array(select t.id from rel_tbl t where t.test_id = p.uuid))",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			got, err := t.build(tt.args.field)
			if (err != nil) != tt.wantErr {
				t1.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t1.Errorf("build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
