package order

import (
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Генератор подзапроса сортировки
type orderSqlGenerator struct{}

// Генерация подзапроса сортировки по параметрам GraphQL
func (o orderSqlGenerator) Generate(params sbuilder.Parameters) *string {
	if 0 == len(params.Arguments.OrderBy) {
		return nil
	}

	orders := params.Arguments.OrderBy
	sort.SliceStable(orders, func(i, j int) bool {
		return orders[i].Priority < orders[j].Priority
	})

	orderStr := []string{}
	for _, order := range orders {
		orderStr = append(orderStr, fmt.Sprintf(`%v %v`, order.By, order.Direction))
	}

	order := strings.Join(orderStr, ", ")

	return &order
}

// Фабрика генератора
func NewOrderSqlGenerator() OrderSqlGeneratorInterface {
	return &orderSqlGenerator{}
}
