package order

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Подставка для тестирования
type OrderSqlGeneratorMock struct {
	Result string
}

// Генерация подзапроса сортировки по параметрам GraphQL
func (o OrderSqlGeneratorMock) Generate(sbuilder.Parameters) *string {
	if 0 == len(o.Result) {
		return nil
	}

	return &o.Result
}
