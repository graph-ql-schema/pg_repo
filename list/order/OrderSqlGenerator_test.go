package order

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
)

// Тестирование генерации подстроки сортировки
func Test_orderSqlGenerator_Generate(t *testing.T) {
	nameDesc := "name desc"
	twoOrder := "date asc, name desc"

	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		{
			name: "Если не переданы параметры сортировки",
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						OrderBy: []orderParser.Order{},
					},
				},
			},
			want: nil,
		},
		{
			name: "Если сортировка только одна",
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "name",
								Direction: "desc",
							},
						},
					},
				},
			},
			want: &nameDesc,
		},
		{
			name: "Если 2 сортировки",
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						OrderBy: []orderParser.Order{
							{
								Priority:  2,
								By:        "date",
								Direction: "asc",
							},
							{
								Priority:  3,
								By:        "name",
								Direction: "desc",
							},
						},
					},
				},
			},
			want: &twoOrder,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := orderSqlGenerator{}
			if got := o.Generate(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", *got, *tt.want)
			}
		})
	}
}
