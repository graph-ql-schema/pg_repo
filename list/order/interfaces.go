package order

import "bitbucket.org/graph-ql-schema/sbuilder/v2"

// Генератор подзапроса сортировки
type OrderSqlGeneratorInterface interface {
	// Генерация подзапроса сортировки по параметрам GraphQL
	Generate(params sbuilder.Parameters) *string
}
