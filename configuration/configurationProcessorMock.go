package configuration

import (
	"fmt"
	"reflect"
)

// Подставка для тестирования
type configurationProcessorMock struct {
	IsAvailableRes    bool
	IsGenerationError bool
	GenerateRes       FieldsConfiguration
}

// Проверка доступности процессора
func (c configurationProcessorMock) isAvailable(reflect.StructField) bool {
	return c.IsAvailableRes
}

// Генерация конфигурации процессором
func (c configurationProcessorMock) generate(reflect.StructField, FieldsConfiguration) (FieldsConfiguration, error) {
	if c.IsGenerationError {
		return FieldsConfiguration{}, fmt.Errorf(`test`)
	}

	return c.GenerateRes, nil
}
