package configuration

// Параметры конфигурации для простого поля
type ScalarFieldConfiguration struct {
	DbName string
}

// Фабрика конфигурации
type TScalarFieldConfigurationFactory = func(dbName string) ScalarFieldConfiguration

func newScalarFieldConfiguration(dbName string) ScalarFieldConfiguration {
	return ScalarFieldConfiguration{
		DbName: dbName,
	}
}
