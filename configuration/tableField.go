package configuration

// Тип, описывающий конфигурацию отношения 1 to Many
type TableFieldConfiguration struct {
	Table      string
	Target     string
	ForeignKey string
	LocalKey   string
}

// Фабрика конфигурации
type TTableFieldConfigurationFactory = func(table string, target string, foreignKey string, localKey string) TableFieldConfiguration

func newTableFieldConfiguration(
	table string,
	target string,
	foreignKey string,
	localKey string,
) TableFieldConfiguration {
	return TableFieldConfiguration{
		Table:      table,
		Target:     target,
		ForeignKey: foreignKey,
		LocalKey:   localKey,
	}
}
