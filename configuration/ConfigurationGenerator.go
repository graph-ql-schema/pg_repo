package configuration

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

// Генератор конфигурации для переданного типа сущности
type configurationGenerator struct {
	processors []configurationProcessor
}

// Генерация конфигурации
func (c configurationGenerator) Generate(entityType reflect.Type) ([]FieldsConfiguration, error) {
	if entityType.Kind() != reflect.Struct {
		return nil, fmt.Errorf(`you shoul pass 'struct' type. current type: %v`, entityType.Kind().String())
	}

	fields := []FieldsConfiguration{}
	for i := 0; i < entityType.NumField(); i++ {
		field := entityType.Field(i)

		jsonTag := field.Tag.Get("json")
		if 0 == len(jsonTag) {
			return nil, fmt.Errorf(`you should set annotation tag 'json' for field '%v'`, field.Name)
		}

		typeTag := field.Tag.Get("pg.type")
		if 0 == len(typeTag) {
			return nil, fmt.Errorf(`you should set annotation tag 'pg.type' for field '%v'`, field.Name)
		}

		alreadyLoad := field.Tag.Get("pg.alreadyLoad")
		if 0 == len(alreadyLoad) {
			alreadyLoad = "false"
		}

		alreadyLoadBool, err := strconv.ParseBool(alreadyLoad)
		if nil != err {
			alreadyLoadBool = false
		}

		config := FieldsConfiguration{
			Field:           field.Name,
			FieldNum:        i,
			GraphQlName:     jsonTag,
			FieldType:       typeTag,
			IsAlreadyLoad:   alreadyLoadBool,
			DbConfiguration: nil,
		}

		isProcAvailable := false
		for _, processor := range c.processors {
			if processor.isAvailable(field) {
				isProcAvailable = true
				config, err := processor.generate(field, config)
				if nil != err {
					return nil, err
				}

				if config.GraphQlName == "-" {
					config.GraphQlName = strings.ToLower(config.Field)
				}

				fields = append(fields, config)
				break
			}
		}

		if false == isProcAvailable {
			return nil, fmt.Errorf(`you does not set any type of field for field '%v'`, field.Name)
		}
	}

	return fields, nil
}

// Фабрика генератора конфигурации
func NewConfigurationGenerator() ConfigurationGeneratorInterface {
	return &configurationGenerator{
		processors: []configurationProcessor{
			newTableConfigurationProcessor(newTableFieldConfiguration),
			newRelationConfigurationProcessor(newRelationFieldConfiguration),
			newArrayConfigurationProcessor(newArrayFieldConfiguration),
			newScalarConfigurationProcessor(newScalarFieldConfiguration),
		},
	}
}
