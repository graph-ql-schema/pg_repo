package configuration

import (
	"reflect"
	"testing"
)

// Тестирование фабрики
func TestNewRelationFieldConfiguration(t *testing.T) {
	type args struct {
		table      string
		primaryKey string
		foreignKey string
		localKey   string
	}
	tests := []struct {
		name string
		args args
		want RelationFieldConfiguration
	}{
		{
			name: "Тестирование фабрики",
			args: args{
				table:      "test",
				primaryKey: "tt",
				foreignKey: "ttt",
				localKey:   "tttt",
			},
			want: RelationFieldConfiguration{
				Table:      "test",
				PrimaryKey: "tt",
				ForeignKey: "ttt",
				LocalKey:   "tttt",
				Conflict: RelationConflictConfiguration{
					Drop: DropPolicyDrop,
					Free: FreePolicyDrop,
					Busy: BusyPolicyReplace,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newRelationFieldConfiguration(tt.args.table, tt.args.primaryKey, tt.args.foreignKey, tt.args.localKey); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newRelationFieldConfiguration() = %v, want %v", got, tt.want)
			}
		})
	}
}
