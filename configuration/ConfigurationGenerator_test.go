package configuration

import (
	"reflect"
	"testing"
)

// Тестирование генерации конфигурации
func Test_configurationGenerator_Generate(t *testing.T) {
	type WithoutTag struct {
		Field string
	}

	type CorrectTypeWithoutPgType struct {
		Field string `pg_repo:"field" json:"field"`
	}

	type CorrectTypeWithoutJson struct {
		Field string `pg_repo:"field" pg.type:"int"`
	}

	type CorrectType struct {
		Field string `pg_repo:"field" json:"field" pg.type:"int"`
	}

	type fields struct {
		processors []configurationProcessor
	}
	type args struct {
		entityType reflect.Type
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []FieldsConfiguration
		wantErr bool
	}{
		{
			name: "Когда переданный тип - не структура",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    false,
						IsGenerationError: false,
						GenerateRes:       FieldsConfiguration{},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf("string"),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда у структуры не заданы аннотации",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    false,
						IsGenerationError: false,
						GenerateRes:       FieldsConfiguration{},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(WithoutTag{}),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда у структуры не задана JSON аннотация",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    false,
						IsGenerationError: false,
						GenerateRes:       FieldsConfiguration{},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(CorrectTypeWithoutJson{}),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда у структуры не задана Pg.Type аннотация",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    false,
						IsGenerationError: false,
						GenerateRes:       FieldsConfiguration{},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(CorrectTypeWithoutPgType{}),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда нет доступных процессоров",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    false,
						IsGenerationError: false,
						GenerateRes:       FieldsConfiguration{},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(CorrectType{}),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда произошла ошибка генерации",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    true,
						IsGenerationError: true,
						GenerateRes: FieldsConfiguration{
							Field:           "test",
							GraphQlName:     "tt",
							DbConfiguration: nil,
						},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(CorrectType{}),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Когда нет ошибок",
			fields: fields{
				processors: []configurationProcessor{
					configurationProcessorMock{
						IsAvailableRes:    true,
						IsGenerationError: false,
						GenerateRes: FieldsConfiguration{
							Field:           "test",
							GraphQlName:     "tt",
							FieldType:       "int",
							DbConfiguration: nil,
						},
					},
				},
			},
			args: args{
				entityType: reflect.TypeOf(CorrectType{}),
			},
			want: []FieldsConfiguration{
				{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "tt",
					FieldType:       "int",
					DbConfiguration: nil,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := configurationGenerator{
				processors: tt.fields.processors,
			}
			got, err := c.Generate(tt.args.entityType)
			if (err != nil) != tt.wantErr {
				t.Errorf("Generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
