package configuration

import "reflect"

// Параметры конфигурации поля
type FieldsConfiguration struct {
	Field           string
	FieldNum        int
	GraphQlName     string
	FieldType       string
	IsAlreadyLoad   bool
	DbConfiguration interface{}
}

// Константы типов хранилища поля
const (
	StorageScalar   string = "scalar"
	StorageArray    string = "array"
	StorageRelation string = "rel"
	StorageTable    string = "rtbl"
)

// Генератор конфигурации для переданного типа сущности
type ConfigurationGeneratorInterface interface {
	// Генерация конфигурации
	Generate(entityType reflect.Type) ([]FieldsConfiguration, error)
}

// Процессор конфигурации
type configurationProcessor interface {
	// Проверка доступности процессора
	isAvailable(field reflect.StructField) bool

	// Генерация конфигурации процессором
	generate(field reflect.StructField, config FieldsConfiguration) (FieldsConfiguration, error)
}
