package configuration

// Параметры конфигурации для поля - массива
type ArrayFieldConfiguration struct {
	DbName string
}

// Фабрика конфигурации
type TArrayFieldConfigurationFactory = func(dbName string) ArrayFieldConfiguration

func newArrayFieldConfiguration(dbName string) ArrayFieldConfiguration {
	return ArrayFieldConfiguration{
		DbName: dbName,
	}
}
