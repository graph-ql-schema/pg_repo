package configuration

import (
	"reflect"
	"strings"
)

// Процессор скалярных типов
type scalarConfigurationProcessor struct {
	factory TScalarFieldConfigurationFactory
}

// Проверка доступности процессора
func (s scalarConfigurationProcessor) isAvailable(field reflect.StructField) bool {
	fName := field.Tag.Get("pg.field")
	storage := field.Tag.Get("pg.storage")

	return 0 != len(fName) && (0 == len(storage) || StorageScalar == strings.ToLower(storage))
}

// Генерация конфигурации процессором
func (s scalarConfigurationProcessor) generate(field reflect.StructField, config FieldsConfiguration) (FieldsConfiguration, error) {
	fName := field.Tag.Get("pg.field")
	scalarConfig := s.factory(fName)

	return FieldsConfiguration{
		Field:           config.Field,
		FieldNum:        config.FieldNum,
		FieldType:       config.FieldType,
		GraphQlName:     config.GraphQlName,
		IsAlreadyLoad:   config.IsAlreadyLoad,
		DbConfiguration: scalarConfig,
	}, nil
}

// Фабрика процессора
func newScalarConfigurationProcessor(factory TScalarFieldConfigurationFactory) configurationProcessor {
	return &scalarConfigurationProcessor{
		factory: factory,
	}
}
