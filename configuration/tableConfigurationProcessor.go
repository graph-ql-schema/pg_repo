package configuration

import (
	"fmt"
	"reflect"
)

// Процессор конфигурации отношений Many to Many
type tableConfigurationProcessor struct {
	factory TTableFieldConfigurationFactory
}

// Проверка доступности процессора
func (t tableConfigurationProcessor) isAvailable(field reflect.StructField) bool {
	storage := field.Tag.Get("pg.storage")

	return StorageTable == storage
}

// Генерация конфигурации процессором
func (t tableConfigurationProcessor) generate(field reflect.StructField, config FieldsConfiguration) (FieldsConfiguration, error) {
	table := field.Tag.Get("pg.rtbl.tbl")
	target := field.Tag.Get("pg.rtbl.target")
	fKey := field.Tag.Get("pg.rtbl.fkey")
	lKey := field.Tag.Get("pg.rtbl.lkey")

	if 0 == len(table) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rtbl.tbl' for field '%v'`, field.Name)
	}

	if 0 == len(target) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rtbl.target' for field '%v'`, field.Name)
	}

	if 0 == len(fKey) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rtbl.fkey' for field '%v'`, field.Name)
	}

	if 0 == len(lKey) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rtbl.lkey' for field '%v'`, field.Name)
	}

	fieldConf := t.factory(table, target, fKey, lKey)
	return FieldsConfiguration{
		Field:           config.Field,
		FieldNum:        config.FieldNum,
		FieldType:       config.FieldType,
		GraphQlName:     config.GraphQlName,
		IsAlreadyLoad:   config.IsAlreadyLoad,
		DbConfiguration: fieldConf,
	}, nil
}

// Фабрика процессора
func newTableConfigurationProcessor(factory TTableFieldConfigurationFactory) configurationProcessor {
	return &tableConfigurationProcessor{
		factory: factory,
	}
}
