package configuration

import (
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_tableConfigurationProcessor_isAvailable(t1 *testing.T) {
	factory := func(table string, target string, foreignKey string, localKey string) TableFieldConfiguration {
		return TableFieldConfiguration{
			Table:      table,
			Target:     target,
			ForeignKey: foreignKey,
			LocalKey:   localKey,
		}
	}

	type TestStructWithoutAnnotation struct {
		Test string
	}

	type TestStruct struct {
		Test string `pg.storage:"rtbl"`
	}

	type TestStructWithIncorrectStorage struct {
		Test string `pg.storage:"array"`
	}

	type fields struct {
		factory TTableFieldConfigurationFactory
	}
	type args struct {
		field reflect.StructField
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на поле без аннотаций",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutAnnotation{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле с не доступным хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithIncorrectStorage{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле с хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableConfigurationProcessor{
				factory: tt.fields.factory,
			}
			if got := t.isAvailable(tt.args.field); got != tt.want {
				t1.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_tableConfigurationProcessor_generate(t1 *testing.T) {
	factory := func(table string, target string, foreignKey string, localKey string) TableFieldConfiguration {
		return TableFieldConfiguration{
			Table:      table,
			Target:     target,
			ForeignKey: foreignKey,
			LocalKey:   localKey,
		}
	}

	type TestStructWithoutTable struct {
		Test string `pg.storage:"rtbl" pg.rtbl.target:"1" pg.rtbl.fkey:"2" pg.rtbl.lkey:"3"`
	}

	type TestStructWithoutTarget struct {
		Test string `pg.storage:"rtbl" pg.rtbl.tbl:"1" pg.rtbl.fkey:"2" pg.rtbl.lkey:"3"`
	}

	type TestStructWithoutFKey struct {
		Test string `pg.storage:"rtbl" pg.rtbl.tbl:"1" pg.rtbl.target:"2" pg.rtbl.lkey:"3"`
	}

	type TestStructWithoutLKey struct {
		Test string `pg.storage:"rtbl" pg.rtbl.tbl:"1" pg.rtbl.target:"2" pg.rtbl.fkey:"3"`
	}

	type TestStruct struct {
		Test string `pg.storage:"rtbl" pg.rtbl.tbl:"1" pg.rtbl.target:"2" pg.rtbl.fkey:"3" pg.rtbl.lkey:"4"`
	}

	type fields struct {
		factory TTableFieldConfigurationFactory
	}
	type args struct {
		field  reflect.StructField
		config FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    FieldsConfiguration
		wantErr bool
	}{
		{
			name: "Тестирование ошибки таблицы",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutTable{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки первичного ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutTarget{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки внешнего ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutFKey{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки локального ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutLKey{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "tt",
				DbConfiguration: TableFieldConfiguration{
					Table:      "1",
					Target:     "2",
					ForeignKey: "3",
					LocalKey:   "4",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableConfigurationProcessor{
				factory: tt.fields.factory,
			}
			got, err := t.generate(tt.args.field, tt.args.config)
			if (err != nil) != tt.wantErr {
				t1.Errorf("generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
