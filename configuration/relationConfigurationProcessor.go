package configuration

import (
	"fmt"
	"reflect"
)

// Процессор конфигурации отношения 1 to Many
type relationConfigurationProcessor struct {
	factory TRelationFieldConfigurationFactory
}

// Проверка доступности процессора
func (r relationConfigurationProcessor) isAvailable(field reflect.StructField) bool {
	storage := field.Tag.Get("pg.storage")

	return StorageRelation == storage
}

// Генерация конфигурации процессором
func (r relationConfigurationProcessor) generate(field reflect.StructField, config FieldsConfiguration) (FieldsConfiguration, error) {
	table := field.Tag.Get("pg.rel.tbl")
	pKey := field.Tag.Get("pg.rel.pkey")
	fKey := field.Tag.Get("pg.rel.fkey")
	lKey := field.Tag.Get("pg.rel.lkey")
	conflictDrop := field.Tag.Get("pg.rel.conflict.drop")
	conflictFree := field.Tag.Get("pg.rel.conflict.free")
	conflictBusy := field.Tag.Get("pg.rel.conflict.busy")

	if 0 == len(table) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rel.tbl' for field '%v'`, field.Name)
	}

	if 0 == len(pKey) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rel.pkey' for field '%v'`, field.Name)
	}

	if 0 == len(fKey) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rel.fkey' for field '%v'`, field.Name)
	}

	if 0 == len(lKey) {
		return FieldsConfiguration{}, fmt.Errorf(`you should set 'pg.rel.lkey' for field '%v'`, field.Name)
	}

	resultConfig := r.factory(table, pKey, fKey, lKey)
	if 0 != len(conflictDrop) {
		switch TDrop(conflictDrop) {
		case DropPolicySkip:
			resultConfig.Conflict.Drop = DropPolicySkip
			break
		case DropPolicyNull:
			resultConfig.Conflict.Drop = DropPolicyNull
			break
		case DropPolicyDrop:
			resultConfig.Conflict.Drop = DropPolicyDrop
			break
		default:
			return FieldsConfiguration{}, fmt.Errorf(`unavailable value for annotation 'pg.rel.conflict.drop' for field '%v': %v`, field.Name, conflictDrop)
		}
	}

	if 0 != len(conflictFree) {
		switch TFree(conflictFree) {
		case FreePolicyDrop:
			resultConfig.Conflict.Free = FreePolicyDrop
			break
		case FreePolicyNull:
			resultConfig.Conflict.Free = FreePolicyNull
			break
		default:
			return FieldsConfiguration{}, fmt.Errorf(`unavailable value for annotation 'pg.rel.conflict.free' for field '%v': %v`, field.Name, conflictFree)
		}
	}

	if 0 != len(conflictBusy) {
		switch TBusy(conflictBusy) {
		case BusyPolicyReplace:
			resultConfig.Conflict.Busy = BusyPolicyReplace
			break
		case BusyPolicySkip:
			resultConfig.Conflict.Busy = BusyPolicySkip
			break
		default:
			return FieldsConfiguration{}, fmt.Errorf(`unavailable value for annotation 'pg.rel.conflict.busy' for field '%v': %v`, field.Name, conflictBusy)
		}
	}

	return FieldsConfiguration{
		Field:           config.Field,
		FieldNum:        config.FieldNum,
		FieldType:       config.FieldType,
		GraphQlName:     config.GraphQlName,
		IsAlreadyLoad:   config.IsAlreadyLoad,
		DbConfiguration: resultConfig,
	}, nil
}

// Фабрика процессора
func newRelationConfigurationProcessor(factory TRelationFieldConfigurationFactory) configurationProcessor {
	return &relationConfigurationProcessor{
		factory: factory,
	}
}
