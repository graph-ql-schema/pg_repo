package configuration

// Тип, описывающий варианты резолва политики Drop
type TDrop string

const (
	DropPolicySkip TDrop = "skip"
	DropPolicyDrop TDrop = "drop"
	DropPolicyNull TDrop = "null"
)

// Тип, описывающий варианты резолва политики Free
type TFree string

const (
	FreePolicyDrop TFree = "drop"
	FreePolicyNull TFree = "null"
)

// Тип, описывающий варианты резолва политики Busy
type TBusy string

const (
	BusyPolicySkip    TBusy = "skip"
	BusyPolicyReplace TBusy = "replace"
)

// Тип, описывающий конфигурацию политик разрешения конфликта для отношения 1 to Many
type RelationConflictConfiguration struct {
	Drop TDrop
	Free TFree
	Busy TBusy
}

// Тип, описывающий конфигурацию отношения 1 to Many
type RelationFieldConfiguration struct {
	Table      string
	PrimaryKey string
	ForeignKey string
	LocalKey   string
	Conflict   RelationConflictConfiguration
}

// Фабрика конфигурации
type TRelationFieldConfigurationFactory = func(table string, primaryKey string, foreignKey string, localKey string) RelationFieldConfiguration

func newRelationFieldConfiguration(
	table string,
	primaryKey string,
	foreignKey string,
	localKey string,
) RelationFieldConfiguration {
	return RelationFieldConfiguration{
		Table:      table,
		PrimaryKey: primaryKey,
		ForeignKey: foreignKey,
		LocalKey:   localKey,
		Conflict: RelationConflictConfiguration{
			Drop: DropPolicyDrop,
			Free: FreePolicyDrop,
			Busy: BusyPolicyReplace,
		},
	}
}
