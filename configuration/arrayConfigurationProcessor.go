package configuration

import (
	"reflect"
)

// Процессор генерации конфигурации массивов
type arrayConfigurationProcessor struct {
	factory TArrayFieldConfigurationFactory
}

// Проверка доступности процессора
func (a arrayConfigurationProcessor) isAvailable(field reflect.StructField) bool {
	fName := field.Tag.Get("pg.field")
	storage := field.Tag.Get("pg.storage")

	return 0 != len(fName) && StorageArray == storage
}

// Генерация конфигурации процессором
func (a arrayConfigurationProcessor) generate(field reflect.StructField, config FieldsConfiguration) (FieldsConfiguration, error) {
	fName := field.Tag.Get("pg.field")
	arrayConfig := a.factory(fName)

	return FieldsConfiguration{
		Field:           config.Field,
		FieldNum:        config.FieldNum,
		GraphQlName:     config.GraphQlName,
		FieldType:       config.FieldType,
		IsAlreadyLoad:   config.IsAlreadyLoad,
		DbConfiguration: arrayConfig,
	}, nil
}

// Фабрика процессора
func newArrayConfigurationProcessor(factory TArrayFieldConfigurationFactory) configurationProcessor {
	return &arrayConfigurationProcessor{
		factory: factory,
	}
}
