package configuration

import (
	"reflect"
	"testing"
)

// Тестирование фабрики
func TestNewArrayFieldConfiguration(t *testing.T) {
	type args struct {
		dbName string
	}
	tests := []struct {
		name string
		args args
		want ArrayFieldConfiguration
	}{
		{
			name: "Тестирование фабрики",
			args: args{
				dbName: "test",
			},
			want: ArrayFieldConfiguration{
				DbName: "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newArrayFieldConfiguration(tt.args.dbName); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newArrayFieldConfiguration() = %v, want %v", got, tt.want)
			}
		})
	}
}
