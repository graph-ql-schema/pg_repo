package configuration

import (
	"reflect"
	"testing"
)

// Проверка доступности процессора
func Test_relationConfigurationProcessor_isAvailable(t *testing.T) {
	factory := func(table string, primaryKey string, foreignKey string, localKey string) RelationFieldConfiguration {
		return RelationFieldConfiguration{
			Table:      table,
			PrimaryKey: primaryKey,
			ForeignKey: foreignKey,
			LocalKey:   localKey,
			Conflict:   RelationConflictConfiguration{},
		}
	}

	type TestStructWithoutAnnotation struct {
		Test string
	}

	type TestStruct struct {
		Test string `pg.storage:"rel"`
	}

	type TestStructWithIncorrectStorage struct {
		Test string `pg.storage:"array"`
	}

	type fields struct {
		factory TRelationFieldConfigurationFactory
	}
	type args struct {
		field reflect.StructField
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на поле без аннотаций",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutAnnotation{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле с не доступным хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithIncorrectStorage{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле с хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := relationConfigurationProcessor{
				factory: tt.fields.factory,
			}
			if got := r.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_relationConfigurationProcessor_generate(t *testing.T) {
	factory := func(table string, primaryKey string, foreignKey string, localKey string) RelationFieldConfiguration {
		return RelationFieldConfiguration{
			Table:      table,
			PrimaryKey: primaryKey,
			ForeignKey: foreignKey,
			LocalKey:   localKey,
			Conflict:   RelationConflictConfiguration{},
		}
	}

	type TestStructWithoutTable struct {
		Test string `pg.storage:"rel" pg.rel.pkey:"1" pg.rel.fkey:"2" pg.rel.lkey:"3"`
	}

	type TestStructWithoutPKey struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.fkey:"2" pg.rel.lkey:"3"`
	}

	type TestStructWithoutFKey struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.lkey:"3"`
	}

	type TestStructWithoutLKey struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3"`
	}

	type TestStruct struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4"`
	}

	type TestStructWithDropErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.drop:"aaa"`
	}

	type TestStructWithoutDropErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.drop:"skip"`
	}

	type TestStructWithFreeErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.free:"aaa"`
	}

	type TestStructWithoutFreeErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.free:"drop"`
	}

	type TestStructWithBusyErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.busy:"aaa"`
	}

	type TestStructWithoutBusyErr struct {
		Test string `pg.storage:"rel" pg.rel.tbl:"1" pg.rel.pkey:"2" pg.rel.fkey:"3" pg.rel.lkey:"4" pg.rel.conflict.busy:"skip"`
	}

	type fields struct {
		factory TRelationFieldConfigurationFactory
	}
	type args struct {
		field  reflect.StructField
		config FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    FieldsConfiguration
		wantErr bool
	}{
		{
			name: "Тестирование ошибки таблицы",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutTable{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки первичного ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutPKey{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки внешнего ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutFKey{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование ошибки локального ключа",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutLKey{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "tt",
				DbConfiguration: RelationFieldConfiguration{
					Table:      "1",
					PrimaryKey: "2",
					ForeignKey: "3",
					LocalKey:   "4",
					Conflict:   RelationConflictConfiguration{},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование с ошибкой политики Drop",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithDropErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок политики Drop",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutDropErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "tt",
				DbConfiguration: RelationFieldConfiguration{
					Table:      "1",
					PrimaryKey: "2",
					ForeignKey: "3",
					LocalKey:   "4",
					Conflict: RelationConflictConfiguration{
						Drop: DropPolicySkip,
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование с ошибкой политики Free",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithFreeErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок политики Free",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutFreeErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "tt",
				DbConfiguration: RelationFieldConfiguration{
					Table:      "1",
					PrimaryKey: "2",
					ForeignKey: "3",
					LocalKey:   "4",
					Conflict: RelationConflictConfiguration{
						Free: FreePolicyDrop,
					},
				},
			},
			wantErr: false,
		},
		{
			name: "Тестирование с ошибкой политики Busy",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithBusyErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want:    FieldsConfiguration{},
			wantErr: true,
		},
		{
			name: "Тестирование без ошибок политики Busy",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutBusyErr{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "tt",
				DbConfiguration: RelationFieldConfiguration{
					Table:      "1",
					PrimaryKey: "2",
					ForeignKey: "3",
					LocalKey:   "4",
					Conflict: RelationConflictConfiguration{
						Busy: BusyPolicySkip,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := relationConfigurationProcessor{
				factory: tt.fields.factory,
			}
			got, err := r.generate(tt.args.field, tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
