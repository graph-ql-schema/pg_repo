package configuration

import (
	"reflect"
	"testing"
)

// Тестирование фабрики конфигурации
func TestNewTableFieldConfiguration(t *testing.T) {
	type args struct {
		table      string
		target     string
		foreignKey string
		localKey   string
	}
	tests := []struct {
		name string
		args args
		want TableFieldConfiguration
	}{
		{
			name: "Тестирование фабрики конфигурации",
			args: args{
				table:      "test",
				target:     "tt",
				foreignKey: "ttt",
				localKey:   "tttt",
			},
			want: TableFieldConfiguration{
				Table:      "test",
				Target:     "tt",
				ForeignKey: "ttt",
				LocalKey:   "tttt",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newTableFieldConfiguration(tt.args.table, tt.args.target, tt.args.foreignKey, tt.args.localKey); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newTableFieldConfiguration() = %v, want %v", got, tt.want)
			}
		})
	}
}
