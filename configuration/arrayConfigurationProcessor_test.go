package configuration

import (
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_arrayConfigurationProcessor_isAvailable(t *testing.T) {
	factory := func(dbName string) ArrayFieldConfiguration {
		return ArrayFieldConfiguration{}
	}

	type TestStructWithoutAnnotation struct {
		Test string
	}

	type TestStruct struct {
		Test string `pg.field:"test"`
	}

	type TestStructWithStorage struct {
		Test string `pg.field:"test" pg.storage:"array"`
	}

	type TestStructWithUnavailableStorage struct {
		Test string `pg.field:"test" pg.storage:"scalar"`
	}

	type fields struct {
		factory TArrayFieldConfigurationFactory
	}
	type args struct {
		field reflect.StructField
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на поле без аннотаций",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithoutAnnotation{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле без хранилища",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
			},
			want: false,
		},
		{
			name: "Тестирование на поле с хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithStorage{}).Field(0),
			},
			want: true,
		},
		{
			name: "Тестирование на поле с не доступным хранилищем",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStructWithUnavailableStorage{}).Field(0),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayConfigurationProcessor{
				factory: tt.fields.factory,
			}
			if got := a.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации процессора
func Test_arrayConfigurationProcessor_generate(t *testing.T) {
	factory := func(dbName string) ArrayFieldConfiguration {
		return ArrayFieldConfiguration{
			DbName: dbName,
		}
	}

	type TestStruct struct {
		Test string `pg.field:"test" pg.storage:"array"`
	}

	type fields struct {
		factory TArrayFieldConfigurationFactory
	}
	type args struct {
		field  reflect.StructField
		config FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    FieldsConfiguration
		wantErr bool
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				factory: factory,
			},
			args: args{
				field: reflect.TypeOf(TestStruct{}).Field(0),
				config: FieldsConfiguration{
					Field:           "test",
					FieldNum:        1,
					GraphQlName:     "test-1",
					DbConfiguration: nil,
				},
			},
			want: FieldsConfiguration{
				Field:       "test",
				FieldNum:    1,
				GraphQlName: "test-1",
				DbConfiguration: ArrayFieldConfiguration{
					DbName: "test",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayConfigurationProcessor{
				factory: tt.fields.factory,
			}
			got, err := a.generate(tt.args.field, tt.args.config)
			if (err != nil) != tt.wantErr {
				t.Errorf("generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generate() got = %v, want %v", got, tt.want)
			}
		})
	}
}
