package configuration

import (
	"reflect"
	"testing"
)

// Тестирование фабрики
func TestNewScalarFieldConfiguration(t *testing.T) {
	type args struct {
		dbName string
	}
	tests := []struct {
		name string
		args args
		want ScalarFieldConfiguration
	}{
		{
			name: "Тестирование фабрики",
			args: args{
				dbName: "test",
			},
			want: ScalarFieldConfiguration{
				DbName: "test",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newScalarFieldConfiguration(tt.args.dbName); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newScalarFieldConfiguration() = %v, want %v", got, tt.want)
			}
		})
	}
}
