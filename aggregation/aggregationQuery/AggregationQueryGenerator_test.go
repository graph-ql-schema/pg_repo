package aggregationQuery

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/variableGenerator"
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/groupBy"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/selectAggregate"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/groupByParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/orderParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/paginationParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование генерации
func Test_aggregationQueryGenerator_Generate(t *testing.T) {
	where := "test=1"
	groupByStr := "test_n, test_f"
	type fields struct {
		primaryTable      string
		primaryTableAlias string
		groupBy           groupBy.GroupByGeneratorInterface
		selectGenerator   selectAggregate.SelectAggregateGeneratorInterface
		filter            filter.WhereGeneratorInterface
		joinGenerator     join.JoinGeneratorInterface
		configuration     []configuration.FieldsConfiguration
		variableGenerator variableGenerator.VariableGeneratorInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Не удалось сгенерировать поля для выборки",
			fields: fields{
				primaryTable:      "test",
				primaryTableAlias: "tt",
				groupBy: groupBy.GroupByGeneratorMock{
					GenerateResult: nil,
				},
				selectGenerator: selectAggregate.SelectAggregateGeneratorMock{
					GenerationResult: "",
				},
				filter: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        nil,
				},
				joinGenerator: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "test",
				},
				configuration:     []configuration.FieldsConfiguration{},
				variableGenerator: &VariableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test",
						},
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Не удалось сгенерировать параметры фильтрации",
			fields: fields{
				primaryTable:      "test",
				primaryTableAlias: "tt",
				groupBy: groupBy.GroupByGeneratorMock{
					GenerateResult: nil,
				},
				selectGenerator: selectAggregate.SelectAggregateGeneratorMock{
					GenerationResult: "test",
				},
				filter: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       true,
					BuildResult:        nil,
				},
				joinGenerator: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "test",
				},
				configuration:     []configuration.FieldsConfiguration{},
				variableGenerator: &VariableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test",
						},
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Тестирование без группировки, сортировки и join",
			fields: fields{
				primaryTable:      "test",
				primaryTableAlias: "tt",
				groupBy: groupBy.GroupByGeneratorMock{
					GenerateResult: nil,
				},
				selectGenerator: selectAggregate.SelectAggregateGeneratorMock{
					GenerationResult: "test",
				},
				filter: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &where,
				},
				joinGenerator: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "test",
				},
				configuration:     []configuration.FieldsConfiguration{},
				variableGenerator: &VariableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test",
						},
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want:    "select test as data from test tt where test=1 limit 10 offset 0",
			wantErr: false,
		},
		{
			name: "Тестирование без join",
			fields: fields{
				primaryTable:      "test",
				primaryTableAlias: "tt",
				groupBy: groupBy.GroupByGeneratorMock{
					GenerateResult: &groupByStr,
				},
				selectGenerator: selectAggregate.SelectAggregateGeneratorMock{
					GenerationResult: "test",
				},
				filter: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &where,
				},
				joinGenerator: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "test",
				},
				configuration:     []configuration.FieldsConfiguration{},
				variableGenerator: &VariableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test",
						},
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want:    "select test as data from test tt where test=1 group by test_n, test_f order by test_n, test_f limit 10 offset 0",
			wantErr: false,
		},
		{
			name: "Тестирование со всеми параметрами",
			fields: fields{
				primaryTable:      "test",
				primaryTableAlias: "tt",
				groupBy: groupBy.GroupByGeneratorMock{
					GenerateResult: &groupByStr,
				},
				selectGenerator: selectAggregate.SelectAggregateGeneratorMock{
					GenerationResult: "test",
				},
				filter: filter.WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &where,
				},
				joinGenerator: join.JoinGeneratorMock{
					GenerateResult: []join.Join{
						{
							Query:       "test_data",
							OnCondition: "test_data.id = tt.id",
						},
						{
							Query:       "test_res",
							OnCondition: "test_res.id = tt.id",
						},
					},
					Alias: "test",
				},
				configuration:     []configuration.FieldsConfiguration{},
				variableGenerator: &VariableGeneratorMock{},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test",
						},
						OrderBy: []orderParser.Order{
							{
								Priority:  1,
								By:        "test",
								Direction: "asc",
							},
						},
						Pagination: &paginationParser.Pagination{
							Limit:  10,
							Offset: 0,
						},
					},
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want:    "select test as data from test tt left join test_data on test_data.id = tt.id left join test_res on test_res.id = tt.id where test=1 group by test_n, test_f order by test_n, test_f limit 10 offset 0",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := aggregationQueryGenerator{
				primaryTable:      tt.fields.primaryTable,
				primaryTableAlias: tt.fields.primaryTableAlias,
				groupBy:           tt.fields.groupBy,
				selectGenerator:   tt.fields.selectGenerator,
				filter:            tt.fields.filter,
				joinGenerator:     tt.fields.joinGenerator,
				configuration:     tt.fields.configuration,
				variableGenerator: tt.fields.variableGenerator,
			}
			got, _, err := a.Generate(context.Background(), tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("Generate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("Generate() got = %v, want %v", *got, tt.want)
					return
				}

				t.Errorf("Generate() got = %v, want %v", got, want)
			}
		})
	}
}
