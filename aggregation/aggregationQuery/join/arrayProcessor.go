package join

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации Join для полей - массивов
type arrayProcessor struct {
	primaryTable           string
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация Join
func (a arrayProcessor) generate(field configuration.FieldsConfiguration, alias string) Join {
	dbConf := field.DbConfiguration.(configuration.ArrayFieldConfiguration)

	return Join{
		Query: fmt.Sprintf(
			`(select unnest(%v) as %v, %v from %v) %v`,
			dbConf.DbName,
			dbConf.DbName,
			a.primaryTablePrimaryKey,
			a.primaryTable,
			alias,
		),
		OnCondition: fmt.Sprintf(
			`%v.%v = %v.%v`,
			alias,
			a.primaryTablePrimaryKey,
			a.primaryTableAlias,
			a.primaryTablePrimaryKey,
		),
	}
}

// Фабрика процессора
func newArrayProcessor(primaryTable string, primaryTableAlias string, primaryTablePrimaryKey string) joinProcessor {
	return &arrayProcessor{
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
