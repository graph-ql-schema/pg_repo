package join

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Результат генерации Join запроса
type Join struct {
	Query       string
	OnCondition string
}

// Генератор Join запросов
type JoinGeneratorInterface interface {
	// Генерация Join
	Generate(fields []configuration.FieldsConfiguration) []Join

	// Получение символического имени Join
	GetAlias(field configuration.FieldsConfiguration) string
}

// Процессор генерации Join запросов
type joinProcessor interface {
	// Проверка доступности процессора
	isAvailable(field configuration.FieldsConfiguration) bool

	// Генерация Join
	generate(field configuration.FieldsConfiguration, alias string) Join
}
