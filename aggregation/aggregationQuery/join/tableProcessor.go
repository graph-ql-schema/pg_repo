package join

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор отношений Many to Many
type tableProcessor struct {
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (t tableProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация Join
func (t tableProcessor) generate(field configuration.FieldsConfiguration, alias string) Join {
	dbConf := field.DbConfiguration.(configuration.TableFieldConfiguration)

	return Join{
		Query: fmt.Sprintf(
			`%v %v`,
			dbConf.Table,
			alias,
		),
		OnCondition: fmt.Sprintf(
			`%v.%v = %v.%v`,
			alias,
			dbConf.ForeignKey,
			t.primaryTableAlias,
			t.primaryTablePrimaryKey,
		),
	}
}

// Фабрика процессора
func newTableProcessor(primaryTableAlias string, primaryTablePrimaryKey string) joinProcessor {
	return &tableProcessor{
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
