package join

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_arrayProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := a.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации подзапроса
func Test_arrayProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field configuration.FieldsConfiguration
		alias string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   Join
	}{
		{
			name: "Тестирование генерации подзапроса",
			fields: fields{
				primaryTable:           "test",
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test_f_db",
					},
				},
				alias: "ttt",
			},
			want: Join{
				Query:       "(select unnest(test_f_db) as test_f_db, id from test) ttt",
				OnCondition: "ttt.id = t.id",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := a.generate(tt.args.field, tt.args.alias); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
