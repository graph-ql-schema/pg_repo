package join

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Генератор Join запросов
type joinGenerator struct {
	processors []joinProcessor
}

// Генерация Join
func (j joinGenerator) Generate(fields []configuration.FieldsConfiguration) []Join {
	joins := []Join{}
	for _, field := range fields {
		for _, processor := range j.processors {
			if processor.isAvailable(field) {
				joins = append(joins, processor.generate(field, j.GetAlias(field)))
			}
		}
	}

	return joins
}

// Получение символического имени Join
func (j joinGenerator) GetAlias(field configuration.FieldsConfiguration) string {
	return fmt.Sprintf(`%v_join`, strings.Replace(field.GraphQlName, ".", "_", -1))
}

// Фабрика генератора
func NewJoinGenerator(primaryTable string, primaryTableAlias string, primaryTablePrimaryKey string) JoinGeneratorInterface {
	return &joinGenerator{
		processors: []joinProcessor{
			newArrayProcessor(primaryTable, primaryTableAlias, primaryTablePrimaryKey),
			newRelationProcessor(primaryTableAlias, primaryTablePrimaryKey),
			newTableProcessor(primaryTableAlias, primaryTablePrimaryKey),
		},
	}
}
