package join

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Подставка для тестирования
type JoinGeneratorMock struct {
	GenerateResult []Join
	Alias          string
}

// Генерация Join
func (j JoinGeneratorMock) Generate([]configuration.FieldsConfiguration) []Join {
	return j.GenerateResult
}

// Получение символического имени Join
func (j JoinGeneratorMock) GetAlias(configuration.FieldsConfiguration) string {
	return j.Alias
}
