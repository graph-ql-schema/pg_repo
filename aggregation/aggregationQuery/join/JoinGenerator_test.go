package join

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование генерации Join
func Test_joinGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []joinProcessor
	}
	type args struct {
		fields []configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []Join
	}{
		{
			name: "Если не переданы поля",
			fields: fields{
				processors: []joinProcessor{
					joinProcessorMock{
						IsAvailable:      true,
						GenerationResult: Join{},
					},
				},
			},
			args: args{
				fields: []configuration.FieldsConfiguration{},
			},
			want: []Join{},
		},
		{
			name: "Если нет доступного процессора",
			fields: fields{
				processors: []joinProcessor{
					joinProcessorMock{
						IsAvailable:      false,
						GenerationResult: Join{},
					},
				},
			},
			args: args{
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test",
						DbConfiguration: nil,
					},
				},
			},
			want: []Join{},
		},
		{
			name: "Нет ошибок",
			fields: fields{
				processors: []joinProcessor{
					joinProcessorMock{
						IsAvailable:      true,
						GenerationResult: Join{},
					},
				},
			},
			args: args{
				fields: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test",
						DbConfiguration: nil,
					},
				},
			},
			want: []Join{
				{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := joinGenerator{
				processors: tt.fields.processors,
			}
			if got := j.Generate(tt.args.fields); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения символического названия Join
func Test_joinGenerator_GetAlias(t *testing.T) {
	type fields struct {
		processors []joinProcessor
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование получения символического названия Join",
			fields: fields{
				processors: []joinProcessor{},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_f",
					DbConfiguration: nil,
				},
			},
			want: "test_f_join",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			j := joinGenerator{
				processors: tt.fields.processors,
			}
			if got := j.GetAlias(tt.args.field); got != tt.want {
				t.Errorf("GetAlias() = %v, want %v", got, tt.want)
			}
		})
	}
}
