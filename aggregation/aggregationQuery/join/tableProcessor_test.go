package join

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_tableProcessor_isAvailable(t1 *testing.T) {
	type fields struct {
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test",
						Target:     "ph_id",
						ForeignKey: "test_f_id",
						LocalKey:   "id",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := t.isAvailable(tt.args.field); got != tt.want {
				t1.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации подзапроса
func Test_tableProcessor_generate(t1 *testing.T) {
	type fields struct {
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field configuration.FieldsConfiguration
		alias string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   Join
	}{
		{
			name: "Тестирование генерации подзапроса",
			fields: fields{
				primaryTableAlias:      "t",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "ph_id",
						ForeignKey: "test_f_id",
						LocalKey:   "id",
					},
				},
				alias: "ttt",
			},
			want: Join{
				Query:       "test_tbl ttt",
				OnCondition: "ttt.test_f_id = t.id",
			},
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := t.generate(tt.args.field, tt.args.alias); !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
