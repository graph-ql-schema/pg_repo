package join

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор для отношений 1 to Many
type relationProcessor struct {
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (r relationProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация Join
func (r relationProcessor) generate(field configuration.FieldsConfiguration, alias string) Join {
	dbConf := field.DbConfiguration.(configuration.RelationFieldConfiguration)

	return Join{
		Query: fmt.Sprintf(
			`%v %v`,
			dbConf.Table,
			alias,
		),
		OnCondition: fmt.Sprintf(
			`%v.%v = %v.%v`,
			alias,
			dbConf.ForeignKey,
			r.primaryTableAlias,
			r.primaryTablePrimaryKey,
		),
	}
}

// Фабрика процессора
func newRelationProcessor(primaryTableAlias string, primaryTablePrimaryKey string) joinProcessor {
	return &relationProcessor{
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
