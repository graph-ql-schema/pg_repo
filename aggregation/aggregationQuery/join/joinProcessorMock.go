package join

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Подставка для тестирования
type joinProcessorMock struct {
	IsAvailable      bool
	GenerationResult Join
}

// Проверка доступности процессора
func (j joinProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return j.IsAvailable
}

// Генерация Join
func (j joinProcessorMock) generate(configuration.FieldsConfiguration, string) Join {
	return j.GenerationResult
}
