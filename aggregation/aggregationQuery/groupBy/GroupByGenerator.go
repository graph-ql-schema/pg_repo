package groupBy

import (
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Генератор группировки для запроса агрегации
type groupByGenerator struct {
	fieldsConfig []configuration.FieldsConfiguration
	nameGetter   fieldNameGetter.FieldNameGetterInterface
}

// Генерация подстроки GroupBy
func (g groupByGenerator) Generate(params sbuilder.Parameters) *string {
	if nil == params.Arguments.GroupBy || 0 == len(params.Arguments.GroupBy) {
		return nil
	}

	groupers := []string{}
	for _, field := range params.Arguments.GroupBy {
		fieldConfig := g.getFieldConfig(field)
		if nil == fieldConfig {
			continue
		}

		groupers = append(groupers, g.nameGetter.GetName(*fieldConfig))
	}

	groupBy := strings.Join(groupers, ", ")
	return &groupBy
}

// Получение конфигурации поля по коду из запроса GraphQL
func (g groupByGenerator) getFieldConfig(field string) *configuration.FieldsConfiguration {
	for _, config := range g.fieldsConfig {
		if config.GraphQlName == field {
			return &config
		}
	}

	return nil
}

// Фабрика генератора
func NewGroupByGenerator(
	fieldsConfig []configuration.FieldsConfiguration,
	nameGetter fieldNameGetter.FieldNameGetterInterface,
) GroupByGeneratorInterface {
	return &groupByGenerator{
		fieldsConfig: fieldsConfig,
		nameGetter:   nameGetter,
	}
}
