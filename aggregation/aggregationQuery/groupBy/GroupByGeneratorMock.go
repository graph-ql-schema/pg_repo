package groupBy

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Подставка для тестирования
type GroupByGeneratorMock struct {
	GenerateResult *string
}

// Генерация подстроки GroupBy
func (g GroupByGeneratorMock) Generate(sbuilder.Parameters) *string {
	return g.GenerateResult
}
