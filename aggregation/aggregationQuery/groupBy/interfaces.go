package groupBy

import "bitbucket.org/graph-ql-schema/sbuilder/v2"

// Генератор группировки для запроса агрегации
type GroupByGeneratorInterface interface {
	// Генерация подстроки GroupBy
	Generate(params sbuilder.Parameters) *string
}
