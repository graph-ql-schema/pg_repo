package groupBy

import (
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/groupByParser"
)

// Тестирование генерации подстроки группировки
func Test_groupByGenerator_Generate(t *testing.T) {
	test := "test, test"

	type fields struct {
		fieldsConfig []configuration.FieldsConfiguration
		nameGetter   fieldNameGetter.FieldNameGetterInterface
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *string
	}{
		{
			name: "Поля группировки не переданы",
			fields: fields{
				fieldsConfig: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				nameGetter: fieldNameGetter.FieldNameGetterMock{
					Name: "test",
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: nil,
					},
				},
			},
			want: nil,
		},
		{
			name: "Передан пустой срез полей",
			fields: fields{
				fieldsConfig: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				nameGetter: fieldNameGetter.FieldNameGetterMock{
					Name: "test",
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{},
					},
				},
			},
			want: nil,
		},
		{
			name: "Передан корректный срез полей",
			fields: fields{
				fieldsConfig: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				nameGetter: fieldNameGetter.FieldNameGetterMock{
					Name: "test",
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						GroupBy: groupByParser.GroupBy{
							"test_f",
							"test_f",
						},
					},
				},
			},
			want: &test,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := groupByGenerator{
				fieldsConfig: tt.fields.fieldsConfig,
				nameGetter:   tt.fields.nameGetter,
			}
			if got := g.Generate(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
