package aggregationQuery

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"context"
)

// Генератор запросов агрегации
type AggregationQueryGeneratorInterface interface {
	// Генерация запроса агрегации
	Generate(
		context context.Context,
		params sbuilder.Parameters,
	) (sql *string, args []interface{}, err error)
}
