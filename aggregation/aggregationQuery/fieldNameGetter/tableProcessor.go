package fieldNameGetter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор отношений Many to Many
type tableProcessor struct {
	join join.JoinGeneratorInterface
}

// Проверка доступности процессора
func (t tableProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); ok {
		return true
	}

	return false
}

// Получение названия поля
func (t tableProcessor) getName(field configuration.FieldsConfiguration) string {
	dbConf := field.DbConfiguration.(configuration.TableFieldConfiguration)

	return fmt.Sprintf(`%v.%v`, t.join.GetAlias(field), dbConf.Target)
}

// Фабрика процессора
func newTableProcessor(join join.JoinGeneratorInterface) nameProcessor {
	return &tableProcessor{
		join: join,
	}
}
