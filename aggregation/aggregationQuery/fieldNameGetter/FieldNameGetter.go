package fieldNameGetter

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Получатель кода поля по переданным параметрам
type fieldNameGetter struct {
	processors []nameProcessor
}

// Получение названия поля
func (f fieldNameGetter) GetName(field configuration.FieldsConfiguration) string {
	for _, processor := range f.processors {
		if processor.isAvailable(field) {
			return processor.getName(field)
		}
	}

	return ""
}

// Фабрика геттера
func NewFieldNameGetter(primaryTableAlias string, join join.JoinGeneratorInterface) FieldNameGetterInterface {
	return &fieldNameGetter{
		processors: []nameProcessor{
			newScalarProcessor(primaryTableAlias),
			newArrayProcessor(join),
			newRelationProcessor(join),
			newTableProcessor(join),
		},
	}
}
