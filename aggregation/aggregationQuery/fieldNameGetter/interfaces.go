package fieldNameGetter

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Получатель кода поля по переданным параметрам
type FieldNameGetterInterface interface {
	// Получение названия поля
	GetName(field configuration.FieldsConfiguration) string
}

// Процессор получателя
type nameProcessor interface {
	// Проверка доступности процессора
	isAvailable(field configuration.FieldsConfiguration) bool

	// Получение названия поля
	getName(field configuration.FieldsConfiguration) string
}
