package fieldNameGetter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор простых полей
type scalarProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (s scalarProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ScalarFieldConfiguration); ok {
		return true
	}

	return false
}

// Получение названия поля
func (s scalarProcessor) getName(field configuration.FieldsConfiguration) string {
	dbConf := field.DbConfiguration.(configuration.ScalarFieldConfiguration)

	return fmt.Sprintf(`%v.%v`, s.primaryTableAlias, dbConf.DbName)
}

// Фабрика процессора
func newScalarProcessor(primaryTableAlias string) nameProcessor {
	return &scalarProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
