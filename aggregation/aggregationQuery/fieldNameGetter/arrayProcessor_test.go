package fieldNameGetter

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности
func Test_arrayProcessor_isAvailable(t *testing.T) {
	type fields struct {
		join join.JoinGeneratorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				join: tt.fields.join,
			}
			if got := a.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Получение названия процессора
func Test_arrayProcessor_getName(t *testing.T) {
	type fields struct {
		join join.JoinGeneratorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование получения названия поля",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: "ttt.test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				join: tt.fields.join,
			}
			if got := a.getName(tt.args.field); got != tt.want {
				t.Errorf("getName() = %v, want %v", got, tt.want)
			}
		})
	}
}
