package fieldNameGetter

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_tableProcessor_isAvailable(t1 *testing.T) {
	type fields struct {
		join join.JoinGeneratorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt_table",
						Target:     "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				join: tt.fields.join,
			}
			if got := t.isAvailable(tt.args.field); got != tt.want {
				t1.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Получение названия
func Test_tableProcessor_getName(t1 *testing.T) {
	type fields struct {
		join join.JoinGeneratorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование получения названия поля",
			fields: fields{
				join: join.JoinGeneratorMock{
					GenerateResult: nil,
					Alias:          "ttt",
				},
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt_table",
						Target:     "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
			},
			want: "ttt.id",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				join: tt.fields.join,
			}
			if got := t.getName(tt.args.field); got != tt.want {
				t1.Errorf("getName() = %v, want %v", got, tt.want)
			}
		})
	}
}
