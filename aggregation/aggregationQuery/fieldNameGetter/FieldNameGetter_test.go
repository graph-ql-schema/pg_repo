package fieldNameGetter

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование получения названия поля
func Test_fieldNameGetter_GetName(t *testing.T) {
	type fields struct {
		processors []nameProcessor
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Есть доступный процессор",
			fields: fields{
				processors: []nameProcessor{
					nameProcessorMock{
						IsAvailable:   true,
						GetNameResult: "ttt",
					},
				},
			},
			args: args{},
			want: "ttt",
		},
		{
			name: "Нет доступного процессора",
			fields: fields{
				processors: []nameProcessor{
					nameProcessorMock{
						IsAvailable:   false,
						GetNameResult: "ttt",
					},
				},
			},
			args: args{},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := fieldNameGetter{
				processors: tt.fields.processors,
			}
			if got := f.GetName(tt.args.field); got != tt.want {
				t.Errorf("GetName() = %v, want %v", got, tt.want)
			}
		})
	}
}
