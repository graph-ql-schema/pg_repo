package fieldNameGetter

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Подставка для тестирования
type FieldNameGetterMock struct {
	Name string
}

// Получение названия поля
func (f FieldNameGetterMock) GetName(configuration.FieldsConfiguration) string {
	return f.Name
}
