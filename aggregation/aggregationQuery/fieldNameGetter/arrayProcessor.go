package fieldNameGetter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор полей массивов
type arrayProcessor struct {
	join join.JoinGeneratorInterface
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Получение названия поля
func (a arrayProcessor) getName(field configuration.FieldsConfiguration) string {
	dbConf := field.DbConfiguration.(configuration.ArrayFieldConfiguration)

	return fmt.Sprintf(`%v.%v`, a.join.GetAlias(field), dbConf.DbName)
}

// Фабрика процессора
func newArrayProcessor(join join.JoinGeneratorInterface) nameProcessor {
	return &arrayProcessor{
		join: join,
	}
}
