package fieldNameGetter

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор отношений 1 to Many
type relationProcessor struct {
	join join.JoinGeneratorInterface
}

// Проверка доступности процессора
func (r relationProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration); ok {
		return true
	}

	return false
}

// Получение названия поля
func (r relationProcessor) getName(field configuration.FieldsConfiguration) string {
	dbConf := field.DbConfiguration.(configuration.RelationFieldConfiguration)

	return fmt.Sprintf(`%v.%v`, r.join.GetAlias(field), dbConf.PrimaryKey)
}

// Фабрика процессора
func newRelationProcessor(join join.JoinGeneratorInterface) nameProcessor {
	return &relationProcessor{
		join: join,
	}
}
