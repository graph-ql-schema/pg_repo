package fieldNameGetter

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Подставка для тестирования
type nameProcessorMock struct {
	IsAvailable   bool
	GetNameResult string
}

// Проверка доступности процессора
func (n nameProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return n.IsAvailable
}

// Получение названия поля
func (n nameProcessorMock) getName(configuration.FieldsConfiguration) string {
	return n.GetNameResult
}
