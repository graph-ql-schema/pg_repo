package aggregationQuery

import (
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Подставка для тестирования
type AggregationQueryGeneratorMock struct {
	IsError bool
	Result  string
}

// Генерация запроса агрегации
func (a AggregationQueryGeneratorMock) Generate(
	context context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	if a.IsError {
		return nil, nil, fmt.Errorf(`test`)
	}

	if 0 == len(a.Result) {
		return nil, nil, nil
	}

	return &a.Result, nil, nil
}
