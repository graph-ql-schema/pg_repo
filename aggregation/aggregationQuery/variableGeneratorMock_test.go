package aggregationQuery

// Подставка для тестирования
type VariableGeneratorMock struct{}

// Замена подставок переменной в передаваемом SQL на реальные переменные
func (v VariableGeneratorMock) SetVariables(sql string) string {
	return sql
}
