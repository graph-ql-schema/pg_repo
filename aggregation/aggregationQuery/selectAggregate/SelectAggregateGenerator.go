package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/selectAggregate/subQueryGenerator"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"context"
	"fmt"
	"strings"
)

// Генератор полей выборки запроса агрегации
type selectAggregateGenerator struct {
	processors []selectProcessor
}

// Генерация полей выборки запроса агрегации
func (s selectAggregateGenerator) Generate(ctx context.Context, params sbuilder.Parameters) string {
	if 0 == len(params.Fields) {
		return ""
	}

	fieldsSql := make([]string, 0, len(params.Fields))
	for _, field := range params.Fields {
		select {
		case <-ctx.Done():
			return ""
		default:
		}

		if res := s.processField(field); res != nil {
			fieldsSql = append(fieldsSql, *res)
		}
	}
	if 0 == len(fieldsSql) {
		return ""
	}

	return fmt.Sprintf(`json_build_object(%v)`, strings.Join(fieldsSql, ", "))
}

// Обработка поля
func (s selectAggregateGenerator) processField(field requestedFieldsParser.GraphQlRequestedFieldInterface) *string {
	result := ""
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			fieldCode, sql := processor.generate(field)
			result = fmt.Sprintf(`'%v', %v`, fieldCode, sql)
		}
	}

	if 0 == len(result) {
		return nil
	}

	return &result
}

// Фабрика генератора
func NewSelectAggregateGenerator(
	primaryTable string,
	primaryTableAlias string,
	primaryTablePrimaryKey string,
	configuration []configuration.FieldsConfiguration,
	nameGetter fieldNameGetter.FieldNameGetterInterface,
) SelectAggregateGeneratorInterface {
	generator := subQueryGenerator.NewSubQueryGenerator(primaryTable, primaryTableAlias, primaryTablePrimaryKey)
	return &selectAggregateGenerator{
		processors: []selectProcessor{
			newCountProcessor(primaryTableAlias, primaryTablePrimaryKey),
			newMinProcessor(configuration, nameGetter),
			newMaxProcessor(configuration, nameGetter),
			newSumProcessor(generator, configuration),
			newAvgProcessor(generator, configuration),
			newVariantsProcessor(configuration, nameGetter),
		},
	}
}
