package selectAggregate

import (
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование доступности процессора
func Test_countProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTableAlias:      "ttt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test", nil),
			},
			want: false,
		},
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTableAlias:      "ttt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField(constants.CountOperationSchemaKey, nil),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countProcessor{
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := c.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_countProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		in0 requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantFieldCode    string
		wantFieldLoadSql string
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				primaryTableAlias:      "ttt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				in0: requestedFieldsParser.NewGraphQlRequestedField(constants.CountOperationSchemaKey, nil),
			},
			wantFieldCode:    constants.CountOperationSchemaKey,
			wantFieldLoadSql: "coalesce(count(distinct ttt.id), 0)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := countProcessor{
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			gotFieldCode, gotFieldLoadSql := c.generate(tt.args.in0)
			if gotFieldCode != tt.wantFieldCode {
				t.Errorf("generate() gotFieldCode = %v, want %v", gotFieldCode, tt.wantFieldCode)
			}
			if gotFieldLoadSql != tt.wantFieldLoadSql {
				t.Errorf("generate() gotFieldLoadSql = %v, want %v", gotFieldLoadSql, tt.wantFieldLoadSql)
			}
		})
	}
}
