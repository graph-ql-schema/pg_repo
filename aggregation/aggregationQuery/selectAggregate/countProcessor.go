package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Процессор поля count
type countProcessor struct {
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (c countProcessor) isAvailable(field requestedFieldsParser.GraphQlRequestedFieldInterface) bool {
	return field.GetFieldName() == constants.CountOperationSchemaKey
}

// Генерация подзапроса
func (c countProcessor) generate(requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string) {
	fieldCode = constants.CountOperationSchemaKey
	fieldLoadSql = `coalesce(count(distinct ` + c.primaryTableAlias + `.` + c.primaryTablePrimaryKey + `), 0)`

	return
}

// Фабрика процессора
func newCountProcessor(primaryTableAlias string, primaryTablePrimaryKey string) selectProcessor {
	return &countProcessor{
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
