package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
	"context"
)

// Генератор полей выборки запроса агрегации
type SelectAggregateGeneratorInterface interface {
	// Генерация полей выборки запроса агрегации
	Generate(ctx context.Context, params sbuilder.Parameters) string
}

// Процессор генерации
type selectProcessor interface {
	// Проверка доступности процессора
	isAvailable(field requestedFieldsParser.GraphQlRequestedFieldInterface) bool

	// Генерация подзапроса
	generate(field requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string)
}
