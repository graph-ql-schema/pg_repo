package subQueryGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор полей массивов
type arrayProcessor struct {
	primaryTable           string
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подзапроса
func (a arrayProcessor) generate(field configuration.FieldsConfiguration, operation string) string {
	dbConf := field.DbConfiguration.(configuration.ArrayFieldConfiguration)
	alias := fmt.Sprintf(`%v_%v`, strings.Replace(dbConf.DbName, ".", "_", -1), operation)
	aliasRoot := fmt.Sprintf(`%v_%v_rt`, strings.Replace(dbConf.DbName, ".", "_", -1), operation)

	return fmt.Sprintf(
		`select %v(%v.%v) from (select unnest(%v.%v) %v from %v %v where %v.%v in (select unnest(array_agg(distinct %v.%v)))) %v`,
		operation,
		aliasRoot,
		dbConf.DbName,
		alias,
		dbConf.DbName,
		dbConf.DbName,
		a.primaryTable,
		alias,
		alias,
		a.primaryTablePrimaryKey,
		a.primaryTableAlias,
		a.primaryTablePrimaryKey,
		aliasRoot,
	)
}

// Фабрика процессора
func newArrayProcessor(primaryTable string, primaryTableAlias string, primaryTablePrimaryKey string) subQueryProcessor {
	return &arrayProcessor{
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
