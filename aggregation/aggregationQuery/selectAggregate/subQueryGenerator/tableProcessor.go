package subQueryGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации подзапроса для отношений Many to Many
type tableProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (t tableProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подзапроса
func (t tableProcessor) generate(field configuration.FieldsConfiguration, operation string) string {
	dbConf := field.DbConfiguration.(configuration.TableFieldConfiguration)
	alias := fmt.Sprintf(`%v_%v`, strings.Replace(dbConf.Target, ".", "_", -1), operation)

	return fmt.Sprintf(
		`select %v(%v.%v) from %v %v where %v.%v in (select unnest(array_agg(distinct %v.%v)))`,
		operation,
		alias,
		dbConf.Target,
		dbConf.Table,
		alias,
		alias,
		dbConf.ForeignKey,
		t.primaryTableAlias,
		dbConf.LocalKey,
	)
}

// Фабрика процессора
func newTableProcessor(primaryTableAlias string) subQueryProcessor {
	return &tableProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
