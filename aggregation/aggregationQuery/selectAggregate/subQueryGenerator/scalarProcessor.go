package subQueryGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Генерация подзапроса для поля основной таблицы
type scalarProcessor struct {
	primaryTable           string
	primaryTableAlias      string
	primaryTablePrimaryKey string
}

// Проверка доступности процессора
func (s scalarProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.ScalarFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подзапроса
func (s scalarProcessor) generate(field configuration.FieldsConfiguration, operation string) string {
	dbConf := field.DbConfiguration.(configuration.ScalarFieldConfiguration)
	alias := fmt.Sprintf(`%v_%v`, strings.Replace(dbConf.DbName, ".", "_", -1), operation)

	return fmt.Sprintf(
		`select %v(%v.%v) from %v %v where %v.%v in (select unnest(array_agg(distinct %v.%v)))`,
		operation,
		alias,
		dbConf.DbName,
		s.primaryTable,
		alias,
		alias,
		s.primaryTablePrimaryKey,
		s.primaryTableAlias,
		s.primaryTablePrimaryKey,
	)
}

// Фабрика процессора
func newScalarProcessor(primaryTable string, primaryTableAlias string, primaryTablePrimaryKey string) subQueryProcessor {
	return &scalarProcessor{
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}
