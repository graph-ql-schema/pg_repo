package subQueryGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Генерация подзапроса для агрегации
type subQueryGenerator struct {
	processors []subQueryProcessor
}

// Генерация подзапроса
func (s subQueryGenerator) Generate(field configuration.FieldsConfiguration, operation string) string {
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			return processor.generate(field, operation)
		}
	}

	return ""
}

// Фабрика генератора
func NewSubQueryGenerator(
	primaryTable string,
	primaryTableAlias string,
	primaryTablePrimaryKey string,
) SubQueryGeneratorInterface {
	return &subQueryGenerator{
		processors: []subQueryProcessor{
			newScalarProcessor(primaryTable, primaryTableAlias, primaryTablePrimaryKey),
			newArrayProcessor(primaryTable, primaryTableAlias, primaryTablePrimaryKey),
			newRelationProcessor(primaryTableAlias),
			newTableProcessor(primaryTableAlias),
		},
	}
}
