package subQueryGenerator

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование генерации запроса
func TestSubQueryGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []subQueryProcessor
	}
	type args struct {
		field     configuration.FieldsConfiguration
		operation string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Если нет доступного процессора",
			fields: fields{
				processors: []subQueryProcessor{
					subQueryProcessorMock{
						IsAvailable:      false,
						GenerationResult: "test",
					},
				},
			},
			args: args{},
			want: "",
		},
		{
			name: "Если есть доступный процессор",
			fields: fields{
				processors: []subQueryProcessor{
					subQueryProcessorMock{
						IsAvailable:      true,
						GenerationResult: "test",
					},
				},
			},
			args: args{},
			want: "test",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := subQueryGenerator{
				processors: tt.fields.processors,
			}
			if got := s.Generate(tt.args.field, tt.args.operation); got != tt.want {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
