package subQueryGenerator

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_arrayProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTable:           "test_tbl",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				primaryTable:           "test_tbl",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := a.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_arrayProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTableAlias      string
		primaryTablePrimaryKey string
	}
	type args struct {
		field     configuration.FieldsConfiguration
		operation string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Валидный тип",
			fields: fields{
				primaryTable:           "test_tbl",
				primaryTableAlias:      "tt",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test_f_db",
					},
				},
				operation: "min",
			},
			want: "select min(test_f_db_min_rt.test_f_db) from (select unnest(test_f_db_min.test_f_db) test_f_db from test_tbl test_f_db_min where test_f_db_min.id in (select unnest(array_agg(distinct tt.id)))) test_f_db_min_rt",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := arrayProcessor{
				primaryTable:           tt.fields.primaryTable,
				primaryTableAlias:      tt.fields.primaryTableAlias,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := a.generate(tt.args.field, tt.args.operation); got != tt.want {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
