package subQueryGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type SubQueryGeneratorMock struct {
	Result string
}

// Генерация подзапроса
func (s SubQueryGeneratorMock) Generate(configuration.FieldsConfiguration, string) string {
	return s.Result
}
