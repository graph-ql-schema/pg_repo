package subQueryGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type subQueryProcessorMock struct {
	IsAvailable      bool
	GenerationResult string
}

// Проверка доступности процессора
func (s subQueryProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return s.IsAvailable
}

// Генерация
func (s subQueryProcessorMock) generate(configuration.FieldsConfiguration, string) string {
	return s.GenerationResult
}
