package subQueryGenerator

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Проверка доступности процессора
func Test_relationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				primaryTableAlias: "tt",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test_f_db",
					},
				},
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				primaryTableAlias: "tt",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_tbl",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := relationProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := r.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_relationProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		field     configuration.FieldsConfiguration
		operation string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Валидный тип",
			fields: fields{
				primaryTableAlias: "tt",
			},
			args: args{
				field: configuration.FieldsConfiguration{
					Field:       "Test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_tbl",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				operation: "min",
			},
			want: "select min(id_min.id) from test_tbl id_min where id_min.user_id in (select unnest(array_agg(distinct tt.uuid)))",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := relationProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := r.generate(tt.args.field, tt.args.operation); got != tt.want {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
