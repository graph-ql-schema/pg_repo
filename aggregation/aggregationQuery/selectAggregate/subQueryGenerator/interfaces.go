package subQueryGenerator

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Генерация подзапроса для агрегации
type SubQueryGeneratorInterface interface {
	// Генерация подзапроса
	Generate(field configuration.FieldsConfiguration, operation string) string
}

// Процессор генерации подзапросов
type subQueryProcessor interface {
	// Проверка доступности процессора
	isAvailable(field configuration.FieldsConfiguration) bool

	// Генерация подзапроса
	generate(field configuration.FieldsConfiguration, operation string) string
}
