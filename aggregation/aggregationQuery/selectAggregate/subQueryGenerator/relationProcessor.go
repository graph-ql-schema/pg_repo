package subQueryGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации подзапроса для отношений 1 to Many
type relationProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (r relationProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	if _, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация подзапроса
func (r relationProcessor) generate(field configuration.FieldsConfiguration, operation string) string {
	dbConf := field.DbConfiguration.(configuration.RelationFieldConfiguration)
	alias := fmt.Sprintf(`%v_%v`, strings.Replace(dbConf.PrimaryKey, ".", "_", -1), operation)

	return fmt.Sprintf(
		`select %v(%v.%v) from %v %v where %v.%v in (select unnest(array_agg(distinct %v.%v)))`,
		operation,
		alias,
		dbConf.PrimaryKey,
		dbConf.Table,
		alias,
		alias,
		dbConf.ForeignKey,
		r.primaryTableAlias,
		dbConf.LocalKey,
	)
}

// Фабрика процессора
func newRelationProcessor(primaryTableAlias string) subQueryProcessor {
	return &relationProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
