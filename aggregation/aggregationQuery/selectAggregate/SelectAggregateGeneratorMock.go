package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"context"
)

// Подставка для тестирования
type SelectAggregateGeneratorMock struct {
	GenerationResult string
}

// Генерация полей выборки запроса агрегации
func (s SelectAggregateGeneratorMock) Generate(context.Context, sbuilder.Parameters) string {
	return s.GenerationResult
}
