package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/selectAggregate/subQueryGenerator"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Процессор генерации для операций Sum и Avg
type sumAvgProcessor struct {
	generator     subQueryGenerator.SubQueryGeneratorInterface
	configuration []configuration.FieldsConfiguration
	processorType string
	operation     string
}

// Проверка доступности процессора
func (s sumAvgProcessor) isAvailable(field requestedFieldsParser.GraphQlRequestedFieldInterface) bool {
	return field.GetFieldName() == s.processorType
}

// Генерация подзапроса
func (s sumAvgProcessor) generate(field requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string) {
	if !field.HasSubFields() {
		return
	}

	fieldCode = s.processorType
	subFields := []string{}
	for _, subField := range field.GetSubFields() {
		config := s.getFieldConfig(subField.GetFieldName())
		if nil == config {
			continue
		}

		subFields = append(
			subFields,
			fmt.Sprintf(
				`'%v', coalesce((%v), %v::%v)`,
				subField.GetFieldName(),
				s.generator.Generate(*config, s.operation),
				helpers.GetDefaultValueByFieldType(config.FieldType),
				config.FieldType,
			),
		)
	}

	fieldLoadSql = fmt.Sprintf(`json_build_object(%v)`, strings.Join(subFields, ", "))

	return
}

// Получение конфигурации поля по его GraphQL коду
func (s sumAvgProcessor) getFieldConfig(field string) *configuration.FieldsConfiguration {
	for _, config := range s.configuration {
		if config.GraphQlName == field {
			return &config
		}
	}

	return nil
}

// Фабрика процессора SUM
func newSumProcessor(
	generator subQueryGenerator.SubQueryGeneratorInterface,
	configuration []configuration.FieldsConfiguration,
) selectProcessor {
	return &sumAvgProcessor{
		generator:     generator,
		configuration: configuration,
		processorType: constants.SumOperationSchemaKey,
		operation:     "sum",
	}
}

// Фабрика процессора AVG
func newAvgProcessor(
	generator subQueryGenerator.SubQueryGeneratorInterface,
	configuration []configuration.FieldsConfiguration,
) selectProcessor {
	return &sumAvgProcessor{
		generator:     generator,
		configuration: configuration,
		processorType: constants.AvgOperationSchemaKey,
		operation:     "avg",
	}
}
