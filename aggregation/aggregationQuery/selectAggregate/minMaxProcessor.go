package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Процессор Max и Min полей
type minMaxProcessor struct {
	configuration []configuration.FieldsConfiguration
	nameGetter    fieldNameGetter.FieldNameGetterInterface
	fieldType     string
	operation     string
}

// Проверка доступности процессора
func (m minMaxProcessor) isAvailable(field requestedFieldsParser.GraphQlRequestedFieldInterface) bool {
	return field.GetFieldName() == m.fieldType
}

// Генерация подзапроса
func (m minMaxProcessor) generate(field requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string) {
	if !field.HasSubFields() {
		return
	}

	fieldCode = m.fieldType
	subFields := []string{}
	for _, subField := range field.GetSubFields() {
		config := m.getFieldConfig(subField.GetFieldName())
		if nil == config {
			continue
		}

		subFields = append(
			subFields,
			`'`+subField.GetFieldName()+`', coalesce(`+
				m.operation+`(distinct `+m.nameGetter.GetName(*config)+
				`), `+helpers.GetDefaultValueByFieldType(config.FieldType)+`::`+config.FieldType+`)`,
		)
	}

	fieldLoadSql = fmt.Sprintf(`json_build_object(%v)`, strings.Join(subFields, ", "))

	return
}

// Получение конфигурации поля по его GraphQL коду
func (m minMaxProcessor) getFieldConfig(field string) *configuration.FieldsConfiguration {
	for _, config := range m.configuration {
		if config.GraphQlName == field {
			return &config
		}
	}

	return nil
}

// Фабрика процессора Min
func newMinProcessor(
	configuration []configuration.FieldsConfiguration,
	nameGetter fieldNameGetter.FieldNameGetterInterface,
) selectProcessor {
	return &minMaxProcessor{
		configuration: configuration,
		nameGetter:    nameGetter,
		fieldType:     constants.MinOperationSchemaKey,
		operation:     "min",
	}
}

// Фабрика процессора Max
func newMaxProcessor(
	configuration []configuration.FieldsConfiguration,
	nameGetter fieldNameGetter.FieldNameGetterInterface,
) selectProcessor {
	return &minMaxProcessor{
		configuration: configuration,
		nameGetter:    nameGetter,
		fieldType:     constants.MaxOperationSchemaKey,
		operation:     "max",
	}
}
