package selectAggregate

import (
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Подставка для тестирования
type selectProcessorMock struct {
	IsAvailable                bool
	FieldCodeGenerateResult    string
	FieldLoadSqlGenerateResult string
}

// Проверка доступности процессора
func (s selectProcessorMock) isAvailable(requestedFieldsParser.GraphQlRequestedFieldInterface) bool {
	return s.IsAvailable
}

// Генерация подзапроса
func (s selectProcessorMock) generate(requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string) {
	return s.FieldCodeGenerateResult, s.FieldLoadSqlGenerateResult
}
