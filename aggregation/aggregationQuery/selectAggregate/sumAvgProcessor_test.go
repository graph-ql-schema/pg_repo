package selectAggregate

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/selectAggregate/subQueryGenerator"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование доступности процессора
func Test_sumAvgProcessor_isAvailable(t *testing.T) {
	type fields struct {
		generator     subQueryGenerator.SubQueryGeneratorInterface
		configuration []configuration.FieldsConfiguration
		processorType string
		operation     string
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				generator:     subQueryGenerator.SubQueryGeneratorMock{},
				processorType: "test",
				operation:     "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				generator:     subQueryGenerator.SubQueryGeneratorMock{},
				processorType: "test",
				operation:     "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test", nil),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sumAvgProcessor{
				generator:     tt.fields.generator,
				configuration: tt.fields.configuration,
				processorType: tt.fields.processorType,
				operation:     tt.fields.operation,
			}
			if got := s.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_sumAvgProcessor_generate(t *testing.T) {
	type fields struct {
		generator     subQueryGenerator.SubQueryGeneratorInterface
		configuration []configuration.FieldsConfiguration
		processorType string
		operation     string
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantFieldCode    string
		wantFieldLoadSql string
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						FieldType:       "int",
						DbConfiguration: nil,
					},
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_n",
						FieldType:       "int",
						DbConfiguration: nil,
					},
				},
				generator: subQueryGenerator.SubQueryGeneratorMock{
					Result: "test",
				},
				processorType: "test",
				operation:     "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField(
					"test",
					requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
						requestedFieldsParser.NewGraphQlRequestedField("test_n", nil),
					},
				),
			},
			wantFieldCode:    "test",
			wantFieldLoadSql: "json_build_object('test_f', coalesce((test), 0::int), 'test_n', coalesce((test), 0::int))",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sumAvgProcessor{
				generator:     tt.fields.generator,
				configuration: tt.fields.configuration,
				processorType: tt.fields.processorType,
				operation:     tt.fields.operation,
			}
			gotFieldCode, gotFieldLoadSql := s.generate(tt.args.field)
			if gotFieldCode != tt.wantFieldCode {
				t.Errorf("generate() gotFieldCode = %v, want %v", gotFieldCode, tt.wantFieldCode)
			}
			if gotFieldLoadSql != tt.wantFieldLoadSql {
				t.Errorf("generate() gotFieldLoadSql = %v, want %v", gotFieldLoadSql, tt.wantFieldLoadSql)
			}
		})
	}
}
