package selectAggregate

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Обработчик генерации подзапроса вариантов
type variantsProcessor struct {
	configuration []configuration.FieldsConfiguration
	nameGetter    fieldNameGetter.FieldNameGetterInterface
}

// Проверка доступности процессора
func (v variantsProcessor) isAvailable(field requestedFieldsParser.GraphQlRequestedFieldInterface) bool {
	return field.GetFieldName() == constants.VariantsOperationSchemaKey
}

// Генерация подзапроса
func (v variantsProcessor) generate(field requestedFieldsParser.GraphQlRequestedFieldInterface) (fieldCode string, fieldLoadSql string) {
	if !field.HasSubFields() {
		return
	}

	fieldCode = constants.VariantsOperationSchemaKey
	subFields := []string{}
	for _, subField := range field.GetSubFields() {
		config := v.getFieldConfig(subField.GetFieldName())
		if nil == config {
			continue
		}

		subFields = append(subFields, fmt.Sprintf(
			`'%v', array_agg(distinct %v)`,
			subField.GetFieldName(),
			v.nameGetter.GetName(*config),
		))
	}

	fieldLoadSql = fmt.Sprintf(`json_build_object(%v)`, strings.Join(subFields, ", "))

	return
}

// Получение конфигурации поля по его GraphQL коду
func (v variantsProcessor) getFieldConfig(field string) *configuration.FieldsConfiguration {
	for _, config := range v.configuration {
		if config.GraphQlName == field {
			return &config
		}
	}

	return nil
}

// Фабрика процессора
func newVariantsProcessor(
	configuration []configuration.FieldsConfiguration,
	nameGetter fieldNameGetter.FieldNameGetterInterface,
) selectProcessor {
	return &variantsProcessor{
		configuration: configuration,
		nameGetter:    nameGetter,
	}
}
