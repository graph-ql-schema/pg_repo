package selectAggregate

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование доступности процессора
func Test_variantsProcessor_isAvailable(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		nameGetter    fieldNameGetter.FieldNameGetterInterface
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				nameGetter:    fieldNameGetter.FieldNameGetterMock{},
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test", nil),
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				nameGetter:    fieldNameGetter.FieldNameGetterMock{},
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField(constants.VariantsOperationSchemaKey, nil),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsProcessor{
				configuration: tt.fields.configuration,
				nameGetter:    tt.fields.nameGetter,
			}
			if got := v.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации подзапроса
func Test_variantsProcessor_generate(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		nameGetter    fieldNameGetter.FieldNameGetterInterface
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantFieldCode    string
		wantFieldLoadSql string
	}{
		{
			name: "Тестирование генерации подзапроса",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_n",
						DbConfiguration: nil,
					},
				},
				nameGetter: fieldNameGetter.FieldNameGetterMock{
					Name: "test",
				},
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField(
					constants.VariantsOperationSchemaKey,
					[]requestedFieldsParser.GraphQlRequestedFieldInterface{
						requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
						requestedFieldsParser.NewGraphQlRequestedField("test_n", nil),
					},
				),
			},
			wantFieldCode:    constants.VariantsOperationSchemaKey,
			wantFieldLoadSql: "json_build_object('test_f', array_agg(distinct test), 'test_n', array_agg(distinct test))",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variantsProcessor{
				configuration: tt.fields.configuration,
				nameGetter:    tt.fields.nameGetter,
			}
			gotFieldCode, gotFieldLoadSql := v.generate(tt.args.field)
			if gotFieldCode != tt.wantFieldCode {
				t.Errorf("generate() gotFieldCode = %v, want %v", gotFieldCode, tt.wantFieldCode)
			}
			if gotFieldLoadSql != tt.wantFieldLoadSql {
				t.Errorf("generate() gotFieldLoadSql = %v, want %v", gotFieldLoadSql, tt.wantFieldLoadSql)
			}
		})
	}
}
