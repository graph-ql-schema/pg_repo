package selectAggregate

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование доступности процессора
func Test_minMaxProcessor_isAvailable(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		nameGetter    fieldNameGetter.FieldNameGetterInterface
		fieldType     string
		operation     string
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				nameGetter:    fieldNameGetter.FieldNameGetterMock{},
				fieldType:     "test",
				operation:     "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
			},
			want: false,
		},
		{
			name: "Валидный тип",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				nameGetter:    fieldNameGetter.FieldNameGetterMock{},
				fieldType:     "test",
				operation:     "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField("test", nil),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := minMaxProcessor{
				configuration: tt.fields.configuration,
				nameGetter:    tt.fields.nameGetter,
				fieldType:     tt.fields.fieldType,
				operation:     tt.fields.operation,
			}
			if got := m.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_minMaxProcessor_generate(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		nameGetter    fieldNameGetter.FieldNameGetterInterface
		fieldType     string
		operation     string
	}
	type args struct {
		field requestedFieldsParser.GraphQlRequestedFieldInterface
	}
	tests := []struct {
		name             string
		fields           fields
		args             args
		wantFieldCode    string
		wantFieldLoadSql string
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						FieldType:       "int",
						DbConfiguration: nil,
					},
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_n",
						FieldType:       "int",
						DbConfiguration: nil,
					},
				},
				nameGetter: fieldNameGetter.FieldNameGetterMock{
					Name: "test",
				},
				fieldType: "test",
				operation: "min",
			},
			args: args{
				field: requestedFieldsParser.NewGraphQlRequestedField(
					"test",
					requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test_f", nil),
						requestedFieldsParser.NewGraphQlRequestedField("test_n", nil),
					},
				),
			},
			wantFieldCode:    "test",
			wantFieldLoadSql: "json_build_object('test_f', coalesce(min(distinct test), 0::int), 'test_n', coalesce(min(distinct test), 0::int))",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := minMaxProcessor{
				configuration: tt.fields.configuration,
				nameGetter:    tt.fields.nameGetter,
				fieldType:     tt.fields.fieldType,
				operation:     tt.fields.operation,
			}
			gotFieldCode, gotFieldLoadSql := m.generate(tt.args.field)
			if gotFieldCode != tt.wantFieldCode {
				t.Errorf("generate() gotFieldCode = %v, want %v", gotFieldCode, tt.wantFieldCode)
			}
			if gotFieldLoadSql != tt.wantFieldLoadSql {
				t.Errorf("generate() gotFieldLoadSql = %v, want %v", gotFieldLoadSql, tt.wantFieldLoadSql)
			}
		})
	}
}
