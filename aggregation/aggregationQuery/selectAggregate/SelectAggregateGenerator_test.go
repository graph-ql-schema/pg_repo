package selectAggregate

import (
	"context"
	"testing"

	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/requestedFieldsParser"
)

// Тестирование генерации
func Test_selectAggregateGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []selectProcessor
	}
	type args struct {
		params sbuilder.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Не переданы поля для генерации",
			fields: fields{
				processors: []selectProcessor{
					selectProcessorMock{
						IsAvailable:                true,
						FieldCodeGenerateResult:    "test",
						FieldLoadSqlGenerateResult: "min(test)",
					},
				},
			},
			args: args{},
			want: "",
		},
		{
			name: "Нет доступных процессоров",
			fields: fields{
				processors: []selectProcessor{
					selectProcessorMock{
						IsAvailable:                false,
						FieldCodeGenerateResult:    "test",
						FieldLoadSqlGenerateResult: "min(test)",
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
					},
				},
			},
			want: "",
		},
		{
			name: "Нет ошибок",
			fields: fields{
				processors: []selectProcessor{
					selectProcessorMock{
						IsAvailable:                true,
						FieldCodeGenerateResult:    "test",
						FieldLoadSqlGenerateResult: "min(test)",
					},
				},
			},
			args: args{
				params: sbuilder.Parameters{
					Fields: requestedFieldsParser.GraphQlRequestedFields{
						requestedFieldsParser.NewGraphQlRequestedField("test", nil),
						requestedFieldsParser.NewGraphQlRequestedField("test_2", nil),
					},
				},
			},
			want: "json_build_object('test', min(test), 'test', min(test))",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := selectAggregateGenerator{
				processors: tt.fields.processors,
			}
			if got := s.Generate(context.Background(), tt.args.params); got != tt.want {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
