package aggregationQuery

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/customizations"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/variableGenerator"
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/fieldNameGetter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/groupBy"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/join"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationQuery/selectAggregate"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"github.com/graphql-go/graphql"
)

// Генератор запросов агрегации
type aggregationQueryGenerator struct {
	primaryTable      string
	primaryTableAlias string
	groupBy           groupBy.GroupByGeneratorInterface
	selectGenerator   selectAggregate.SelectAggregateGeneratorInterface
	filter            filter.WhereGeneratorInterface
	joinGenerator     join.JoinGeneratorInterface
	configuration     []configuration.FieldsConfiguration
	variableGenerator variableGenerator.VariableGeneratorInterface
}

// Генерация запроса агрегации
func (a aggregationQueryGenerator) Generate(
	context context.Context,
	params sbuilder.Parameters,
) (sql *string, args []interface{}, err error) {
	selectFields := a.selectGenerator.Generate(context, params)
	select {
	case <-context.Done():
		return
	default:
	}

	if 0 == len(selectFields) {
		err = fmt.Errorf(`failed to generate select fields for aggregate query`)
		return
	}

	where, args, err := a.filter.Build(context, a.filter.GetOperation(params))
	select {
	case <-context.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`failed to generate filter parameters for aggregate query: %v`, err.Error())
		return
	}

	query := fmt.Sprintf(
		`select %v as data from %v %v`,
		selectFields,
		a.primaryTable,
		a.primaryTableAlias,
	)

	joins := a.joinGenerator.Generate(a.configuration)
	select {
	case <-context.Done():
		return
	default:
	}

	for _, sql := range joins {
		query = fmt.Sprintf(`%v left join %v on %v`, query, sql.Query, sql.OnCondition)
	}

	if nil != where {
		query = fmt.Sprintf(`%v where %v`, query, *where)
	}

	groupBySql := a.groupBy.Generate(params)
	select {
	case <-context.Done():
		return
	default:
	}

	if nil != groupBySql {
		query = fmt.Sprintf(`%v group by %v order by %v`, query, *groupBySql, *groupBySql)
	}

	query = a.variableGenerator.SetVariables(
		fmt.Sprintf(`%v limit %v offset %v`, query, params.Arguments.Pagination.Limit, params.Arguments.Pagination.Offset),
	)

	sql = &query

	return
}

// Констуктор генератора
func NewAggregationQueryGenerator(
	object *graphql.Object,
	primaryTable string,
	primaryTablePrimaryKey string,
	configuration []configuration.FieldsConfiguration,
) AggregationQueryGeneratorInterface {
	primaryTableAlias := "pr_agg"
	joinGenerator := join.NewJoinGenerator(primaryTable, primaryTableAlias, primaryTablePrimaryKey)
	nameGetter := fieldNameGetter.NewFieldNameGetter(primaryTableAlias, joinGenerator)

	return &aggregationQueryGenerator{
		primaryTable:      primaryTable,
		primaryTableAlias: primaryTableAlias,
		groupBy:           groupBy.NewGroupByGenerator(configuration, nameGetter),
		selectGenerator: selectAggregate.NewSelectAggregateGenerator(
			primaryTable,
			primaryTableAlias,
			primaryTablePrimaryKey,
			configuration,
			nameGetter,
		),
		filter:            filter.NewWhereGenerator(object, primaryTableAlias, configuration),
		joinGenerator:     join.NewJoinGenerator(primaryTable, primaryTableAlias, primaryTablePrimaryKey),
		configuration:     configuration,
		variableGenerator: variableGenerator.VariableGenerator,
	}
}

// Констуктор генератора с кастомизацией
func NewAggregationQueryGeneratorWithCustomizations(
	object *graphql.Object,
	primaryTable string,
	primaryTablePrimaryKey string,
	configuration []configuration.FieldsConfiguration,
	customizations []customizations.CustomizationInterface,
) AggregationQueryGeneratorInterface {
	primaryTableAlias := "pr_agg"
	joinGenerator := join.NewJoinGenerator(primaryTable, primaryTableAlias, primaryTablePrimaryKey)
	nameGetter := fieldNameGetter.NewFieldNameGetter(primaryTableAlias, joinGenerator)

	return &aggregationQueryGenerator{
		primaryTable:      primaryTable,
		primaryTableAlias: primaryTableAlias,
		groupBy:           groupBy.NewGroupByGenerator(configuration, nameGetter),
		selectGenerator: selectAggregate.NewSelectAggregateGenerator(
			primaryTable,
			primaryTableAlias,
			primaryTablePrimaryKey,
			configuration,
			nameGetter,
		),
		filter:            filter.NewWhereGeneratorWithCustomizations(object, primaryTableAlias, configuration, customizations),
		joinGenerator:     join.NewJoinGenerator(primaryTable, primaryTableAlias, primaryTablePrimaryKey),
		configuration:     configuration,
		variableGenerator: variableGenerator.VariableGenerator,
	}
}
