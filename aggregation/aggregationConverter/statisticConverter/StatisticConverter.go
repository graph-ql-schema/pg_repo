package statisticConverter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Конвертер значений поля статистики агрегации
type statisticConverter struct {
	processors []statisticProcessor
}

// Конвертация значения поля
func (s statisticConverter) Convert(field string, value interface{}) (interface{}, error) {
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			return processor.convert(value)
		}
	}

	return nil, fmt.Errorf(`no available processor for field '%v'`, field)
}

// Конструктор конвертера
func NewStatisticConverter(object *graphql.Object) StatisticConverterInterface {
	return &statisticConverter{
		processors: []statisticProcessor{
			newDateTimeProcessor(object),
			newFloatProcessor(),
		},
	}
}
