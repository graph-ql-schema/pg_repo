package statisticConverter

import (
	"reflect"
	"testing"
	"time"

	gql_root_type_getter "bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Проверка доступности процессора
func Test_dateTimeProcessor_isAvailable(t *testing.T) {
	var objectConfig = graphql.NewObject(
		graphql.ObjectConfig{
			Name: "role",
			Fields: graphql.Fields{
				"field_1": &graphql.Field{
					Type: graphql.DateTime,
					Name: "field_1",
				},
				"field_2": &graphql.Field{
					Type: graphql.String,
					Name: "field_2",
				},
			},
			Description: "Role entity",
		},
	)

	type fields struct {
		object     *graphql.Object
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		field string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
				object:     objectConfig,
			},
			args: args{
				field: "field_1",
			},
			want: true,
		},
		{
			name: "Тестирование на не валидном поле",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
				object:     objectConfig,
			},
			args: args{
				field: "field_2",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dateTimeProcessor{
				object:     tt.fields.object,
				typeGetter: tt.fields.typeGetter,
			}
			if got := d.isAvailable(tt.args.field); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации
func Test_dateTimeProcessor_convert(t *testing.T) {
	date, _ := time.Parse(time.RFC3339, "2014-11-12T11:45:26.371Z")
	dateUnix := time.Unix(date.Unix(), 0)
	onlyDate, _ := time.Parse("2006-01-02", "2021-06-15")

	type fields struct {
		object     *graphql.Object
		typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
	}
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: false,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "2021-06-15",
			},
			want:    onlyDate,
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: "2014-11-12T11:45:26.371Z",
			},
			want:    date,
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: dateUnix.Unix(),
			},
			want:    dateUnix,
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				typeGetter: &graphQlRootTypeGetterMock{},
			},
			args: args{
				value: date,
			},
			want:    date,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dateTimeProcessor{
				object:     tt.fields.object,
				typeGetter: tt.fields.typeGetter,
			}
			got, err := d.convert(tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
