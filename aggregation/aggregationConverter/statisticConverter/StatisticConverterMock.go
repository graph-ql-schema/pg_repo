package statisticConverter

import "fmt"

// Подставка для тестирования
type StatisticConverterMock struct {
	IsError       bool
	ConvertResult interface{}
}

// Конвертация значения поля
func (s StatisticConverterMock) Convert(string, interface{}) (interface{}, error) {
	if s.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return s.ConvertResult, nil
}
