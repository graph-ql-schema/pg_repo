package statisticConverter

import "fmt"

// Подставка для тестирования
type statisticProcessorMock struct {
	IsAvailable    bool
	IsConvertError bool
	ConvertResult  interface{}
}

// Проверка доступности процессора
func (s statisticProcessorMock) isAvailable(string) bool {
	return s.IsAvailable
}

// Конвертация значения поля
func (s statisticProcessorMock) convert(interface{}) (interface{}, error) {
	if s.IsConvertError {
		return nil, fmt.Errorf(`test`)
	}

	return s.ConvertResult, nil
}
