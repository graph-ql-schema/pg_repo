package statisticConverter

import (
	"reflect"
	"testing"
)

// Тестирование конвертации значений
func Test_floatProcessor_convert(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Тестирование на не валидном значении",
			args: args{
				value: "string",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Тестирование на валидном значении",
			args: args{
				value: 2.5,
			},
			want:    2.5,
			wantErr: false,
		},
		{
			name: "Тестирование на валидном значении",
			args: args{
				value: "2.5",
			},
			want:    2.5,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := floatProcessor{}
			got, err := f.convert(tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
