package statisticConverter

// Конвертер значений поля статистики агрегации
type StatisticConverterInterface interface {
	// Конвертация значения поля
	Convert(field string, value interface{}) (interface{}, error)
}

// Процессор конвертации значения поля статистики агрегации
type statisticProcessor interface {
	// Проверка доступности процессора
	isAvailable(field string) bool

	// Конвертация значения поля
	convert(value interface{}) (interface{}, error)
}
