package statisticConverter

import (
	"reflect"
	"testing"
)

// Тестирование конвертации значений
func Test_statisticConverter_Convert(t *testing.T) {
	type fields struct {
		processors []statisticProcessor
	}
	type args struct {
		field string
		value interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "Нет доступного процессора",
			fields: fields{
				processors: []statisticProcessor{
					statisticProcessorMock{
						IsAvailable:    false,
						IsConvertError: false,
						ConvertResult:  nil,
					},
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка конвертации в процессоре",
			fields: fields{
				processors: []statisticProcessor{
					statisticProcessorMock{
						IsAvailable:    true,
						IsConvertError: true,
						ConvertResult:  nil,
					},
				},
			},
			args:    args{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				processors: []statisticProcessor{
					statisticProcessorMock{
						IsAvailable:    true,
						IsConvertError: false,
						ConvertResult:  "test",
					},
				},
			},
			args:    args{},
			want:    "test",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := statisticConverter{
				processors: tt.fields.processors,
			}
			got, err := s.Convert(tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("Convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
