package statisticConverter

import "github.com/graphql-go/graphql"

// Сервис получения корневого типа GraphQL по переданному.
type graphQlRootTypeGetterMock struct {
	IsCalled        bool
	IsNotNullReturn bool
	IsListReturn    bool
	IsNullReturn    bool
}

// Проверяет, что переданный тип является Nullable
func (g *graphQlRootTypeGetterMock) IsNullable(graphql.Type) bool {
	return g.IsNullReturn
}

// Проверяет, что переданный тип является List
func (g *graphQlRootTypeGetterMock) IsList(graphql.Type) bool {
	return g.IsListReturn
}

// Проверяет, что переданный тип не является NotNull
func (g *graphQlRootTypeGetterMock) IsNotNull(graphql.Type) bool {
	return g.IsNotNullReturn
}

// Получение корневого типа поля, которое обернут в NotNull или в List
func (g *graphQlRootTypeGetterMock) GetRootType(gType graphql.Type) graphql.Type {
	g.IsCalled = true

	return gType
}
