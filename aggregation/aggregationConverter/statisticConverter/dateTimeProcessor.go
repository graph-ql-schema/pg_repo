package statisticConverter

import (
	"fmt"
	"time"

	"bitbucket.org/graph-ql-schema/gql-root-type-getter"
	"github.com/graphql-go/graphql"
)

// Процессор конвертации значений DateTime
type dateTimeProcessor struct {
	object     *graphql.Object
	typeGetter gql_root_type_getter.GraphQlRootTypeGetterInterface
}

// Проверка доступности процессора
func (d dateTimeProcessor) isAvailable(field string) bool {
	fieldData := d.object.Fields()[field]
	fieldType := d.typeGetter.GetRootType(fieldData.Type)

	return fieldType == graphql.DateTime
}

// Конвертация значения поля
func (d dateTimeProcessor) convert(value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case int:
		return time.Unix(int64(val), 0), nil
	case int8:
		return time.Unix(int64(val), 0), nil
	case int16:
		return time.Unix(int64(val), 0), nil
	case int32:
		return time.Unix(int64(val), 0), nil
	case int64:
		return time.Unix(val, 0), nil
	case uint:
		return time.Unix(int64(val), 0), nil
	case uint8:
		return time.Unix(int64(val), 0), nil
	case uint16:
		return time.Unix(int64(val), 0), nil
	case uint32:
		return time.Unix(int64(val), 0), nil
	case uint64:
		return time.Unix(int64(val), 0), nil
	case float32:
		return time.Unix(int64(val), 0), nil
	case float64:
		return time.Unix(int64(val), 0), nil
	case time.Time:
		return val, nil
	case string:
		dTime, err := time.Parse(time.RFC3339, val)
		if nil == err {
			return dTime, nil
		}

		dTime, err = time.Parse("2006-01-02", val)
		if nil == err {
			return dTime, nil
		}

		dTime, err = time.Parse(time.RFC3339Nano, val)
		if nil != err {
			return nil, fmt.Errorf(`can't parse datetime for field: %v'`, err.Error())
		}

		return dTime, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type datetime, value '%v'`, value)
	}
}

// Фабрика процессора
func newDateTimeProcessor(object *graphql.Object) statisticProcessor {
	return &dateTimeProcessor{
		object:     object,
		typeGetter: gql_root_type_getter.GraphQlRootTypeGetterFactory(),
	}
}
