package statisticConverter

import (
	"fmt"
	"strconv"
)

// Дефолтный процессор для конвертации статистики
type floatProcessor struct{}

// Проверка доступности процессора
func (f floatProcessor) isAvailable(string) bool {
	return true
}

// Конвертация значения поля
func (f floatProcessor) convert(value interface{}) (interface{}, error) {
	switch val := value.(type) {
	case uint:
		return float64(val), nil
	case uint8:
		return float64(val), nil
	case uint16:
		return float64(val), nil
	case uint32:
		return float64(val), nil
	case uint64:
		return float64(val), nil
	case int:
		return float64(val), nil
	case int8:
		return float64(val), nil
	case int16:
		return float64(val), nil
	case int32:
		return float64(val), nil
	case int64:
		return float64(val), nil
	case float64:
		return val, nil
	case float32:
		return val, nil
	case string:
		res, err := strconv.ParseFloat(val, 0)
		if nil != err {
			return nil, err
		}

		return res, nil
	default:
		return nil, fmt.Errorf(`passed value is not of type float, value '%v'`, value)
	}
}

// Фабрика процессора
func newFloatProcessor() statisticProcessor {
	return &floatProcessor{}
}
