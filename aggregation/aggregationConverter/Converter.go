package aggregationConverter

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter/statisticConverter"
	"github.com/graphql-go/graphql"
)

// Конвертер значений, полученных из БД в валидный для GraphQL тип
type converter struct {
	object    *graphql.Object
	statistic statisticConverter.StatisticConverterInterface
	variants  gql_sql_converter.GraphQlSqlConverterInterface
}

// Конвертация значений
func (c converter) Convert(aggregation Aggregation) (Aggregation, error) {
	result := Aggregation{}
	result.Count = aggregation.Count

	val, err := c.convertStatistic(aggregation.Min)
	if nil != err {
		return Aggregation{}, err
	}
	result.Min = val

	val, err = c.convertStatistic(aggregation.Max)
	if nil != err {
		return Aggregation{}, err
	}
	result.Max = val

	val, err = c.convertStatistic(aggregation.Sum)
	if nil != err {
		return Aggregation{}, err
	}
	result.Sum = val

	val, err = c.convertStatistic(aggregation.Avg)
	if nil != err {
		return Aggregation{}, err
	}
	result.Avg = val

	variantsVal, err := c.convertVariants(aggregation.Variants)
	if nil != err {
		return Aggregation{}, err
	}
	result.Variants = variantsVal

	return result, nil
}

// Конвертация вариантов значений
func (c converter) convertVariants(source map[string]Variants) (map[string]Variants, error) {
	if nil == source {
		return nil, nil
	}

	result := map[string]Variants{}
	for field, variants := range source {
		items := Variants{}
		for _, variant := range variants {
			if nil == variant {
				result[field] = nil
				continue
			}

			converted, err := c.variants.ToBaseType(c.object, field, variant)
			if nil != err {
				return nil, err
			}

			items = append(items, converted)
		}

		result[field] = items
	}

	return result, nil
}

// Конвертация статистического поля
func (c converter) convertStatistic(source StatisticType) (StatisticType, error) {
	if nil == source {
		return nil, nil
	}

	result := StatisticType{}
	for field, val := range source {
		if nil == val {
			result[field] = nil
			continue
		}

		converted, err := c.statistic.Convert(field, val)
		if nil != err {
			return nil, err
		}

		result[field] = converted
	}

	return result, nil
}

// Конструктор конвертера
func NewConverter(object *graphql.Object) ConverterInterface {
	return &converter{
		object:    object,
		statistic: statisticConverter.NewStatisticConverter(object),
		variants:  gql_sql_converter.NewGraphQlSqlConverter(),
	}
}
