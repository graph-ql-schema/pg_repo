package aggregationConverter

import (
	"reflect"
	"testing"

	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter/statisticConverter"
	"github.com/graphql-go/graphql"
)

// Тестирование конвертации значений
func Test_converter_Convert(t *testing.T) {
	type fields struct {
		object    *graphql.Object
		statistic statisticConverter.StatisticConverterInterface
		variants  gql_sql_converter.GraphQlSqlConverterInterface
	}
	type args struct {
		aggregation Aggregation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    Aggregation
		wantErr bool
	}{
		{
			name: "Проброс Count",
			fields: fields{
				object:    nil,
				statistic: statisticConverter.StatisticConverterMock{},
				variants:  graphQlSqlConverterMock{},
			},
			args: args{
				aggregation: Aggregation{
					Count: 1,
				},
			},
			want: Aggregation{
				Count: 1,
			},
			wantErr: false,
		},
		{
			name: "Ошибка при конвертации статистики",
			fields: fields{
				object: nil,
				statistic: statisticConverter.StatisticConverterMock{
					IsError:       true,
					ConvertResult: nil,
				},
				variants: graphQlSqlConverterMock{},
			},
			args: args{
				aggregation: Aggregation{
					Avg: StatisticType{
						"test": 123,
					},
				},
			},
			want:    Aggregation{},
			wantErr: true,
		},
		{
			name: "Конвертация статистики",
			fields: fields{
				object: nil,
				statistic: statisticConverter.StatisticConverterMock{
					IsError:       false,
					ConvertResult: "test",
				},
				variants: graphQlSqlConverterMock{},
			},
			args: args{
				aggregation: Aggregation{
					Avg: StatisticType{
						"test": 123,
					},
				},
			},
			want: Aggregation{
				Avg: StatisticType{
					"test": "test",
				},
			},
			wantErr: false,
		},
		{
			name: "Ошибка конвертации вариантов",
			fields: fields{
				object:    nil,
				statistic: statisticConverter.StatisticConverterMock{},
				variants: graphQlSqlConverterMock{
					IsToBaseTypeError: true,
					ToBaseTypeResult:  nil,
				},
			},
			args: args{
				aggregation: Aggregation{
					Variants: map[string]Variants{
						"test": {
							1,
							2,
						},
					},
				},
			},
			want:    Aggregation{},
			wantErr: true,
		},
		{
			name: "Конвертация вариантов",
			fields: fields{
				object:    nil,
				statistic: statisticConverter.StatisticConverterMock{},
				variants: graphQlSqlConverterMock{
					IsToBaseTypeError: false,
					ToBaseTypeResult:  "test",
				},
			},
			args: args{
				aggregation: Aggregation{
					Variants: map[string]Variants{
						"test": {
							1,
							2,
						},
					},
				},
			},
			want: Aggregation{
				Variants: map[string]Variants{
					"test": {
						"test",
						"test",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := converter{
				object:    tt.fields.object,
				statistic: tt.fields.statistic,
				variants:  tt.fields.variants,
			}
			got, err := c.Convert(tt.args.aggregation)
			if (err != nil) != tt.wantErr {
				t.Errorf("Convert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Convert() got = %v, want %v", got, tt.want)
			}
		})
	}
}
