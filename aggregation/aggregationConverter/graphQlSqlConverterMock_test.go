package aggregationConverter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type graphQlSqlConverterMock struct {
	IsToBaseTypeError bool
	ToBaseTypeResult  interface{}
}

// Конвертация в базовый тип, например в строку или число
func (g graphQlSqlConverterMock) ToBaseType(*graphql.Object, string, interface{}) (interface{}, error) {
	if g.IsToBaseTypeError {
		return nil, fmt.Errorf(`test`)
	}

	return g.ToBaseTypeResult, nil
}

// Конвертация в SQL like значение
func (g graphQlSqlConverterMock) ToSQLValue(*graphql.Object, string, interface{}) (string, error) {
	return "", nil
}
