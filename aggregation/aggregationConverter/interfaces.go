package aggregationConverter

type StatisticType = map[string]interface{}
type Variants = []interface{}

// Результат агрегации
type Aggregation struct {
	Count    int64               `json:"count"`
	Avg      StatisticType       `json:"avg"`
	Min      StatisticType       `json:"min"`
	Max      StatisticType       `json:"max"`
	Sum      StatisticType       `json:"sum"`
	Variants map[string]Variants `json:"variants"`
}

// Конвертер значений, полученных из БД в валидный для GraphQL тип
type ConverterInterface interface {
	// Конвертация значений
	Convert(aggregation Aggregation) (Aggregation, error)
}
