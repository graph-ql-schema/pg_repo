package pg_repo

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"context"
	"database/sql"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Результат вставки/обновления значений
type InsertUpdateResult struct {
	AffectedRows int           `json:"affected_rows"`
	Returning    []interface{} `json:"returning"`
}

// Результат удаления значений
type DeleteResult struct {
	AffectedRows int `json:"affected_rows"`
}

// Репозиторий GraphQL сущности
type PgRepositoryInterface interface {
	// Получение листинга сущностей по переданным параметрам GraphQL
	GetListByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]interface{}, error)

	// Получение листинга сущностей по кастомному SQL запросу
	GetListByCustomSql(ctx context.Context, callback queryGenerator.TSqlModificationCallback, tx *sql.Tx) ([]interface{}, error)

	// Получение результатов агрегации сущностей по переданным параметрам GraphQL
	GetAggregationByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]aggregationConverter.Aggregation, error)

	// Вставка значений по переданным параметрам GraphQL
	InsertEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*InsertUpdateResult, error)

	// Обновление значений по переданным параметрам GraphQL
	UpdateEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*InsertUpdateResult, error)

	// Удаление значений по переданным параметрам GraphQL
	DeleteEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*DeleteResult, error)

	// Регистрация подписчика на события
	SubscribeToEvents(subscriber events.EventsSubscriberInterface)

	// Отправка событий в зарегистрированных подписчиков. Для внутренних нужд.
	DispatchEvents(ctx context.Context, events []events.Event, tx *sql.Tx) error

	// Получение списка значений первичного ключа для переданных параметров GraphQL
	GetPrimaryKeyValuesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]interface{}, error)
}
