package customizations

import (
	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"github.com/graphql-go/graphql"
)

// Интерфейс кастомизатора
type CustomizationInterface interface {
	// Получение типа кастомизации
	GetType() int64

	// Применение кастомизатора
	Customize() interface{}
}

// Интерфейс для кастомизации процессора операций
type SimpleOperationProcessorInterface interface {
	// Получение типа операции
	GetOperationType() string

	// Получение операторв
	GetOperator() string

	// Получение GraphQl объекта
	GetObject() *graphql.Object

	// Получение конвертера значений SQL
	GetSqlValueConverter() gql_sql_converter.GraphQlSqlConverterInterface

	// Получение генератора оперций
	GetOperationGenerator() simpleOperationGenerator.SimpleOperationGeneratorInterface
}
