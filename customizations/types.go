package customizations

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"github.com/graphql-go/graphql"
)

// Тип, описывающий результат кастомизации
type TWhereGeneratorProcessor = func(*graphql.Object, simpleOperationGenerator.SimpleOperationGeneratorInterface) SimpleOperationProcessorInterface
