module bitbucket.org/graph-ql-schema/pg_repo/v2

go 1.14

require (
	bitbucket.org/graph-ql-schema/gql-root-type-getter v1.3.2
	bitbucket.org/graph-ql-schema/gql-sql-converter v1.2.6
	bitbucket.org/graph-ql-schema/nullable v1.0.5
	bitbucket.org/graph-ql-schema/sbuilder/v2 v2.4.2
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/graphql-go/graphql v0.8.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/valyala/fasthttp v1.39.0 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90 // indirect
	golang.org/x/sync v0.0.0-20220819030929-7fc1605a5dde // indirect
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
	golang.org/x/term v0.0.0-20220722155259-a9ba230a4035 // indirect
	google.golang.org/appengine v1.6.6 // indirect
)
