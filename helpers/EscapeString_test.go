package helpers

import "testing"

// Тестирование экранирования строк
func Test_escapeString(t *testing.T) {
	type args struct {
		value string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Тестирование на пустой строке",
			args: args{
				value: "",
			},
			want: "",
		},
		{
			name: "Тестирование на корректном значении строки",
			args: args{
				value: "test12",
			},
			want: "test12",
		},
		{
			name: "Тестирование на не корректном значении строки",
			args: args{
				value: `"test12"`,
			},
			want: `\"test12\"`,
		},
		{
			name: "Тестирование на не корректном значении строки",
			args: args{
				value: `'test12'`,
			},
			want: `\'test12\'`,
		},
		{
			name: "Тестирование на не корректном значении строки",
			args: args{
				value: `\'test12\'`,
			},
			want: `\\\'test12\\\'`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EscapeString(tt.args.value); got != tt.want {
				t.Errorf("EscapeString() = %v, want %v", got, tt.want)
			}
		})
	}
}
