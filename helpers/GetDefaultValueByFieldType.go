package helpers

// Получение дефолтных значений по переданному типу поля
func GetDefaultValueByFieldType(fieldType string) string {
	switch fieldType {
	case "date":
		return "now()"
	case "timestamp":
		return "now()"
	case "timestamptz":
		return "now()"
	case "time":
		return "now()"
	case "timetz":
		return "now()"
	case "bit":
		return "''"
	case "varbit":
		return "''"
	case "box":
		return "''"
	case "bytea":
		return "''"
	case "character":
		return "''"
	case "char":
		return "''"
	case "varchar":
		return "''"
	case "character varying":
		return "''"
	case "text":
		return "''"
	case "uuid":
		return "''"
	case "json":
		return "'{}'"
	case "jsonb":
		return "'{}'"
	case "boolean":
		return "'{}'"
	case "bool":
		return "false"
	}

	return "0"
}
