package helpers

import "strings"

// Экранирование строки, не валидной для SQL для исключения SQL injection
func EscapeString(value string) string {
	result := strings.Replace(value, `\`, `\\`, -1)
	replace := map[string]string{
		"\x1a": "\\Z",
		"\\0":  "\\\\0",
		"\n":   "\\n",
		"\r":   "\\r",
		"'":    `\'`,
		`"`:    `\"`,
	}

	for b, a := range replace {
		result = strings.Replace(result, b, a, -1)
	}

	return result
}
