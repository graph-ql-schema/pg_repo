package resolveMapBuilder

// Подставка для тестирования
type resolveMapBuilderProcessorMock struct {
	IsAvailable   bool
	ConvertResult []string
}

// Проверка доступности процессора
func (r resolveMapBuilderProcessorMock) isAvailable(interface{}) bool {
	return r.IsAvailable
}

// Конвертация значений процессора
func (r resolveMapBuilderProcessorMock) convert(interface{}) []string {
	return r.ConvertResult
}
