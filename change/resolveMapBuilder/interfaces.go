package resolveMapBuilder

// Генератор карты значений полей для резолвера отношений
type ResolveMapBuilderInterface interface {
	// Генерация значений
	Generate(data map[string]interface{}) map[string][]string
}

// Процессор резолвера
type resolveMapBuilderProcessor interface {
	// Проверка доступности процессора
	isAvailable(value interface{}) bool

	// Конвертация значений процессора
	convert(value interface{}) []string
}
