package resolveMapBuilder

import (
	"fmt"
	"reflect"
)

// Процессор срезов
type sliceProcessor struct{}

// Конструктор процессора
func newSliceProcessor() resolveMapBuilderProcessor {
	return &sliceProcessor{}
}

// Проверка доступности процессора
func (s sliceProcessor) isAvailable(value interface{}) bool {
	return reflect.TypeOf(value).Kind() == reflect.Slice
}

// Конвертация значений процессора
func (s sliceProcessor) convert(value interface{}) []string {
	ss := reflect.ValueOf(value)
	result := []string{}

	for i := 0; i < ss.Len(); i++ {
		notProcessed := ss.Index(i).Interface()

		val := ""
		switch parsed := notProcessed.(type) {
		case string:
			val = fmt.Sprintf(`'%v'`, parsed)
			break
		default:
			val = fmt.Sprintf(`%v`, parsed)
		}

		result = append(result, val)
	}

	return result
}
