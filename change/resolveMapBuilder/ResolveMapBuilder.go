package resolveMapBuilder

import "sync"

// Результат обработки единичного поля
type generationResult struct {
	Field  string
	Result []string
}

// Генератор карты значений полей для резолвера отношений
type resolveMapBuilder struct {
	processors []resolveMapBuilderProcessor
}

// Конструктор процессора
func NewResolveMapBuilder() ResolveMapBuilderInterface {
	return &resolveMapBuilder{
		processors: []resolveMapBuilderProcessor{
			newNotSliceProcessor(),
			newSliceProcessor(),
		},
	}
}

// Генерация значений
func (r resolveMapBuilder) Generate(data map[string]interface{}) map[string][]string {
	if 0 == len(data) {
		return map[string][]string{}
	}

	var wg sync.WaitGroup
	itemsChan := make(chan generationResult)
	resultChan := make(chan map[string][]string)
	completeChan := make(chan bool)

	defer close(itemsChan)
	defer close(resultChan)
	defer close(completeChan)

	// Запускаем чтение результатов
	go r.read(itemsChan, resultChan, completeChan)

	// Асинхронная обработка полей
	wg.Add(len(data))
	for field, val := range data {
		go r.process(&wg, itemsChan, field, val)
	}

	wg.Wait()
	completeChan <- true

	return <-resultChan
}

// Асинхронное чтение результатов обработки
func (r resolveMapBuilder) read(
	itemsChan chan generationResult,
	resultChan chan map[string][]string,
	completeChan chan bool,
) {
	result := map[string][]string{}
	for {
		select {
		case item := <-itemsChan:
			result[item.Field] = item.Result
			break
		case _ = <-completeChan:
			resultChan <- result
			return
		}
	}
}

// Асинхронная обработка одного поля
func (r resolveMapBuilder) process(wg *sync.WaitGroup, itemsChan chan generationResult, field string, values interface{}) {
	defer wg.Done()

	for _, processor := range r.processors {
		if processor.isAvailable(values) {
			itemsChan <- generationResult{
				Field:  field,
				Result: processor.convert(values),
			}
		}
	}
}
