package resolveMapBuilder

import (
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_notSliceProcessor_isAvailable(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Не корректный тип",
			args: args{
				value: []string{},
			},
			want: false,
		},
		{
			name: "Корректный тип",
			args: args{
				value: "test",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notSliceProcessor{}
			if got := n.isAvailable(tt.args.value); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации
func Test_notSliceProcessor_convert(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Передан Int",
			args: args{
				value: 1,
			},
			want: []string{"1"},
		},
		{
			name: "Передан String",
			args: args{
				value: "1",
			},
			want: []string{"'1'"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := notSliceProcessor{}
			if got := n.convert(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() = %v, want %v", got, tt.want)
			}
		})
	}
}
