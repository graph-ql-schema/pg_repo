package resolveMapBuilder

// Подставка для тестирования
type ResolveMapBuilderMock struct {
	Result map[string][]string
}

// Генерация значений
func (r ResolveMapBuilderMock) Generate(map[string]interface{}) map[string][]string {
	return r.Result
}
