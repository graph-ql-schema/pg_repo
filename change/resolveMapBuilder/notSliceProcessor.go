package resolveMapBuilder

import (
	"fmt"
	"reflect"
)

// Процессор скалярных значений
type notSliceProcessor struct{}

// Конструктор процессора
func newNotSliceProcessor() resolveMapBuilderProcessor {
	return &notSliceProcessor{}
}

// Проверка доступности процессора
func (n notSliceProcessor) isAvailable(value interface{}) bool {
	return reflect.TypeOf(value).Kind() != reflect.Slice
}

// Конвертация значений процессора
func (n notSliceProcessor) convert(value interface{}) []string {
	val := ""
	switch parsed := value.(type) {
	case string:
		val = fmt.Sprintf(`'%v'`, parsed)
		break
	default:
		val = fmt.Sprintf(`%v`, parsed)
	}

	return []string{val}
}
