package resolveMapBuilder

import (
	"reflect"
	"testing"
)

// Тестирование генерации
func Test_resolveMapBuilder_Generate(t *testing.T) {
	type fields struct {
		processors []resolveMapBuilderProcessor
	}
	type args struct {
		data map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[string][]string
	}{
		{
			name: "Не переданы поля для конвертации",
			fields: fields{
				processors: []resolveMapBuilderProcessor{
					resolveMapBuilderProcessorMock{
						IsAvailable:   true,
						ConvertResult: []string{"test"},
					},
				},
			},
			args: args{
				data: map[string]interface{}{},
			},
			want: map[string][]string{},
		},
		{
			name: "Если есть доступный процессор",
			fields: fields{
				processors: []resolveMapBuilderProcessor{
					resolveMapBuilderProcessorMock{
						IsAvailable:   true,
						ConvertResult: []string{"test"},
					},
				},
			},
			args: args{
				data: map[string]interface{}{
					"test":   1,
					"test-2": 1,
				},
			},
			want: map[string][]string{
				"test":   {"test"},
				"test-2": {"test"},
			},
		},
		{
			name: "Если нет доступного процессора",
			fields: fields{
				processors: []resolveMapBuilderProcessor{
					resolveMapBuilderProcessorMock{
						IsAvailable:   false,
						ConvertResult: []string{"test"},
					},
				},
			},
			args: args{
				data: map[string]interface{}{
					"test":   1,
					"test-2": 1,
				},
			},
			want: map[string][]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := resolveMapBuilder{
				processors: tt.fields.processors,
			}
			if got := r.Generate(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
