package resolveMapBuilder

import (
	"reflect"
	"testing"
)

// Тестирование доступности процессора
func Test_sliceProcessor_isAvailable(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Не корректный тип",
			args: args{
				value: "test",
			},
			want: false,
		},
		{
			name: "Корректный тип",
			args: args{
				value: []string{},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sliceProcessor{}
			if got := s.isAvailable(tt.args.value); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование конвертации значений
func Test_sliceProcessor_convert(t *testing.T) {
	type args struct {
		value interface{}
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "Передан Int",
			args: args{
				value: []int{1},
			},
			want: []string{"1"},
		},
		{
			name: "Передан String",
			args: args{
				value: []string{"1"},
			},
			want: []string{"'1'"},
		},
		{
			name: "Передано несколько значений",
			args: args{
				value: []interface{}{1, "1"},
			},
			want: []string{"1", "'1'"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := sliceProcessor{}
			if got := s.convert(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convert() = %v, want %v", got, tt.want)
			}
		})
	}
}
