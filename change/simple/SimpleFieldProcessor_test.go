package simple

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"context"
	"testing"
)

// Тестирование получения кода поля
func Test_simpleFieldProcessor_GetFieldCode(t *testing.T) {
	type fields struct {
		processors []typedProcessorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Нет доступного процессора",
			fields: fields{
				processors: []typedProcessorInterface{
					typedProcessorMock{
						IsAvailable: false,
						FieldCode:   "",
					},
				},
			},
			args: args{},
			want: "",
		},
		{
			name: "Есть доступный процессор",
			fields: fields{
				processors: []typedProcessorInterface{
					typedProcessorMock{
						IsAvailable: true,
						FieldCode:   "ttt",
					},
				},
			},
			args: args{},
			want: "ttt",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleFieldProcessor{
				processors: tt.fields.processors,
			}
			if got := s.GetFieldCode(tt.args.field); got != tt.want {
				t.Errorf("GetFieldCode() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга значения поля
func Test_simpleFieldProcessor_ParseFieldValue(t *testing.T) {
	type fields struct {
		processors []typedProcessorInterface
	}
	type args struct {
		field configuration.FieldsConfiguration
		value interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Нет доступного процессора",
			fields: fields{
				processors: []typedProcessorInterface{
					typedProcessorMock{
						IsAvailable:       false,
						IsFieldValueError: false,
						FieldValue:        "ttt",
					},
				},
			},
			args:    args{},
			want:    "",
			wantErr: false,
		},
		{
			name: "Есть доступный процессор. Возвращает ошибку.",
			fields: fields{
				processors: []typedProcessorInterface{
					typedProcessorMock{
						IsAvailable:       true,
						IsFieldValueError: true,
						FieldValue:        "",
					},
				},
			},
			args:    args{},
			want:    "",
			wantErr: true,
		},
		{
			name: "Есть доступный процессор. Нет ошибки.",
			fields: fields{
				processors: []typedProcessorInterface{
					typedProcessorMock{
						IsAvailable:       true,
						IsFieldValueError: false,
						FieldValue:        "ttt",
					},
				},
			},
			args:    args{},
			want:    "ttt",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleFieldProcessor{
				processors: tt.fields.processors,
			}
			got, _, err := s.ParseFieldValue(context.Background(), tt.args.field, tt.args.value)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseFieldValue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ParseFieldValue() got = %v, want %v", got, tt.want)
			}
		})
	}
}
