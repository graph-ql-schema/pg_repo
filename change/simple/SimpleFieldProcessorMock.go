package simple

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"context"
	"fmt"
)

// Подставка для тестирования
type SimpleFieldProcessorMock struct {
	FieldCode         string
	FieldValue        string
	IsFieldValueError bool
	Args              []interface{}
}

// Получение кода поля для вставки в SQL запрос
func (s SimpleFieldProcessorMock) GetFieldCode(configuration.FieldsConfiguration) string {
	return s.FieldCode
}

// Парсинг значения поля для вставки в SQL запрос
func (s SimpleFieldProcessorMock) ParseFieldValue(
	ctx context.Context,
	field configuration.FieldsConfiguration,
	value interface{},
) (sql string, args []interface{}, err error) {
	if s.IsFieldValueError {
		return "", nil, fmt.Errorf(`test`)
	}

	return s.FieldValue, s.Args, nil
}
