package simple

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"context"
)

// Сервис обработки простых полей для изменения
type SimpleFieldProcessorInterface interface {
	// Получение кода поля для вставки в SQL запрос
	GetFieldCode(field configuration.FieldsConfiguration) string

	// Парсинг значения поля для вставки в SQL запрос
	ParseFieldValue(
		ctx context.Context,
		field configuration.FieldsConfiguration,
		value interface{},
	) (sql string, args []interface{}, err error)
}

// Процессор для обработки каждого типа конфигурации отдельно
type typedProcessorInterface interface {
	// Проверка доступности процессора
	isAvailable(field configuration.FieldsConfiguration) bool

	// Получение кода поля для вставки в SQL запрос
	getFieldCode(field configuration.FieldsConfiguration) string

	// Парсинг значения поля для вставки в SQL запрос
	parseFieldValue(
		ctx context.Context,
		field configuration.FieldsConfiguration,
		value interface{},
	) (sql string, args []interface{}, err error)
}
