package simple

import (
    gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
    "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
    "bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
    "bitbucket.org/graph-ql-schema/pg_repo/v2/for_tests"
    "context"
    "fmt"
    "github.com/graphql-go/graphql"
    "testing"
)

// Тестирование доступности процессора
func Test_arrayProcessor_isAvailable(t *testing.T) {
    type fields struct {
        object         *graphql.Object
        valueConverter gql_sql_converter.GraphQlSqlConverterInterface
    }
    type args struct {
        field configuration.FieldsConfiguration
    }
    tests := []struct {
        name   string
        fields fields
        args   args
        want   bool
    }{
        {
            name:   "Не корректный тип конфига",
            fields: fields{},
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:           "test",
                    FieldNum:        0,
                    GraphQlName:     "test_f",
                    DbConfiguration: configuration.ScalarFieldConfiguration{},
                },
            },
            want: false,
        },
        {
            name:   "Корректный тип конфига",
            fields: fields{},
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:           "test",
                    FieldNum:        0,
                    GraphQlName:     "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{},
                },
            },
            want: true,
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            a := arrayProcessor{
                object:         tt.fields.object,
                valueConverter: tt.fields.valueConverter,
            }
            if got := a.isAvailable(tt.args.field); got != tt.want {
                t.Errorf("isAvailable() = %v, want %v", got, tt.want)
            }
        })
    }
}

// Тестирование получения кода поля
func Test_arrayProcessor_getFieldCode(t *testing.T) {
    type fields struct {
        object         *graphql.Object
        valueConverter gql_sql_converter.GraphQlSqlConverterInterface
    }
    type args struct {
        field configuration.FieldsConfiguration
    }
    tests := []struct {
        name   string
        fields fields
        args   args
        want   string
    }{
        {
            name:   "Тестирование получения кода поля",
            fields: fields{},
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:       "test",
                    FieldNum:    0,
                    GraphQlName: "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{
                        DbName: "ff",
                    },
                },
            },
            want: "ff",
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            a := arrayProcessor{
                object:         tt.fields.object,
                valueConverter: tt.fields.valueConverter,
            }
            if got := a.getFieldCode(tt.args.field); got != tt.want {
                t.Errorf("getFieldCode() = %v, want %v", got, tt.want)
            }
        })
    }
}

// Тестирование получения значения поля
func Test_arrayProcessor_parseFieldValue(t *testing.T) {
    type fields struct {
        object         *graphql.Object
        valueConverter gql_sql_converter.GraphQlSqlConverterInterface
    }
    type args struct {
        field configuration.FieldsConfiguration
        value interface{}
    }
    tests := []struct {
        name    string
        fields  fields
        args    args
        want    string
        wantErr bool
    }{
        {
            name: "Нет ошибок",
            fields: fields{
                object: nil,
                valueConverter: for_tests.GraphQlSqlConverterMock{
                    IsToBaseTypeError: false,
                    ToSQLValueResult:  "tt",
                },
            },
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:       "test",
                    FieldNum:    0,
                    GraphQlName: "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{
                        DbName: "ff",
                    },
                },
                value: "tt",
            },
            want:    fmt.Sprintf("array[%v]", consts.ArgumentPlaceholder),
            wantErr: false,
        },
        {
            name: "Нет ошибок. Передан массив значений.",
            fields: fields{
                object: nil,
                valueConverter: for_tests.GraphQlSqlConverterMock{
                    IsToBaseTypeError: false,
                    ToSQLValueResult:  "tt",
                },
            },
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:       "test",
                    FieldNum:    0,
                    FieldType:   "string",
                    GraphQlName: "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{
                        DbName: "ff",
                    },
                },
                value: []string{"tt", "tt"},
            },
            want: fmt.Sprintf(
                "array[%v::string, %v::string]", consts.ArgumentPlaceholder, consts.ArgumentPlaceholder),
            wantErr: false,
        },
        {
            name: "Ошибка конвертации значения",
            fields: fields{
                object: nil,
                valueConverter: for_tests.GraphQlSqlConverterMock{
                    IsToBaseTypeError: true,
                    ToSQLValueResult:  "tt",
                },
            },
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:       "test",
                    FieldNum:    0,
                    GraphQlName: "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{
                        DbName: "ff",
                    },
                },
                value: "tt",
            },
            want:    "",
            wantErr: true,
        },
        {
            name: "Ошибка конвертации значения. Передан массив значений.",
            fields: fields{
                object: nil,
                valueConverter: for_tests.GraphQlSqlConverterMock{
                    IsToBaseTypeError: true,
                    ToSQLValueResult:  "tt",
                },
            },
            args: args{
                field: configuration.FieldsConfiguration{
                    Field:       "test",
                    FieldNum:    0,
                    GraphQlName: "test_f",
                    DbConfiguration: configuration.ArrayFieldConfiguration{
                        DbName: "ff",
                    },
                },
                value: []string{"tt", "ff"},
            },
            want:    "",
            wantErr: true,
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            a := arrayProcessor{
                object:         tt.fields.object,
                valueConverter: tt.fields.valueConverter,
            }
            got, _, err := a.parseFieldValue(context.Background(), tt.args.field, tt.args.value)
            if (err != nil) != tt.wantErr {
                t.Errorf("parseFieldValue() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if got != tt.want {
                t.Errorf("parseFieldValue() got = %v, want %v", got, tt.want)
            }
        })
    }
}
