package simple

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"context"
	"fmt"
)

// Подставка для тестирования
type typedProcessorMock struct {
	IsAvailable       bool
	FieldCode         string
	IsFieldValueError bool
	FieldValue        string
	Args              []interface{}
}

// Проверка доступности процессора
func (t typedProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return t.IsAvailable
}

// Получение кода поля для вставки в SQL запрос
func (t typedProcessorMock) getFieldCode(configuration.FieldsConfiguration) string {
	return t.FieldCode
}

// Парсинг значения поля для вставки в SQL запрос
func (t typedProcessorMock) parseFieldValue(
	ctx context.Context,
	field configuration.FieldsConfiguration,
	value interface{},
) (sql string, args []interface{}, err error) {
	if t.IsFieldValueError {
		return "", nil, fmt.Errorf(`test`)
	}

	return t.FieldValue, t.Args, nil
}
