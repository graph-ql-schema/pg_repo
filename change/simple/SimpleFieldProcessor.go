package simple

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"context"
	"github.com/graphql-go/graphql"
)

// Сервис обработки простых полей для изменения
type simpleFieldProcessor struct {
	processors []typedProcessorInterface
}

// Конструктор сервиса
func NewSimpleFieldProcessor(object *graphql.Object) SimpleFieldProcessorInterface {
	return &simpleFieldProcessor{
		processors: []typedProcessorInterface{
			newScalarProcessor(object),
			newArrayProcessor(object),
		},
	}
}

// Получение кода поля для вставки в SQL запрос
func (s simpleFieldProcessor) GetFieldCode(field configuration.FieldsConfiguration) string {
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			return processor.getFieldCode(field)
		}
	}

	return ""
}

// Парсинг значения поля для вставки в SQL запрос
func (s simpleFieldProcessor) ParseFieldValue(
	ctx context.Context,
	field configuration.FieldsConfiguration,
	value interface{},
) (sql string, args []interface{}, err error) {
	for _, processor := range s.processors {
		if processor.isAvailable(field) {
			return processor.parseFieldValue(ctx, field, value)
		}
	}

	return "", nil, nil
}
