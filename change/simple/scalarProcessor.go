package simple

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"context"
	"github.com/graphql-go/graphql"
)

// Процессор для простых полей
type scalarProcessor struct {
	object         *graphql.Object
	valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Конструктор процессора
func newScalarProcessor(object *graphql.Object) typedProcessorInterface {
	return &scalarProcessor{
		object:         object,
		valueConverter: gql_sql_converter.NewGraphQlSqlConverter(),
	}
}

// Проверка доступности процессора
func (s scalarProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
	_, ok := field.DbConfiguration.(configuration.ScalarFieldConfiguration)

	return ok
}

// Получение кода поля для вставки в SQL запрос
func (s scalarProcessor) getFieldCode(field configuration.FieldsConfiguration) string {
	dbConf := field.DbConfiguration.(configuration.ScalarFieldConfiguration)

	return dbConf.DbName
}

// Парсинг значения поля для вставки в SQL запрос
func (s scalarProcessor) parseFieldValue(
	ctx context.Context,
	field configuration.FieldsConfiguration,
	value interface{},
) (sql string, args []interface{}, err error) {
	select {
	case <-ctx.Done():
		return
	default:
	}

	var val interface{}
	val, err = s.valueConverter.ToBaseType(s.object, field.GraphQlName, value)
	if nil != err {
		return
	}

	sql = consts.ArgumentPlaceholder
	args = []interface{}{val}

	return
}
