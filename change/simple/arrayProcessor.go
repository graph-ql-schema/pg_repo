package simple

import (
    "bitbucket.org/graph-ql-schema/gql-sql-converter"
    "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
    "bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
    "context"
    "fmt"
    "github.com/graphql-go/graphql"
    "reflect"
    "strings"
)

// Процессор массивов
type arrayProcessor struct {
    object         *graphql.Object
    valueConverter gql_sql_converter.GraphQlSqlConverterInterface
}

// Конструктор процессора
func newArrayProcessor(object *graphql.Object) typedProcessorInterface {
    return &arrayProcessor{
        object:         object,
        valueConverter: gql_sql_converter.NewGraphQlSqlConverter(),
    }
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(field configuration.FieldsConfiguration) bool {
    _, ok := field.DbConfiguration.(configuration.ArrayFieldConfiguration)

    return ok
}

// Получение кода поля для вставки в SQL запрос
func (a arrayProcessor) getFieldCode(field configuration.FieldsConfiguration) string {
    dbConf := field.DbConfiguration.(configuration.ArrayFieldConfiguration)

    return dbConf.DbName
}

// Парсинг значения поля для вставки в SQL запрос
func (a arrayProcessor) parseFieldValue(
    ctx context.Context,
    field configuration.FieldsConfiguration,
    value interface{},
) (sql string, args []interface{}, err error) {
    select {
    case <-ctx.Done():
        return
    default:
    }

    values := []string{}
    args = []interface{}{}

    if reflect.TypeOf(value).Kind() == reflect.Slice {
        s := reflect.ValueOf(value)

        args = make([]interface{}, 0, s.Len())
        values = make([]string, 0, s.Len())

        for i := 0; i < s.Len(); i++ {
            select {
            case <-ctx.Done():
                return
            default:
            }

            var val interface{}
            val, err = a.valueConverter.ToBaseType(a.object, field.GraphQlName, s.Index(i).Interface())
            if nil != err {
                return
            }

            values = append(values, consts.ArgumentPlaceholder+`::`+field.FieldType)
            args = append(args, val)
        }
    } else {
        var val interface{}
        val, err = a.valueConverter.ToBaseType(a.object, field.GraphQlName, value)
        if nil != err {
            return
        }

        values = append(values, consts.ArgumentPlaceholder)
        args = append(args, val)
    }

    if 0 == len(values) {
        sql = "'{}'"
        return
    }

    sql = fmt.Sprintf(`array[%v]`, strings.Join(values, ", "))
    return
}
