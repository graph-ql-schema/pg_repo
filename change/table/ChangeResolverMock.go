package table

import (
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type ChangeResolverMock struct {
	ResolveInsertError bool
	ResolveUpdateError bool
	ResolveDeleteError bool
}

// Резолвинг конфликтов вставки родительской сущности
func (c ChangeResolverMock) ResolveInsert(string, map[string][]string, *sql.Tx) error {
	if c.ResolveInsertError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Резолвинг конфликтов обновления родительской сущности
func (c ChangeResolverMock) ResolveUpdate(string, map[string][]string, *sql.Tx) error {
	if c.ResolveUpdateError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Резолвинг конфликтов удаления родительской сущности
func (c ChangeResolverMock) ResolveDelete(string, *sql.Tx) error {
	if c.ResolveDeleteError {
		return fmt.Errorf(`test`)
	}

	return nil
}
