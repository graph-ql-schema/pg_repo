package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type singleChangeResolverMock struct {
	IsInsertError bool
	IsUpdateError bool
	IsDeleteError bool
}

// Резолвинг конфликтов вставки родительской сущности
func (s singleChangeResolverMock) resolveInsert(string, map[string][]string, configuration.FieldsConfiguration, *sql.Tx) error {
	if s.IsInsertError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Резолвинг конфликтов обновления родительской сущности
func (s singleChangeResolverMock) resolveUpdate(string, map[string][]string, configuration.FieldsConfiguration, *sql.Tx) error {
	if s.IsUpdateError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Резолвинг конфликтов удаления родительской сущности
func (s singleChangeResolverMock) resolveDelete(string, configuration.FieldsConfiguration, *sql.Tx) error {
	if s.IsDeleteError {
		return fmt.Errorf(`test`)
	}

	return nil
}
