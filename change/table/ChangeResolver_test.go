package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"github.com/sirupsen/logrus"
	"testing"
)

// Тестирование резолвинга вставки
func Test_changeResolver_ResolveInsert(t *testing.T) {
	type fields struct {
		configuration        []configuration.FieldsConfiguration
		singleChangeResolver singleChangeResolverInterface
		log                  *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Нет поддерживаемых полей",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: true,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := changeResolver{
				configuration:        tt.fields.configuration,
				singleChangeResolver: tt.fields.singleChangeResolver,
				log:                  tt.fields.log,
			}
			if err := c.ResolveInsert(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("ResolveInsert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование резолвинга обновления
func Test_changeResolver_ResolveUpdate(t *testing.T) {
	type fields struct {
		configuration        []configuration.FieldsConfiguration
		singleChangeResolver singleChangeResolverInterface
		log                  *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Нет поддерживаемых полей",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: true,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := changeResolver{
				configuration:        tt.fields.configuration,
				singleChangeResolver: tt.fields.singleChangeResolver,
				log:                  tt.fields.log,
			}
			if err := c.ResolveUpdate(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("ResolveUpdate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование резолвинга удаления
func Test_changeResolver_ResolveDelete(t *testing.T) {
	type fields struct {
		configuration        []configuration.FieldsConfiguration
		singleChangeResolver singleChangeResolverInterface
		log                  *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Нет поддерживаемых полей",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: true,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:       "test",
						FieldNum:    0,
						GraphQlName: "test_f",
						DbConfiguration: configuration.TableFieldConfiguration{
							Table:      "test_db",
							Target:     "tt",
							ForeignKey: "ii",
							LocalKey:   "ss",
						},
					},
				},
				singleChangeResolver: singleChangeResolverMock{
					IsInsertError: false,
					IsUpdateError: false,
					IsDeleteError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := changeResolver{
				configuration:        tt.fields.configuration,
				singleChangeResolver: tt.fields.singleChangeResolver,
				log:                  tt.fields.log,
			}
			if err := c.ResolveDelete(tt.args.parentTablePrimaryKeyValue, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("ResolveDelete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
