package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table/tableRepository"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"github.com/sirupsen/logrus"
	"testing"
)

// Тестирование резолвинга вставки
func Test_singleChangeResolver_resolveInsert(t *testing.T) {
	type fields struct {
		tableRepository tableRepository.RepositoryInterface
		log             *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		field                      configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Если не переданы значения для вставки",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
		{
			name: "Если возникла ошибка получения локального ключа",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   true,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Если возникла ошибка вставки значений",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: true,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Ошибок не возникло",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleChangeResolver{
				tableRepository: tt.fields.tableRepository,
				log:             tt.fields.log,
			}
			if err := s.resolveInsert(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.field, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("resolveInsert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование резолвинга обноления
func Test_singleChangeResolver_resolveUpdate(t *testing.T) {
	type fields struct {
		tableRepository tableRepository.RepositoryInterface
		log             *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		field                      configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Если возникла ошибка получения локального ключа",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   true,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Если возникла ошибка удаления текущих сущностей",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: true,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Если не переданы значения для вставки",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
		{
			name: "Если возникла ошибка вставки значений",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: true,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Ошибок не возникло",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_f": {"1"},
				},
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleChangeResolver{
				tableRepository: tt.fields.tableRepository,
				log:             tt.fields.log,
			}
			if err := s.resolveUpdate(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.field, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("resolveUpdate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование резолвинга удаления
func Test_singleChangeResolver_resolveDelete(t *testing.T) {
	type fields struct {
		tableRepository tableRepository.RepositoryInterface
		log             *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		field                      configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Если возникла ошибка получения локального ключа",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   true,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Если возникла ошибка удаления текущих сущностей",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: true,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Ошибок не возникло",
			fields: fields{
				tableRepository: tableRepository.RepositoryMock{
					IsGetLocalKeyByPrimaryKeyError:   false,
					IsDeleteRelationsByLocalKeyError: false,
					IsInsertRelationsByLocalKeyError: false,
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				field: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_tbl",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleChangeResolver{
				tableRepository: tt.fields.tableRepository,
				log:             tt.fields.log,
			}
			if err := s.resolveDelete(tt.args.parentTablePrimaryKeyValue, tt.args.field, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("resolveDelete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
