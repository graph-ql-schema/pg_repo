package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table/tableRepository"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

// Резолвер обновления таблицы связи для отношения Many to Many для единичного поля
type singleChangeResolver struct {
	tableRepository tableRepository.RepositoryInterface
	log             *logrus.Entry
}

// Конструктор сервиса
func newSingleChangeResolver(
	primaryTable string,
	primaryTablePrimaryKey string,
	executor executor.QueryExecutorInterface,
) singleChangeResolverInterface {
	return &singleChangeResolver{
		tableRepository: tableRepository.NewRepository(primaryTable, primaryTablePrimaryKey, executor),
		log:             helpers.NewLogger(fmt.Sprintf(`changeResolver(%v -> %v)`, primaryTable, primaryTablePrimaryKey)),
	}
}

// Резолвинг конфликтов вставки родительской сущности
func (s singleChangeResolver) resolveInsert(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	field configuration.FieldsConfiguration,
	tx *sql.Tx,
) error {
	ids, ok := relationIdsToSetUp[field.GraphQlName]
	if !ok || 0 == len(ids) {
		return nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)

	localKey, err := s.tableRepository.GetLocalKeyByPrimaryKey(parentTablePrimaryKeyValue, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":                       500,
			"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
			"ids":                        ids,
			"field":                      field,
			"err":                        err,
		}).Error(`Failed to get local key for relation`)

		return err
	}

	err = s.tableRepository.InsertRelationsByLocalKey(localKey, ids, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":     500,
			"localKey": localKey,
			"ids":      ids,
			"field":    field.Field,
			"err":      err,
		}).Error(`Failed to insert relation`)

		return err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	s.log.WithFields(logrus.Fields{
		"code":          200,
		"localKey":      localKey,
		"ids":           ids,
		"field":         field.Field,
		"executionTime": finish - starts,
	}).Debug(`Inserted relation data for field`)

	return nil
}

// Резолвинг конфликтов обновления родительской сущности
func (s singleChangeResolver) resolveUpdate(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	field configuration.FieldsConfiguration,
	tx *sql.Tx,
) error {
	ids, ok := relationIdsToSetUp[field.GraphQlName]
	if !ok {
		return nil
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)

	localKey, err := s.tableRepository.GetLocalKeyByPrimaryKey(parentTablePrimaryKeyValue, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":                       500,
			"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
			"ids":                        ids,
			"field":                      field,
			"err":                        err,
		}).Error(`Failed to get local key for relation`)

		return err
	}

	err = s.tableRepository.DeleteRelationsByLocalKey(localKey, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":     500,
			"localKey": localKey,
			"field":    field.Field,
			"err":      err,
		}).Error(`Failed to delete relation`)

		return err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	s.log.WithFields(logrus.Fields{
		"code":          200,
		"localKey":      localKey,
		"field":         field.Field,
		"executionTime": finish - starts,
	}).Debug(`Deleted relation data for field`)

	if 0 == len(ids) {
		return nil
	}

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	err = s.tableRepository.InsertRelationsByLocalKey(localKey, ids, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":     500,
			"localKey": localKey,
			"ids":      ids,
			"field":    field.Field,
			"err":      err,
		}).Error(`Failed to insert relation`)

		return err
	}

	finish = time.Now().UnixNano() / int64(time.Millisecond)
	s.log.WithFields(logrus.Fields{
		"code":          200,
		"localKey":      localKey,
		"ids":           ids,
		"field":         field.Field,
		"executionTime": finish - starts,
	}).Debug(`Inserted relation data for field`)

	return nil
}

// Резолвинг конфликтов удаления родительской сущности
func (s singleChangeResolver) resolveDelete(
	parentTablePrimaryKeyValue string,
	field configuration.FieldsConfiguration,
	tx *sql.Tx,
) error {
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	localKey, err := s.tableRepository.GetLocalKeyByPrimaryKey(parentTablePrimaryKeyValue, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":                       500,
			"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
			"field":                      field,
			"err":                        err,
		}).Error(`Failed to get local key for relation`)

		return err
	}

	err = s.tableRepository.DeleteRelationsByLocalKey(localKey, field, tx)
	if nil != err {
		s.log.WithFields(logrus.Fields{
			"code":     500,
			"localKey": localKey,
			"field":    field.Field,
			"err":      err,
		}).Error(`Failed to delete relation`)

		return err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	s.log.WithFields(logrus.Fields{
		"code":          200,
		"localKey":      localKey,
		"field":         field.Field,
		"executionTime": finish - starts,
	}).Debug(`Deleted relation data for field`)

	return nil
}
