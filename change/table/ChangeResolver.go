package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

// Резолвер обновления таблицы связи для отношения Many to Many
type changeResolver struct {
	configuration        []configuration.FieldsConfiguration
	singleChangeResolver singleChangeResolverInterface
	log                  *logrus.Entry
}

// Конструктор сервиса
func NewChangeResolver(
	configuration []configuration.FieldsConfiguration,
	primaryTable string,
	primaryTablePrimaryKey string,
	executor executor.QueryExecutorInterface,
) ChangeResolverInterface {
	return &changeResolver{
		configuration:        configuration,
		singleChangeResolver: newSingleChangeResolver(primaryTable, primaryTablePrimaryKey, executor),
		log:                  helpers.NewLogger(fmt.Sprintf(`changeResolver(%v -> %v)`, primaryTable, primaryTablePrimaryKey)),
	}
}

// Резолвинг конфликтов вставки родительской сущности
func (c changeResolver) ResolveInsert(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	tx *sql.Tx,
) error {
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	for _, field := range c.configuration {
		if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); !ok {
			continue
		}

		err := c.singleChangeResolver.resolveInsert(parentTablePrimaryKeyValue, relationIdsToSetUp, field, tx)
		if nil != err {
			return err
		}
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.log.WithFields(logrus.Fields{
		"code":                       200,
		"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
		"relationIdsToSetUp":         relationIdsToSetUp,
		"executionTime":              finish - starts,
	}).Debug(`Inserted relation data`)

	return nil
}

// Резолвинг конфликтов обновления родительской сущности
func (c changeResolver) ResolveUpdate(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	tx *sql.Tx,
) error {
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	for _, field := range c.configuration {
		if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); !ok {
			continue
		}

		err := c.singleChangeResolver.resolveUpdate(parentTablePrimaryKeyValue, relationIdsToSetUp, field, tx)
		if nil != err {
			return err
		}
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.log.WithFields(logrus.Fields{
		"code":                       200,
		"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
		"relationIdsToSetUp":         relationIdsToSetUp,
		"executionTime":              finish - starts,
	}).Debug(`Updated relation data`)

	return nil
}

// Резолвинг конфликтов удаления родительской сущности
func (c changeResolver) ResolveDelete(
	parentTablePrimaryKeyValue string,
	tx *sql.Tx,
) error {
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	for _, field := range c.configuration {
		if _, ok := field.DbConfiguration.(configuration.TableFieldConfiguration); !ok {
			continue
		}

		err := c.singleChangeResolver.resolveDelete(parentTablePrimaryKeyValue, field, tx)
		if nil != err {
			return err
		}
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.log.WithFields(logrus.Fields{
		"code":                       200,
		"parentTablePrimaryKeyValue": parentTablePrimaryKeyValue,
		"executionTime":              finish - starts,
	}).Debug(`Deleted relation data`)

	return nil
}
