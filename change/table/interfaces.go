package table

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
)

// Резолвер обновления таблицы связи для отношения Many to Many
type ChangeResolverInterface interface {
	// Резолвинг конфликтов вставки родительской сущности
	ResolveInsert(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		tx *sql.Tx,
	) error

	// Резолвинг конфликтов обновления родительской сущности
	ResolveUpdate(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		tx *sql.Tx,
	) error

	// Резолвинг конфликтов удаления родительской сущности
	ResolveDelete(
		parentTablePrimaryKeyValue string,
		tx *sql.Tx,
	) error
}

// Резолвер обновления таблицы связи для отношения Many to Many для единичного поля
type singleChangeResolverInterface interface {
	// Резолвинг конфликтов вставки родительской сущности
	resolveInsert(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		field configuration.FieldsConfiguration,
		tx *sql.Tx,
	) error

	// Резолвинг конфликтов обновления родительской сущности
	resolveUpdate(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		field configuration.FieldsConfiguration,
		tx *sql.Tx,
	) error

	// Резолвинг конфликтов удаления родительской сущности
	resolveDelete(
		parentTablePrimaryKeyValue string,
		field configuration.FieldsConfiguration,
		tx *sql.Tx,
	) error
}
