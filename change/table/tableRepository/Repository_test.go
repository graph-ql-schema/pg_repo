package tableRepository

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table/tableRepository/sqlGenerator"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"testing"
)

// Тестирование получения локального ключа
func Test_repository_GetLocalKeyByPrimaryKey(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		executor               executor.QueryExecutorInterface
		generator              sqlGenerator.TableChangeSqlGeneratorInterface
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Локальный ключ совпадает с первичным",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"uuid"},
						ItemsToSet: []interface{}{
							2,
						},
					},
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "id",
					},
				},
				tx: nil,
			},
			want:    "1",
			wantErr: false,
		},
		{
			name: "Ошибка исполнения запроса получения локального ключа",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"uuid"},
						ItemsToSet: []interface{}{
							2,
						},
					},
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "Ошибка сканирования результата получения локального ключа",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     true,
						ColumnsPull: []string{"uuid"},
						ItemsToSet: []interface{}{
							2,
						},
					},
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "В результатах сканирования вернулся null",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"uuid"},
						ItemsToSet: []interface{}{
							nil,
						},
					},
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "В результатах сканирования вернулся корректный результат",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"uuid"},
						ItemsToSet: []interface{}{
							2,
						},
					},
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			want:    "2",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := repository{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				executor:               tt.fields.executor,
				generator:              tt.fields.generator,
			}
			got, err := r.GetLocalKeyByPrimaryKey(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLocalKeyByPrimaryKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetLocalKeyByPrimaryKey() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование удаления отношений по локальному ключу
func Test_repository_DeleteRelationsByLocalKey(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		executor               executor.QueryExecutorInterface
		generator              sqlGenerator.TableChangeSqlGeneratorInterface
	}
	type args struct {
		localKey    string
		fieldConfig configuration.FieldsConfiguration
		tx          *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Ошибка исполнения запроса",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: true,
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				localKey: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Нет ошибки исполнения запроса",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				localKey: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := repository{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				executor:               tt.fields.executor,
				generator:              tt.fields.generator,
			}
			if err := r.DeleteRelationsByLocalKey(tt.args.localKey, tt.args.fieldConfig, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("DeleteRelationsByLocalKey() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование вставки значений отношений
func Test_repository_InsertRelationsByLocalKey(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		executor               executor.QueryExecutorInterface
		generator              sqlGenerator.TableChangeSqlGeneratorInterface
	}
	type args struct {
		localKey    string
		relationIds []string
		fieldConfig configuration.FieldsConfiguration
		tx          *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Не переданы ID для вставки",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				localKey:    "1",
				relationIds: []string{},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
		{
			name: "Ошибка исполнения запроса",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: true,
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				localKey:    "1",
				relationIds: []string{"1"},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			wantErr: true,
		},
		{
			name: "Нет ошибки исполнения запроса",
			fields: fields{
				primaryTable:           "user",
				primaryTablePrimaryKey: "id",
				executor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				generator: sqlGenerator.TableChangeSqlGeneratorMock{
					GenerateLocalKeySelectQueryResult: "test",
					GenerateDeleteQueryResult:         "test",
					GenerateInsertQueryResult:         "test",
				},
			},
			args: args{
				localKey:    "1",
				relationIds: []string{"1"},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "test_db",
						Target:     "phone_id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
					},
				},
				tx: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := repository{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				executor:               tt.fields.executor,
				generator:              tt.fields.generator,
			}
			if err := r.InsertRelationsByLocalKey(tt.args.localKey, tt.args.relationIds, tt.args.fieldConfig, tt.args.tx); (err != nil) != tt.wantErr {
				t.Errorf("InsertRelationsByLocalKey() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
