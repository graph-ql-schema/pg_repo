package tableRepository

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table/tableRepository/sqlGenerator"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"fmt"
)

// Репозиторий таблицы связи отношения Many to Many
type repository struct {
	primaryTable           string
	primaryTablePrimaryKey string
	executor               executor.QueryExecutorInterface
	generator              sqlGenerator.TableChangeSqlGeneratorInterface
}

// Конструктор репозитория
func NewRepository(
	primaryTable string,
	primaryTablePrimaryKey string,
	executor executor.QueryExecutorInterface,
) RepositoryInterface {
	return &repository{
		primaryTable:           primaryTable,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		executor:               executor,
		generator:              sqlGenerator.NewTableChangeSqlGenerator(primaryTable, primaryTablePrimaryKey),
	}
}

// Получение локального ключа по первичному
func (r repository) GetLocalKeyByPrimaryKey(
	parentTablePrimaryKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) (string, error) {
	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	if r.primaryTablePrimaryKey == dbConfig.LocalKey {
		return parentTablePrimaryKeyValue, nil
	}

	sqlQuery := r.generator.GenerateLocalKeySelectQuery(parentTablePrimaryKeyValue, fieldConfig)
	rows, err := r.executor.ExecuteListQuery(sqlQuery, nil, tx)
	if nil != err {
		return "", fmt.Errorf(
			`failed to get local key for '%v' -> '%v: %v': %v`,
			r.primaryTable,
			r.primaryTablePrimaryKey,
			parentTablePrimaryKeyValue,
			err.Error(),
		)
	}

	id := ""
	defer rows.Close()
	for rows.Next() {
		var baseIdValue interface{}
		err := rows.Scan(&baseIdValue)
		if nil != err {
			return "", fmt.Errorf(
				`failed to get local key for '%v' -> '%v: %v': %v`,
				r.primaryTable,
				r.primaryTablePrimaryKey,
				parentTablePrimaryKeyValue,
				err.Error(),
			)
		}

		if nil != baseIdValue {
			id = fmt.Sprintf(`%v`, baseIdValue)
		}
	}

	return id, nil
}

// Удаление значений отношения по переданному локальному ключу
func (r repository) DeleteRelationsByLocalKey(
	localKey string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) error {
	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	sqlQuery := r.generator.GenerateDeleteQuery(localKey, fieldConfig)
	err := r.executor.ExecuteChangeQuery(sqlQuery, nil, tx)
	if nil != err {
		return fmt.Errorf(
			`failed to delete relations by local key for '%v' -> '%v: %v' -> relation: '%v': %v`,
			r.primaryTable,
			dbConfig.LocalKey,
			localKey,
			dbConfig.Table,
			err.Error(),
		)
	}

	return nil
}

// Вставка значений отношений по переднному локальному ключу и целевым ID
func (r repository) InsertRelationsByLocalKey(
	localKey string,
	relationIds []string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) error {
	if 0 == len(relationIds) {
		return nil
	}

	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	sqlQuery := r.generator.GenerateInsertQuery(localKey, relationIds, fieldConfig)
	err := r.executor.ExecuteChangeQuery(sqlQuery, nil, tx)
	if nil != err {
		return fmt.Errorf(
			`failed to insert relations by local key for '%v' -> '%v: %v' -> relation: '%v': %v`,
			r.primaryTable,
			dbConfig.LocalKey,
			localKey,
			dbConfig.Table,
			err.Error(),
		)
	}

	return nil
}
