package sqlGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type TableChangeSqlGeneratorMock struct {
	GenerateLocalKeySelectQueryResult string
	GenerateDeleteQueryResult         string
	GenerateInsertQueryResult         string
}

// Генерация запроса получения значений локального ключа для поля
func (t TableChangeSqlGeneratorMock) GenerateLocalKeySelectQuery(string, configuration.FieldsConfiguration) string {
	return t.GenerateLocalKeySelectQueryResult
}

// Генерация запроса удаления текущих дочерних сущностей для родительской сущности.
func (t TableChangeSqlGeneratorMock) GenerateDeleteQuery(string, configuration.FieldsConfiguration) string {
	return t.GenerateDeleteQueryResult
}

// Генерация запроса вставки значений дочерних сущностей для родительской сущности.
func (t TableChangeSqlGeneratorMock) GenerateInsertQuery(string, []string, configuration.FieldsConfiguration) string {
	return t.GenerateInsertQueryResult
}
