package sqlGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"testing"
)

// Тестирование генерации
func Test_tableChangeSqlGenerator_GenerateLocalKeySelectQuery(t1 *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Когда локальный ключ совпадает с первичным",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "test_t",
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt",
						Target:     "uuid",
						ForeignKey: "user_id",
						LocalKey:   "test_t",
					},
				},
			},
			want: "",
		},
		{
			name: "Когда локальный ключ не совпадает с первичным",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt",
						Target:     "uuid",
						ForeignKey: "user_id",
						LocalKey:   "test_t",
					},
				},
			},
			want: "select user_id_local_key.test_t from test user_id_local_key where user_id_local_key.id = 1",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableChangeSqlGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := t.GenerateLocalKeySelectQuery(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig); got != tt.want {
				t1.Errorf("GenerateLocalKeySelectQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса удаления
func Test_tableChangeSqlGenerator_GenerateDeleteQuery(t1 *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
	}
	type args struct {
		parentTableLocalKeyValue string
		fieldConfig              configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации запроса удаления",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt",
						Target:     "uuid",
						ForeignKey: "user_id",
						LocalKey:   "test_t",
					},
				},
			},
			want: "delete from ttt user_id_delete where user_id_delete.user_id = 1",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableChangeSqlGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := t.GenerateDeleteQuery(tt.args.parentTableLocalKeyValue, tt.args.fieldConfig); got != tt.want {
				t1.Errorf("GenerateDeleteQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запросов вставки
func Test_tableChangeSqlGenerator_GenerateInsertQuery(t1 *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
	}
	type args struct {
		parentTableLocalKeyValue string
		relationIds              []string
		fieldConfig              configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Когда не переданы ID",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				parentTableLocalKeyValue: "1",
				relationIds:              []string{},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt",
						Target:     "uuid",
						ForeignKey: "user_id",
						LocalKey:   "test_t",
					},
				},
			},
			want: "",
		},
		{
			name: "Переданы корректные данные",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
			},
			args: args{
				parentTableLocalKeyValue: "1",
				relationIds:              []string{"1", "2"},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "ttt",
						Target:     "uuid",
						ForeignKey: "user_id",
						LocalKey:   "test_t",
					},
				},
			},
			want: "insert into ttt(uuid, user_id) values (1, 1), (2, 1)",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableChangeSqlGenerator{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := t.GenerateInsertQuery(tt.args.parentTableLocalKeyValue, tt.args.relationIds, tt.args.fieldConfig); got != tt.want {
				t1.Errorf("GenerateInsertQuery() = %v, want %v", got, tt.want)
			}
		})
	}
}
