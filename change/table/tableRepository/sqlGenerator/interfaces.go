package sqlGenerator

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Генератор запросов изменения таблицы связи для отношения Many to Many
type TableChangeSqlGeneratorInterface interface {
	// Генерация запроса получения значений локального ключа для поля
	GenerateLocalKeySelectQuery(
		parentTablePrimaryKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
	) string

	// Генерация запроса удаления текущих дочерних сущностей для родительской сущности.
	GenerateDeleteQuery(
		parentTableLocalKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
	) string

	// Генерация запроса вставки значений дочерних сущностей для родительской сущности.
	GenerateInsertQuery(
		parentTableLocalKeyValue string,
		relationIds []string,
		fieldConfig configuration.FieldsConfiguration,
	) string
}
