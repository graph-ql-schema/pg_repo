package sqlGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"fmt"
	"strings"
)

// Генератор запросов изменения таблицы связи для отношения Many to Many
type tableChangeSqlGenerator struct {
	primaryTable           string
	primaryTablePrimaryKey string
}

// Конструктор сервиса
func NewTableChangeSqlGenerator(primaryTable string, primaryTablePrimaryKey string) TableChangeSqlGeneratorInterface {
	return &tableChangeSqlGenerator{
		primaryTable:           primaryTable,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}

// Генерация запроса получения значений локального ключа для поля
func (t tableChangeSqlGenerator) GenerateLocalKeySelectQuery(
	parentTablePrimaryKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
) string {
	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	if t.primaryTablePrimaryKey == dbConfig.LocalKey {
		return ""
	}

	alias := fmt.Sprintf(`%v_local_key`, dbConfig.ForeignKey)
	return fmt.Sprintf(
		`select %v.%v from %v %v where %v.%v = %v`,
		alias,
		dbConfig.LocalKey,
		t.primaryTable,
		alias,
		alias,
		t.primaryTablePrimaryKey,
		parentTablePrimaryKeyValue,
	)
}

// Генерация запроса удаления текущих дочерних сущностей для родительской сущности.
func (t tableChangeSqlGenerator) GenerateDeleteQuery(
	parentTableLocalKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
) string {
	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	alias := fmt.Sprintf(`%v_delete`, dbConfig.ForeignKey)

	return fmt.Sprintf(
		`delete from %v %v where %v.%v = %v`,
		dbConfig.Table,
		alias,
		alias,
		dbConfig.ForeignKey,
		parentTableLocalKeyValue,
	)
}

// Генерация запроса вставки значений дочерних сущностей для родительской сущности.
func (t tableChangeSqlGenerator) GenerateInsertQuery(
	parentTableLocalKeyValue string,
	relationIds []string,
	fieldConfig configuration.FieldsConfiguration,
) string {
	if 0 == len(relationIds) {
		return ""
	}

	dbConfig := fieldConfig.DbConfiguration.(configuration.TableFieldConfiguration)
	insertions := []string{}

	for _, id := range relationIds {
		insertions = append(insertions, fmt.Sprintf(`(%v, %v)`, id, parentTableLocalKeyValue))
	}

	return fmt.Sprintf(
		`insert into %v(%v, %v) values %v`,
		dbConfig.Table,
		dbConfig.Target,
		dbConfig.ForeignKey,
		strings.Join(insertions, `, `),
	)
}
