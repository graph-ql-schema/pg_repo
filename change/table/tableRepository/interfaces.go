package tableRepository

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
)

// Репозиторий таблицы связи отношения Many to Many
type RepositoryInterface interface {
	// Получение локального ключа по первичному
	GetLocalKeyByPrimaryKey(
		parentTablePrimaryKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
		tx *sql.Tx,
	) (string, error)

	// Удаление значений отношения по переданному локальному ключу
	DeleteRelationsByLocalKey(
		localKey string,
		fieldConfig configuration.FieldsConfiguration,
		tx *sql.Tx,
	) error

	// Вставка значений отношений по переднному локальному ключу и целевым ID
	InsertRelationsByLocalKey(
		localKey string,
		relationIds []string,
		fieldConfig configuration.FieldsConfiguration,
		tx *sql.Tx,
	) error
}
