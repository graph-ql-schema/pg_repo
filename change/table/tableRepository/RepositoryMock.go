package tableRepository

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type RepositoryMock struct {
	IsGetLocalKeyByPrimaryKeyError   bool
	IsDeleteRelationsByLocalKeyError bool
	IsInsertRelationsByLocalKeyError bool
}

// Получение локального ключа по первичному
func (r RepositoryMock) GetLocalKeyByPrimaryKey(
	parentTablePrimaryKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) (string, error) {
	if r.IsGetLocalKeyByPrimaryKeyError {
		return "", fmt.Errorf(`test`)
	}

	return parentTablePrimaryKeyValue, nil
}

// Удаление значений отношения по переданному локальному ключу
func (r RepositoryMock) DeleteRelationsByLocalKey(string, configuration.FieldsConfiguration, *sql.Tx) error {
	if r.IsDeleteRelationsByLocalKeyError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Вставка значений отношений по переднному локальному ключу и целевым ID
func (r RepositoryMock) InsertRelationsByLocalKey(string, []string, configuration.FieldsConfiguration, *sql.Tx) error {
	if r.IsInsertRelationsByLocalKeyError {
		return fmt.Errorf(`test`)
	}

	return nil
}
