package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/policies"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/rowsLoader"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"fmt"
)

// Системный резолвер конфлитков для одиночного поля
type singleFieldResolver struct {
	queryExecutor      executor.QueryExecutorInterface
	rowsLoader         rowsLoader.LoaderInterface
	dropPolicyResolver policies.PolicyResolverInterface
	freePolicyResolver policies.PolicyResolverInterface
	busyPolicyResolver policies.PolicyResolverInterface
}

// Конструктор резолвера
func newSingleFieldResolver(
	queryExecutor executor.QueryExecutorInterface,
	primaryTablePrimaryKey string,
	primaryTable string,
) singleFieldResolverInterface {
	return &singleFieldResolver{
		queryExecutor:      queryExecutor,
		rowsLoader:         rowsLoader.NewLoader(queryExecutor, primaryTablePrimaryKey, primaryTable),
		dropPolicyResolver: policies.NewDropPolicyResolver(),
		freePolicyResolver: policies.NewFreePolicyResolver(),
		busyPolicyResolver: policies.NewBusyPolicyResolver(),
	}
}

// Резолвинг конфликтов вставки родительской сущности
//
// Логика работы резолвера:
//  1. Необходимо получить сущности, которые будут привязаны к родительской
//  2. Резолвим запросы для полученных сущностей:
//     2.1 Для политики Skip мы пропускаем запрос на обновление, если дочерняя сущность уже занята, остальные обновляем
//     2.2 Для политики Replace мы обновляем все дочерние сущности
//  3. Выполняем полученные запросы
//  4. Возвращаем возникшие события изменений
func (s singleFieldResolver) resolveInsert(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp []string,
	fieldConfiguration configuration.FieldsConfiguration,
	tx *sql.Tx,
) ([]events.Event, error) {
	dbConf := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)

	relationRows, err := s.rowsLoader.LoadRowsByRelationPrimaryKeys(relationIdsToSetUp, fieldConfiguration, tx)
	if nil != err {
		return nil, fmt.Errorf(`insert '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	queries, eventsResult := s.busyPolicyResolver.Resolve(parentTablePrimaryKeyValue, fieldConfiguration, relationRows)
	for _, sqlQuery := range queries {
		err := s.queryExecutor.ExecuteChangeQuery(sqlQuery, nil, tx)
		if nil != err {
			return nil, fmt.Errorf(`insert '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
		}
	}

	return eventsResult, nil
}

// Резолвинг конфликтов обновления родительской сущности
//
// Логика работы резолвера:
//  1. Необходимо получить уже привязанные сущности (Старые)
//  2. Необходимо получить те сущности, что хотим привязать (по переданным первичным ключам) (Новые)
//  3. Получаем список сущностей, которые необходимо отвязать, для этого проверяем каждую старую сущность, есть
//     ли ее ID в списке новых сущностей. Если ее нет в списке, необходимо обработать ее по политике Free.
//  4. Резолвим запросы для политики Free
//  5. Резолвим запросы для политики Busy (передаем все новые сущности)
//  6. Выполняем все полученные запросы
//  7. Возвращаем все возникшие события
func (s singleFieldResolver) resolveUpdate(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp []string,
	fieldConfiguration configuration.FieldsConfiguration,
	tx *sql.Tx,
) ([]events.Event, error) {
	dbConf := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)

	newRows, err := s.rowsLoader.LoadRowsByRelationPrimaryKeys(relationIdsToSetUp, fieldConfiguration, tx)
	if nil != err {
		return nil, fmt.Errorf(`update '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	oldRows, err := s.rowsLoader.LoadRowsByParentPrimaryKey(parentTablePrimaryKeyValue, fieldConfiguration, tx)
	if nil != err {
		return nil, fmt.Errorf(`update '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	rowsToFree := s.getRowsToFree(oldRows, newRows, dbConf)

	queriesFreeRows, eventsFreeRowsResult := s.freePolicyResolver.Resolve(parentTablePrimaryKeyValue, fieldConfiguration, rowsToFree)
	queriesNewRows, eventsNewRowsResult := s.busyPolicyResolver.Resolve(parentTablePrimaryKeyValue, fieldConfiguration, newRows)

	err = s.executeUpdateQueries(queriesFreeRows, tx, dbConf)
	if nil != err {
		return nil, err
	}

	err = s.executeUpdateQueries(queriesNewRows, tx, dbConf)
	if nil != err {
		return nil, err
	}

	result := eventsFreeRowsResult
	for _, event := range eventsNewRowsResult {
		result = append(result, event)
	}

	return result, nil
}

// Выполняем запросы обновления
func (s singleFieldResolver) executeUpdateQueries(
	queries []string,
	tx *sql.Tx,
	dbConfig configuration.RelationFieldConfiguration,
) error {
	for _, sqlQuery := range queries {
		err := s.queryExecutor.ExecuteChangeQuery(sqlQuery, nil, tx)
		if nil != err {
			return fmt.Errorf(`update '%v -> %v' resolve error: %v`, dbConfig.Table, dbConfig.PrimaryKey, err.Error())
		}
	}

	return nil
}

// Получает список сущностей, которые необходимо отвязать.
//
// Проверяет каждую старую сущность, есть ли ее ID в списке новых сущностей. Если ее нет в списке,
// необходимо обработать ее по политике Free.
func (s singleFieldResolver) getRowsToFree(
	oldRows []map[string]string,
	newRows []map[string]string,
	dbConfig configuration.RelationFieldConfiguration,
) []map[string]string {
	rowsToFree := []map[string]string{}
	for _, oldRow := range oldRows {
		oldRowId, ok := oldRow[dbConfig.PrimaryKey]
		if !ok {
			continue
		}

		// Ищем уже привязанную сущность среди тех, которые надо привязать.
		// Если она присутствует, то пропускаем ее.
		isAvailable := false
		for _, newRow := range newRows {
			newRowId, ok := newRow[dbConfig.PrimaryKey]
			if !ok {
				continue
			}

			if newRowId == oldRowId {
				isAvailable = true
			}
		}

		// Если сущность не найдена, то добавляем ее в результат для дальнейшего освобождения.
		if !isAvailable {
			rowsToFree = append(rowsToFree, oldRow)
		}
	}

	return rowsToFree
}

// Резолвинг конфликтов удаления родительской сущности
//
// Логика работы резолвера:
//  1. Необходимо получить сущности, которые были привязаны к родительской
//  2. Резолвим запросы для полученных сущностей по политике Drop
//  3. Выполняем полученные запросы
//  4. Возвращаем возникшие события изменений
func (s singleFieldResolver) resolveDelete(
	parentTablePrimaryKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	tx *sql.Tx,
) ([]events.Event, error) {
	dbConf := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)

	relationRows, err := s.rowsLoader.LoadRowsByParentPrimaryKey(parentTablePrimaryKeyValue, fieldConfiguration, tx)
	if nil != err {
		return nil, fmt.Errorf(`delete '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	queries, eventsResult := s.dropPolicyResolver.Resolve(parentTablePrimaryKeyValue, fieldConfiguration, relationRows)
	for _, sqlQuery := range queries {
		err := s.queryExecutor.ExecuteChangeQuery(sqlQuery, nil, tx)
		if nil != err {
			return nil, fmt.Errorf(`delete '%v -> %v' resolve error: %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
		}
	}

	return eventsResult, nil
}
