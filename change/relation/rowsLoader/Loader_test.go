package rowsLoader

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/rowsLoader/query"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"reflect"
	"testing"
)

// Тестирование загрузки строк дочерних сущностей
func Test_loader_LoadRowsByParentPrimaryKey(t *testing.T) {
	type fields struct {
		queryExecutor  executor.QueryExecutorInterface
		queryGenerator query.GeneratorInterface
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []map[string]string
		wantErr bool
	}{
		{
			name: "Если исполнитель вернул ошибку",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult:  nil,
				},
				queryGenerator: query.GeneratorMock{
					Result: "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "123",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_t",
						PrimaryKey: "tt",
						ForeignKey: "ttt",
						LocalKey:   "asd",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Запрос выполнился корректно",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       3,
						IsError:     false,
						ColumnsPull: []string{"test", "test_t"},
					},
				},
				queryGenerator: query.GeneratorMock{
					Result: "test",
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "123",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_t",
						PrimaryKey: "tt",
						ForeignKey: "ttt",
						LocalKey:   "asd",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want: []map[string]string{
				{
					"test":   "",
					"test_t": "",
				},
				{
					"test":   "",
					"test_t": "",
				},
				{
					"test":   "",
					"test_t": "",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loader{
				queryExecutor:  tt.fields.queryExecutor,
				queryGenerator: tt.fields.queryGenerator,
			}
			got, err := l.LoadRowsByParentPrimaryKey(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadRowsByParentPrimaryKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadRowsByParentPrimaryKey() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование парсинга данных ответа от сервера
func Test_loader_parseData(t *testing.T) {
	type fields struct {
		queryExecutor  executor.QueryExecutorInterface
		queryGenerator query.GeneratorInterface
	}
	type args struct {
		data map[string]interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   map[string]string
	}{
		{
			name:   "Парсинг null значений",
			fields: fields{},
			args: args{
				data: map[string]interface{}{
					"test": nil,
				},
			},
			want: map[string]string{
				"test": "",
			},
		},
		{
			name:   "Парсинг не null значений",
			fields: fields{},
			args: args{
				data: map[string]interface{}{
					"test": 123,
				},
			},
			want: map[string]string{
				"test": "123",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loader{
				queryExecutor:  tt.fields.queryExecutor,
				queryGenerator: tt.fields.queryGenerator,
			}
			if got := l.parseData(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseData() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения строк дочерних сущностей по переданным первичным ключам
func Test_loader_LoadRowsByRelationPrimaryKeys(t *testing.T) {
	type fields struct {
		queryExecutor  executor.QueryExecutorInterface
		queryGenerator query.GeneratorInterface
	}
	type args struct {
		relationPrimaryKeys []string
		fieldConfig         configuration.FieldsConfiguration
		tx                  *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []map[string]string
		wantErr bool
	}{
		{
			name: "Если исполнитель вернул ошибку",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult:  nil,
				},
				queryGenerator: query.GeneratorMock{
					Result: "test",
				},
			},
			args: args{
				relationPrimaryKeys: []string{"1", "2"},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_t",
						PrimaryKey: "tt",
						ForeignKey: "ttt",
						LocalKey:   "asd",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Запрос выполнился корректно",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       3,
						IsError:     false,
						ColumnsPull: []string{"test", "test_t"},
					},
				},
				queryGenerator: query.GeneratorMock{
					Result: "test",
				},
			},
			args: args{
				relationPrimaryKeys: []string{"1", "2"},
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_t",
						PrimaryKey: "tt",
						ForeignKey: "ttt",
						LocalKey:   "asd",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want: []map[string]string{
				{
					"test":   "",
					"test_t": "",
				},
				{
					"test":   "",
					"test_t": "",
				},
				{
					"test":   "",
					"test_t": "",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loader{
				queryExecutor:  tt.fields.queryExecutor,
				queryGenerator: tt.fields.queryGenerator,
			}
			got, err := l.LoadRowsByRelationPrimaryKeys(tt.args.relationPrimaryKeys, tt.args.fieldConfig, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadRowsByRelationPrimaryKeys() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadRowsByRelationPrimaryKeys() got = %v, want %v", got, tt.want)
			}
		})
	}
}
