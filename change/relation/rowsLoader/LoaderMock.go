package rowsLoader

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type LoaderMock struct {
	LoadRowsByParentPrimaryKeyError     bool
	LoadRowsByRelationPrimaryKeysError  bool
	LoadRowsByParentPrimaryKeyResult    []map[string]string
	LoadRowsByRelationPrimaryKeysResult []map[string]string
}

// Загрузка строк дочерней сущности по первичному ключу родительской таблицы
func (l LoaderMock) LoadRowsByParentPrimaryKey(string, configuration.FieldsConfiguration, *sql.Tx) ([]map[string]string, error) {
	if l.LoadRowsByParentPrimaryKeyError {
		return nil, fmt.Errorf(`test`)
	}

	return l.LoadRowsByParentPrimaryKeyResult, nil
}

// Загрузка строк дочерних сущностей по первичному ключу дочерней сущности
func (l LoaderMock) LoadRowsByRelationPrimaryKeys([]string, configuration.FieldsConfiguration, *sql.Tx) ([]map[string]string, error) {
	if l.LoadRowsByRelationPrimaryKeysError {
		return nil, fmt.Errorf(`test`)
	}

	return l.LoadRowsByRelationPrimaryKeysResult, nil
}
