package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"testing"
)

// Проверка доступности процессора
func Test_primaryKeyProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTablePrimaryKey string
	}
	type args struct {
		fieldConfig configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Когда первичный ключ не совпадает с локальным",
			fields: fields{
				primaryTablePrimaryKey: "id",
			},
			args: args{
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: false,
		},
		{
			name: "Когда первичный ключ совпадает с локальным",
			fields: fields{
				primaryTablePrimaryKey: "id",
			},
			args: args{
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "id",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := primaryKeyProcessor{
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := p.isAvailable(tt.args.fieldConfig); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации запроса
func Test_primaryKeyProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTablePrimaryKey string
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации запроса",
			fields: fields{
				primaryTablePrimaryKey: "id",
			},
			args: args{
				parentTablePrimaryKeyValue: "110",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "id",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: "select uuid4_load.uuid4, uuid4_load.user_id from test_db uuid4_load where uuid4_load.user_id = 110",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := primaryKeyProcessor{
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
			}
			if got := p.generate(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig); got != tt.want {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
