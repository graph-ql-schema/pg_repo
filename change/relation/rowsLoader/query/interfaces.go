package query

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Генератор SQL запросов для загрузки дочерних сущностей
type GeneratorInterface interface {
	// Генерация запроса для загрузки строк дочерних сущнсотей для резолвера конфликтов
	GenerateQueryForLoadRowsByParentPrimaryKey(
		parentTablePrimaryKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
	) string
}

// Процессор генерации SQL запросов для загрузки дочерних сущностей
type generatorProcessor interface {
	// Проверка доступности процессора
	isAvailable(fieldConfig configuration.FieldsConfiguration) bool

	// Генерация запроса
	generate(
		parentTablePrimaryKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
	) string
}
