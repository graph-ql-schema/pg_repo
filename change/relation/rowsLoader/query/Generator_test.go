package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"testing"
)

// Тестирование генерации запросов
func Test_generator_GenerateQueryForLoadRowsByParentPrimaryKey(t *testing.T) {
	type fields struct {
		processors []generatorProcessor
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Если есть доступный процессор",
			fields: fields{
				processors: []generatorProcessor{
					generatorProcessorMock{
						IsAvailable: true,
						Result:      "test",
					},
				},
			},
			args: args{},
			want: "test",
		},
		{
			name: "Если есть доступный процессор",
			fields: fields{
				processors: []generatorProcessor{
					generatorProcessorMock{
						IsAvailable: false,
						Result:      "test",
					},
				},
			},
			args: args{},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			g := generator{
				processors: tt.fields.processors,
			}
			if got := g.GenerateQueryForLoadRowsByParentPrimaryKey(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig); got != tt.want {
				t.Errorf("GenerateQueryForLoadRowsByParentPrimaryKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
