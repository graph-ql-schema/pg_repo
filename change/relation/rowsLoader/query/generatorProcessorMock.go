package query

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Подставка для тестирования
type generatorProcessorMock struct {
	IsAvailable bool
	Result      string
}

// Проверка доступности процессора
func (g generatorProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return g.IsAvailable
}

// Генерация запроса
func (g generatorProcessorMock) generate(string, configuration.FieldsConfiguration) string {
	return g.Result
}
