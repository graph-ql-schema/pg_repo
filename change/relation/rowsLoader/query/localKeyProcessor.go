package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"fmt"
)

// Процессор генерации запросов, когда первичный ключ основной таблицы не является локальным ключем для отношения
type localKeyProcessor struct {
	primaryTablePrimaryKey string
	primaryTable           string
}

// Конструктор процессора
func newLocalKeyProcessor(primaryTablePrimaryKey string, primaryTable string) generatorProcessor {
	return &localKeyProcessor{primaryTablePrimaryKey: primaryTablePrimaryKey, primaryTable: primaryTable}
}

// Проверка доступности процесстоа
func (l localKeyProcessor) isAvailable(fieldConfig configuration.FieldsConfiguration) bool {
	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)

	return dbConf.LocalKey != l.primaryTablePrimaryKey
}

// Генерация запроса
func (l localKeyProcessor) generate(parentTablePrimaryKeyValue string, fieldConfig configuration.FieldsConfiguration) string {
	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)
	alias := fmt.Sprintf(`%v_load`, dbConf.PrimaryKey)
	primaryTableAlias := fmt.Sprintf(`%v_load_primary`, dbConf.PrimaryKey)

	return fmt.Sprintf(
		`select %v.%v, %v.%v from %v %v where %v.%v in (select %v.%v from %v %v where %v.%v = %v)`,
		alias,
		dbConf.PrimaryKey,
		alias,
		dbConf.ForeignKey,
		dbConf.Table,
		alias,
		alias,
		dbConf.ForeignKey,
		primaryTableAlias,
		dbConf.LocalKey,
		l.primaryTable,
		primaryTableAlias,
		primaryTableAlias,
		l.primaryTablePrimaryKey,
		parentTablePrimaryKeyValue,
	)
}
