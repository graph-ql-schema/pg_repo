package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type GeneratorMock struct {
	Result string
}

// Генерация запроса для загрузки строк дочерних сущнсотей для резолвера конфликтов
func (g GeneratorMock) GenerateQueryForLoadRowsByParentPrimaryKey(string, configuration.FieldsConfiguration) string {
	return g.Result
}
