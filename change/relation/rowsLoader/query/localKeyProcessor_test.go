package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"testing"
)

// Проверка доступности процессора
func Test_localKeyProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTablePrimaryKey string
		primaryTable           string
	}
	type args struct {
		fieldConfig configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Когда первичный ключ не совпадает с локальным",
			fields: fields{
				primaryTablePrimaryKey: "id",
				primaryTable:           "user",
			},
			args: args{
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: true,
		},
		{
			name: "Когда первичный ключ совпадает с локальным",
			fields: fields{
				primaryTablePrimaryKey: "id",
				primaryTable:           "user",
			},
			args: args{
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "id",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := localKeyProcessor{
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				primaryTable:           tt.fields.primaryTable,
			}
			if got := l.isAvailable(tt.args.fieldConfig); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_localKeyProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTablePrimaryKey string
		primaryTable           string
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfig                configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации запроса",
			fields: fields{
				primaryTablePrimaryKey: "id",
				primaryTable:           "user",
			},
			args: args{
				parentTablePrimaryKeyValue: "110",
				fieldConfig: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "uuid4",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
			},
			want: "select uuid4_load.uuid4, uuid4_load.user_id from test_db uuid4_load where uuid4_load.user_id in (select uuid4_load_primary.uuid from user uuid4_load_primary where uuid4_load_primary.id = 110)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := localKeyProcessor{
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				primaryTable:           tt.fields.primaryTable,
			}
			if got := l.generate(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfig); got != tt.want {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
