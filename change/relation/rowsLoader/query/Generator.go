package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Генератор SQL запросов для загрузки дочерних сущностей
type generator struct {
	processors []generatorProcessor
}

// Конструктор генератора
func NewGenerator(primaryTablePrimaryKey string, primaryTable string) GeneratorInterface {
	return &generator{
		processors: []generatorProcessor{
			newPrimaryKeyProcessor(primaryTablePrimaryKey),
			newLocalKeyProcessor(primaryTablePrimaryKey, primaryTable),
		},
	}
}

// Генерация запроса для загрузки строк дочерних сущнсотей для резолвера конфликтов
func (g generator) GenerateQueryForLoadRowsByParentPrimaryKey(
	parentTablePrimaryKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
) string {
	for _, processor := range g.processors {
		if processor.isAvailable(fieldConfig) {
			return processor.generate(parentTablePrimaryKeyValue, fieldConfig)
		}
	}

	return ""
}
