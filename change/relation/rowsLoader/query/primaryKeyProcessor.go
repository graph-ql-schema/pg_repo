package query

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"fmt"
)

// Процессор генерации запросов, когда первичный ключ основной таблицы является локальным ключем для отношения
type primaryKeyProcessor struct {
	primaryTablePrimaryKey string
}

// Конструктор процессора
func newPrimaryKeyProcessor(primaryTablePrimaryKey string) generatorProcessor {
	return &primaryKeyProcessor{
		primaryTablePrimaryKey: primaryTablePrimaryKey,
	}
}

// Проверка доступности процесстоа
func (p primaryKeyProcessor) isAvailable(fieldConfig configuration.FieldsConfiguration) bool {
	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)

	return dbConf.LocalKey == p.primaryTablePrimaryKey
}

// Генерация запроса
func (p primaryKeyProcessor) generate(parentTablePrimaryKeyValue string, fieldConfig configuration.FieldsConfiguration) string {
	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)
	alias := fmt.Sprintf(`%v_load`, dbConf.PrimaryKey)

	return fmt.Sprintf(
		`select %v.%v, %v.%v from %v %v where %v.%v = %v`,
		alias,
		dbConf.PrimaryKey,
		alias,
		dbConf.ForeignKey,
		dbConf.Table,
		alias,
		alias,
		dbConf.ForeignKey,
		parentTablePrimaryKeyValue,
	)
}
