package rowsLoader

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/rowsLoader/query"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"strings"
)

// Загрузчик строк дочерних сущнсотей для разрешения конфликта
type loader struct {
	queryExecutor  executor.QueryExecutorInterface
	queryGenerator query.GeneratorInterface
}

// Конструктор сервиса
func NewLoader(
	queryExecutor executor.QueryExecutorInterface,
	primaryTablePrimaryKey string,
	primaryTable string,
) LoaderInterface {
	return &loader{
		queryExecutor:  queryExecutor,
		queryGenerator: query.NewGenerator(primaryTablePrimaryKey, primaryTable),
	}
}

// Загрузка строк дочерних сущностей по первичному ключу дочерней сущности
func (l loader) LoadRowsByRelationPrimaryKeys(
	relationPrimaryKeys []string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) ([]map[string]string, error) {
	if 0 == len(relationPrimaryKeys) {
		return nil, nil
	}

	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)
	alias := fmt.Sprintf(`%v_load_by_pkey`, dbConf.PrimaryKey)

	sqlQuery := fmt.Sprintf(
		`select %v.%v, %v.%v from %v %v where %v.%v in (%v)`,
		alias,
		dbConf.PrimaryKey,
		alias,
		dbConf.ForeignKey,
		dbConf.Table,
		alias,
		alias,
		dbConf.PrimaryKey,
		strings.Join(relationPrimaryKeys, `, `),
	)

	rows, err := l.queryExecutor.ExecuteListQuery(sqlQuery, nil, tx)
	if nil != err {
		return nil, fmt.Errorf(`failed to load entities for relation '%v -> %v': %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	defer rows.Close()

	result := []map[string]string{}
	for rows.Next() {
		data := map[string]interface{}{}
		err := sqlx.MapScan(rows, data)
		if nil != err {
			return nil, fmt.Errorf(`failed to load entities for relation '%v -> %v': %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
		}

		result = append(result, l.parseData(data))
	}

	return result, nil
}

// Загрузка строк дочерней сущности по первичному ключу родительской таблицы
func (l loader) LoadRowsByParentPrimaryKey(
	parentTablePrimaryKeyValue string,
	fieldConfig configuration.FieldsConfiguration,
	tx *sql.Tx,
) ([]map[string]string, error) {
	dbConf := fieldConfig.DbConfiguration.(configuration.RelationFieldConfiguration)
	sqlQuery := l.queryGenerator.GenerateQueryForLoadRowsByParentPrimaryKey(parentTablePrimaryKeyValue, fieldConfig)

	rows, err := l.queryExecutor.ExecuteListQuery(sqlQuery, nil, tx)
	if nil != err {
		return nil, fmt.Errorf(`failed to load entities for relation '%v -> %v': %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
	}

	defer rows.Close()

	result := []map[string]string{}
	for rows.Next() {
		data := map[string]interface{}{}
		err := sqlx.MapScan(rows, data)
		if nil != err {
			return nil, fmt.Errorf(`failed to load entities for relation '%v -> %v': %v`, dbConf.Table, dbConf.PrimaryKey, err.Error())
		}

		result = append(result, l.parseData(data))
	}

	return result, nil
}

// Парсинг данных ответа от сервера
func (l loader) parseData(data map[string]interface{}) map[string]string {
	item := map[string]string{}
	for code, val := range data {
		data := ""
		if nil != val {
			data = fmt.Sprintf(`%v`, val)
		}

		item[code] = data
	}

	return item
}
