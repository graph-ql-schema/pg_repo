package rowsLoader

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"database/sql"
)

// Загрузчик строк дочерних сущнсотей для разрешения конфликта
type LoaderInterface interface {
	// Загрузка строк дочерней сущности по первичному ключу родительской таблицы
	LoadRowsByParentPrimaryKey(
		parentTablePrimaryKeyValue string,
		fieldConfig configuration.FieldsConfiguration,
		tx *sql.Tx,
	) ([]map[string]string, error)

	// Загрузка строк дочерних сущностей по первичному ключу дочерней сущности
	LoadRowsByRelationPrimaryKeys(
		relationPrimaryKeys []string,
		fieldConfig configuration.FieldsConfiguration,
		tx *sql.Tx,
	) ([]map[string]string, error)
}
