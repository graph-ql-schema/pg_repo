package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type singleFieldResolverMock struct {
	IsInsertError bool
	IsUpdateError bool
	IsDeleteError bool
	InsertResult  []events.Event
	UpdateResult  []events.Event
	DeleteResult  []events.Event
}

// Резолвинг конфликтов вставки родительской сущности
func (s singleFieldResolverMock) resolveInsert(string, []string, configuration.FieldsConfiguration, *sql.Tx) ([]events.Event, error) {
	if s.IsInsertError {
		return nil, fmt.Errorf(`test`)
	}

	return s.InsertResult, nil
}

// Резолвинг конфликтов обновления родительской сущности
func (s singleFieldResolverMock) resolveUpdate(string, []string, configuration.FieldsConfiguration, *sql.Tx) ([]events.Event, error) {
	if s.IsUpdateError {
		return nil, fmt.Errorf(`test`)
	}

	return s.UpdateResult, nil
}

// Резолвинг конфликтов удаления родительской сущности
func (s singleFieldResolverMock) resolveDelete(string, configuration.FieldsConfiguration, *sql.Tx) ([]events.Event, error) {
	if s.IsDeleteError {
		return nil, fmt.Errorf(`test`)
	}

	return s.DeleteResult, nil
}
