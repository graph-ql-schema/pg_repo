package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
)

// Процессор обработки решений Skip для политики Drop
type skipDropProcessor struct{}

// Конструктор процессора Skip политики Drop
func newDropPolicySkipProcessor() resolverProcessorInterface {
	return &skipDropProcessor{}
}

// Проверка доступности события
func (s skipDropProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	dbConfig, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	if !ok {
		return false
	}

	return dbConfig.Conflict.Drop == configuration.DropPolicySkip
}

// Резолвинг политики
func (s skipDropProcessor) resolve(string, configuration.FieldsConfiguration, map[string]string) (*string, *events.Event) {
	return nil, nil
}
