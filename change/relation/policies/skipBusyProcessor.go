package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"fmt"
)

// Процессор обработки решений Skip для политики Busy
type skipBusyProcessor struct{}

// Конструктор процессора Skip политики Busy
func newBusyPolicySkipProcessor() resolverProcessorInterface {
	return &skipBusyProcessor{}
}

// Проверка доступности события
func (s skipBusyProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	dbConfig, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	if !ok {
		return false
	}

	return dbConfig.Conflict.Busy == configuration.BusyPolicySkip
}

// Резолвинг политики
func (s skipBusyProcessor) resolve(
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRow map[string]string,
) (*string, *events.Event) {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	foreignKeyValue, ok := relationRow[dbConfig.ForeignKey]
	if !ok {
		return nil, nil
	}

	// Если поле не установлено, то пропускать обновление нет смысла
	if foreignKeyValue != "" {
		return nil, nil
	}

	id, ok := relationRow[dbConfig.PrimaryKey]
	if !ok {
		return nil, nil
	}

	event := events.Event{
		Type:            events.Updated,
		Table:           dbConfig.Table,
		PrimaryKey:      dbConfig.PrimaryKey,
		PrimaryKeyValue: id,
	}

	alias := fmt.Sprintf(`%v_skip`, dbConfig.ForeignKey)
	query := fmt.Sprintf(
		`update %v %v set %v = %v where %v.%v = %v`,
		dbConfig.Table,
		alias,
		dbConfig.ForeignKey,
		parentTableLocalKeyValue,
		alias,
		dbConfig.PrimaryKey,
		id,
	)

	return &query, &event
}
