package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"fmt"
)

// Callback проверки доступности для политики Drop
var dropPolicyNull tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool {
	return relationConfig.Conflict.Drop == configuration.DropPolicyNull
}

// Callback проверки доступности для политики Free
var freePolicyNull tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool {
	return relationConfig.Conflict.Free == configuration.FreePolicyNull
}

// Процессор обработки решений Null
type nullProcessor struct {
	checkAvailability tCheckAvailabilityCallback
}

// Проверка доступности события
func (n nullProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	dbConfig, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	if !ok {
		return false
	}

	return n.checkAvailability(dbConfig)
}

// Резолвинг политики
func (n nullProcessor) resolve(
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRow map[string]string,
) (*string, *events.Event) {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	foreignKeyValue, ok := relationRow[dbConfig.ForeignKey]
	if !ok {
		return nil, nil
	}

	if foreignKeyValue != parentTableLocalKeyValue {
		return nil, nil
	}

	id, ok := relationRow[dbConfig.PrimaryKey]
	if !ok {
		return nil, nil
	}

	event := events.Event{
		Type:            events.Updated,
		Table:           dbConfig.Table,
		PrimaryKey:      dbConfig.PrimaryKey,
		PrimaryKeyValue: id,
	}

	alias := fmt.Sprintf(`%v_null`, dbConfig.ForeignKey)
	query := fmt.Sprintf(
		`update %v %v set %v = null where %v.%v = %v`,
		dbConfig.Table,
		alias,
		dbConfig.ForeignKey,
		alias,
		dbConfig.PrimaryKey,
		id,
	)

	return &query, &event
}

// Конструктор процессора Null политики Drop
func newDropPolicyNullProcessor() resolverProcessorInterface {
	return &nullProcessor{
		checkAvailability: dropPolicyNull,
	}
}

// Конструктор процессора Null политики Free
func newFreePolicyNullProcessor() resolverProcessorInterface {
	return &nullProcessor{
		checkAvailability: freePolicyNull,
	}
}
