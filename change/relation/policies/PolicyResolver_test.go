package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"reflect"
	"testing"
)

// Тестирование резолвинга
func Test_policyResolver_Resolve(t *testing.T) {
	type fields struct {
		processors []resolverProcessorInterface
	}
	type args struct {
		parentTableLocalKeyValue string
		fieldConfiguration       configuration.FieldsConfiguration
		relationRows             []map[string]string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []string
		want1  []events.Event
	}{
		{
			name: "Передан пустой набор строк",
			fields: fields{
				processors: []resolverProcessorInterface{
					resolverProcessorMock{
						IsAvailable: true,
						ResolveSql:  "test",
						ResolveEvent: &events.Event{
							Type:            "test",
							Table:           "test_t",
							PrimaryKey:      "tt",
							PrimaryKeyValue: "ttt",
						},
					},
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration:       configuration.FieldsConfiguration{},
				relationRows:             []map[string]string{},
			},
			want:  []string{},
			want1: []events.Event{},
		},
		{
			name: "Нет доступного процессора",
			fields: fields{
				processors: []resolverProcessorInterface{
					resolverProcessorMock{
						IsAvailable: false,
						ResolveSql:  "test",
						ResolveEvent: &events.Event{
							Type:            "test",
							Table:           "test_t",
							PrimaryKey:      "tt",
							PrimaryKeyValue: "ttt",
						},
					},
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration:       configuration.FieldsConfiguration{},
				relationRows: []map[string]string{
					{"test": "1"},
					{"test": "2"},
				},
			},
			want:  []string{},
			want1: []events.Event{},
		},
		{
			name: "Процессор не вернул результат",
			fields: fields{
				processors: []resolverProcessorInterface{
					resolverProcessorMock{
						IsAvailable:  true,
						ResolveSql:   "",
						ResolveEvent: nil,
					},
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration:       configuration.FieldsConfiguration{},
				relationRows: []map[string]string{
					{"test": "1"},
					{"test": "2"},
				},
			},
			want:  []string{},
			want1: []events.Event{},
		},
		{
			name: "Все корректно",
			fields: fields{
				processors: []resolverProcessorInterface{
					resolverProcessorMock{
						IsAvailable: true,
						ResolveSql:  "test",
						ResolveEvent: &events.Event{
							Type:            "test",
							Table:           "test_t",
							PrimaryKey:      "tt",
							PrimaryKeyValue: "ttt",
						},
					},
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration:       configuration.FieldsConfiguration{},
				relationRows: []map[string]string{
					{"test": "1"},
					{"test": "2"},
				},
			},
			want: []string{
				"test",
				"test",
			},
			want1: []events.Event{
				{
					Type:            "test",
					Table:           "test_t",
					PrimaryKey:      "tt",
					PrimaryKeyValue: "ttt",
				},
				{
					Type:            "test",
					Table:           "test_t",
					PrimaryKey:      "tt",
					PrimaryKeyValue: "ttt",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := policyResolver{
				processors: tt.fields.processors,
			}
			got, got1 := p.Resolve(tt.args.parentTableLocalKeyValue, tt.args.fieldConfiguration, tt.args.relationRows)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Resolve() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Resolve() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
