package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"reflect"
	"testing"
)

// Проверка доступности процессора
func Test_dropProcessor_isAvailable(t *testing.T) {
	type fields struct {
		checkAvailability tCheckAvailabilityCallback
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Не валидный тип конфига",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Проверка доступности",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dropProcessor{
				checkAvailability: tt.fields.checkAvailability,
			}
			if got := d.isAvailable(tt.args.fieldConfiguration); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование резолвинга политики
func Test_dropProcessor_resolve(t *testing.T) {
	type fields struct {
		checkAvailability tCheckAvailabilityCallback
	}
	type args struct {
		parentTableLocalKeyValue string
		fieldConfiguration       configuration.FieldsConfiguration
		relationRow              map[string]string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
		want1  *events.Event
	}{
		{
			name: "Если не передано значение внешнего ключа в строке значений",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
				relationRow: map[string]string{
					"id": "123",
				},
			},
			want:  "",
			want1: nil,
		},
		{
			name: "Если не совпадает значение локального и внешнего ключа",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
				relationRow: map[string]string{
					"id":      "123",
					"user_id": "2",
				},
			},
			want:  "",
			want1: nil,
		},
		{
			name: "Если не передано значение первичного ключа подчиненной таблицы",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
				relationRow: map[string]string{
					"user_id": "1",
				},
			},
			want:  "",
			want1: nil,
		},
		{
			name: "Данные корректны",
			fields: fields{
				checkAvailability: func(relationConfig configuration.RelationFieldConfiguration) bool {
					return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
				},
			},
			args: args{
				parentTableLocalKeyValue: "1",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
				relationRow: map[string]string{
					"id":      "222",
					"user_id": "1",
				},
			},
			want: "delete from test_db user_id_drop where user_id_drop.id = 222",
			want1: &events.Event{
				Type:            events.Deleted,
				Table:           "test_db",
				PrimaryKey:      "id",
				PrimaryKeyValue: "222",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := dropProcessor{
				checkAvailability: tt.fields.checkAvailability,
			}
			got, got1 := d.resolve(tt.args.parentTableLocalKeyValue, tt.args.fieldConfiguration, tt.args.relationRow)

			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("resolve() got = %v, want %v", *got, tt.want)
				} else {
					t.Errorf("resolve() got = %v, want %v", got, want)
				}
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("resolve() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
