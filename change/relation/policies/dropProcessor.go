package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"fmt"
)

// Callback проверки доступности для политики Drop
var dropPolicyDrop tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool {
	return relationConfig.Conflict.Drop == configuration.DropPolicyDrop
}

// Callback проверки доступности для политики Free
var freePolicyDrop tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool {
	return relationConfig.Conflict.Free == configuration.FreePolicyDrop
}

// Процессор обработки решений Drop
type dropProcessor struct {
	checkAvailability tCheckAvailabilityCallback
}

// Проверка доступности события
func (d dropProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	dbConfig, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	if !ok {
		return false
	}

	return d.checkAvailability(dbConfig)
}

// Резолвинг политики
func (d dropProcessor) resolve(
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRow map[string]string,
) (*string, *events.Event) {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	foreignKeyValue, ok := relationRow[dbConfig.ForeignKey]
	if !ok {
		return nil, nil
	}

	if foreignKeyValue != parentTableLocalKeyValue {
		return nil, nil
	}

	id, ok := relationRow[dbConfig.PrimaryKey]
	if !ok {
		return nil, nil
	}

	event := events.Event{
		Type:            events.Deleted,
		Table:           dbConfig.Table,
		PrimaryKey:      dbConfig.PrimaryKey,
		PrimaryKeyValue: id,
	}

	alias := fmt.Sprintf(`%v_drop`, dbConfig.ForeignKey)
	query := fmt.Sprintf(
		`delete from %v %v where %v.%v = %v`,
		dbConfig.Table,
		alias,
		alias,
		dbConfig.PrimaryKey,
		id,
	)

	return &query, &event
}

// Конструктор процессора Drop политики Drop
func newDropPolicyDropProcessor() resolverProcessorInterface {
	return &dropProcessor{
		checkAvailability: dropPolicyDrop,
	}
}

// Конструктор процессора Drop политики Free
func newFreePolicyDropProcessor() resolverProcessorInterface {
	return &dropProcessor{
		checkAvailability: freePolicyDrop,
	}
}
