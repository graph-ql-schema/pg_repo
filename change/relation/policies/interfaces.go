package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
)

// Резолвер политик конфликта для отношений 1 to Many
type PolicyResolverInterface interface {
	// Резолвинг политики
	Resolve(
		parentTableLocalKeyValue string,
		fieldConfiguration configuration.FieldsConfiguration,
		relationRows []map[string]string,
	) ([]string, []events.Event)
}

// Callback проверки доступности
type tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool

// Интерфейс процессора резолвера политики
type resolverProcessorInterface interface {
	// Проверка доступности события
	isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool

	// Резолвинг политики
	resolve(
		parentTableLocalKeyValue string,
		fieldConfiguration configuration.FieldsConfiguration,
		relationRow map[string]string,
	) (*string, *events.Event)
}
