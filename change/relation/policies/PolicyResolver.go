package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"sync"
)

// Результат асинхронной обработки строки
type asyncResolveResult struct {
	Sql   string
	Event events.Event
}

// Резолвер политик
type policyResolver struct {
	processors []resolverProcessorInterface
}

// Резолвинг политики
func (p policyResolver) Resolve(
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRows []map[string]string,
) ([]string, []events.Event) {
	if 0 == len(relationRows) {
		return []string{}, []events.Event{}
	}

	var wg sync.WaitGroup
	resultChan := make(chan asyncResolveResult)
	resultSqlChan := make(chan []string)
	resultEventsChan := make(chan []events.Event)
	completeChan := make(chan bool)

	defer close(resultChan)
	defer close(resultSqlChan)
	defer close(resultEventsChan)
	defer close(completeChan)

	// Запускаем асинхронное чтение результатов
	go p.asyncReadResult(resultChan, resultSqlChan, resultEventsChan, completeChan)

	// Запускаем асинхронную обработку строк
	wg.Add(len(relationRows))
	for _, row := range relationRows {
		go p.asyncResolveRow(&wg, resultChan, parentTableLocalKeyValue, fieldConfiguration, row)
	}

	wg.Wait()
	completeChan <- true

	sqlResult := <-resultSqlChan
	eventsResult := <-resultEventsChan

	return sqlResult, eventsResult
}

// Асинхронное чтение результатов парсинга
func (p policyResolver) asyncReadResult(
	resultChan chan asyncResolveResult,
	resultSqlChan chan []string,
	resultEventsChan chan []events.Event,
	completeChan chan bool,
) {
	sqlResult := []string{}
	eventsResult := []events.Event{}

	for {
		select {
		case item := <-resultChan:
			sqlResult = append(sqlResult, item.Sql)
			eventsResult = append(eventsResult, item.Event)
			break
		case _ = <-completeChan:
			resultSqlChan <- sqlResult
			resultEventsChan <- eventsResult

			return
		}
	}
}

// Асинхронная обработка строки значений
func (p policyResolver) asyncResolveRow(
	wg *sync.WaitGroup,
	resultChan chan asyncResolveResult,
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRow map[string]string,
) {
	defer wg.Done()

	for _, processor := range p.processors {
		if !processor.isAvailable(fieldConfiguration) {
			continue
		}

		sql, event := processor.resolve(parentTableLocalKeyValue, fieldConfiguration, relationRow)
		if nil == sql || nil == event {
			continue
		}

		resultChan <- asyncResolveResult{
			Sql:   *sql,
			Event: *event,
		}
	}
}

// Резолвер политик Drop
func NewDropPolicyResolver() PolicyResolverInterface {
	return &policyResolver{
		processors: []resolverProcessorInterface{
			newDropPolicyDropProcessor(),
			newDropPolicyNullProcessor(),
			newDropPolicySkipProcessor(),
		},
	}
}

// Резолвер политик Free
func NewFreePolicyResolver() PolicyResolverInterface {
	return &policyResolver{
		processors: []resolverProcessorInterface{
			newFreePolicyDropProcessor(),
			newFreePolicyNullProcessor(),
		},
	}
}

// Резолвер политик Busy
func NewBusyPolicyResolver() PolicyResolverInterface {
	return &policyResolver{
		processors: []resolverProcessorInterface{
			newBusyPolicyReplaceProcessor(),
			newBusyPolicySkipProcessor(),
		},
	}
}
