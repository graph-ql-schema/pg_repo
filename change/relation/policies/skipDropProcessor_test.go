package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"testing"
)

// Проверка доступности процессора
func Test_skipProcessor_isAvailable(t *testing.T) {
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Не валидный тип конфига",
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Проверка доступности",
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicySkip,
						},
					},
				},
			},
			want: true,
		},
		{
			name: "Не корректный тип политики",
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "test_db",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict: configuration.RelationConflictConfiguration{
							Drop: configuration.DropPolicyDrop,
						},
					},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := skipDropProcessor{}
			if got := s.isAvailable(tt.args.fieldConfiguration); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}
