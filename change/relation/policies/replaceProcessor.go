package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"fmt"
)

// Callback проверки доступности для политики Busy
var busyPolicyReplace tCheckAvailabilityCallback = func(relationConfig configuration.RelationFieldConfiguration) bool {
	return relationConfig.Conflict.Busy == configuration.BusyPolicyReplace
}

// Процессор обработки решений Replace
type replaceProcessor struct {
	checkAvailability tCheckAvailabilityCallback
}

// Проверка доступности события
func (r replaceProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	dbConfig, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	if !ok {
		return false
	}

	return r.checkAvailability(dbConfig)
}

// Резолвинг политики
func (r replaceProcessor) resolve(
	parentTableLocalKeyValue string,
	fieldConfiguration configuration.FieldsConfiguration,
	relationRow map[string]string,
) (*string, *events.Event) {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	foreignKeyValue, ok := relationRow[dbConfig.ForeignKey]
	if !ok {
		return nil, nil
	}

	if foreignKeyValue == parentTableLocalKeyValue {
		return nil, nil
	}

	id, ok := relationRow[dbConfig.PrimaryKey]
	if !ok {
		return nil, nil
	}

	event := events.Event{
		Type:            events.Updated,
		Table:           dbConfig.Table,
		PrimaryKey:      dbConfig.PrimaryKey,
		PrimaryKeyValue: id,
	}

	alias := fmt.Sprintf(`%v_replace`, dbConfig.ForeignKey)
	query := fmt.Sprintf(
		`update %v %v set %v = %v where %v.%v = %v`,
		dbConfig.Table,
		alias,
		dbConfig.ForeignKey,
		parentTableLocalKeyValue,
		alias,
		dbConfig.PrimaryKey,
		id,
	)

	return &query, &event
}

// Конструктор процессора Replace политики Busy
func newBusyPolicyReplaceProcessor() resolverProcessorInterface {
	return &replaceProcessor{
		checkAvailability: busyPolicyReplace,
	}
}
