package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
)

// Подставка для тестирования
type PolicyResolverMock struct {
	SqlResult    []string
	EventsResult []events.Event
}

// Резолвинг политики
func (p PolicyResolverMock) Resolve(string, configuration.FieldsConfiguration, []map[string]string) ([]string, []events.Event) {
	return p.SqlResult, p.EventsResult
}
