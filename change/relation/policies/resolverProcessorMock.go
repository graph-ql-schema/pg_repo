package policies

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
)

// Подставка для тестирования
type resolverProcessorMock struct {
	IsAvailable  bool
	ResolveSql   string
	ResolveEvent *events.Event
}

// Проверка доступности события
func (r resolverProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return r.IsAvailable
}

// Резолвинг политики
func (r resolverProcessorMock) resolve(string, configuration.FieldsConfiguration, map[string]string) (*string, *events.Event) {
	if 0 == len(r.ResolveSql) {
		return nil, nil
	}

	return &r.ResolveSql, r.ResolveEvent
}
