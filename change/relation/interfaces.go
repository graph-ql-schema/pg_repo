package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"database/sql"
)

// Резолвер конфликтов
type ConflictResolverInterface interface {
	// Резолвинг конфликтов вставки родительской сущности
	ResolveInsert(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		tx *sql.Tx,
	) ([]events.Event, error)

	// Резолвинг конфликтов обновления родительской сущности
	ResolveUpdate(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp map[string][]string,
		tx *sql.Tx,
	) ([]events.Event, error)

	// Резолвинг конфликтов удаления родительской сущности
	ResolveDelete(
		parentTablePrimaryKeyValue string,
		tx *sql.Tx,
	) ([]events.Event, error)
}

// Системный резолвер конфлитков для одиночного поля
type singleFieldResolverInterface interface {
	// Резолвинг конфликтов вставки родительской сущности
	resolveInsert(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp []string,
		fieldConfiguration configuration.FieldsConfiguration,
		tx *sql.Tx,
	) ([]events.Event, error)

	// Резолвинг конфликтов обновления родительской сущности
	resolveUpdate(
		parentTablePrimaryKeyValue string,
		relationIdsToSetUp []string,
		fieldConfiguration configuration.FieldsConfiguration,
		tx *sql.Tx,
	) ([]events.Event, error)

	// Резолвинг конфликтов удаления родительской сущности
	resolveDelete(
		parentTablePrimaryKeyValue string,
		fieldConfiguration configuration.FieldsConfiguration,
		tx *sql.Tx,
	) ([]events.Event, error)
}
