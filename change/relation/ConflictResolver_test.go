package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

// Тестирование резолвинга вставки
func Test_conflictResolver_ResolveInsert(t *testing.T) {
	type fields struct {
		fieldsConfiguration []configuration.FieldsConfiguration
		fieldResolver       singleFieldResolverInterface
		logger              *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Поля не переданы",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsInsertError: false,
					InsertResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger:              helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Переданы поля не подходящего типа",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsInsertError: false,
					InsertResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:           "test_1",
						FieldNum:        0,
						GraphQlName:     "test_1",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "test_2",
						FieldNum:        0,
						GraphQlName:     "test_2",
						DbConfiguration: configuration.ArrayFieldConfiguration{},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsInsertError: true,
					InsertResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsInsertError: false,
					InsertResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want: []events.Event{
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := conflictResolver{
				fieldsConfiguration: tt.fields.fieldsConfiguration,
				fieldResolver:       tt.fields.fieldResolver,
				logger:              tt.fields.logger,
			}
			got, err := c.ResolveInsert(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("ResolveInsert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ResolveInsert() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование резолвинга обновления сущности
func Test_conflictResolver_ResolveUpdate(t *testing.T) {
	type fields struct {
		fieldsConfiguration []configuration.FieldsConfiguration
		fieldResolver       singleFieldResolverInterface
		logger              *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         map[string][]string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Поля не переданы",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsUpdateError: false,
					UpdateResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger:              helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Переданы поля не подходящего типа",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsUpdateError: false,
					UpdateResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:           "test_1",
						FieldNum:        0,
						GraphQlName:     "test_1",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "test_2",
						FieldNum:        0,
						GraphQlName:     "test_2",
						DbConfiguration: configuration.ArrayFieldConfiguration{},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Не переданы новые значения для полей",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsUpdateError: false,
					UpdateResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp:         map[string][]string{},
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsUpdateError: true,
					UpdateResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_1": {"1", "2"},
					"test_2": {"1", "2"},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsUpdateError: false,
					UpdateResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				relationIdsToSetUp: map[string][]string{
					"test_1": {"1", "2"},
					"test_2": {"1", "2"},
				},
				tx: nil,
			},
			want: []events.Event{
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := conflictResolver{
				fieldsConfiguration: tt.fields.fieldsConfiguration,
				fieldResolver:       tt.fields.fieldResolver,
				logger:              tt.fields.logger,
			}
			got, err := c.ResolveUpdate(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("ResolveUpdate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ResolveUpdate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование резолвинга удаления сущности
func Test_conflictResolver_ResolveDelete(t *testing.T) {
	type fields struct {
		fieldsConfiguration []configuration.FieldsConfiguration
		fieldResolver       singleFieldResolverInterface
		logger              *logrus.Entry
	}
	type args struct {
		parentTablePrimaryKeyValue string
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Поля не переданы",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsDeleteError: false,
					DeleteResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger:              helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Переданы поля не подходящего типа",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsDeleteError: false,
					DeleteResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:           "test_1",
						FieldNum:        0,
						GraphQlName:     "test_1",
						DbConfiguration: configuration.ScalarFieldConfiguration{},
					},
					{
						Field:           "test_2",
						FieldNum:        0,
						GraphQlName:     "test_2",
						DbConfiguration: configuration.ArrayFieldConfiguration{},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			want:    []events.Event{},
			wantErr: false,
		},
		{
			name: "Ошибка резолвинга поля",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsDeleteError: true,
					DeleteResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				fieldResolver: singleFieldResolverMock{
					IsDeleteError: false,
					DeleteResult: []events.Event{
						{
							Type:            "test",
							Table:           "tt",
							PrimaryKey:      "ttt",
							PrimaryKeyValue: "tttt",
						},
					},
				},
				logger: helpers.NewLogger(`test`),
				fieldsConfiguration: []configuration.FieldsConfiguration{
					{
						Field:       "test_1",
						FieldNum:    0,
						GraphQlName: "test_1",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_1",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
					{
						Field:       "test_2",
						FieldNum:    0,
						GraphQlName: "test_2",
						DbConfiguration: configuration.RelationFieldConfiguration{
							Table:      "test_db_2",
							PrimaryKey: "id",
							ForeignKey: "user_id",
							LocalKey:   "uuid",
							Conflict:   configuration.RelationConflictConfiguration{},
						},
					},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "1",
				tx:                         nil,
			},
			want: []events.Event{
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
				{
					Type:            "test",
					Table:           "tt",
					PrimaryKey:      "ttt",
					PrimaryKeyValue: "tttt",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := conflictResolver{
				fieldsConfiguration: tt.fields.fieldsConfiguration,
				fieldResolver:       tt.fields.fieldResolver,
				logger:              tt.fields.logger,
			}
			got, err := c.ResolveDelete(tt.args.parentTablePrimaryKeyValue, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("ResolveDelete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ResolveDelete() got = %v, want %v", got, tt.want)
			}
		})
	}
}
