package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/policies"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation/rowsLoader"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"database/sql"
	"reflect"
	"testing"
)

// Тестирование резолвинга запросов обновления
func Test_singleFieldResolver_resolveUpdate(t *testing.T) {
	type fields struct {
		queryExecutor      executor.QueryExecutorInterface
		rowsLoader         rowsLoader.LoaderInterface
		dropPolicyResolver policies.PolicyResolverInterface
		freePolicyResolver policies.PolicyResolverInterface
		busyPolicyResolver policies.PolicyResolverInterface
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         []string
		fieldConfiguration         configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Не удалось получить новые значения сущности",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: true,
					LoadRowsByParentPrimaryKeyResult: []map[string]string{
						{"id": "2", "user_id": "1"},
						{"id": "3", "user_id": "1"},
					},
					LoadRowsByRelationPrimaryKeysResult: nil,
				},
				dropPolicyResolver: nil,
				freePolicyResolver: nil,
				busyPolicyResolver: nil,
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Не удалось получить старые значения сущности",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    true,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult:   nil,
					LoadRowsByRelationPrimaryKeysResult: []map[string]string{
						{"id": "1", "user_id": "1"},
						{"id": "2", "user_id": "1"},
					},
				},
				dropPolicyResolver: nil,
				freePolicyResolver: nil,
				busyPolicyResolver: nil,
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка при выполнении запроса к БД",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: true,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult: []map[string]string{
						{"id": "2", "user_id": "1"},
						{"id": "3", "user_id": "1"},
					},
					LoadRowsByRelationPrimaryKeysResult: []map[string]string{
						{"id": "1", "user_id": "1"},
						{"id": "2", "user_id": "1"},
					},
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок исполнения",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult: []map[string]string{
						{"id": "2", "user_id": "1"},
						{"id": "3", "user_id": "1"},
					},
					LoadRowsByRelationPrimaryKeysResult: []map[string]string{
						{"id": "1", "user_id": "1"},
						{"id": "2", "user_id": "1"},
					},
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want: []events.Event{
				{
					Type:            "free",
					Table:           "phones",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
				{
					Type:            "busy",
					Table:           "phones",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleFieldResolver{
				queryExecutor:      tt.fields.queryExecutor,
				rowsLoader:         tt.fields.rowsLoader,
				dropPolicyResolver: tt.fields.dropPolicyResolver,
				freePolicyResolver: tt.fields.freePolicyResolver,
				busyPolicyResolver: tt.fields.busyPolicyResolver,
			}
			got, err := s.resolveUpdate(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.fieldConfiguration, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("resolveUpdate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("resolveUpdate() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения строк для освобождения
func Test_singleFieldResolver_getRowsToFree(t *testing.T) {
	type fields struct {
		queryExecutor      executor.QueryExecutorInterface
		rowsLoader         rowsLoader.LoaderInterface
		dropPolicyResolver policies.PolicyResolverInterface
		freePolicyResolver policies.PolicyResolverInterface
		busyPolicyResolver policies.PolicyResolverInterface
	}
	type args struct {
		oldRows  []map[string]string
		newRows  []map[string]string
		dbConfig configuration.RelationFieldConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []map[string]string
	}{
		{
			name:   "Передано 2 коллекции, одно общее значение.",
			fields: fields{},
			args: args{
				oldRows: []map[string]string{
					{"id": "2", "user_id": "1"},
					{"id": "3", "user_id": "1"},
				},
				newRows: []map[string]string{
					{"id": "1", "user_id": "1"},
					{"id": "2", "user_id": "1"},
				},
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{
				{"id": "3", "user_id": "1"},
			},
		},
		{
			name:   "Передано 2 коллекции, новая пустая.",
			fields: fields{},
			args: args{
				oldRows: []map[string]string{
					{"id": "2", "user_id": "1"},
					{"id": "3", "user_id": "1"},
				},
				newRows: []map[string]string{},
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{
				{"id": "2", "user_id": "1"},
				{"id": "3", "user_id": "1"},
			},
		},
		{
			name:   "Передано 2 коллекции, новая null.",
			fields: fields{},
			args: args{
				oldRows: []map[string]string{
					{"id": "2", "user_id": "1"},
					{"id": "3", "user_id": "1"},
				},
				newRows: nil,
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{
				{"id": "2", "user_id": "1"},
				{"id": "3", "user_id": "1"},
			},
		},
		{
			name:   "Передано 2 коллекции, старая пустая.",
			fields: fields{},
			args: args{
				oldRows: []map[string]string{},
				newRows: []map[string]string{
					{"id": "2", "user_id": "1"},
					{"id": "3", "user_id": "1"},
				},
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{},
		},
		{
			name:   "Передано 2 коллекции, старая null.",
			fields: fields{},
			args: args{
				oldRows: nil,
				newRows: []map[string]string{
					{"id": "2", "user_id": "1"},
					{"id": "3", "user_id": "1"},
				},
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{},
		},
		{
			name:   "Коллекции не переданы.",
			fields: fields{},
			args: args{
				oldRows: nil,
				newRows: nil,
				dbConfig: configuration.RelationFieldConfiguration{
					Table:      "phones",
					PrimaryKey: "id",
					ForeignKey: "user_id",
					LocalKey:   "uuid",
					Conflict:   configuration.RelationConflictConfiguration{},
				},
			},
			want: []map[string]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleFieldResolver{
				queryExecutor:      tt.fields.queryExecutor,
				rowsLoader:         tt.fields.rowsLoader,
				dropPolicyResolver: tt.fields.dropPolicyResolver,
				freePolicyResolver: tt.fields.freePolicyResolver,
				busyPolicyResolver: tt.fields.busyPolicyResolver,
			}
			if got := s.getRowsToFree(tt.args.oldRows, tt.args.newRows, tt.args.dbConfig); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getRowsToFree() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование резолвинга вставки
func Test_singleFieldResolver_resolveInsert(t *testing.T) {
	type fields struct {
		queryExecutor      executor.QueryExecutorInterface
		rowsLoader         rowsLoader.LoaderInterface
		dropPolicyResolver policies.PolicyResolverInterface
		freePolicyResolver policies.PolicyResolverInterface
		busyPolicyResolver policies.PolicyResolverInterface
	}
	type args struct {
		parentTablePrimaryKeyValue string
		relationIdsToSetUp         []string
		fieldConfiguration         configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Не удалось получить новые значения сущности",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:     false,
					LoadRowsByRelationPrimaryKeysError:  true,
					LoadRowsByParentPrimaryKeyResult:    nil,
					LoadRowsByRelationPrimaryKeysResult: nil,
				},
				dropPolicyResolver: nil,
				freePolicyResolver: nil,
				busyPolicyResolver: nil,
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка при выполнении запроса к БД",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: true,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult:   nil,
					LoadRowsByRelationPrimaryKeysResult: []map[string]string{
						{"id": "1", "user_id": "1"},
						{"id": "2", "user_id": "1"},
					},
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок исполнения",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult:   nil,
					LoadRowsByRelationPrimaryKeysResult: []map[string]string{
						{"id": "1", "user_id": "1"},
						{"id": "2", "user_id": "1"},
					},
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				relationIdsToSetUp:         []string{"1", "2"},
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want: []events.Event{
				{
					Type:            "busy",
					Table:           "phones",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleFieldResolver{
				queryExecutor:      tt.fields.queryExecutor,
				rowsLoader:         tt.fields.rowsLoader,
				dropPolicyResolver: tt.fields.dropPolicyResolver,
				freePolicyResolver: tt.fields.freePolicyResolver,
				busyPolicyResolver: tt.fields.busyPolicyResolver,
			}
			got, err := s.resolveInsert(tt.args.parentTablePrimaryKeyValue, tt.args.relationIdsToSetUp, tt.args.fieldConfiguration, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("resolveInsert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("resolveInsert() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование резолвинга удаления сущности
func Test_singleFieldResolver_resolveDelete(t *testing.T) {
	type fields struct {
		queryExecutor      executor.QueryExecutorInterface
		rowsLoader         rowsLoader.LoaderInterface
		dropPolicyResolver policies.PolicyResolverInterface
		freePolicyResolver policies.PolicyResolverInterface
		busyPolicyResolver policies.PolicyResolverInterface
	}
	type args struct {
		parentTablePrimaryKeyValue string
		fieldConfiguration         configuration.FieldsConfiguration
		tx                         *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []events.Event
		wantErr bool
	}{
		{
			name: "Не удалось получить старые значения сущности",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:     true,
					LoadRowsByRelationPrimaryKeysError:  false,
					LoadRowsByParentPrimaryKeyResult:    nil,
					LoadRowsByRelationPrimaryKeysResult: nil,
				},
				dropPolicyResolver: nil,
				freePolicyResolver: nil,
				busyPolicyResolver: nil,
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Ошибка при выполнении запроса к БД",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: true,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult: []map[string]string{
						{"id": "2", "user_id": "1"},
						{"id": "3", "user_id": "1"},
					},
					LoadRowsByRelationPrimaryKeysResult: nil,
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок исполнения",
			fields: fields{
				queryExecutor: executor.QueryExecutorMock{
					IsExecuteChangeQueryError: false,
				},
				rowsLoader: rowsLoader.LoaderMock{
					LoadRowsByParentPrimaryKeyError:    false,
					LoadRowsByRelationPrimaryKeysError: false,
					LoadRowsByParentPrimaryKeyResult: []map[string]string{
						{"id": "2", "user_id": "1"},
						{"id": "3", "user_id": "1"},
					},
					LoadRowsByRelationPrimaryKeysResult: nil,
				},
				dropPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "drop",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				freePolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "free",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
				busyPolicyResolver: policies.PolicyResolverMock{
					SqlResult: []string{"test"},
					EventsResult: []events.Event{{
						Type:            "busy",
						Table:           "phones",
						PrimaryKey:      "id",
						PrimaryKeyValue: "1",
					}},
				},
			},
			args: args{
				parentTablePrimaryKeyValue: "id",
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_f",
					DbConfiguration: configuration.RelationFieldConfiguration{
						Table:      "phones",
						PrimaryKey: "id",
						ForeignKey: "user_id",
						LocalKey:   "uuid",
						Conflict:   configuration.RelationConflictConfiguration{},
					},
				},
				tx: nil,
			},
			want: []events.Event{
				{
					Type:            "drop",
					Table:           "phones",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := singleFieldResolver{
				queryExecutor:      tt.fields.queryExecutor,
				rowsLoader:         tt.fields.rowsLoader,
				dropPolicyResolver: tt.fields.dropPolicyResolver,
				freePolicyResolver: tt.fields.freePolicyResolver,
				busyPolicyResolver: tt.fields.busyPolicyResolver,
			}
			got, err := s.resolveDelete(tt.args.parentTablePrimaryKeyValue, tt.args.fieldConfiguration, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("resolveDelete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("resolveDelete() got = %v, want %v", got, tt.want)
			}
		})
	}
}
