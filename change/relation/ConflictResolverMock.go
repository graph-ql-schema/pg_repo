package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type ConflictResolverMock struct {
	ResolveInsertError  bool
	ResolveInsertResult []events.Event
	ResolveUpdateError  bool
	ResolveUpdateResult []events.Event
	ResolveDeleteError  bool
	ResolveDeleteResult []events.Event
}

// Резолвинг конфликтов вставки родительской сущности
func (c ConflictResolverMock) ResolveInsert(string, map[string][]string, *sql.Tx) ([]events.Event, error) {
	if c.ResolveInsertError {
		return nil, fmt.Errorf(`test`)
	}

	return c.ResolveInsertResult, nil
}

// Резолвинг конфликтов обновления родительской сущности
func (c ConflictResolverMock) ResolveUpdate(string, map[string][]string, *sql.Tx) ([]events.Event, error) {
	if c.ResolveUpdateError {
		return nil, fmt.Errorf(`test`)
	}

	return c.ResolveUpdateResult, nil
}

// Резолвинг конфликтов удаления родительской сущности
func (c ConflictResolverMock) ResolveDelete(string, *sql.Tx) ([]events.Event, error) {
	if c.ResolveDeleteError {
		return nil, fmt.Errorf(`test`)
	}

	return c.ResolveDeleteResult, nil
}
