package relation

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"database/sql"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

// Резолвер конфликтов
type conflictResolver struct {
	fieldsConfiguration []configuration.FieldsConfiguration
	fieldResolver       singleFieldResolverInterface
	logger              *logrus.Entry
}

// Конструктор сервиса
func NewConflictResolver(
	fieldsConfiguration []configuration.FieldsConfiguration,
	queryExecutor executor.QueryExecutorInterface,
	primaryTablePrimaryKey string,
	primaryTable string,
) ConflictResolverInterface {
	return &conflictResolver{
		fieldsConfiguration: fieldsConfiguration,
		fieldResolver:       newSingleFieldResolver(queryExecutor, primaryTablePrimaryKey, primaryTable),
		logger:              helpers.NewLogger(fmt.Sprintf(`conflictResolver('%v' -> '%v')`, primaryTable, primaryTablePrimaryKey)),
	}
}

// Резолвинг конфликтов вставки родительской сущности
//
// Распараллеливание не даст профита из-за единственной транзакции, в рамках которой
// выполняются все запросы к БД. Рефакторить не требуется. Аналогично для других методов.
func (c conflictResolver) ResolveInsert(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	tx *sql.Tx,
) ([]events.Event, error) {
	if 0 == len(c.fieldsConfiguration) {
		return []events.Event{}, nil
	}

	result := []events.Event{}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	for _, field := range c.fieldsConfiguration {
		starts := time.Now().UnixNano() / int64(time.Millisecond)
		dbConfig, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration)
		if !ok {
			continue
		}

		// Если новые значения не переданы, считаем, что запрошена установка пустого списка
		ids, ok := relationIdsToSetUp[field.GraphQlName]
		if !ok {
			ids = []string{}
		}

		eventsResult, err := c.fieldResolver.resolveInsert(
			parentTablePrimaryKeyValue,
			ids,
			field,
			tx,
		)

		if nil != err {
			c.logger.WithFields(logrus.Fields{
				"code":   500,
				"err":    err,
				"entity": parentTablePrimaryKeyValue,
				"field":  field.Field,
				"table":  dbConfig.Table,
				"ids":    ids,
			}).Error(`Failed to resolve relation insert`)

			return nil, err
		}

		newResult := make([]events.Event, len(result)+len(eventsResult))
		newResultIndex := 0

		for i := range result {
			newResult[i] = result[i]
			newResultIndex++
		}

		for _, eventItem := range eventsResult {
			newResult[newResultIndex] = eventItem
			newResultIndex++
		}

		result = newResult

		finish := time.Now().UnixNano() / int64(time.Millisecond)
		c.logger.WithFields(logrus.Fields{
			"code":          200,
			"entity":        parentTablePrimaryKeyValue,
			"field":         field.Field,
			"table":         dbConfig.Table,
			"eventsResult":  eventsResult,
			"ids":           ids,
			"executionTime": finish - starts,
		}).Debug(`Resolved relation insert`)
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.logger.WithFields(logrus.Fields{
		"code":               200,
		"entity":             parentTablePrimaryKeyValue,
		"relationIdsToSetUp": relationIdsToSetUp,
		"result":             result,
		"executionTime":      finish - starts,
	}).Debug(`Resolved relation insert`)

	return result, nil
}

// Резолвинг конфликтов обновления родительской сущности
func (c conflictResolver) ResolveUpdate(
	parentTablePrimaryKeyValue string,
	relationIdsToSetUp map[string][]string,
	tx *sql.Tx,
) ([]events.Event, error) {
	if 0 == len(c.fieldsConfiguration) {
		return []events.Event{}, nil
	}

	result := []events.Event{}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	for _, field := range c.fieldsConfiguration {
		starts := time.Now().UnixNano() / int64(time.Millisecond)
		dbConfig, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration)
		if !ok {
			continue
		}

		// Если новые значения не переданы, считаем, что обновление отношения не запрошено
		ids, ok := relationIdsToSetUp[field.GraphQlName]
		if !ok {
			continue
		}

		eventsResult, err := c.fieldResolver.resolveUpdate(
			parentTablePrimaryKeyValue,
			ids,
			field,
			tx,
		)

		if nil != err {
			c.logger.WithFields(logrus.Fields{
				"code":   500,
				"err":    err,
				"entity": parentTablePrimaryKeyValue,
				"field":  field.Field,
				"table":  dbConfig.Table,
				"ids":    ids,
			}).Error(`Failed to resolve relation update`)

			return nil, err
		}

		newResult := make([]events.Event, len(result)+len(eventsResult))
		newResultIndex := 0

		for i := range result {
			newResult[i] = result[i]
			newResultIndex++
		}

		for _, eventItem := range eventsResult {
			newResult[newResultIndex] = eventItem
			newResultIndex++
		}

		result = newResult

		finish := time.Now().UnixNano() / int64(time.Millisecond)
		c.logger.WithFields(logrus.Fields{
			"code":          200,
			"entity":        parentTablePrimaryKeyValue,
			"field":         field.Field,
			"table":         dbConfig.Table,
			"eventsResult":  eventsResult,
			"ids":           ids,
			"executionTime": finish - starts,
		}).Debug(`Resolved relation update`)
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.logger.WithFields(logrus.Fields{
		"code":               200,
		"entity":             parentTablePrimaryKeyValue,
		"relationIdsToSetUp": relationIdsToSetUp,
		"result":             result,
		"executionTime":      finish - starts,
	}).Debug(`Resolved relation update`)

	return result, nil
}

// Резолвинг конфликтов удаления родительской сущности
func (c conflictResolver) ResolveDelete(
	parentTablePrimaryKeyValue string,
	tx *sql.Tx,
) ([]events.Event, error) {
	if 0 == len(c.fieldsConfiguration) {
		return []events.Event{}, nil
	}

	result := []events.Event{}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	for _, field := range c.fieldsConfiguration {
		starts := time.Now().UnixNano() / int64(time.Millisecond)
		dbConfig, ok := field.DbConfiguration.(configuration.RelationFieldConfiguration)
		if !ok {
			continue
		}

		eventsResult, err := c.fieldResolver.resolveDelete(
			parentTablePrimaryKeyValue,
			field,
			tx,
		)

		if nil != err {
			c.logger.WithFields(logrus.Fields{
				"code":   500,
				"err":    err,
				"entity": parentTablePrimaryKeyValue,
				"field":  field.Field,
				"table":  dbConfig.Table,
			}).Error(`Failed to resolve relation delete`)

			return nil, err
		}

		newResult := make([]events.Event, len(result)+len(eventsResult))
		newResultIndex := 0

		for i := range result {
			newResult[i] = result[i]
			newResultIndex++
		}

		for _, eventItem := range eventsResult {
			newResult[newResultIndex] = eventItem
			newResultIndex++
		}

		result = newResult

		finish := time.Now().UnixNano() / int64(time.Millisecond)
		c.logger.WithFields(logrus.Fields{
			"code":          200,
			"entity":        parentTablePrimaryKeyValue,
			"field":         field.Field,
			"table":         dbConfig.Table,
			"eventsResult":  eventsResult,
			"executionTime": finish - starts,
		}).Debug(`Resolved relation delete`)
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	c.logger.WithFields(logrus.Fields{
		"code":          200,
		"entity":        parentTablePrimaryKeyValue,
		"result":        result,
		"executionTime": finish - starts,
	}).Debug(`Resolved relation delete`)

	return result, nil
}
