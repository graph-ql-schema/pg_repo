package primaryChanges

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"database/sql"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

// Служба обработки основных изменений. Изменяет только основную таблицу.
type primaryChangesService struct {
	primaryTable           string
	primaryTablePrimaryKey string
	generator              queryGenerator.QueryGeneratorInterface
	executor               executor.QueryExecutorInterface
	log                    *logrus.Entry
}

// Конструктор сервиса
func NewPrimaryChangesService(
	primaryTable string,
	primaryTablePrimaryKey string,
	generator queryGenerator.QueryGeneratorInterface,
	executor executor.QueryExecutorInterface,
) *primaryChangesService {
	return &primaryChangesService{
		primaryTable:           primaryTable,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		generator:              generator,
		executor:               executor,
		log:                    helpers.NewLogger(fmt.Sprintf(`primaryChangesService(%v)`, primaryTable)),
	}
}

// Вставка одиночной сущности из GraphQL запроса
//
// Возвращает:
//  1. ID вставленной сущности (если есть)
//  2. Событие вставки (если возникло)
//  3. Ошибку (если произошла)
func (p primaryChangesService) InsertObject(
	ctx context.Context,
	object objectsParser.Object,
	tx *sql.Tx,
) (*string, *events.Event, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	insertQuery, args, err := p.generator.GenerateInsertQueryByGraphQlParameters(ctx, object)
	select {
	case <-ctx.Done():
		return nil, nil, nil
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' insert query: %v`, p.primaryTable, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":   400,
			"object": object,
			"error":  err,
		}).Warning(`Failed to generate insert query`)

		return nil, nil, err
	}

	if nil == insertQuery {
		err := fmt.Errorf(`'%v' insert query: failed to generate sql query`, p.primaryTable)
		p.log.WithFields(logrus.Fields{
			"code":   400,
			"object": object,
			"error":  err,
		}).Warning(`Failed to generate insert query`)

		return nil, nil, err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"object":        object,
		"query":         insertQuery,
		"executionTime": finish - starts,
	}).Debug(`Generated insert query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(*insertQuery, args, tx)
	finish = time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": insertQuery,
			"error": err,
		}).Warning(`Failed to execute insert query`)

		return nil, nil, err
	}

	defer rows.Close()
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"query":         insertQuery,
		"executionTime": finish - starts,
	}).Debug(`Executed insert query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	id := ""
	clearId := ""
	for rows.Next() {
		var rawId interface{}
		err := rows.Scan(&rawId)

		if nil != err {
			p.log.WithFields(logrus.Fields{
				"code":  500,
				"query": insertQuery,
				"error": err,
			}).Error(`Failed to parse insert query result`)

			return nil, nil, err
		}

		clearId = fmt.Sprintf(`%v`, rawId)
		switch parsedId := rawId.(type) {
		case string:
			id = fmt.Sprintf(`'%v'`, parsedId)
			break
		default:
			id = fmt.Sprintf(`%v`, parsedId)
		}
	}
	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"id":            id,
		"clearId":       clearId,
		"executionTime": finish - starts,
	}).Debug(`Parsed insert query`)

	if 0 == len(id) {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": insertQuery,
			"error": err,
		}).Warning(`Insert query is not returned id`)

		return nil, nil, fmt.Errorf(`insert query is not returned id`)
	}

	return &id, &events.Event{
		Type:            events.Created,
		Table:           p.primaryTable,
		PrimaryKey:      p.primaryTablePrimaryKey,
		PrimaryKeyValue: clearId,
	}, nil
}

// Обновление сущностей по переданным параметрам GraphQL
//
// Возвращает:
//  1. Срез ID обновшенных сущностей (если есть)
//  2. Срез событий вставки (если возникли)
//  3. Ошибку (если произошла)
func (p primaryChangesService) Update(
	params sbuilder.Parameters,
	tx *sql.Tx,
) ([]string, []events.Event, error) {
	ctx := params.Context
	if nil == ctx {
		ctx = context.Background()
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	updateQuery, args, err := p.generator.GenerateUpdateQueryByGraphQlParameters(ctx, params)

	select {
	case <-ctx.Done():
		return nil, nil, nil
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' update query: %v`, p.primaryTable, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"error":     err,
		}).Warning(`Failed to generate update query`)

		return nil, nil, err
	}

	if nil == updateQuery {
		p.log.WithFields(logrus.Fields{
			"code": 400,
		}).Warning(`Can't generate update query`)

		return nil, nil, nil
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"arguments":     params.Arguments,
		"query":         updateQuery,
		"executionTime": finish - starts,
	}).Debug(`Generated update query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(*updateQuery, args, tx)
	finish = time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": updateQuery,
			"error": err,
		}).Warning(`Failed to execute update query`)

		return nil, nil, err
	}

	defer rows.Close()
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"query":         updateQuery,
		"executionTime": finish - starts,
	}).Debug(`Executed update query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	ids := []string{}
	eventsResult := []events.Event{}
	for rows.Next() {
		var rawId interface{}
		err := rows.Scan(&rawId)

		if nil != err {
			p.log.WithFields(logrus.Fields{
				"code":  500,
				"query": updateQuery,
				"error": err,
			}).Error(`Failed to parse update query result`)

			return nil, nil, err
		}

		id := ""
		clearId := fmt.Sprintf(`%v`, rawId)
		switch parsedId := rawId.(type) {
		case string:
			id = fmt.Sprintf(`'%v'`, parsedId)
			break
		default:
			id = fmt.Sprintf(`%v`, parsedId)
		}

		ids = append(ids, id)
		eventsResult = append(eventsResult, events.Event{
			Type:            events.Updated,
			Table:           p.primaryTable,
			PrimaryKey:      p.primaryTablePrimaryKey,
			PrimaryKeyValue: clearId,
		})
	}
	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"ids":           ids,
		"events":        eventsResult,
		"executionTime": finish - starts,
	}).Debug(`Parsed update query`)

	return ids, eventsResult, nil
}

// Удаление сущностей по переданным параметрам GraphQL
//
// Возвращает:
//  1. Срез ID удаленных сущностей (если есть)
//  2. Срез событий вставки (если возникли)
//  3. Ошибку (если произошла)
func (p primaryChangesService) Delete(
	params sbuilder.Parameters,
	tx *sql.Tx,
) ([]string, []events.Event, error) {
	ctx := params.Context
	if nil == ctx {
		ctx = context.Background()
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	deleteQuery, args, err := p.generator.GenerateDeleteQueryByGraphQlParameters(ctx, params)

	select {
	case <-ctx.Done():
		return nil, nil, nil
	default:
	}

	if nil != err {
		err = fmt.Errorf(`'%v' delete query: %v`, p.primaryTable, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":      500,
			"arguments": params.Arguments,
			"error":     err,
		}).Error(`Failed to generate delete query`)

		return nil, nil, err
	}

	if nil == deleteQuery {
		err := fmt.Errorf(`'%v' delete query: failed to generate sql query`, p.primaryTable)
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"error":     err,
		}).Warning(`Failed to generate delete query`)

		return nil, nil, err
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":      100,
		"arguments": params.Arguments,
		"query":     deleteQuery,
	}).Debug(`Generated delete query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(*deleteQuery, args, tx)
	finish = time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": deleteQuery,
			"error": err,
		}).Warning(`Failed to execute delete query`)

		return nil, nil, err
	}

	defer rows.Close()
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"query":         deleteQuery,
		"executionTime": finish - starts,
	}).Debug(`Executed delete query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	ids := []string{}
	eventsResult := []events.Event{}
	for rows.Next() {
		var rawId interface{}
		err := rows.Scan(&rawId)

		if nil != err {
			p.log.WithFields(logrus.Fields{
				"code":  500,
				"query": deleteQuery,
				"error": err,
			}).Error(`Failed to parse delete query result`)

			return nil, nil, err
		}

		id := ""
		clearId := fmt.Sprintf(`%v`, rawId)
		switch parsedId := rawId.(type) {
		case string:
			id = fmt.Sprintf(`'%v'`, parsedId)
			break
		default:
			id = fmt.Sprintf(`%v`, parsedId)
		}

		ids = append(ids, id)
		eventsResult = append(eventsResult, events.Event{
			Type:            events.Deleted,
			Table:           p.primaryTable,
			PrimaryKey:      p.primaryTablePrimaryKey,
			PrimaryKeyValue: clearId,
		})
	}
	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":          100,
		"ids":           ids,
		"events":        eventsResult,
		"executionTime": finish - starts,
	}).Debug(`Parsed delete query`)

	return ids, eventsResult, nil
}
