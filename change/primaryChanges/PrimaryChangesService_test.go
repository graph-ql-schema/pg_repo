package primaryChanges

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"database/sql"
	"github.com/sirupsen/logrus"
	"reflect"
	"testing"
)

// Тестирование вставки объекта
func Test_primaryChangesService_InsertObject(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
	}
	type args struct {
		object objectsParser.Object
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		want1   *events.Event
		wantErr bool
	}{
		{
			name: "Генератор запроса вернул ошибку",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  true,
					GenerateInsertQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    "",
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Генератор запроса вернул пустой запрос",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  false,
					GenerateInsertQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    "",
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Ошибка выполнения запроса",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  false,
					GenerateInsertQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    "",
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Ошибка парсинга результата",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  false,
					GenerateInsertQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     true,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    "",
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  false,
					GenerateInsertQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: "1",
			want1: &events.Event{
				Type:            events.Created,
				Table:           "test",
				PrimaryKey:      "id",
				PrimaryKeyValue: "1",
			},
			wantErr: false,
		},
		{
			name: "Нет ошибок. Строковый ID.",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateInsertQueryByGraphQlParametersError:  false,
					GenerateInsertQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							`1`,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: "'1'",
			want1: &events.Event{
				Type:            events.Created,
				Table:           "test",
				PrimaryKey:      "id",
				PrimaryKeyValue: "1",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := primaryChangesService{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
			}
			got, got1, err := p.InsertObject(context.Background(), tt.args.object, tt.args.tx)
			var want *string
			if 0 != len(tt.want) {
				want = &tt.want
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("InsertObject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, want) {
				if nil != got {
					t.Errorf("InsertObject() got = %v, want %v", *got, tt.want)
				} else {
					t.Errorf("InsertObject() got = %v, want %v", got, want)
				}
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("InsertObject() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

// Тестирование обновления сущностей
func Test_primaryChangesService_Update(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		want1   []events.Event
		wantErr bool
	}{
		{
			name: "Генератор запроса вернул ошибку",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  true,
					GenerateUpdateQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Генератор запроса вернул пустой запрос",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  false,
					GenerateUpdateQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: false,
		},
		{
			name: "Ошибка выполнения запроса",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  false,
					GenerateUpdateQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Ошибка парсинга результата",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  false,
					GenerateUpdateQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     true,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  false,
					GenerateUpdateQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: []string{"1", "1"},
			want1: []events.Event{
				{
					Type:            events.Updated,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
				{
					Type:            events.Updated,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
		{
			name: "Нет ошибок. Строковый ID.",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateUpdateQueryByGraphQlParametersError:  false,
					GenerateUpdateQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							`1`,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: []string{"'1'", "'1'"},
			want1: []events.Event{
				{
					Type:            events.Updated,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
				{
					Type:            events.Updated,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := primaryChangesService{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
			}
			got, got1, err := p.Update(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Update() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

// Тестирование удаления сущностей
func Test_primaryChangesService_Delete(t *testing.T) {
	type fields struct {
		primaryTable           string
		primaryTablePrimaryKey string
		generator              queryGenerator.QueryGeneratorInterface
		executor               executor.QueryExecutorInterface
		log                    *logrus.Entry
	}
	type args struct {
		params sbuilder.Parameters
		tx     *sql.Tx
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		want1   []events.Event
		wantErr bool
	}{
		{
			name: "Генератор запроса вернул ошибку",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  true,
					GenerateDeleteQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Генератор запроса вернул пустой запрос",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  false,
					GenerateDeleteQueryByGraphQlParametersResult: "",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Ошибка выполнения запроса",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  false,
					GenerateDeleteQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: true,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       1,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Ошибка парсинга результата",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  false,
					GenerateDeleteQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     true,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args:    args{},
			want:    nil,
			want1:   nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  false,
					GenerateDeleteQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							1,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: []string{"1", "1"},
			want1: []events.Event{
				{
					Type:            events.Deleted,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
				{
					Type:            events.Deleted,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
		{
			name: "Нет ошибок. Строковый ID.",
			fields: fields{
				primaryTable:           "test",
				primaryTablePrimaryKey: "id",
				generator: queryGenerator.QueryGeneratorMock{
					GenerateDeleteQueryByGraphQlParametersError:  false,
					GenerateDeleteQueryByGraphQlParametersResult: "test",
				},
				executor: executor.QueryExecutorMock{
					IsExecuteListQueryError: false,
					ExecuteListQueryResult: &executor.RowsStub{
						Items:       2,
						IsError:     false,
						ColumnsPull: []string{"id"},
						ItemsToSet: []interface{}{
							`1`,
						},
					},
				},
				log: helpers.NewLogger(`test`),
			},
			args: args{},
			want: []string{"'1'", "'1'"},
			want1: []events.Event{
				{
					Type:            events.Deleted,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
				{
					Type:            events.Deleted,
					Table:           "test",
					PrimaryKey:      "id",
					PrimaryKeyValue: "1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := primaryChangesService{
				primaryTable:           tt.fields.primaryTable,
				primaryTablePrimaryKey: tt.fields.primaryTablePrimaryKey,
				generator:              tt.fields.generator,
				executor:               tt.fields.executor,
				log:                    tt.fields.log,
			}
			got, got1, err := p.Delete(tt.args.params, tt.args.tx)
			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Delete() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Delete() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
