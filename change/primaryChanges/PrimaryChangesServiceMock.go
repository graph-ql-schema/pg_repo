package primaryChanges

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type PrimaryChangesServiceMock struct {
	InsertObjectError bool
	InsertObjectId    string
	InsertObjectEvent *events.Event
	UpdateError       bool
	UpdateIds         []string
	UpdateEvents      []events.Event
	DeleteError       bool
	DeleteIds         []string
	DeleteEvents      []events.Event
}

// Вставка одиночной сущности из GraphQL запроса
//
// Возвращает:
//  1. ID вставленной сущности (если есть)
//  2. Событие вставки (если возникло)
//  3. Ошибку (если произошла)
func (p PrimaryChangesServiceMock) InsertObject(
	ctx context.Context,
	object objectsParser.Object,
	tx *sql.Tx,
) (*string, *events.Event, error) {
	if p.InsertObjectError {
		return nil, nil, fmt.Errorf(`test`)
	}

	return &p.InsertObjectId, p.InsertObjectEvent, nil
}

// Обновление сущностей по переданным параметрам GraphQL
//
// Возвращает:
//  1. Срез ID обновшенных сущностей (если есть)
//  2. Срез событий вставки (если возникли)
//  3. Ошибку (если произошла)
func (p PrimaryChangesServiceMock) Update(sbuilder.Parameters, *sql.Tx) ([]string, []events.Event, error) {
	if p.UpdateError {
		return nil, nil, fmt.Errorf(`test`)
	}

	return p.UpdateIds, p.UpdateEvents, nil
}

// Удаление сущностей по переданным параметрам GraphQL
//
// Возвращает:
//  1. Срез ID удаленных сущностей (если есть)
//  2. Срез событий вставки (если возникли)
//  3. Ошибку (если произошла)
func (p PrimaryChangesServiceMock) Delete(sbuilder.Parameters, *sql.Tx) ([]string, []events.Event, error) {
	if p.DeleteError {
		return nil, nil, fmt.Errorf(`test`)
	}

	return p.DeleteIds, p.DeleteEvents, nil
}
