package primaryChanges

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/objectsParser"
	"context"
	"database/sql"
)

// Служба обработки основных изменений. Изменяет только основную таблицу.
type PrimaryChangesServiceInterface interface {
	// Вставка одиночной сущности из GraphQL запроса
	//
	// Возвращает:
	//  1. ID вставленной сущности (если есть)
	//  2. Событие вставки (если возникло)
	//  3. Ошибку (если произошла)
	InsertObject(
		ctx context.Context,
		object objectsParser.Object,
		tx *sql.Tx,
	) (*string, *events.Event, error)

	// Обновление сущностей по переданным параметрам GraphQL
	//
	// Возвращает:
	//  1. Срез ID обновшенных сущностей (если есть)
	//  2. Срез событий вставки (если возникли)
	//  3. Ошибку (если произошла)
	Update(
		params sbuilder.Parameters,
		tx *sql.Tx,
	) ([]string, []events.Event, error)

	// Удаление сущностей по переданным параметрам GraphQL
	//
	// Возвращает:
	//  1. Срез ID удаленных сущностей (если есть)
	//  2. Срез событий вставки (если возникли)
	//  3. Ошибку (если произошла)
	Delete(
		params sbuilder.Parameters,
		tx *sql.Tx,
	) ([]string, []events.Event, error)
}
