package events

import (
	"context"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type eventsSubscriberMock struct {
	IsError        bool
	RollbackStatus bool
}

// Обработка возникшего события
func (e eventsSubscriberMock) Process(context.Context, Event, *sql.Tx) error {
	if e.IsError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Откат изменений
func (e *eventsSubscriberMock) Rollback(context.Context, error) {
	e.RollbackStatus = true
}
