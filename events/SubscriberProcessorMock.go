package events

import (
	"context"
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type SubscriberProcessorMock struct {
	IsError bool
}

// Обработка события подписчиком
func (s SubscriberProcessorMock) Process(context.Context, []Event, *sql.Tx, []EventsSubscriberInterface) error {
	if s.IsError {
		return fmt.Errorf(`test`)
	}

	return nil
}
