package events

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"context"
	"database/sql"
	"github.com/sirupsen/logrus"
	"runtime/debug"
)

// Обработчик событий. Передает их в подписчиков.
type subscriberProcessor struct {
	logger *logrus.Entry
}

// Конструктор обработчика
func NewSubscriberProcessor() SubscriberProcessorInterface {
	return &subscriberProcessor{
		logger: helpers.NewLogger(`SubscriberProcessor`),
	}
}

// Обработка события подписчиком
func (s subscriberProcessor) Process(ctx context.Context, events []Event, tx *sql.Tx, subscribers []EventsSubscriberInterface) error {
	defer func() {
		err := recover()
		if nil != err {
			s.logger.WithFields(logrus.Fields{
				"code":  500,
				"trace": string(debug.Stack()),
			}).Error(`Panic on processing events`)

			panic(err)
		}
	}()

	executedSubscribers := []EventsSubscriberInterface{}
	for _, subscriber := range subscribers {
		executedSubscribers = append(executedSubscribers, subscriber)
		for _, event := range events {
			err := subscriber.Process(ctx, event, tx)
			if nil == err {
				continue
			}

			for _, executed := range executedSubscribers {
				executed.Rollback(ctx, err)
			}

			return err
		}
	}

	return nil
}
