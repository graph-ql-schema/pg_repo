package events

import (
	"context"
	"database/sql"
)

// Тип возникшего события
type EventType string

const (
	Created EventType = "created"
	Updated EventType = "updated"
	Deleted EventType = "deleted"
)

// Сущность события
type Event struct {
	Type            EventType // Тип возникшего события
	Table           string    // Таблица, в которой произошло событие
	PrimaryKey      string    // Первичный ключ, по которому было выполнено изменение
	PrimaryKeyValue string    // Значение первичного ключа для измененной сущности
}

// Подписчик на событие
type EventsSubscriberInterface interface {
	// Обработка возникшего события
	Process(ctx context.Context, event Event, tx *sql.Tx) error

	// Откат изменений
	Rollback(ctx context.Context, err error)
}

// Обработчик событий. Передает их в подписчиков.
type SubscriberProcessorInterface interface {
	// Обработка события подписчиком
	Process(ctx context.Context, events []Event, tx *sql.Tx, subscribers []EventsSubscriberInterface) error
}
