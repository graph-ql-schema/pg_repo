package events

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"context"
	"database/sql"
	"testing"
)

// Тестирование обработки
func Test_subscriberProcessor_Process(t *testing.T) {
	type args struct {
		event       []Event
		tx          *sql.Tx
		subscribers []EventsSubscriberInterface
	}
	tests := []struct {
		name          string
		args          args
		wantErr       bool
		checkRollback bool
	}{
		{
			name: "Нет переданных подписчиков",
			args: args{
				event:       []Event{{}},
				tx:          nil,
				subscribers: []EventsSubscriberInterface{},
			},
			wantErr:       false,
			checkRollback: false,
		},
		{
			name: "Подписчик возвращает ошибку",
			args: args{
				event: []Event{{}},
				tx:    nil,
				subscribers: []EventsSubscriberInterface{
					&eventsSubscriberMock{
						IsError: true,
					},
				},
			},
			wantErr:       true,
			checkRollback: true,
		},
		{
			name: "Подписчик не возвращает ошибку",
			args: args{
				event: []Event{{}},
				tx:    nil,
				subscribers: []EventsSubscriberInterface{
					&eventsSubscriberMock{
						IsError: false,
					},
				},
			},
			wantErr:       false,
			checkRollback: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := subscriberProcessor{
				logger: helpers.NewLogger(`test`),
			}
			if err := s.Process(context.Background(), tt.args.event, tt.args.tx, tt.args.subscribers); (err != nil) != tt.wantErr {
				t.Errorf("Process() error = %v, wantErr %v", err, tt.wantErr)
			}

			if tt.checkRollback {
				for i, subscriber := range tt.args.subscribers {
					s := subscriber.(*eventsSubscriberMock)
					if !s.RollbackStatus {
						t.Errorf("Process() rollback is not called for subscriber %v", i+1)
					}
				}
			}
		})
	}
}
