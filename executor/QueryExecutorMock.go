package executor

import (
	"database/sql"
	"fmt"
)

// Подставка для тестирования
type QueryExecutorMock struct {
	IsExecuteChangeQueryError bool
	IsExecuteListQueryError   bool
	ExecuteListQueryResult    Rows
	IsStartTransactionError   bool
}

// Старт транзакции
func (q QueryExecutorMock) StartTransaction() (*sql.Tx, error) {
	if q.IsStartTransactionError {
		return nil, fmt.Errorf(`test`)
	}

	return nil, nil
}

// Выполнение запроса изменения данных
func (q QueryExecutorMock) ExecuteChangeQuery(string, []interface{}, *sql.Tx) error {
	if q.IsExecuteChangeQueryError {
		return fmt.Errorf(`test`)
	}

	return nil
}

// Выполнение sql запроса. Если транзакция не передана, создает новую.
func (q QueryExecutorMock) ExecuteListQuery(string, []interface{}, *sql.Tx) (Rows, error) {
	if q.IsExecuteListQueryError {
		return nil, fmt.Errorf(`test`)
	}

	return q.ExecuteListQueryResult, nil
}
