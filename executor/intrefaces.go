package executor

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
)

// Интерфейс строки ответа
type Rows interface {
	sqlx.ColScanner
	Next() bool
	Close() error
}

// Исполнитель SQL запросов
type QueryExecutorInterface interface {
	// Выполнение sql запроса. Если транзакция не передана, создает новую.
	ExecuteListQuery(query string, args []interface{}, tx *sql.Tx) (Rows, error)

	// Выполнение запроса изменения данных
	ExecuteChangeQuery(query string, args []interface{}, tx *sql.Tx) error

	// Старт транзакции
	StartTransaction() (*sql.Tx, error)
}
