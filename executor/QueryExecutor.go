package executor

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

// Исполнитель SQL запросов
type queryExecutor struct {
	client *sqlx.DB
}

// Старт транзакции
func (q queryExecutor) StartTransaction() (*sql.Tx, error) {
	return q.client.Begin()
}

// Выполнение запроса изменения данных
func (q queryExecutor) ExecuteChangeQuery(query string, args []interface{}, tx *sql.Tx) error {
	logrus.WithFields(logrus.Fields{
		"query": query,
		"args":  args,
	}).Debug(`Executing query`)

	if nil != tx {
		_, err := tx.Exec(query, args...)
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"query": query,
			}).Error(`Failed to execute query`)
		}

		return err
	}

	_, err := q.client.Exec(query, args...)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"query": query,
		}).Error(`Failed to execute query`)
	}

	return err
}

// Выполнение sql запроса. Если транзакция не передана, создает новую.
func (q queryExecutor) ExecuteListQuery(query string, args []interface{}, tx *sql.Tx) (Rows, error) {
	logrus.WithFields(logrus.Fields{
		"query": query,
		"args":  args,
	}).Debug(`Executing query`)

	if nil != tx {
		rows, err := tx.Query(query, args...)
		if nil != err {
			logrus.WithFields(logrus.Fields{
				"query": query,
				"args":  args,
			}).Error(`Failed to execute query`)
		}

		return rows, err
	}

	rows, err := q.client.Query(query, args...)
	if nil != err {
		logrus.WithFields(logrus.Fields{
			"query": query,
		}).Error(`Failed to execute query`)

		return nil, err
	}

	return rows, nil
}

// Фабрика исполнителя
func NewQueryExecutor(client *sqlx.DB) QueryExecutorInterface {
	return &queryExecutor{
		client: client,
	}
}
