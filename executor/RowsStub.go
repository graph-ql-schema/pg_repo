package executor

import (
	"errors"
	"fmt"
	"reflect"
)

// Подставка для тестирования
type RowsStub struct {
	Items       int
	current     int
	IsError     bool
	ColumnsPull []string
	ItemsToSet  []interface{}
}

func (r RowsStub) Close() error {
	return nil
}

func (r RowsStub) Columns() ([]string, error) {
	if r.IsError {
		return nil, fmt.Errorf(`test`)
	}

	return r.ColumnsPull, nil
}

func (r RowsStub) Scan(variables ...interface{}) error {
	if r.IsError {
		return fmt.Errorf(`test`)
	}

	if 0 == len(r.ItemsToSet) {
		return nil
	}

	for i := range variables {
		// Эта часть взята из библиотеки sql. Служит для реализации заполнения значений.
		sv := reflect.ValueOf(r.ItemsToSet[i])
		dpv := reflect.ValueOf(variables[i])

		if dpv.Kind() != reflect.Ptr {
			return errors.New("destination not a pointer")
		}
		if dpv.IsNil() {
			return errors.New("destination is nil")
		}

		dv := reflect.Indirect(dpv)
		if sv.IsValid() && sv.Type().AssignableTo(dv.Type()) {
			dv.Set(sv)
		}

		if dv.Kind() == sv.Kind() && sv.Type().ConvertibleTo(dv.Type()) {
			dv.Set(sv.Convert(dv.Type()))
		}
	}

	return nil
}

func (r RowsStub) Err() error {
	if r.IsError {
		return fmt.Errorf(`test`)
	}

	return nil
}

func (r *RowsStub) Next() bool {
	if r.current == r.Items {
		return false
	}

	r.current++

	return true
}
