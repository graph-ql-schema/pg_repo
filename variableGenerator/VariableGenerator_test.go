package variableGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"fmt"
	"testing"
)

func Test_variableGenerator_SetVariables(t *testing.T) {
	type args struct {
		sql string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Тестирование замены переменных",
			args: args{
				sql: fmt.Sprintf("select * from table where id = %v and name like %v", consts.ArgumentPlaceholder, consts.ArgumentPlaceholder),
			},
			want: "select * from table where id = $1 and name like $2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := variableGenerator{}
			if got := v.SetVariables(tt.args.sql); got != tt.want {
				t.Errorf("SetVariables() = %v, want %v", got, tt.want)
			}
		})
	}
}
