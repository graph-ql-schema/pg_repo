package variableGenerator

// Singleton сервиса
var VariableGenerator VariableGeneratorInterface

// Инициализируем Singleton сервиса
func init() {
	VariableGenerator = newVariableGenerator()
}
