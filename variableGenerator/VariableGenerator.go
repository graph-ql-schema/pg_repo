package variableGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"fmt"
	"strings"
)

// Сервис генерации переменных по подставкам для запросов SQL
type variableGenerator struct{}

// Замена подставок переменной в передаваемом SQL на реальные переменные
func (v variableGenerator) SetVariables(sql string) string {
	index := 0
	for strings.Index(sql, consts.ArgumentPlaceholder) != -1 {
		index = index + 1
		sql = strings.Replace(sql, consts.ArgumentPlaceholder, fmt.Sprintf(`$%v`, index), 1)
	}

	return sql
}

// Конмтруктор сервиса
func newVariableGenerator() VariableGeneratorInterface {
	return &variableGenerator{}
}
