package filter

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"context"
)

// Генератор параметров фильтрации
type WhereGeneratorInterface interface {
	// Генерация запроса
	Build(ctx context.Context, operation whereOrHavingParser.Operation) (*string, []interface{}, error)

	// Получение операции сравнения из переданного GraphQL запроса
	GetOperation(params types.Parameters) whereOrHavingParser.Operation
}

// Процессор обработки параметров генерации
type whereProcessorInterface interface {
	// Установка корневого генератора параметров фильтрации
	setRootGenerator(generator WhereGeneratorInterface)

	// Проверка доступности процессора
	isAvailable(operation whereOrHavingParser.Operation) bool

	// Генерация фильтра
	build(
		ctx context.Context,
		operation whereOrHavingParser.Operation,
		fieldConfiguration *configuration.FieldsConfiguration,
	) (*string, []interface{}, error)
}
