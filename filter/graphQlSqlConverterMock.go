package filter

import (
	"fmt"

	"github.com/graphql-go/graphql"
)

// Подставка для тестирования
type graphQlSqlConverterMock struct {
	IsSqlErr bool
}

// Конвертация в базовый тип, например в строку или число
func (g graphQlSqlConverterMock) ToBaseType(object *graphql.Object, field string, value interface{}) (interface{}, error) {
	if g.IsSqlErr {
		return "", fmt.Errorf(`test`)
	}

	return value, nil
}

// Конвертация в SQL like значение
func (g graphQlSqlConverterMock) ToSQLValue(object *graphql.Object, field string, value interface{}) (string, error) {
	if g.IsSqlErr {
		return "", fmt.Errorf(`test`)
	}

	return fmt.Sprintf(`%v`, value), nil
}
