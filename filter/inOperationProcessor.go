package filter

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"context"
	"fmt"
	"github.com/graphql-go/graphql"
	"reflect"
	"strings"
)

// Процессор операции IN
type inOperationProcessor struct {
	object             *graphql.Object
	sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
}

// Установка корневого генератора параметров фильтрации
func (i inOperationProcessor) setRootGenerator(WhereGeneratorInterface) {}

// Проверка доступности процессора
func (i inOperationProcessor) isAvailable(operation whereOrHavingParser.Operation) bool {
	return operation.Type() == constants.InSchemaKey
}

// Генерация фильтра
func (i inOperationProcessor) build(
	ctx context.Context,
	operation whereOrHavingParser.Operation,
	fieldConfiguration *configuration.FieldsConfiguration,
) (sql *string, args []interface{}, err error) {
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil == fieldConfiguration {
		err = fmt.Errorf(`field "%v" - operation 'IN': you should pass field configuration`, operation.Field())
		return
	}

	if reflect.TypeOf(operation.Value()).Kind() != reflect.Slice {
		err = fmt.Errorf(`field '%v' - operation 'IN': you should pass array of values to IN filter`, fieldConfiguration.GraphQlName)
		return
	}

	valuesStr, args, withNull, err := i.getValuesString(ctx, operation.Value(), fieldConfiguration)
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil != err {
		err = fmt.Errorf(`field '%v' - operation 'IN': %v`, fieldConfiguration.GraphQlName, err.Error())
		return
	}

	sqlStr := i.operationGenerator.Generate(*fieldConfiguration, valuesStr, func(field string, value string) string {
		sqlParts := make([]string, 0, 2)
		if 0 != len(value) {
			sqlParts = append(sqlParts, fmt.Sprintf(`%v in %v`, field, value))
		}

		if withNull {
			sqlParts = append(sqlParts, fmt.Sprintf(`%v is null`, field))
		}

		sqlStr := fmt.Sprintf(`%v`, strings.Join(sqlParts, " or "))
		if 2 == len(sqlParts) {
			sqlStr = fmt.Sprintf(`(%v)`, sqlStr)
		}

		return sqlStr
	})

	if sqlStr == "" {
		err = fmt.Errorf(`field '%v' - operation 'IN': failed to generate operation. no available processors for this configuration`, fieldConfiguration.GraphQlName)
		return
	}

	sql = &sqlStr

	return
}

// Генерация строки значений для операции
func (i inOperationProcessor) getValuesString(
	ctx context.Context,
	values interface{},
	field *configuration.FieldsConfiguration,
) (sql string, args []interface{}, hasNull bool, err error) {
	s := reflect.ValueOf(values)
	if 0 == s.Len() {
		return
	}

	args = make([]interface{}, 0, s.Len())
	sqlParts := make([]string, 0, s.Len())

	for j := 0; j < s.Len(); j++ {
		select {
		case <-ctx.Done():
			return
		default:
		}

		var item interface{}
		switch true {
		case s.Index(j).Interface() == nil:
			hasNull = true
			continue
		default:
			item, err = i.sqlValueConverter.ToBaseType(i.object, field.GraphQlName, s.Index(j).Interface())
		}

		if nil != err {
			err = fmt.Errorf(`item[%v]: %v`, j, err.Error())
			return
		}

		args = append(args, item)
		sqlParts = append(sqlParts, consts.ArgumentPlaceholder)
	}

	if 0 == len(sqlParts) {
		return
	}

	sql = fmt.Sprintf(`(%v)`, strings.Join(sqlParts, ", "))
	return
}

// Фабрика процессора
func newInOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &inOperationProcessor{
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}
