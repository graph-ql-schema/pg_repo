package filter

import (
	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"context"
	"fmt"
	"github.com/graphql-go/graphql"
)

// Структура процессора
type simpleOperationProcessor struct {
	operationType      string
	operator           string
	object             *graphql.Object
	sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
}

// Установка корневого генератора параметров фильтрации
func (s simpleOperationProcessor) setRootGenerator(WhereGeneratorInterface) {}

// Проверка доступности процессора
func (s simpleOperationProcessor) isAvailable(operation whereOrHavingParser.Operation) bool {
	return operation.Type() == s.operationType
}

// Генерация фильтра
func (s simpleOperationProcessor) build(
	_ context.Context,
	operation whereOrHavingParser.Operation,
	fieldConfiguration *configuration.FieldsConfiguration,
) (sql *string, args []interface{}, err error) {
	if nil == fieldConfiguration {
		err = fmt.Errorf(`field "%v" - operation '%v' you should pass field configuration`, operation.Field(), s.operationType)
		return
	}

	value, err := s.sqlValueConverter.ToBaseType(s.object, fieldConfiguration.GraphQlName, operation.Value())
	if nil != err {
		err = fmt.Errorf(`field '%v' - operation '%v': %v`, fieldConfiguration.GraphQlName, s.operationType, err.Error())
		return
	}

	args = []interface{}{}
	sqlValue := "null"

	if value != nil {
		sqlValue = "value"
		args = append(args, value)
	}

	sqlStr := s.operationGenerator.Generate(*fieldConfiguration, sqlValue, func(field string, value string) string {
		if value == "null" {
			return fmt.Sprintf(`%v is null`, field)
		}

		return fmt.Sprintf(`%v %v %v`, field, s.operator, consts.ArgumentPlaceholder)
	})

	if sqlStr == "" {
		err = fmt.Errorf(`field '%v' - operation '%v': failed to generate operation. no available processors for this configuration`, fieldConfiguration.GraphQlName, s.operationType)
		return
	}

	sql = &sqlStr
	return
}

// Фабрика процессора операции Equals
func newEqualsOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.EqualsSchemaKey,
		operator:           "=",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}

// Фабрика процессора операции More
func newMoreOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.MoreSchemaKey,
		operator:           ">",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}

// Фабрика процессора операции More or Equals
func newMoreOrEqualsOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.MoreOrEqualsSchemaKey,
		operator:           ">=",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}

// Фабрика процессора операции Less
func newLessOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.LessSchemaKey,
		operator:           "<",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}

// Фабрика процессора операции Less or Equals
func newLessOrEqualsOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.LessOrEqualsSchemaKey,
		operator:           "<=",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}

// Фабрика процессора операции Like
func newLikeOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &simpleOperationProcessor{
		operationType:      constants.LikeSchemaKey,
		operator:           "ilike",
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}
