package filter

import (
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Процессор операций NOT
type notOperationProcessor struct {
	generator WhereGeneratorInterface
}

// Установка корневого генератора параметров фильтрации
func (n *notOperationProcessor) setRootGenerator(generator WhereGeneratorInterface) {
	n.generator = generator
}

// Проверка доступности процессора
func (n *notOperationProcessor) isAvailable(operation whereOrHavingParser.Operation) bool {
	return operation.Type() == constants.NotSchemaKey
}

// Генерация фильтра
func (n *notOperationProcessor) build(
	ctx context.Context,
	operation whereOrHavingParser.Operation,
	fieldConfiguration *configuration.FieldsConfiguration,
) (sql *string, args []interface{}, err error) {
	subOperation, ok := operation.Value().(whereOrHavingParser.Operation)
	if !ok {
		err = fmt.Errorf(`operation 'NOT': you should pass a sub operation`)
		return
	}

	sql, args, err = n.generator.Build(ctx, subOperation)
	if nil != err {
		sql = nil
		err = fmt.Errorf(`operation 'NOT' -> %v`, err.Error())
		return
	}

	if nil == sql || 0 == len(*sql) {
		sql = nil
		err = fmt.Errorf(`operation 'NOT': sub operations has empty query`)
		return
	}

	result := fmt.Sprintf(`not(%v)`, *sql)
	sql = &result

	return
}

// Фабрика процессора
func newNotOperationProcessor() whereProcessorInterface {
	return &notOperationProcessor{}
}
