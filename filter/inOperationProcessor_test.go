package filter

import (
	"context"
	"reflect"
	"testing"

	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/graphql-go/graphql"
)

// Тестирование доступности процессора
func Test_inOperationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		object             *graphql.Object
		sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
		operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
	}
	type args struct {
		operation whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на доступной операции",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, 1, "data", "="),
			},
			want: true,
		},
		{
			name: "Тестирование на не доступной операции",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.EqualsSchemaKey, 1, "data", "="),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperationProcessor{
				object:             tt.fields.object,
				sqlValueConverter:  tt.fields.sqlValueConverter,
				operationGenerator: tt.fields.operationGenerator,
			}
			if got := i.isAvailable(tt.args.operation); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_inOperationProcessor_build(t *testing.T) {
	res := "test"
	type fields struct {
		object             *graphql.Object
		sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
		operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
	}
	type args struct {
		operation          whereOrHavingParser.Operation
		fieldConfiguration *configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если передана пустая конфигурация поля",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation:          whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, []int{1}, "data", "="),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если значение операции не является массивом",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, 1, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_obj",
					DbConfiguration: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если конвертер значений возвращает ошибку",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: true,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, []int{1, 2}, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_obj",
					DbConfiguration: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если генератор сравнений возвращает пустое значение",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, []int{1, 2}, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_obj",
					DbConfiguration: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation(constants.InSchemaKey, []int{1, 2}, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_obj",
					DbConfiguration: nil,
				},
			},
			want:    &res,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := inOperationProcessor{
				object:             tt.fields.object,
				sqlValueConverter:  tt.fields.sqlValueConverter,
				operationGenerator: tt.fields.operationGenerator,
			}
			got, _, err := i.build(context.Background(), tt.args.operation, tt.args.fieldConfiguration)
			if (err != nil) != tt.wantErr {
				t.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				val := "nil"
				if nil != got {
					val = *got
				}

				t.Errorf("build() got = %v, want %v", val, *tt.want)
			}
		})
	}
}
