package filter

import (
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Подставка для тестирования
type whereProcessorMock struct {
	IsAvailable  bool
	isBuildError bool
	BuildResult  *string
}

// Установка корневого генератора параметров фильтрации
func (w whereProcessorMock) setRootGenerator(WhereGeneratorInterface) {}

// Проверка доступности процессора
func (w whereProcessorMock) isAvailable(whereOrHavingParser.Operation) bool {
	return w.IsAvailable
}

// Генерация фильтра
func (w whereProcessorMock) build(context.Context, whereOrHavingParser.Operation, *configuration.FieldsConfiguration) (*string, []interface{}, error) {
	if w.isBuildError {
		return nil, nil, fmt.Errorf(`test`)
	}

	return w.BuildResult, nil, nil
}
