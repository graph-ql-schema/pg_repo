package simpleOperationGenerator

import (
	"fmt"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации сравнения для простого поля
type scalarProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (s scalarProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	if _, ok := fieldConfiguration.DbConfiguration.(configuration.ScalarFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация операции
func (s scalarProcessor) generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string {
	dbConf := fieldConfiguration.DbConfiguration.(configuration.ScalarFieldConfiguration)
	return operationCallback(
		fmt.Sprintf(`%v.%v`, s.primaryTableAlias, dbConf.DbName),
		value,
	)
}

// Фабрика процессора
func newScalarProcessor(primaryTableAlias string) generationProcessorInterface {
	return &scalarProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
