package simpleOperationGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Генератор простых операций сравнения для генератора фильтра
type simpleOperationGenerator struct {
	processors []generationProcessorInterface
}

// Генерация операция сравнения для поля
func (s simpleOperationGenerator) Generate(
	fieldConfiguration configuration.FieldsConfiguration,
	value string,
	operationCallback TOperationGenerator,
) string {
	for _, processor := range s.processors {
		if processor.isAvailable(fieldConfiguration) {
			return processor.generate(fieldConfiguration, value, operationCallback)
		}
	}

	return ""
}

// Фабрика генератора
func NewSimpleOperationGenerator(primaryTableAlias string) SimpleOperationGeneratorInterface {
	return &simpleOperationGenerator{
		processors: []generationProcessorInterface{
			newScalarProcessor(primaryTableAlias),
			newArrayProcessor(primaryTableAlias),
			newRelationProcessor(primaryTableAlias),
			newTableProcessor(primaryTableAlias),
		},
	}
}
