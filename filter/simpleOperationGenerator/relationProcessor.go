package simpleOperationGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации операции сравнения для отношения 1 to Many
type relationProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (r relationProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	if _, ok := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration); ok {
		return true
	}

	return false
}

// Проверка доступности процессора
func (r relationProcessor) generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.RelationFieldConfiguration)
	tableAlias := fmt.Sprintf(`%v_relation`, strings.Replace(dbConfig.Table, ".", "_", -1))

	return fmt.Sprintf(
		`exists(select %v.%v from %v %v where %v and %v.%v = %v.%v)`,
		tableAlias,
		dbConfig.PrimaryKey,
		dbConfig.Table,
		tableAlias,
		operationCallback(fmt.Sprintf(`%v.%v`, tableAlias, dbConfig.PrimaryKey), value),
		tableAlias,
		dbConfig.ForeignKey,
		r.primaryTableAlias,
		dbConfig.LocalKey,
	)
}

// Фабрика процессора
func newRelationProcessor(primaryTableAlias string) generationProcessorInterface {
	return &relationProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
