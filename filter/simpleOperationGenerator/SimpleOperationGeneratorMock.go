package simpleOperationGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type SimpleOperationGeneratorMock struct {
	GenerationResult string
}

// Генерация операция сравнения для поля
func (s SimpleOperationGeneratorMock) Generate(configuration.FieldsConfiguration, string, TOperationGenerator) string {
	return s.GenerationResult
}
