package simpleOperationGenerator

import (
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование генерации
func Test_simpleOperationGenerator_Generate(t *testing.T) {
	type fields struct {
		processors []generationProcessorInterface
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
		value              string
		operationCallback  TOperationGenerator
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование с доступным процессором",
			fields: fields{
				processors: []generationProcessorInterface{
					generationProcessorMock{
						IsAvailable:      true,
						GenerationResult: "test_gen",
					},
				},
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:           "test_f",
					FieldNum:        0,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
				value:             "test",
				operationCallback: nil,
			},
			want: "test_gen",
		},
		{
			name: "Тестирование без доступного процессора",
			fields: fields{
				processors: []generationProcessorInterface{
					generationProcessorMock{
						IsAvailable:      false,
						GenerationResult: "test_gen",
					},
				},
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:           "test_f",
					FieldNum:        0,
					GraphQlName:     "tt",
					DbConfiguration: nil,
				},
				value:             "test",
				operationCallback: nil,
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperationGenerator{
				processors: tt.fields.processors,
			}
			if got := s.Generate(tt.args.fieldConfiguration, tt.args.value, tt.args.operationCallback); got != tt.want {
				t.Errorf("Generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
