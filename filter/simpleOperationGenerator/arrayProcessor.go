package simpleOperationGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации фильтра по массиву значений
type arrayProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (a arrayProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	if _, ok := fieldConfiguration.DbConfiguration.(configuration.ArrayFieldConfiguration); ok {
		return true
	}

	return false
}

// Генерация операции
func (a arrayProcessor) generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.ArrayFieldConfiguration)
	tableAlias := fmt.Sprintf(`%v_array_unnested_value`, strings.Replace(dbConfig.DbName, ".", "_", -1))
	fieldAlias := fmt.Sprintf(`%v_val`, strings.Replace(dbConfig.DbName, ".", "_", -1))

	return fmt.Sprintf(
		`exists(select %v.%v from (select unnest(%v.%v) as %v) %v where %v)`,
		tableAlias,
		fieldAlias,
		a.primaryTableAlias,
		dbConfig.DbName,
		fieldAlias,
		tableAlias,
		operationCallback(fmt.Sprintf(`%v.%v`, tableAlias, fieldAlias), value),
	)
}

// Фабрика процессора
func newArrayProcessor(primaryTableAlias string) generationProcessorInterface {
	return &arrayProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
