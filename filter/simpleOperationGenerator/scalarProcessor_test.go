package simpleOperationGenerator

import (
	"fmt"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_scalarProcessor_isAvailable(t *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ArrayFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := scalarProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := s.isAvailable(tt.args.fieldConfiguration); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_scalarProcessor_generate(t *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
		value              string
		operationCallback  TOperationGenerator
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
				value: "'123'",
				operationCallback: func(field string, value string) string {
					return fmt.Sprintf(`%v = %v`, field, value)
				},
			},
			want: "ttt.test = '123'",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := scalarProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := s.generate(tt.args.fieldConfiguration, tt.args.value, tt.args.operationCallback); got != tt.want {
				t.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
