package simpleOperationGenerator

import (
	"fmt"
	"strings"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Процессор генерации сравнения значений для отношений Many to Many
type tableProcessor struct {
	primaryTableAlias string
}

// Проверка доступности процессора
func (t tableProcessor) isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool {
	if _, ok := fieldConfiguration.DbConfiguration.(configuration.TableFieldConfiguration); ok {
		return true
	}

	return false
}

// Проверка доступности процессора
func (t tableProcessor) generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string {
	dbConfig := fieldConfiguration.DbConfiguration.(configuration.TableFieldConfiguration)
	tableAlias := fmt.Sprintf(`%v_relation`, strings.Replace(dbConfig.Table, ".", "_", -1))

	return fmt.Sprintf(
		`exists(select %v.%v from %v %v where %v and %v.%v = %v.%v)`,
		tableAlias,
		dbConfig.Target,
		dbConfig.Table,
		tableAlias,
		operationCallback(fmt.Sprintf(`%v.%v`, tableAlias, dbConfig.Target), value),
		tableAlias,
		dbConfig.ForeignKey,
		t.primaryTableAlias,
		dbConfig.LocalKey,
	)
}

// Фабрика процессора
func newTableProcessor(primaryTableAlias string) generationProcessorInterface {
	return &tableProcessor{
		primaryTableAlias: primaryTableAlias,
	}
}
