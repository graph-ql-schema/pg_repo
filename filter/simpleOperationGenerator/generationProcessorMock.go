package simpleOperationGenerator

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Подставка для тестирования
type generationProcessorMock struct {
	IsAvailable      bool
	GenerationResult string
}

// Проверка доступности процессора
func (g generationProcessorMock) isAvailable(configuration.FieldsConfiguration) bool {
	return g.IsAvailable
}

// Генерация операции
func (g generationProcessorMock) generate(configuration.FieldsConfiguration, string, TOperationGenerator) string {
	return g.GenerationResult
}
