package simpleOperationGenerator

import (
	"fmt"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
)

// Тестирование доступности процессора
func Test_tableProcessor_isAvailable(t1 *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на не валидном значении",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.ScalarFieldConfiguration{
						DbName: "test",
					},
				},
			},
			want: false,
		},
		{
			name: "Тестирование на валидном значении",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "rel_tbl",
						Target:     "id",
						ForeignKey: "test_id",
						LocalKey:   "uuid",
					},
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := t.isAvailable(tt.args.fieldConfiguration); got != tt.want {
				t1.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_tableProcessor_generate(t1 *testing.T) {
	type fields struct {
		primaryTableAlias string
	}
	type args struct {
		fieldConfiguration configuration.FieldsConfiguration
		value              string
		operationCallback  TOperationGenerator
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name: "Тестирование генерации",
			fields: fields{
				primaryTableAlias: "ttt",
			},
			args: args{
				fieldConfiguration: configuration.FieldsConfiguration{
					Field:       "test",
					FieldNum:    0,
					GraphQlName: "test_1",
					DbConfiguration: configuration.TableFieldConfiguration{
						Table:      "rel_tbl",
						Target:     "id",
						ForeignKey: "test_id",
						LocalKey:   "uuid",
					},
				},
				value: "'123'",
				operationCallback: func(field string, value string) string {
					return fmt.Sprintf(`%v = %v`, field, value)
				},
			},
			want: "exists(select rel_tbl_relation.id from rel_tbl rel_tbl_relation where rel_tbl_relation.id = '123' and rel_tbl_relation.test_id = ttt.uuid)",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := tableProcessor{
				primaryTableAlias: tt.fields.primaryTableAlias,
			}
			if got := t.generate(tt.args.fieldConfiguration, tt.args.value, tt.args.operationCallback); got != tt.want {
				t1.Errorf("generate() = %v, want %v", got, tt.want)
			}
		})
	}
}
