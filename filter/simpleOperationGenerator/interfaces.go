package simpleOperationGenerator

import "bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"

// Генератор операции
type TOperationGenerator = func(field string, value string) string

// Генератор простых операций сравнения для генератора фильтра
type SimpleOperationGeneratorInterface interface {
	// Генерация операция сравнения для поля
	Generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string
}

// Процессор генерации операции
type generationProcessorInterface interface {
	// Проверка доступности процессора
	isAvailable(fieldConfiguration configuration.FieldsConfiguration) bool

	// Генерация операции
	generate(fieldConfiguration configuration.FieldsConfiguration, value string, operationCallback TOperationGenerator) string
}
