package filter

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
)

// Тестирование генерации подзапроса
func Test_whereGenerator_Build(t *testing.T) {
	test := "test"
	type fields struct {
		configuration []configuration.FieldsConfiguration
		processors    []whereProcessorInterface
	}
	type args struct {
		operation whereOrHavingParser.Operation
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если не передана операция",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []whereProcessorInterface{
					whereProcessorMock{
						IsAvailable:  true,
						isBuildError: false,
						BuildResult:  &test,
					},
				},
			},
			args: args{
				operation: nil,
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "Если нет процессора",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []whereProcessorInterface{},
			},
			args: args{
				operation: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если нет доступного процессора",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []whereProcessorInterface{
					whereProcessorMock{
						IsAvailable:  false,
						isBuildError: false,
						BuildResult:  &test,
					},
				},
			},
			args: args{
				operation: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Процессор возвращает ошибку",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []whereProcessorInterface{
					whereProcessorMock{
						IsAvailable:  true,
						isBuildError: true,
						BuildResult:  &test,
					},
				},
			},
			args: args{
				operation: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{
					{
						Field:           "test",
						FieldNum:        0,
						GraphQlName:     "test_f",
						DbConfiguration: nil,
					},
				},
				processors: []whereProcessorInterface{
					whereProcessorMock{
						IsAvailable:  true,
						isBuildError: false,
						BuildResult:  &test,
					},
				},
			},
			args: args{
				operation: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
			},
			want:    &test,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereGenerator{
				configuration: tt.fields.configuration,
				processors:    tt.fields.processors,
			}
			got, _, err := w.Build(context.Background(), tt.args.operation)
			if (err != nil) != tt.wantErr {
				t.Errorf("Build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Build() got = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование получения операции
func Test_whereGenerator_GetOperation(t *testing.T) {
	type fields struct {
		configuration []configuration.FieldsConfiguration
		processors    []whereProcessorInterface
	}
	type args struct {
		params types.Parameters
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   whereOrHavingParser.Operation
	}{
		{
			name: "Когда операций нет",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				processors:    []whereProcessorInterface{},
			},
			args: args{
				params: types.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Where:  nil,
						Having: nil,
					},
				},
			},
			want: nil,
		},
		{
			name: "Когда задана только Where",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				processors:    []whereProcessorInterface{},
			},
			args: args{
				params: types.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Where:  whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
						Having: nil,
					},
				},
			},
			want: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
		},
		{
			name: "Когда задана только Having",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				processors:    []whereProcessorInterface{},
			},
			args: args{
				params: types.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Where:  nil,
						Having: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
					},
				},
			},
			want: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
		},
		{
			name: "Когда заданы Where и Having",
			fields: fields{
				configuration: []configuration.FieldsConfiguration{},
				processors:    []whereProcessorInterface{},
			},
			args: args{
				params: types.Parameters{
					Arguments: argumentsParser.ParsedArguments{
						Where:  whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
						Having: whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
					},
				},
			},
			want: whereOrHavingParser.NewAndOrOperation(
				constants.AndSchemaKey,
				[]whereOrHavingParser.Operation{
					whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
					whereOrHavingParser.NewBetweenOperation(whereOrHavingParser.BetweenValue{1, 2}, "test"),
				},
				constants.AndSchemaKey,
			),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := whereGenerator{
				configuration: tt.fields.configuration,
				processors:    tt.fields.processors,
			}
			if got := w.GetOperation(tt.args.params); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOperation() = %v, want %v", got, tt.want)
			}
		})
	}
}
