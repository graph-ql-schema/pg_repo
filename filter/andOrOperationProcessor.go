package filter

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"context"
	"fmt"
	"strings"
)

// Процессор операций AND и OR
type andOrOperationProcessor struct {
	generator           WhereGeneratorInterface
	availableOperations []string
	operator            string
}

// Установка корневого генератора параметров фильтрации
func (a *andOrOperationProcessor) setRootGenerator(generator WhereGeneratorInterface) {
	a.generator = generator
}

// Проверка доступности процессора
func (a *andOrOperationProcessor) isAvailable(operation whereOrHavingParser.Operation) bool {
	for _, operationCode := range a.availableOperations {
		if operationCode == operation.Type() {
			return true
		}
	}

	return false
}

// Генерация фильтра
func (a *andOrOperationProcessor) build(
	ctx context.Context,
	operation whereOrHavingParser.Operation,
	fieldConfiguration *configuration.FieldsConfiguration,
) (*string, []interface{}, error) {
	operations, ok := operation.Value().([]whereOrHavingParser.Operation)
	if !ok {
		return nil, nil, fmt.Errorf(`operation '%v': you should pass an array of sub operations`, operation.Type())
	}

	if 0 == len(operations) {
		return nil, nil, nil
	}

	sqlParts := []string{}
	arguments := []interface{}{}
	for _, operation := range operations {
		select {
		case <-ctx.Done():
			return nil, nil, nil
		default:
		}

		sql, args, err := a.generator.Build(ctx, operation)
		if nil != err {
			return nil, nil, fmt.Errorf(`operation '%v' -> %v`, operation.Type(), err.Error())
		}

		sqlParts = append(sqlParts, fmt.Sprintf(`(%v)`, *sql))
		arguments = append(arguments, args...)
	}

	result := strings.Join(sqlParts, fmt.Sprintf(` %v `, a.operator))

	return &result, arguments, nil
}

// Фабрика процессора AND
func newAndOperationProcessor() whereProcessorInterface {
	return &andOrOperationProcessor{
		generator: nil,
		availableOperations: []string{
			constants.AndSchemaKey,
			constants.WhereSchemaKey,
			constants.HavingSchemaKey,
		},
		operator: "and",
	}
}

// Фабрика процессора OR
func newOrOperationProcessor() whereProcessorInterface {
	return &andOrOperationProcessor{
		generator: nil,
		availableOperations: []string{
			constants.OrSchemaKey,
		},
		operator: "or",
	}
}
