package filter

import (
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
)

// Подставка для тестирования
type WhereGeneratorMock struct {
	GetOperationResult whereOrHavingParser.Operation
	IsBuildError       bool
	BuildResult        *string
	Args               []interface{}
}

// Генерация запроса
func (w WhereGeneratorMock) Build(context.Context, whereOrHavingParser.Operation) (*string, []interface{}, error) {
	if w.IsBuildError {
		return nil, nil, fmt.Errorf(`test`)
	}

	return w.BuildResult, w.Args, nil
}

// Получение операции сравнения из переданного GraphQL запроса
func (w WhereGeneratorMock) GetOperation(types.Parameters) whereOrHavingParser.Operation {
	return w.GetOperationResult
}
