package filter

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/graphql-go/graphql"
)

// Тестирование доступности процессора
func Test_simpleOperationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		operationType      string
		operator           string
		object             *graphql.Object
		sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
		operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
	}
	type args struct {
		operation whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на доступной операции",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test", 1, "data", "="),
			},
			want: true,
		},
		{
			name: "Тестирование на не доступной операции",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test-1", 1, "data", "="),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperationProcessor{
				operationType:      tt.fields.operationType,
				operator:           tt.fields.operator,
				object:             tt.fields.object,
				sqlValueConverter:  tt.fields.sqlValueConverter,
				operationGenerator: tt.fields.operationGenerator,
			}
			if got := s.isAvailable(tt.args.operation); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_simpleOperationProcessor_build(t *testing.T) {
	str := "test"
	type fields struct {
		operationType      string
		operator           string
		object             *graphql.Object
		sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
		operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
	}
	type args struct {
		operation          whereOrHavingParser.Operation
		fieldConfiguration *configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если не передана конфигурация поля",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation:          whereOrHavingParser.NewSimpleOperation("test", 1, "data", "="),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если есть ошибка конвертации значения поля",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: true,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test", 1, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_1",
					DbConfiguration: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если не вернулась SQL строка для фильтрации",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test", 1, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_1",
					DbConfiguration: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Без ошибок",
			fields: fields{
				operationType: "test",
				operator:      "=",
				object: graphql.NewObject(
					graphql.ObjectConfig{
						Name: "Role",
						Fields: graphql.Fields{
							"date": &graphql.Field{
								Type: graphql.DateTime,
								Name: "Role DateTime",
							},
						},
						Description: "Test entity",
					},
				),
				sqlValueConverter: graphQlSqlConverterMock{
					IsSqlErr: false,
				},
				operationGenerator: simpleOperationGenerator.SimpleOperationGeneratorMock{
					GenerationResult: "test",
				},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test", 1, "data", "="),
				fieldConfiguration: &configuration.FieldsConfiguration{
					Field:           "test",
					FieldNum:        0,
					GraphQlName:     "test_1",
					DbConfiguration: nil,
				},
			},
			want:    &str,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := simpleOperationProcessor{
				operationType:      tt.fields.operationType,
				operator:           tt.fields.operator,
				object:             tt.fields.object,
				sqlValueConverter:  tt.fields.sqlValueConverter,
				operationGenerator: tt.fields.operationGenerator,
			}
			got, _, err := s.build(context.Background(), tt.args.operation, tt.args.fieldConfiguration)
			if (err != nil) != tt.wantErr {
				t.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
