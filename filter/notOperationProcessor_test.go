package filter

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Тестирование установки главного генератора
func Test_notOperationProcessor_setRootGenerator(t *testing.T) {
	type fields struct {
		generator WhereGeneratorInterface
	}
	type args struct {
		generator WhereGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование установки главного генератора",
			fields: fields{
				generator: nil,
			},
			args: args{
				generator: &WhereGeneratorMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notOperationProcessor{
				generator: tt.fields.generator,
			}

			n.setRootGenerator(tt.args.generator)
			if nil == n.generator {
				t.Errorf("setRootGenerator() generator is not set")
			}
		})
	}
}

// Тестирование доступности процессора
func Test_notOperationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		generator WhereGeneratorInterface
	}
	type args struct {
		operation whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на доступном типе",
			fields: fields{
				generator: &WhereGeneratorMock{},
			},
			args: args{
				operation: whereOrHavingParser.NewNotOperation(
					whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				),
			},
			want: true,
		},
		{
			name: "Тестирование на не доступном типе",
			fields: fields{
				generator: &WhereGeneratorMock{},
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notOperationProcessor{
				generator: tt.fields.generator,
			}
			if got := n.isAvailable(tt.args.operation); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_notOperationProcessor_build(t *testing.T) {
	empty := ""
	test := "test"
	testRes := "not(test)"

	type fields struct {
		generator WhereGeneratorInterface
	}
	type args struct {
		operation          whereOrHavingParser.Operation
		fieldConfiguration *configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если передан не корректный тип подоперации",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
			},
			args: args{
				operation:          whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если ошибка при генерации подоперации",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       true,
					BuildResult:        &test,
				},
			},
			args: args{
				operation: whereOrHavingParser.NewNotOperation(
					whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если подоперация пуста",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &empty,
				},
			},
			args: args{
				operation: whereOrHavingParser.NewNotOperation(
					whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если подоперация не возвращена",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        nil,
				},
			},
			args: args{
				operation: whereOrHavingParser.NewNotOperation(
					whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				),
				fieldConfiguration: nil,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
			},
			args: args{
				operation: whereOrHavingParser.NewNotOperation(
					whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
				),
				fieldConfiguration: nil,
			},
			want:    &testRes,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &notOperationProcessor{
				generator: tt.fields.generator,
			}
			got, _, err := n.build(context.Background(), tt.args.operation, tt.args.fieldConfiguration)
			if (err != nil) != tt.wantErr {
				t.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				gotVal := "nil"
				if nil != got {
					gotVal = fmt.Sprintf(`%v`, *got)
				}

				wantVal := "nil"
				if nil != tt.want {
					wantVal = fmt.Sprintf(`%v`, *tt.want)
				}

				t.Errorf("build() got = %v, want %v", gotVal, wantVal)
			}
		})
	}
}
