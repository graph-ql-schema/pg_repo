package filter

import (
	gql_sql_converter "bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/customizations"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/types"
	"context"
	"fmt"
	"github.com/graphql-go/graphql"
)

// Генератор параметров фильтрации
type whereGenerator struct {
	configuration []configuration.FieldsConfiguration
	processors    []whereProcessorInterface
}

// Генерация запроса
func (w whereGenerator) Build(ctx context.Context, operation whereOrHavingParser.Operation) (*string, []interface{}, error) {
	if nil == operation {
		return nil, nil, nil
	}

	fieldConfig := w.getFieldConfiguration(operation)
	for _, processor := range w.processors {
		if processor.isAvailable(operation) {
			return processor.build(ctx, operation, fieldConfig)
		}
	}

	return nil, nil, fmt.Errorf(`operation '%v': no available operation processor`, operation.Type())
}

// Получение операции сравнения из переданного GraphQL запроса
func (w whereGenerator) GetOperation(params types.Parameters) whereOrHavingParser.Operation {
	if nil != params.Arguments.Where && nil != params.Arguments.Having {
		return whereOrHavingParser.NewAndOrOperation(
			constants.AndSchemaKey,
			[]whereOrHavingParser.Operation{params.Arguments.Where, params.Arguments.Having},
			constants.AndSchemaKey,
		)
	}

	if nil != params.Arguments.Where {
		return params.Arguments.Where
	}

	return params.Arguments.Having
}

// Получение конфигурации поля, если она есть
func (w whereGenerator) getFieldConfiguration(operation whereOrHavingParser.Operation) *configuration.FieldsConfiguration {
	for _, config := range w.configuration {
		if operation.Field() == config.GraphQlName {
			return &config
		}
	}

	return nil
}

// Установка корневого генератора в процессоры
func (w *whereGenerator) setRootGenerator() {
	for _, processor := range w.processors {
		processor.setRootGenerator(w)
	}
}

// Фабрика генератора
func NewWhereGenerator(
	object *graphql.Object,
	primaryTableAlias string,
	configuration []configuration.FieldsConfiguration,
) WhereGeneratorInterface {
	sqlValueConverter := gql_sql_converter.NewGraphQlSqlConverter()
	operationGenerator := simpleOperationGenerator.NewSimpleOperationGenerator(primaryTableAlias)

	result := &whereGenerator{
		configuration: configuration,
		processors: []whereProcessorInterface{
			newAndOperationProcessor(),
			newOrOperationProcessor(),
			newNotOperationProcessor(),
			newInOperationProcessor(object, sqlValueConverter, operationGenerator),
			newBetweenOperationProcessor(object, sqlValueConverter, operationGenerator),
			newEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newMoreOperationProcessor(object, sqlValueConverter, operationGenerator),
			newMoreOrEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLessOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLessOrEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLikeOperationProcessor(object, sqlValueConverter, operationGenerator),
		},
	}

	result.setRootGenerator()

	return result
}

// Фабрика генератора
func NewWhereGeneratorWithCustomizations(
	object *graphql.Object,
	primaryTableAlias string,
	configuration []configuration.FieldsConfiguration,
	customizationItems []customizations.CustomizationInterface,
) WhereGeneratorInterface {
	sqlValueConverter := gql_sql_converter.NewGraphQlSqlConverter()
	operationGenerator := simpleOperationGenerator.NewSimpleOperationGenerator(primaryTableAlias)

	result := &whereGenerator{
		configuration: configuration,
		processors: []whereProcessorInterface{
			newAndOperationProcessor(),
			newOrOperationProcessor(),
			newNotOperationProcessor(),
			newInOperationProcessor(object, sqlValueConverter, operationGenerator),
			newBetweenOperationProcessor(object, sqlValueConverter, operationGenerator),
			newEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newMoreOperationProcessor(object, sqlValueConverter, operationGenerator),
			newMoreOrEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLessOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLessOrEqualsOperationProcessor(object, sqlValueConverter, operationGenerator),
			newLikeOperationProcessor(object, sqlValueConverter, operationGenerator),
		},
	}

	for _, customization := range customizationItems {
		if customization.GetType() == customizations.CustomizationTypeWhereGeneratorProcessor {
			constructor := customization.Customize().(customizations.TWhereGeneratorProcessor)
			processor := constructor(object, operationGenerator)

			result.processors = append(
				result.processors,
				simpleOperationProcessor{
					operationType:      processor.GetOperationType(),
					operator:           processor.GetOperator(),
					object:             processor.GetObject(),
					sqlValueConverter:  processor.GetSqlValueConverter(),
					operationGenerator: processor.GetOperationGenerator(),
				},
			)
		}
	}

	result.setRootGenerator()

	return result
}
