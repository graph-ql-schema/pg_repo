package filter

import (
	"context"
	"reflect"
	"testing"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
)

// Тестирование установки главного генератора
func Test_andOrOperationProcessor_setRootGenerator(t *testing.T) {
	type fields struct {
		generator           WhereGeneratorInterface
		availableOperations []string
		operator            string
	}
	type args struct {
		generator WhereGeneratorInterface
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "Тестирование установки главного генератора",
			fields: fields{
				generator:           nil,
				availableOperations: nil,
				operator:            "",
			},
			args: args{
				generator: &WhereGeneratorMock{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrOperationProcessor{
				generator:           tt.fields.generator,
				availableOperations: tt.fields.availableOperations,
				operator:            tt.fields.operator,
			}

			a.setRootGenerator(tt.args.generator)
			if nil == a.generator {
				t.Errorf("setRootGenerator() generator is not set")
			}
		})
	}
}

// Тестирование доступности процессора
func Test_andOrOperationProcessor_isAvailable(t *testing.T) {
	type fields struct {
		generator           WhereGeneratorInterface
		availableOperations []string
		operator            string
	}
	type args struct {
		operation whereOrHavingParser.Operation
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Тестирование на доступном типе",
			fields: fields{
				generator:           &WhereGeneratorMock{},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test_n", 1, "data", "="),
			},
			want: true,
		},
		{
			name: "Тестирование на доступном типе",
			fields: fields{
				generator:           &WhereGeneratorMock{},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test_f", 1, "data", "="),
			},
			want: true,
		},
		{
			name: "Тестирование на не доступном типе",
			fields: fields{
				generator:           &WhereGeneratorMock{},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test_l", 1, "data", "="),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrOperationProcessor{
				generator:           tt.fields.generator,
				availableOperations: tt.fields.availableOperations,
				operator:            tt.fields.operator,
			}
			if got := a.isAvailable(tt.args.operation); got != tt.want {
				t.Errorf("isAvailable() = %v, want %v", got, tt.want)
			}
		})
	}
}

// Тестирование генерации
func Test_andOrOperationProcessor_build(t *testing.T) {
	test := "test"
	twoOps := "(test) and (test)"
	oneOp := "(test)"

	type fields struct {
		generator           WhereGeneratorInterface
		availableOperations []string
		operator            string
	}
	type args struct {
		operation          whereOrHavingParser.Operation
		fieldConfiguration *configuration.FieldsConfiguration
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *string
		wantErr bool
	}{
		{
			name: "Если значение операции не корректно",
			fields: fields{
				generator:           &WhereGeneratorMock{},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewSimpleOperation("test_f", 1, "data", "="),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Если передан пустой срез подопераций",
			fields: fields{
				generator:           &WhereGeneratorMock{},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewAndOrOperation(constants.AndSchemaKey, []whereOrHavingParser.Operation{}, constants.AndSchemaKey),
			},
			want:    nil,
			wantErr: false,
		},
		{
			name: "Если произошла ошибка генерации подоперации",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       true,
					BuildResult:        &test,
				},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewAndOrOperation(constants.AndSchemaKey, []whereOrHavingParser.Operation{
					whereOrHavingParser.NewSimpleOperation("test", 1, "test", "="),
					whereOrHavingParser.NewSimpleOperation("test", 1, "test", "="),
				}, constants.AndSchemaKey),
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Нет ошибок (2 операции)",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewAndOrOperation(constants.AndSchemaKey, []whereOrHavingParser.Operation{
					whereOrHavingParser.NewSimpleOperation("test", 1, "test", "="),
					whereOrHavingParser.NewSimpleOperation("test", 1, "test", "="),
				}, constants.AndSchemaKey),
			},
			want:    &twoOps,
			wantErr: false,
		},
		{
			name: "Нет ошибок (1 операция)",
			fields: fields{
				generator: &WhereGeneratorMock{
					GetOperationResult: nil,
					IsBuildError:       false,
					BuildResult:        &test,
				},
				availableOperations: []string{"test_n", "test_f"},
				operator:            "and",
			},
			args: args{
				operation: whereOrHavingParser.NewAndOrOperation(constants.AndSchemaKey, []whereOrHavingParser.Operation{
					whereOrHavingParser.NewSimpleOperation("test", 1, "test", "="),
				}, constants.AndSchemaKey),
			},
			want:    &oneOp,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &andOrOperationProcessor{
				generator:           tt.fields.generator,
				availableOperations: tt.fields.availableOperations,
				operator:            tt.fields.operator,
			}
			got, _, err := a.build(context.Background(), tt.args.operation, tt.args.fieldConfiguration)
			if (err != nil) != tt.wantErr {
				t.Errorf("build() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("build() got = %v, want %v", got, tt.want)
			}
		})
	}
}
