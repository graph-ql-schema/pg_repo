package filter

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/consts"
	"context"
	"fmt"

	"bitbucket.org/graph-ql-schema/gql-sql-converter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/filter/simpleOperationGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/constants"
	"bitbucket.org/graph-ql-schema/sbuilder/v2/requestParser/argumentsParser/whereOrHavingParser"
	"github.com/graphql-go/graphql"
)

// Процессор операции Between
type betweenOperationProcessor struct {
	object             *graphql.Object
	sqlValueConverter  gql_sql_converter.GraphQlSqlConverterInterface
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface
}

// Установка корневого генератора параметров фильтрации
func (b betweenOperationProcessor) setRootGenerator(WhereGeneratorInterface) {}

// Проверка доступности процессора
func (b betweenOperationProcessor) isAvailable(operation whereOrHavingParser.Operation) bool {
	return operation.Type() == constants.BetweenSchemaKey
}

// Генерация фильтра
func (b betweenOperationProcessor) build(
	ctx context.Context,
	operation whereOrHavingParser.Operation,
	fieldConfiguration *configuration.FieldsConfiguration,
) (sql *string, args []interface{}, err error) {
	select {
	case <-ctx.Done():
		return nil, nil, nil
	default:
	}

	args = []interface{}{}
	if nil == fieldConfiguration {
		err = fmt.Errorf(`field "%v" -> operation 'BETWEEN': you should pass field configuration`, operation.Field())
		return
	}

	val, ok := operation.Value().(whereOrHavingParser.BetweenValue)
	if !ok {
		err = fmt.Errorf(`field "%v" -> operation 'BETWEEN': you should pass value as 'whereOrHavingParser.BetweenValue' structure`, operation.Field())
		return
	}

	from, err := b.sqlValueConverter.ToSQLValue(b.object, fieldConfiguration.GraphQlName, val.From)
	if nil != err {
		err = fmt.Errorf(`field "%v" -> operation 'BETWEEN': incorrect 'from' value. %v`, fieldConfiguration.GraphQlName, err.Error())
		return
	}

	to, err := b.sqlValueConverter.ToSQLValue(b.object, fieldConfiguration.GraphQlName, val.To)
	if nil != err {
		err = fmt.Errorf(`field "%v" -> operation 'BETWEEN': incorrect 'to' value. %v`, fieldConfiguration.GraphQlName, err.Error())
		return
	}

	args = append(args, from)
	args = append(args, to)

	value := fmt.Sprintf(`%v and %v`, consts.ArgumentPlaceholder, consts.ArgumentPlaceholder)

	operationSql := b.operationGenerator.Generate(*fieldConfiguration, value, func(field string, value string) string {
		return fmt.Sprintf(`%v between %v`, field, value)
	})

	if operationSql == "" {
		err = fmt.Errorf(`field '%v', operation 'BETWEEN': failed to generate operation. no available processors for this configuration`, fieldConfiguration.GraphQlName)
		return
	}

	sql = &operationSql
	return
}

// Фабрика процессора
func newBetweenOperationProcessor(
	object *graphql.Object,
	sqlValueConverter gql_sql_converter.GraphQlSqlConverterInterface,
	operationGenerator simpleOperationGenerator.SimpleOperationGeneratorInterface,
) whereProcessorInterface {
	return &betweenOperationProcessor{
		object:             object,
		sqlValueConverter:  sqlValueConverter,
		operationGenerator: operationGenerator,
	}
}
