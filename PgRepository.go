package pg_repo

import (
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/primaryChanges"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/relation"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/resolveMapBuilder"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/change/table"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/customizations"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/events"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/helpers"
	"context"
	"database/sql"
	"fmt"
	"github.com/graphql-go/graphql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"reflect"
	"strings"
	"time"

	"bitbucket.org/graph-ql-schema/pg_repo/v2/aggregation/aggregationConverter"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/configuration"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/executor"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/list/resultParser"
	"bitbucket.org/graph-ql-schema/pg_repo/v2/queryGenerator"
	"bitbucket.org/graph-ql-schema/sbuilder/v2"
)

// Репозиторий GraphQL сущности
type pgRepository struct {
	entityType             reflect.Type
	primaryTable           string
	primaryTableAlias      string
	primaryTablePrimaryKey string
	configuration          []configuration.FieldsConfiguration
	generator              queryGenerator.QueryGeneratorInterface
	executor               executor.QueryExecutorInterface
	log                    *logrus.Entry
	resultParser           resultParser.ResultParserInterface
	relationChange         relation.ConflictResolverInterface
	tableChange            table.ChangeResolverInterface
	primaryChange          primaryChanges.PrimaryChangesServiceInterface
	resolveMapper          resolveMapBuilder.ResolveMapBuilderInterface
	subscribers            []events.EventsSubscriberInterface
	eventsProcessor        events.SubscriberProcessorInterface
}

// Фабрика репозитория
func NewPgRepository(
	object *graphql.Object,
	entityType reflect.Type,
	primaryTable string,
	primaryTablePrimaryKey string,
	client *sqlx.DB,
) (PgRepositoryInterface, error) {
	configurator := configuration.NewConfigurationGenerator()
	config, err := configurator.Generate(entityType)
	if nil != err {
		return nil, err
	}

	primaryTableAlias := "pr"

	executorService := executor.NewQueryExecutor(client)
	generator := queryGenerator.NewQueryGenerator(object, primaryTable, primaryTableAlias, primaryTablePrimaryKey, config)
	return &pgRepository{
		entityType:             entityType,
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		configuration:          config,
		generator:              generator,
		executor:               executorService,
		log:                    helpers.NewLogger(fmt.Sprintf(`pgRepository(%v)`, primaryTable)),
		resultParser:           resultParser.NewResultParser(object, entityType),
		relationChange:         relation.NewConflictResolver(config, executorService, primaryTablePrimaryKey, primaryTable),
		tableChange:            table.NewChangeResolver(config, primaryTable, primaryTablePrimaryKey, executorService),
		primaryChange:          primaryChanges.NewPrimaryChangesService(primaryTable, primaryTablePrimaryKey, generator, executorService),
		resolveMapper:          resolveMapBuilder.NewResolveMapBuilder(),
		subscribers:            []events.EventsSubscriberInterface{},
		eventsProcessor:        events.NewSubscriberProcessor(),
	}, nil
}

// Фабрика репозитория
func NewPgRepositoryWithCustomizations(
	object *graphql.Object,
	entityType reflect.Type,
	primaryTable string,
	primaryTablePrimaryKey string,
	client *sqlx.DB,
	customizations []customizations.CustomizationInterface,
) (PgRepositoryInterface, error) {
	configurator := configuration.NewConfigurationGenerator()
	config, err := configurator.Generate(entityType)
	if nil != err {
		return nil, err
	}

	primaryTableAlias := "pr"

	executorService := executor.NewQueryExecutor(client)
	generator := queryGenerator.NewQueryGeneratorWithCustomization(
		object,
		primaryTable,
		primaryTableAlias,
		primaryTablePrimaryKey,
		config,
		customizations,
	)
	return &pgRepository{
		entityType:             entityType,
		primaryTable:           primaryTable,
		primaryTableAlias:      primaryTableAlias,
		primaryTablePrimaryKey: primaryTablePrimaryKey,
		configuration:          config,
		generator:              generator,
		executor:               executorService,
		log:                    helpers.NewLogger(fmt.Sprintf(`pgRepository(%v)`, primaryTable)),
		resultParser:           resultParser.NewResultParser(object, entityType),
		relationChange:         relation.NewConflictResolver(config, executorService, primaryTablePrimaryKey, primaryTable),
		tableChange:            table.NewChangeResolver(config, primaryTable, primaryTablePrimaryKey, executorService),
		primaryChange:          primaryChanges.NewPrimaryChangesService(primaryTable, primaryTablePrimaryKey, generator, executorService),
		resolveMapper:          resolveMapBuilder.NewResolveMapBuilder(),
		subscribers:            []events.EventsSubscriberInterface{},
		eventsProcessor:        events.NewSubscriberProcessor(),
	}, nil
}

// Отправка событий в зарегистрированных подписчиков. Для внутренних нужд
func (p *pgRepository) DispatchEvents(ctx context.Context, events []events.Event, tx *sql.Tx) error {
	if nil == ctx {
		ctx = context.Background()
	}

	return p.eventsProcessor.Process(ctx, events, tx, p.subscribers)
}

// Удаление значений по переданным параметрам GraphQL
func (p *pgRepository) DeleteEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*DeleteResult, error) {
	txNeedProcess := false
	if nil == tx {
		var err error
		tx, err = p.executor.StartTransaction()
		txNeedProcess = true

		if nil != err {
			return nil, fmt.Errorf(`failed to start delete transaction`)
		}
	}

	result, err := p.deleteEntitiesByGraphQlParametersWithTransaction(params, tx)
	if nil != err {
		if txNeedProcess && nil != tx {
			_ = tx.Rollback()
		}

		return nil, err
	}

	if txNeedProcess && nil != tx {
		_ = tx.Commit()
	}

	return result, nil
}

// Удаление сущностей в рамках переданной транзакции
func (p pgRepository) deleteEntitiesByGraphQlParametersWithTransaction(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*DeleteResult, error) {
	eventsToSend := []events.Event{}
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	// Необходимо изначально получить список сущностей, что будут удалены, т.к. необходимо принять решение,
	// как именно обработать их связанные сущности, а для этого необходимы корректные данные в исходной
	// таблице.
	itemIds, err := p.GetPrimaryKeyValuesByGraphQlParameters(params, tx)
	if nil != err {
		return nil, err
	}

	for _, id := range itemIds {
		parsedId := ""
		switch parsed := id.(type) {
		case string:
			parsedId = fmt.Sprintf(`'%v'`, parsed)
			break
		default:
			parsedId = fmt.Sprintf(`%v`, parsed)
		}

		eventsResolved, err := p.relationChange.ResolveDelete(parsedId, tx)
		if nil != err {
			return nil, err
		}

		for _, event := range eventsResolved {
			eventsToSend = append(eventsToSend, event)
		}

		err = p.tableChange.ResolveDelete(parsedId, tx)
		if nil != err {
			return nil, err
		}
	}

	processedIds, deleteEvents, err := p.primaryChange.Delete(params, tx)
	if nil != err {
		return nil, err
	}

	// Если система обработала разное количество сущностей: например загрузила больше ID чем удалила
	// или наоборот, то это необходимо исправить, т.к. это может нарушить целостность данных в системе.
	if len(processedIds) != len(itemIds) {
		err = fmt.Errorf(`failed to delete objects - something wrong`)
		p.log.WithFields(logrus.Fields{
			"code":         500,
			"arguments":    params.Arguments,
			"processedIds": processedIds,
			"itemIds":      itemIds,
		}).Error("Removed entity list is not equals loaded ID list. Need check.")

		return nil, err
	}

	for _, event := range deleteEvents {
		eventsToSend = append(eventsToSend, event)
	}

	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":         100,
		"time":         finish - starts,
		"processedIds": processedIds,
	}).Debug(`Processed object deleting`)

	err = p.eventsProcessor.Process(params.Context, eventsToSend, tx, p.subscribers)
	if nil != err {
		err = fmt.Errorf(`failed to delete objects. events subscriber returns error - %v`, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":         400,
			"eventsToSend": eventsToSend,
			"err":          err.Error(),
		}).Warning(`Failed to delete object`)

		return nil, err
	}

	return &DeleteResult{
		AffectedRows: len(processedIds),
	}, nil
}

// Получение списка значений первичного ключа для переданных параметров GraphQL
func (p *pgRepository) GetPrimaryKeyValuesByGraphQlParameters(
	params sbuilder.Parameters,
	tx *sql.Tx,
) ([]interface{}, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, args, err := p.generator.GenerateGetIdsQueryByGraphQlParameters(params.Context, params)
	finish := time.Now().UnixNano() / int64(time.Millisecond)
	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"err":       err,
		}).Warning(`Failed to generate primary key list query`)

		return nil, err
	}

	if nil == query {
		err = fmt.Errorf(`failed to generate "Get ID's" query: generator returns empty result`)
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"err":       err,
		}).Warning(`Failed to generate primary key list query`)

		return nil, err
	}

	p.log.WithFields(logrus.Fields{
		"code":      100,
		"arguments": params.Arguments,
		"query":     *query,
		"time":      finish - starts,
	}).Debug(`Generated primary key list query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(*query, args, tx)
	finish = time.Now().UnixNano() / int64(time.Millisecond)
	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": *query,
			"err":   err.Error(),
		}).Warning(`Failed to execute primary key list query`)

		return nil, err
	}

	defer rows.Close()

	p.log.WithFields(logrus.Fields{
		"code":  100,
		"query": *query,
		"time":  finish - starts,
	}).Debug(`Executed primary key list query`)

	starts = time.Now().UnixNano() / int64(time.Millisecond)
	ids := []interface{}{}
	for rows.Next() {
		var id interface{}
		err := rows.Scan(&id)
		if nil != err {
			p.log.WithFields(logrus.Fields{
				"code": 400,
				"err":  err.Error(),
			}).Warning(`Failed to parse result for primary key list query`)

			return nil, err
		}

		ids = append(ids, id)
	}

	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code": 100,
		"ids":  ids,
		"time": finish - starts,
	}).Debug(`Parsed results for primary key list query`)

	return ids, nil
}

// Регистрация подписчика на события
func (p *pgRepository) SubscribeToEvents(subscriber events.EventsSubscriberInterface) {
	p.subscribers = append(p.subscribers, subscriber)
}

// Обновление значений по переданным параметрам GraphQL
func (p *pgRepository) UpdateEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*InsertUpdateResult, error) {
	if 0 == len(params.Arguments.Set) {
		return nil, fmt.Errorf(`noone field passed to update`)
	}

	txNeedProcess := false
	if nil == tx {
		var err error
		tx, err = p.executor.StartTransaction()
		txNeedProcess = true

		if nil != err {
			return nil, fmt.Errorf(`failed to start update transaction`)
		}
	}

	result, err := p.updateEntitiesByGraphQlParametersWithTransaction(params, tx)
	if nil != err {
		if txNeedProcess && nil != tx {
			_ = tx.Rollback()
		}

		return nil, err
	}

	if txNeedProcess && nil != tx {
		_ = tx.Commit()
	}

	return result, nil
}

// Обновление сущностей в рамках переданной транзакции
func (p pgRepository) updateEntitiesByGraphQlParametersWithTransaction(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*InsertUpdateResult, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	itemIds, eventsToSend, err := p.primaryChange.Update(params, tx)
	if nil != err {
		return nil, err
	}

	// Если не было ошибки, но и не вернулись ID и события, значит обновления не произошло,
	// а, следовательно, необходимо загрузить ID сущностей, которые по идее должны быть
	// затронуты обновлением и привязать события к ним.
	if nil == itemIds || 0 == len(itemIds) {
		itemIds = []string{}
		eventsToSend = []events.Event{}

		loadedIds, err := p.GetPrimaryKeyValuesByGraphQlParameters(params, tx)
		if nil != err {
			return nil, err
		}

		for _, id := range loadedIds {
			parsedId := fmt.Sprintf(`%v`, id)
			itemIds = append(itemIds, parsedId)
			eventsToSend = append(eventsToSend, events.Event{
				Type:            events.Updated,
				Table:           p.primaryTable,
				PrimaryKey:      p.primaryTablePrimaryKey,
				PrimaryKeyValue: parsedId,
			})
		}
	}

	data := p.resolveMapper.Generate(params.Arguments.Set)
	for _, id := range itemIds {
		eventsResolved, err := p.relationChange.ResolveUpdate(id, data, tx)
		if nil != err {
			return nil, err
		}

		for _, event := range eventsResolved {
			eventsToSend = append(eventsToSend, event)
		}

		err = p.tableChange.ResolveUpdate(id, data, tx)
		if nil != err {
			return nil, err
		}
	}
	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":      100,
		"arguments": params.Arguments,
		"events":    eventsToSend,
		"time":      finish - starts,
	}).Debug(`Processed object updating`)

	isNeedReturning := false
	for _, field := range params.Fields {
		if field.GetFieldName() == "returning" {
			isNeedReturning = true
			break
		}
	}

	returning := []interface{}{}
	if isNeedReturning && 0 != len(itemIds) {
		items, err := p.GetListByCustomSql(params.Context, func(sql string) (result string, args []interface{}) {
			return fmt.Sprintf(
				`%v where %v.%v in ('%v')`,
				sql,
				p.primaryTableAlias,
				p.primaryTablePrimaryKey,
				strings.Join(itemIds, "', '"),
			), nil
		}, tx)
		if nil != err {
			return nil, err
		}

		returning = items
	}

	err = p.eventsProcessor.Process(params.Context, eventsToSend, tx, p.subscribers)
	if nil != err {
		err = fmt.Errorf(`failed to update objects. events subscriber returns error - %v`, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":         400,
			"eventsToSend": eventsToSend,
			"err":          err.Error(),
		}).Warning(`Failed to update object`)

		return nil, err
	}

	return &InsertUpdateResult{
		AffectedRows: len(itemIds),
		Returning:    returning,
	}, nil
}

// Вставка значений по переданным параметрам GraphQL
func (p pgRepository) InsertEntitiesByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) (*InsertUpdateResult, error) {
	if 0 == len(params.Arguments.Objects) {
		return nil, fmt.Errorf(`noone object passed to insert`)
	}

	txNeedProcess := false
	if nil == tx {
		var err error
		tx, err = p.executor.StartTransaction()
		txNeedProcess = true

		if nil != err {
			return nil, fmt.Errorf(`failed to start insert transaction`)
		}
	}

	result, err := p.insertEntitiesByGraphQlParametersWithTransaction(params, tx)
	if nil != err {
		if txNeedProcess && nil != tx {
			_ = tx.Rollback()
		}

		return nil, err
	}

	if txNeedProcess && nil != tx {
		_ = tx.Commit()
	}

	return result, nil
}

// Вставка сущностей в рамках переданной транзакции
func (p pgRepository) insertEntitiesByGraphQlParametersWithTransaction(
	params sbuilder.Parameters,
	tx *sql.Tx,
) (*InsertUpdateResult, error) {
	itemIds := []string{}
	eventsToSend := []events.Event{}
	starts := time.Now().UnixNano() / int64(time.Millisecond)

	for _, object := range params.Arguments.Objects {
		id, event, err := p.primaryChange.InsertObject(params.Context, object, tx)
		if nil != err {
			return nil, err
		}

		eventsToSend = append(eventsToSend, *event)
		data := p.resolveMapper.Generate(object)

		eventsResolved, err := p.relationChange.ResolveInsert(*id, data, tx)
		if nil != err {
			return nil, err
		}

		for _, event := range eventsResolved {
			eventsToSend = append(eventsToSend, event)
		}

		err = p.tableChange.ResolveInsert(*id, data, tx)
		if nil != err {
			return nil, err
		}

		itemIds = append(itemIds, *id)
	}
	finish := time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":    100,
		"objects": params.Arguments.Objects,
		"events":  eventsToSend,
		"time":    finish - starts,
	}).Debug(`Processed object insertion`)

	isNeedReturning := false
	for _, field := range params.Fields {
		if field.GetFieldName() == "returning" {
			isNeedReturning = true
			break
		}
	}

	returning := []interface{}{}
	if isNeedReturning && 0 != len(itemIds) {
		items, err := p.GetListByCustomSql(params.Context, func(sql string) (result string, args []interface{}) {
			return fmt.Sprintf(
				`%v where %v.%v in ('%v')`,
				sql,
				p.primaryTableAlias,
				p.primaryTablePrimaryKey,
				strings.Join(itemIds, "', '"),
			), nil
		}, tx)
		if nil != err {
			return nil, err
		}

		returning = items
	}

	err := p.eventsProcessor.Process(params.Context, eventsToSend, tx, p.subscribers)
	if nil != err {
		err = fmt.Errorf(`failed to insert objects. events subscriber returns error - %v`, err.Error())
		p.log.WithFields(logrus.Fields{
			"code":         400,
			"eventsToSend": eventsToSend,
			"err":          err.Error(),
		}).Warning(`Failed to insert object`)

		return nil, err
	}

	return &InsertUpdateResult{
		AffectedRows: len(itemIds),
		Returning:    returning,
	}, nil
}

// Получение результатов агрегации сущностей по переданным параметрам GraphQL
func (p pgRepository) GetAggregationByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]aggregationConverter.Aggregation, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, args, err := p.generator.GenerateAggregationQueryByGraphQlParameters(params.Context, params)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"fields":    params.Fields,
			"err":       err.Error(),
		}).Warning(`Failed to generate aggregation query`)

		return nil, err
	}

	if nil == query {
		err := fmt.Errorf(`'%v' aggregation query: failed to generate sql query`, p.primaryTable)
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"fields":    params.Fields,
			"err":       err.Error(),
		}).Warning(`Failed to generate aggregation query`)

		return nil, err
	}

	p.log.WithFields(logrus.Fields{
		"code":      100,
		"arguments": params.Arguments,
		"fields":    params.Fields,
		"query":     query,
		"time":      finish - starts,
	}).Debug(`Generated aggregation query`)

	startsQuery := time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(*query, args, tx)
	finish = time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": query,
			"error": err.Error(),
		}).Warning(`Failed to execute aggregation query`)

		return nil, err
	}

	defer rows.Close()

	p.log.WithFields(logrus.Fields{
		"code":  100,
		"query": query,
		"time":  finish - startsQuery,
	}).Debug(`Executed aggregation query`)

	if nil == rows {
		err := fmt.Errorf(`failed to execute aggregation query, empty rows result returns`)
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": query,
			"error": err.Error(),
		}).Warning(`Failed to execute aggregation query`)

		return nil, err
	}

	result, err := p.resultParser.ParseAggregationRows(rows)
	if nil != err {
		return nil, err
	}

	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
		"query":  query,
		"time":   finish - starts,
	}).Debug(`Collected aggregation query data`)

	return result, nil
}

// Получение листинга сущностей по переданным параметрам GraphQL
func (p pgRepository) GetListByGraphQlParameters(params sbuilder.Parameters, tx *sql.Tx) ([]interface{}, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, args, err := p.generator.GenerateListQueryByGraphQlParameters(params.Context, params)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"fields":    params.Fields,
			"err":       err.Error(),
		}).Warning(`Failed to generate list query`)

		return nil, err
	}

	p.log.WithFields(logrus.Fields{
		"code":      100,
		"arguments": params.Arguments,
		"fields":    params.Fields,
		"query":     query,
		"time":      finish - starts,
	}).Debug(`Generated list query`)

	if nil == query {
		err := fmt.Errorf(`'%v' list query: failed to generate sql query`, p.primaryTable)
		p.log.WithFields(logrus.Fields{
			"code":      400,
			"arguments": params.Arguments,
			"fields":    params.Fields,
			"err":       err.Error(),
		}).Warning(`Failed to generate query`)

		return nil, err
	}

	fields := []configuration.FieldsConfiguration{}
	for _, field := range params.Fields {
		for _, fieldConf := range p.configuration {
			if fieldConf.GraphQlName == field.GetFieldName() ||
				fieldConf.IsAlreadyLoad ||
				p.primaryTablePrimaryKey == fieldConf.GraphQlName {
				fields = append(fields, fieldConf)
			}
		}
	}

	result, err := p.getEntitiesList(*query, args, tx, fields)
	if nil != err {
		return nil, err
	}

	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
		"query":  query,
		"time":   finish - starts,
	}).Debug(`Collected list query data`)

	return result, nil
}

// Получение листинга сущностей по кастомному SQL запросу
func (p pgRepository) GetListByCustomSql(
	ctx context.Context,
	callback queryGenerator.TSqlModificationCallback,
	tx *sql.Tx,
) ([]interface{}, error) {
	if nil != ctx {
		ctx = context.Background()
	}

	starts := time.Now().UnixNano() / int64(time.Millisecond)
	query, args, err := p.generator.GenerateListQueryByCustomSql(ctx, callback)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code": 400,
			"err":  err.Error(),
		}).Warning(`Failed to generate custom query`)

		return nil, err
	}

	p.log.WithFields(logrus.Fields{
		"code":  100,
		"query": query,
		"time":  finish - starts,
	}).Debug(`Generated custom list query`)

	if nil == query {
		err := fmt.Errorf(`'%v' custom list query: failed to generate sql query`, p.primaryTable)
		p.log.WithFields(logrus.Fields{
			"code": 400,
			"err":  err.Error(),
		}).Warning(`Failed to generate custom query`)

		return nil, err
	}

	result, err := p.getEntitiesList(*query, args, tx, p.configuration)
	if nil != err {
		return nil, err
	}

	finish = time.Now().UnixNano() / int64(time.Millisecond)
	p.log.WithFields(logrus.Fields{
		"code":   100,
		"result": result,
		"query":  query,
		"time":   finish - starts,
	}).Debug(`Collected list query data`)

	return result, nil
}

// Получение списка сущностей по переданному запросу SQL
func (p pgRepository) getEntitiesList(
	query string,
	args []interface{},
	tx *sql.Tx,
	fields []configuration.FieldsConfiguration,
) ([]interface{}, error) {
	starts := time.Now().UnixNano() / int64(time.Millisecond)
	rows, err := p.executor.ExecuteListQuery(query, args, tx)
	finish := time.Now().UnixNano() / int64(time.Millisecond)

	if nil != err {
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": query,
			"err":   err.Error(),
		}).Warning(`Failed to execute get list query`)

		return nil, err
	}

	defer rows.Close()

	p.log.WithFields(logrus.Fields{
		"code":   100,
		"query":  query,
		"fields": fields,
		"time":   finish - starts,
	}).Debug(`Executed list query`)

	if nil == rows {
		err := fmt.Errorf(`failed to execute get list query, empty rows result returns`)
		p.log.WithFields(logrus.Fields{
			"code":  400,
			"query": query,
			"err":   err.Error(),
		}).Warning(`Failed to execute get list query`)

		return nil, err
	}

	return p.resultParser.ParseListRows(rows, fields)
}
